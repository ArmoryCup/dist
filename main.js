(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-SG": "./node_modules/moment/locale/en-SG.js",
	"./en-SG.js": "./node_modules/moment/locale/en-SG.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<kt-splash-screen *ngIf=\"loader\"></kt-splash-screen>\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/auth/auth-notice/auth-notice.component.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/auth/auth-notice/auth-notice.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [hidden]=\"!message\" class=\"alert alert-{{type}}\" role=\"alert\" #alertNotice>\n\t<div class=\"alert-text\" [innerHTML]=\"message\"></div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/auth/auth.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/auth/auth.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"kt-grid kt-grid--hor kt-grid--root kt-login kt-login--v1\" id=\"kt_login\">\n\t<div class=\"kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile\">\n\t\t<!--begin::Aside-->\n\t\t<div class=\"kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside\" style=\"background-image: url(../../../assets/media/bg/bg-4.jpg);\">\n\t\t\t<div class=\"kt-grid__item\">\n\t\t\t\t<a [routerLink]=\"['/']\" class=\"kt-login__logo\">\n<!--\t\t\t\t\t<img src=\"./assets/media/logos/logo-4.png\" alt=\"\">-->\n\t\t\t\t\t<span style=\"color: white; font-size: 2em; font-weight: bold\">PRS_Logo</span>\n\n\t\t\t\t</a>\n\t\t\t</div>\n\t\t\t<div class=\"kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver\">\n\t\t\t\t<div class=\"kt-grid__item kt-grid__item--middle\">\n\t\t\t\t\t<h3 class=\"kt-login__title\">Welcome to The Peer Review System!</h3>\n\t\t\t\t\t<h4 class=\"kt-login__subtitle\">The quality and potential contribution of each manuscript is evaluated\n\t\t\t\t\tby one's peers in the scientific community.</h4>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"kt-grid__item\">\n\t\t\t\t<div class=\"kt-login__info\">\n\t\t\t\t\t<div class=\"kt-login__copyright\">\n\t\t\t\t\t\t© {{today|date:'yyyy'}} Blue Marble Enterprises, Inc.\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"kt-login__menu\">\n\t\t\t\t\t\t<a [routerLink]=\"['/about']\"  class=\"kt-link\">About</a>\n\t\t\t\t\t\t<a [routerLink]=\"['/contact']\"  class=\"kt-link\">Contact</a>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<!--begin::Aside-->\n\n\t\t<!--begin::Content-->\n\t\t<div class=\"kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1 kt-login__wrapper\">\n\t\t\t<router-outlet></router-outlet>\n\t\t</div>\n\t\t<!--end::Content-->\n\t</div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/auth/login/login.component.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/auth/login/login.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--begin::Body-->\n<div class=\"kt-login__body\">\n\t<!--begin::Signin-->\n\t<div class=\"kt-login__form\">\n\t\t<div class=\"kt-login__title\">\n\t\t\t<h3>Sign In</h3>\n\t\t</div>\n\n\t\t<kt-auth-notice></kt-auth-notice>\n\n\t\t<!--begin::Form-->\n\t\t<form class=\"kt-form\" [formGroup]=\"loginForm\" autocomplete=\"off\">\n\t\t\t<div class=\"form-group\">\n\t\t\t\t<mat-form-field>\n\t\t\t\t\t<input matInput type=\"username\" placeholder=\"username\"\n\t\t\t\t\t\t   formControlName=\"username\" autocomplete=\"off\"\n\t\t\t\t\t/>\n\t\t\t\t\t<mat-error *ngIf=\"isControlHasError('username','required')\">\n\t\t\t\t\t\t<strong>{{ 'AUTH.VALIDATION.REQUIRED_FIELD' | translate }}</strong>\n\t\t\t\t\t</mat-error>\n\t\t\t\t\t<mat-error *ngIf=\"isControlHasError('username','username')\">\n\t\t\t\t\t\t<strong>{{ 'AUTH.VALIDATION.INVALID_FIELD' | translate }}</strong>\n\t\t\t\t\t</mat-error>\n\t\t\t\t\t<mat-error *ngIf=\"isControlHasError('username','minlength')\">\n\t\t\t\t\t\t<strong>{{ 'AUTH.VALIDATION.MIN_LENGTH_FIELD' | translate }} 3</strong>\n\t\t\t\t\t</mat-error>\n\t\t\t\t\t<mat-error *ngIf=\"isControlHasError('username','maxlength')\">\n\t\t\t\t\t\t<strong>{{ 'AUTH.VALIDATION.MAX_LENGTH_FIELD' | translate }} 320</strong>\n\t\t\t\t\t</mat-error>\n\t\t\t\t</mat-form-field>\n\t\t\t</div>\n\t\t\t<div class=\"form-group\">\n\t\t\t\t<mat-form-field>\n\t\t\t\t\t<input matInput type=\"password\" placeholder=\"password\"\n\t\t\t\t\t\t   formControlName=\"password\" autocomplete=\"off\"\n\t\t\t\t\t/>\n\t\t\t\t\t<mat-error *ngIf=\"isControlHasError('password','required')\">\n\t\t\t\t\t\t<strong>{{ 'AUTH.VALIDATION.REQUIRED_FIELD' | translate }}</strong>\n\t\t\t\t\t</mat-error>\n\t\t\t\t\t<mat-error *ngIf=\"isControlHasError('password','minlength')\">\n\t\t\t\t\t\t<strong>{{ 'AUTH.VALIDATION.MIN_LENGTH_FIELD' | translate }} 3</strong>\n\t\t\t\t\t</mat-error>\n\t\t\t\t\t<mat-error *ngIf=\"isControlHasError('password','maxlength')\">\n\t\t\t\t\t\t<strong>{{ 'AUTH.VALIDATION.MAX_LENGTH_FIELD' | translate }} 100</strong>\n\t\t\t\t\t</mat-error>\n\t\t\t\t</mat-form-field>\n\t\t\t</div>\n\t\t\t<!--begin::Action-->\n\t\t\t<div class=\"kt-login__actions\">\n<!--\t\t\t\t<a href=\"javascript:;\" routerLink=\"/auth/forgot-password\" class=\"kt-link kt-login__link-forgot\">-->\n<!--\t\t\t\t\tForgot Password-->\n<!--\t\t\t\t</a>-->\n\t\t\t\t<button routerLink=\"/\" id=\"kt_login_signup_cancel\" class=\"btn btn-secondary btn-elevate kt-login__btn-secondary\">Back</button>\n\t\t\t\t<button (click)=\"submit()\"\n\t\t\t\t\t\tid=\"kt_login_signin_submit\"\n\t\t\t\t\t\tclass=\"btn btn-primary btn-elevate kt-login__btn-primary\">Submit</button>\n\t\t\t</div>\n\t\t\t<!--end::Action-->\n\t\t</form>\n\t\t<!--end::Form-->\n\n\t</div>\n\t<!--end::Signin-->\n</div>\n<!--end::Body-->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/main/about-page/about.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/main/about-page/about.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"mt-5\">\n\t<div class=\"container\">\n\t\t<div class=\"card-deck\">\n\t\t\t<div class=\"card\">\n\t\t\t\t<div class=\"card-header\"><h4>What is Peer Review?</h4></div>\n\t\t\t\t<div class=\"card-body\">\n\t\t\t\t\t<p> Peer review is a long-standing system used to assess the quality of a work\n\t\t\t\t\t\tbefore it is published.\n\t\t\t\t\t\t<span class=\"pl-2\">\n\t\t\t\t\t\t\tIndependent experts in the relevant area of interest assess\n\t\t\t\t\t\t\tsubmitted works for originality, validity and significance to help editors determine\n\t\t\t\t\t\t\twhether a work should be published as a standard reference for the scientists or\n\t\t\t\t\t\t\tprofessional associates of a science, industry or discipline.\n\t\t\t\t\t\t</span>\n\t\t\t\t\t</p>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div class=\"card\">\n\t\t\t\t<div class=\"card-header\"><h4>Who performs peer review?</h4></div>\n\t\t\t\t<div class=\"card-body\">\n\t\t\t\t\t<p>Experts with knowledge of the industry and practice are invited to review and rate\n\t\t\t\t\t\tworks.\n\t\t\t\t\t\t<span class=\"pl-2\">\n\t\t\t\t\t\t\tMultiple reviewers are used for each work. Contact the Administrator if you\n\t\t\t\t\t\t\twould like to be added as a reviewer.\n\t\t\t\t\t\t</span>\n\t\t\t\t\t</p>\n\t\t\t\t\t<p> Contact the Administrator via\n\t\t\t\t\t\t<a [routerLink]=\"['/contact']\"><u>Contact Form</u> </a> if you would like to be added as a reviewer.</p>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<div class=\"my-5\">\n\t<div class=\"container\">\n\t\t<div class=\"card-deck\">\n\t\t\t<div class=\"card\">\n\t\t\t\t<div class=\"card-header\"><h4>How is peer review accomplished?</h4></div>\n\t\t\t\t<div class=\"card-body\">\n\t\t\t\t\t<p> Reviewers read works by an assigned deadline.\n\t\t\t\t\t\t<span class=\"pl-2\">\n\t\t\t\t\t\t\tThey evaluate the work that was submitted using the format and guided scoring\n\t\t\t\t\t\t\tsystem set up by the profession or industry association.\n\t\t\t\t\t\t</span>\n\t\t\t\t\t</p>\n\t\t\t\t\t<p>\n\t\t\t\t\t\tMulitple reviewers give their assessment and a final consensus review and score\n\t\t\t\t\t\tare determined by combining the reviews.\n\t\t\t\t\t</p>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div class=\"card\">\n\t\t\t\t<div class=\"card-header\"><h4>What happens to reviewed works?</h4></div>\n\t\t\t\t<div class=\"card-body\">\n\t\t\t\t\t<p> A work that is accepted and earns a score above a certain threshold will be displayed on the\n\t\t\t\t\t\tpublic page of the Peer Review System.\n\t\t\t\t\t</p>\n\t\t\t\t\t<p>\n\t\t\t\t\t\tA work that is accepted and needs correction, quality improvement or receives a score below the\n\t\t\t\t\t\tthreshold will not be published.\n\t\t\t\t\t\t<span class=\"pl-2\">\n\t\t\t\t\t\t\tFeedback may be sent to the author.\n\t\t\t\t\t\t</span>\n\t\t\t\t\t\t<span class=\"pl-2\">\n\t\t\t\t\t\t\tAuthors may try again to submit the work when it is improved.\n\t\t\t\t\t\t</span>\n\t\t\t\t\t</p>\n\t\t\t\t\t<p>\n\t\t\t\t\t\tA work that is rejected and not reviewed did not meet the criteria of works for this discipline.\n\t\t\t\t\t\t<span class=\"pl-2\">\n\t\t\t\t\t\t\tThe Author receives a rejection notice from the Administrator. Feedback may or may not be sent\n\t\t\t\t\t\t\tto the Author.\n\t\t\t\t\t\t</span>\n\t\t\t\t\t</p>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<div>\n\n\t<div class=\"container-fluid text-center bottom\">\n\t\t<div class=\"container\">\n\t\t\t<div class=\"card-deck mt-5\">\n\t\t\t\t<div class=\"card\">\n\t\t\t\t\t<div class=\"mt-4 img\">\n\t\t\t\t\t\t<span class=\"fa-stack\">\n\t\t\t\t\t\t<i class=\"fas fa-file-alt fa-stack-2x\"></i>\n\t\t\t\t\t\t\t<i class=\"fas fa-upload fa-stack-1x\" id=\"upload-icon\"></i>\n\t\t\t\t\t\t</span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"card-body\">\n\t\t\t\t\t\t<h4 class=\"card-title\">Submit Your Work</h4>\n\t\t\t\t\t\t<p class=\"card-text\">Your work will be assessed for originality, validity and significance</p>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n<!--\t\t\t\t-->\n\n<!--\t\t\t\t-->\n\n\n<!--\t\t\t\t-->\n\n\t\t\t\t<div class=\"card\">\n\n\t\t\t\t\t<div class=\"mt-4\">\n\t\t\t\t\t\t<i class=\"fa fa-users\" aria-hidden=\"true\" style=\"font-size: 79px\"></i>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"card-body\">\n\t\t\t\t\t\t<h4 class=\"card-title\">Work Is Under Review</h4>\n\t\t\t\t\t\t<p class=\"card-text\">Our experts with knowledge of the industry and practice\n\t\t\t\t\t\t\twill review and rate your work.</p>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"card\">\n\t\t\t\t\t<div class=\"mt-4\">\n\t\t\t\t\t\t<i class=\"fa fa-thumbs-up\" aria-hidden=\"true\" style=\"font-size: 77px\"></i>\n\t\t\t\t\t</div>\n\n\n\t\t\t\t\t<div class=\"card-body\">\n\t\t\t\t\t\t<h4 class=\"card-title\">Get Score</h4>\n\t\t\t\t\t\t<p class=\"card-text\">Your work will recieve a final consensus review and score from our reviewers</p>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t</div>\n\n\n\t</div>\n\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/main/contact-page/contact.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/main/contact-page/contact.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"d-flex justify-content-center\" style=\"height: 82%;\">\n\t<div class=\"container my-5 d-flex justify-content-center\" style=\"height:550px;\">\n\n\n\t\t<div class=\"contact\">\n\t\t\t<div class=\"mt-4\">\n\t\t\t\t<span class=\"text-center my-5\"><h3>How Can We Help you</h3></span>\n\n\t\t\t\t<!--begin::Form-->\n\t\t\t\t<form class=\"my-5\" [formGroup]=\"contactForm\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<mat-form-field>\n\t\t\t\t\t\t\t<mat-label>Full Name</mat-label>\n\t\t\t\t\t\t\t<input matInput type=\"text\" placeholder=\"fullname\" formControlName=\"fullname\"\n\t\t\t\t\t\t\t\t   autocomplete=\"off\"/>\n\t\t\t\t\t\t\t<mat-error *ngIf=\"isControlHasError('fullname','required')\">\n\t\t\t\t\t\t\t\t<strong>Required field</strong>\n\t\t\t\t\t\t\t</mat-error>\n\t\t\t\t\t\t\t<mat-error *ngIf=\"isControlHasError('fullname','minlength')\">\n\t\t\t\t\t\t\t\t<strong>Minimum field length: 3</strong>\n\t\t\t\t\t\t\t</mat-error>\n\t\t\t\t\t\t\t<mat-error *ngIf=\"isControlHasError('fullname','maxlength')\">\n\t\t\t\t\t\t\t\t<strong>Maximum field length: 320</strong>\n\t\t\t\t\t\t\t</mat-error>\n\t\t\t\t\t\t</mat-form-field>\n\t\t\t\t\t</div>\n\n\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<mat-form-field>\n\t\t\t\t\t\t\t<mat-label>Email</mat-label>\n\t\t\t\t\t\t\t<input matInput type=\"email\" placeholder=\"email\" formControlName=\"email\"\n\t\t\t\t\t\t\t\t   autocomplete=\"off\"/>\n\t\t\t\t\t\t\t<mat-error *ngIf=\"isControlHasError('email','required')\">\n\t\t\t\t\t\t\t\t<strong>Required field</strong>\n\t\t\t\t\t\t\t</mat-error>\n\t\t\t\t\t\t\t<mat-error *ngIf=\"isControlHasError('email','email')\">\n\t\t\t\t\t\t\t\t<strong>Invalid field</strong>\n\t\t\t\t\t\t\t</mat-error>\n\t\t\t\t\t\t\t<mat-error *ngIf=\"isControlHasError('email','minlength')\">\n\t\t\t\t\t\t\t\t<strong>Minimum field length: 3</strong>\n\t\t\t\t\t\t\t</mat-error>\n\t\t\t\t\t\t\t<mat-error *ngIf=\"isControlHasError('email','maxlength')\">\n\t\t\t\t\t\t\t\t<strong>Maximum field length: 320</strong>\n\t\t\t\t\t\t\t</mat-error>\n\t\t\t\t\t\t</mat-form-field>\n\t\t\t\t\t</div>\n\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<mat-form-field>\n\t\t\t\t\t\t\t<mat-label>Subject</mat-label>\n\t\t\t\t\t\t\t<input matInput type=\"text\" placeholder=\"subject\" formControlName=\"subject\"\n\t\t\t\t\t\t\t\t   autocomplete=\"off\"/>\n\t\t\t\t\t\t\t<mat-error *ngIf=\"isControlHasError('subject','required')\">\n\t\t\t\t\t\t\t\t<strong>Required field</strong>\n\t\t\t\t\t\t\t</mat-error>\n\t\t\t\t\t\t\t<mat-error *ngIf=\"isControlHasError('subject','minlength')\">\n\t\t\t\t\t\t\t\t<strong>Minimum field length: 3</strong>\n\t\t\t\t\t\t\t</mat-error>\n\t\t\t\t\t\t\t<mat-error *ngIf=\"isControlHasError('subject','maxlength')\">\n\t\t\t\t\t\t\t\t<strong>Maximum field length: 320</strong>\n\t\t\t\t\t\t\t</mat-error>\n\t\t\t\t\t\t</mat-form-field>\n\t\t\t\t\t</div>\n\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<mat-form-field>\n\t\t\t\t\t\t\t<mat-label>Message</mat-label>\n\t\t\t\t\t\t\t<textarea matInput\n\t\t\t\t\t\t\t\t\t  cdkTextareaAutosize\n\t\t\t\t\t\t\t\t\t  #autosize=\"cdkTextareaAutosize\"\n\t\t\t\t\t\t\t\t\t  cdkAutosizeMinRows=\"4\"\n\t\t\t\t\t\t\t\t\t  cdkAutosizeMaxRows=\"4\"\n\t\t\t\t\t\t\t\t\t  formControlName=\"cmessage\"\n\t\t\t\t\t\t\t></textarea>\n\t\t\t\t\t\t\t<mat-error *ngIf=\"isControlHasError('cmessage','required')\">\n\t\t\t\t\t\t\t\t<strong>Required field</strong>\n\t\t\t\t\t\t\t</mat-error>\n\t\t\t\t\t\t\t<mat-error *ngIf=\"isControlHasError('cmessage','minlength')\">\n\t\t\t\t\t\t\t\t<strong> Minimum field length: 3</strong>\n\t\t\t\t\t\t\t</mat-error>\n\t\t\t\t\t\t\t<mat-error *ngIf=\"isControlHasError('cmessage','maxlength')\">\n\t\t\t\t\t\t\t\t<strong> Maximum field length: 100</strong>\n\t\t\t\t\t\t\t</mat-error>\n\t\t\t\t\t\t</mat-form-field>\n\t\t\t\t\t</div>\n\n\t\t\t\t\t<!--begin::Action-->\n\t\t\t\t\t<div class=\"d-flex justify-content-center\">\n\t\t\t\t\t\t<span class=\"mr-4\">\n\t\t\t\t\t\t\t<button (click)=\"submit()\"\n\t\t\t\t\t\t\t\tclass=\"btn btn-primary btn-elevate\">Submit\n\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t</span>\n\n\t\t\t\t\t\t<span>\n\t\t\t\t\t\t\t<button routerLink=\"/\"\n\t\t\t\t\t\t\t\tclass=\"btn btn-secondary btn-elevate\">Cancel\n\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t</span>\n\t\t\t\t\t</div>\n\t\t\t\t</form>\n\t\t\t</div>\n\t\t</div>\n\n\t</div>\n\n\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/main/main.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/main/main.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid \" style=\"background: url('../assets/media/bg/bg-4.jpg') no-repeat 1% 26%;\">\n\t<div class=\"header-company \">\n\t\t<nav class=\"navbar navbar-expand-lg navbar-dark \">\n\t\t\t<div class=\"d-flex flex-grow-1\">\n\t\t\t\t<span class=\"w-100 d-lg-none d-block\"><!-- hidden spacer to center brand on mobile --></span>\n\t\t\t\t<a class=\"navbar-brand\" routerLink=\"/home\">\n\t\t\t\t\t<img style=\"width: 118px\" src=\"../assets/media/bg/logo.png\" alt=\"PRS\"/>\n\t\t\t\t</a>\n\t\t\t</div>\n\t\t\t<div class=\"collapse navbar-collapse flex-grow-1 text-right \" id=\"myNavbar7\">\n\t\t\t\t<ul class=\"navbar-nav ml-auto flex-nowrap font-weight-bold\" style=\"font-size: 15px\">\n\t\t\t\t\t<li class=\"nav-item \">\n\t\t\t\t\t\t<a [routerLink]=\"['/home']\" routerLinkActive=\"active\" class=\"nav-link\">Home</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li class=\"nav-item\">\n\t\t\t\t\t\t<a [routerLink]=\"['/about']\"   routerLinkActive=\"active\" class=\"nav-link\">About</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li class=\"nav-item\">\n\t\t\t\t\t\t<a [routerLink]=\"['/contact']\"  routerLinkActive=\"active\" class=\"nav-link\">Contact Us</a>\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t</nav>\n\t\t<div class=\"h-50\" style=\"color: white; position: relative; margin-top: 20px\">\n\t\t\t<div class=\"d-flex justify-content-center header-about text-center\">\n\t\t\t<span>\n\t\t\t\t<h2>{{title}}</h2>\n\t\t\t</span>\n\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/content/crud/action-natification/action-notification.component.html":
/*!******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/content/crud/action-natification/action-notification.component.html ***!
  \******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"kt-mat-snackbar\">\n\t<div class=\"kt-mat-snackbar__message\">{{data.message}}</div>\n\t<div class=\"kt-mat-snackbar__btn\">\n\t\t<button *ngIf=\"data.showUndoButton\" type=\"button\" mat-button color=\"primary\" (click)=\"onDismissWithAction()\">\n\t\t\tUndo\n\t\t</button>\n\t</div>\n\t<div class=\"kt-mat-snackbar__close\">\n\t\t<button *ngIf=\"data.showCloseButton\" \n\t\t\tmat-icon-button \n\t\t\t(click)=\"onDismiss()\"\n\t\t\tcolor=\"warn\">\n\t\t\t<mat-icon>clear</mat-icon>\n\t\t</button>\n\t</div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/content/crud/alert/alert.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/content/crud/alert/alert.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"kt-mat-alert  kt-mat-alert--{{type}}\" role=\"alert\">\n\t<div class=\"kt-mat-alert__icon\">\n\t\t<i class=\"la la-warning\"></i>\n\t</div>\n\t<div class=\"kt-mat-alert__text\">\n\t\t<ng-content></ng-content>\n\t</div>\n\t<div class=\"kt-mat-alert__close\" *ngIf=\"showCloseButton\">\n\t\t<button type=\"button\" \n\t\t\tmat-icon-button \n\t\t\tcolor=\"warn\" \n\t\t\t(click)=\"closeAlert()\">\n\t\t\t<mat-icon class=\"material-icons\">clear</mat-icon>\n\t\t</button>\n\t</div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/content/crud/delete-entity-dialog/delete-entity-dialog.component.html":
/*!********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/content/crud/delete-entity-dialog/delete-entity-dialog.component.html ***!
  \********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"kt-portlet\">\n\t<div class=\"kt-portlet__head kt-portlet__head__custom\">\n\t\t<div class=\"kt-portlet__head-progress\">\n\t\t\t<!-- here can place a progress bar-->\n\t\t\t<mat-progress-bar mode=\"indeterminate\" *ngIf=\"viewLoading\"></mat-progress-bar>\n\t\t</div>\n\t\t<div class=\"kt-portlet__head-label\">\n\t\t\t<h3 class=\"kt-portlet__head-title\">{{data.title}}</h3>\n\t\t\t<span class=\"kt-portlet__head-icon kt-hide\">\n\t\t\t\t<i class=\"la la-gear\"></i>\n\t\t\t</span>\n\t\t</div>\n\t</div>\n\t<div class=\"kt-form\">\n\t\t<div class=\"kt-portlet__body\">\n\t\t\t<div class=\"form-group kt-form__group row\">\n\t\t\t\t<div class=\"col-lg-12\">\n\t\t\t\t\t{{viewLoading ? data.waitDesciption : data.description}}\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"kt-portlet__foot kt-portlet__no-border kt-portlet__foot--fit text-right\">\n\t\t\t<div class=\"kt-form__actions kt-form__actions--sm\">\n\t\t\t\t<button mat-button (click)=\"onNoClick()\">Cancel</button>&nbsp;\n\t\t\t\t<button mat-button (click)=\"onYesClick()\" color=\"primary\" cdkFocusInitial [disabled]=\"viewLoading\">Delete</button>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/content/crud/fetch-entity-dialog/fetch-entity-dialog.component.html":
/*!******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/content/crud/fetch-entity-dialog/fetch-entity-dialog.component.html ***!
  \******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"kt-portlet\">\n\t<div class=\"kt-portlet__head kt-portlet__head__custom\">\n\t\t<div class=\"kt-portlet__head-label\">\n\t\t\t<h3 class=\"kt-portlet__head-title\">Fetch selected elements</h3>\n\t\t\t<span class=\"kt-portlet__head-icon kt-hide\">\n\t\t\t\t<i class=\"la la-gear\"></i>\n\t\t\t</span>\n\t\t</div>\n\t</div>\n\t<div class=\"kt-form kt-form--label-align-right kt-form--group-seperator-dashed\">\n\t\t<div class=\"kt-portlet__body kt-padding-0\">\n\t\t\t<div class=\"kt-list-timeline kt-list-timeline--skin-light kt-padding-30\">\n\t\t\t\t<div class=\"kt-list-timeline__items\">\n\t\t\t\t\t<div class=\"kt-list-timeline__item\" *ngFor=\"let message of data\">\n\t\t\t\t\t\t<span class=\"kt-list-timeline__badge kt-list-timeline__badge--{{getItemCssClassByStatus(message.status)}}\"></span>\n\t\t\t\t\t\t<span class=\"kt-list-timeline__text\">{{message.text}}\n\t\t\t\t\t\t\t<span class=\"kt-badge kt-badge--{{getItemCssClassByStatus(message.status)}} kt-badge--wide\">ID: {{message.id}}</span>\n\t\t\t\t\t\t</span>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"kt-portlet__foot kt-portlet__no-border kt-portlet__foot--fit text-right\">\n\t\t\t<div class=\"kt-form__actions kt-form__actions--solid\">\n\t\t\t\t<button mat-button mat-raised-button (click)=\"onNoClick()\">Cancel</button>&nbsp;\n\t\t\t\t<button mat-button mat-raised-button color=\"primary\" [mat-dialog-close]=\"data.animal\" cdkFocusInitial>Ok</button>&nbsp;\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/content/crud/update-status-dialog/update-status-dialog.component.html":
/*!********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/content/crud/update-status-dialog/update-status-dialog.component.html ***!
  \********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"kt-portlet\" [ngClass]=\"{'kt-portlet--body-progress' : viewLoading, 'kt-portlet--body-progress-overlay' : loadingAfterSubmit }\">\n\t<div class=\"kt-portlet__head kt-portlet__head__custom\">\n\t\t<div class=\"kt-portlet__head-label\">\n\t\t\t<h3 class=\"kt-portlet__head-title\">{{data.title}}</h3>\n\t\t\t<span class=\"kt-portlet__head-icon kt-hide\">\n\t\t\t\t<i class=\"la la-gear\"></i>\n\t\t\t</span>\n\t\t</div>\n\t</div>\n\t<div class=\"kt-form kt-form--label-align-right kt-form--group-seperator-dashed\">\n\t\t<div class=\"kt-portlet__body\">\n\t\t\t<div class=\"kt-portlet__body-progress\">\n\t\t\t\t<mat-spinner [diameter]=\"20\"></mat-spinner>\n\t\t\t</div>\n\t\t\t<div class=\"kt-list-timeline kt-list-timeline--skin-light kt-padding-30\">\n\t\t\t\t<div class=\"kt-list-timeline__items\">\n\t\t\t\t\t<div class=\"kt-list-timeline__item\" *ngFor=\"let message of data.messages\">\n\t\t\t\t\t\t<span class=\"kt-list-timeline__badge kt-list-timeline__badge--{{message.statusCssClass}}\"></span>\n\t\t\t\t\t\t<span class=\"kt-list-timeline__text\">{{message.text}}\n\t\t\t\t\t\t\t<span class=\"kt-badge kt-badge--{{message.statusCssClass}} kt-badge--wide\">{{message.statusTitle}}</span>\n\t\t\t\t\t\t</span>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"kt-portlet__foot kt-portlet__no-border kt-portlet__foot--fit text-right\">\n\t\t\t<div class=\"kt-form__actions kt-form__actions--solid\">\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<div class=\"col-lg-12\">\n\t\t\t\t\t\t<mat-form-field>\n\t\t\t\t\t\t\t<mat-select placeholder=\"Status\" [formControl]=\"selectedStatusForUpdate\">\n\t\t\t\t\t\t\t\t<mat-option *ngFor=\"let status of data.statuses\" value=\"{{ status.value }}\">{{status.text}}</mat-option>\n\t\t\t\t\t\t\t</mat-select>\n\t\t\t\t\t\t\t<mat-hint align=\"start\">Select\n\t\t\t\t\t\t\t\t<strong>Status</strong> for rows updating</mat-hint>\n\t\t\t\t\t\t</mat-form-field>\n\t\t\t\t\t\t&nbsp;\n\t\t\t\t\t\t<button type=\"button\" mat-raised-button color=\"primary\" (click)=\"updateStatus()\" [disabled]=\"selectedStatusForUpdate.value.length == 0\">Update status</button>&nbsp;\n\t\t\t\t\t\t<button type=\"button\" mat-raised-button (click)=\"onNoClick()\">Cancel</button>\n\t\t\t\t\t</div>\n\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/content/general/error/error.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/content/general/error/error.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [ngClass]=\"'kt-' + this.type\" [ngStyle]=\"{'background-image': 'url('+image+')'}\"\n\tclass=\"kt-grid__item kt-grid__item--fluid kt-grid\">\n\n\t<ng-container *ngIf=\"type === 'error-v1'\">\n\t\t<div class=\"kt-error-v1__container\">\n\t\t\t<h1 class=\"kt-error-v1__number\">{{code}}</h1>\n\t\t\t<p class=\"kt-error-v1__desc\" [innerHTML]=\"desc\"></p>\n\t\t</div>\n\t</ng-container>\n\n\t<ng-container *ngIf=\"type === 'error-v2'\">\n\t\t<div class=\"kt-error_container\">\n\t\t\t<div class=\"kt-error_title2 kt-font-light\">\n\t\t\t\t<h1>{{title}}</h1>\n\t\t\t</div>\n\t\t\t<span class=\"kt-error_desc kt-font-light\" [innerHTML]=\"desc\"></span>\n\t\t</div>\n\t</ng-container>\n\n\t<ng-container *ngIf=\"type === 'error-v3'\">\n\t\t<div class=\"kt-error_container\">\n\t\t\t<div class=\"kt-error_number\">\n\t\t\t\t<h1>{{code}}</h1>\n\t\t\t</div>\n\t\t\t<p class=\"kt-error_title kt-font-light\">\n\t\t\t\t{{title}}\n\t\t\t</p>\n\t\t\t<p class=\"kt-error_subtitle\">\n\t\t\t\t{{subtitle}}\n\t\t\t</p>\n\t\t\t<p class=\"kt-error_description\" [innerHTML]=\"desc\"></p>\n\t\t</div>\n\t</ng-container>\n\n\t<ng-container *ngIf=\"type === 'error-v4'\">\n\t\t<div class=\"kt-error_container\">\n\t\t\t<h1 class=\"kt-error_number\">\n\t\t\t\t{{code}}\n\t\t\t</h1>\n\t\t\t<p class=\"kt-error_title\">\n\t\t\t\t{{title}}\n\t\t\t</p>\n\t\t\t<p class=\"kt-error_subtitle\">\n\t\t\t\t{{subtitle}}\n\t\t\t</p>\n\t\t\t<p class=\"kt-error_description\" [innerHTML]=\"desc\"></p>\n\t\t</div>\n\t</ng-container>\n\n\t<ng-container *ngIf=\"type === 'error-v5'\">\n\t\t<div class=\"kt-error_container\">\n\t\t\t<div class=\"kt-error_title\">\n\t\t\t\t<h1>{{title}}</h1>\n\t\t\t</div>\n\t\t\t<p class=\"kt-error_subtitle\">\n\t\t\t\t{{subtitle}}\n\t\t\t</p>\n\t\t\t<p class=\"kt-error_description\" [innerHTML]=\"desc\"></p>\n\t\t</div>\n\t</ng-container>\n\n\t<ng-container *ngIf=\"type === 'error-v6'\">\n\t\t<div class=\"kt-error_container\">\n\t\t\t<div class=\"kt-error_subtitle kt-font-light\">\n\t\t\t\t<h1>{{title}}</h1>\n\t\t\t</div>\n\t\t\t<p class=\"kt-error_description kt-font-light\" [innerHTML]=\"desc\"></p>\n\t\t</div>\n\t</ng-container>\n\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/content/general/notice/notice.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/content/general/notice/notice.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"alert alert-light alert-elevate\" [ngClass]=\"classes\" role=\"alert\">\n\t<div *ngIf=\"icon\" class=\"alert-icon alert-icon-top\">\n\t\t<i [ngClass]=\"icon\"></i>\n\t</div>\n\t<div class=\"alert-text\">\n\t\t<ng-content></ng-content>\n\t</div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/content/general/portlet/portlet.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/content/general/portlet/portlet.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"kt-portlet\" [ngClass]=\"class\" #portlet>\n\t<ng-content></ng-content>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/context-menu/context-menu.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/layout/context-menu/context-menu.component.html ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div ngbDropdown [placement]=\"'bottom-right'\" class=\"dropdown dropdown-inline\">\n\t<button ngbDropdownToggle type=\"button\" class=\"btn btn-clean btn-sm btn-icon btn-icon-md\">\n\t\t<i class=\"flaticon-more-v2\"></i>\n\t</button>\n\t<div ngbDropdownMenu class=\"dropdown-menu\">\n\t\t<ul class=\"kt-nav\">\n\t\t\t<li class=\"kt-nav__item\">\n\t\t\t\t<a href=\"javascript:;\" class=\"kt-nav__link\">\n\t\t\t\t\t<i class=\"kt-nav__link-icon flaticon2-line-chart\"></i>\n\t\t\t\t\t<span class=\"kt-nav__link-text\">Reports</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li class=\"kt-nav__item\">\n\t\t\t\t<a href=\"javascript:;\" class=\"kt-nav__link\">\n\t\t\t\t\t<i class=\"kt-nav__link-icon flaticon2-send\"></i>\n\t\t\t\t\t<span class=\"kt-nav__link-text\">Messages</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li class=\"kt-nav__item\">\n\t\t\t\t<a href=\"javascript:;\" class=\"kt-nav__link\">\n\t\t\t\t\t<i class=\"kt-nav__link-icon flaticon2-pie-chart-1\"></i>\n\t\t\t\t\t<span class=\"kt-nav__link-text\">Charts</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li class=\"kt-nav__item\">\n\t\t\t\t<a href=\"javascript:;\" class=\"kt-nav__link\">\n\t\t\t\t\t<i class=\"kt-nav__link-icon flaticon2-avatar\"></i>\n\t\t\t\t\t<span class=\"kt-nav__link-text\">Members</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li class=\"kt-nav__item\">\n\t\t\t\t<a href=\"javascript:;\" class=\"kt-nav__link\">\n\t\t\t\t\t<i class=\"kt-nav__link-icon flaticon2-settings\"></i>\n\t\t\t\t\t<span class=\"kt-nav__link-text\">Settings</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t</ul>\n\t</div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/context-menu2/context-menu2.component.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/layout/context-menu2/context-menu2.component.html ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div ngbDropdown [placement]=\"'bottom-right'\" class=\"dropdown\">\n\t<a ngbDropdownToggle href=\"javascript:;\" class=\"btn btn-label-brand btn-bold btn-sm dropdown-toggle\">\n\t\tExport\n\t</a>\n\t<div ngbDropdownMenu class=\"dropdown-menu dropdown-menu-fit\">\n\t\t<ul class=\"kt-nav\">\n\t\t\t<li class=\"kt-nav__section kt-nav__section--first\">\n\t\t\t\t<span class=\"kt-nav__section-text\">Choose an action:</span>\n\t\t\t</li>\n\t\t\t<li class=\"kt-nav__item\">\n\t\t\t\t<a href=\"javascript:;\" class=\"kt-nav__link\">\n\t\t\t\t\t<i class=\"kt-nav__link-icon flaticon2-graph-1\"></i>\n\t\t\t\t\t<span class=\"kt-nav__link-text\">Export</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li class=\"kt-nav__item\">\n\t\t\t\t<a href=\"javascript:;\" class=\"kt-nav__link\">\n\t\t\t\t\t<i class=\"kt-nav__link-icon flaticon2-calendar-4\"></i>\n\t\t\t\t\t<span class=\"kt-nav__link-text\">Save</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li class=\"kt-nav__item\">\n\t\t\t\t<a href=\"javascript:;\" class=\"kt-nav__link\">\n\t\t\t\t\t<i class=\"kt-nav__link-icon flaticon2-layers-1\"></i>\n\t\t\t\t\t<span class=\"kt-nav__link-text\">Import</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li class=\"kt-nav__item\">\n\t\t\t\t<a href=\"javascript:;\" class=\"kt-nav__link\">\n\t\t\t\t\t<i class=\"kt-nav__link-icon flaticon2-calendar-4\"></i>\n\t\t\t\t\t<span class=\"kt-nav__link-text\">Update</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li class=\"kt-nav__item\">\n\t\t\t\t<a href=\"javascript:;\" class=\"kt-nav__link\">\n\t\t\t\t\t<i class=\"kt-nav__link-icon flaticon2-file-1\"></i>\n\t\t\t\t\t<span class=\"kt-nav__link-text\">Customize</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t</ul>\n\t</div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/quick-panel/quick-panel.component.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/layout/quick-panel/quick-panel.component.html ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- begin::Offcanvas Toolbar Quick Actions -->\n<div ktOffcanvas [options]=\"offcanvasOptions\" id=\"kt_quick_panel\" class=\"kt-quick-panel\" [ngStyle]=\"{'overflow': 'hidden'}\">\n\t<a href=\"javascript:;\" class=\"kt-quick-panel__close\" id=\"kt_quick_panel_close_btn\"><i class=\"flaticon2-delete\"></i></a>\n\n\t<div class=\"kt-quick-panel__nav\">\n\t\t<ul ktTabClickEvent class=\"nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand kt-notification-item-padding-x\" role=\"tablist\">\n\t\t\t<li class=\"nav-item\">\n\t\t\t\t<a class=\"nav-link active\" href=\"javascript:;\" (click)=\"tab.select('tab-id-1');\" role=\"tab\">Notifications</a>\n\t\t\t</li>\n\t\t\t<li class=\"nav-item\">\n\t\t\t\t<a class=\"nav-link\" href=\"javascript:;\" (click)=\"tab.select('tab-id-2');\" role=\"tab\">Audit Logs</a>\n\t\t\t</li>\n\t\t\t<li class=\"nav-item\">\n\t\t\t\t<a class=\"nav-link\" href=\"javascript:;\" (click)=\"tab.select('tab-id-3');\" role=\"tab\">Settings</a>\n\t\t\t</li>\n\t\t</ul>\n\t</div>\n\n\t<div class=\"kt-quick-panel__content\">\n\t\t<ngb-tabset #tab=\"ngbTabset\">\n\t\t\t<ngb-tab id=\"tab-id-1\">\n\t\t\t\t<ng-template ngbTabContent>\n\t\t\t\t\t<div [perfectScrollbar]=\"{wheelPropagation: false}\" [ngStyle]=\"{'max-height': '85vh', 'position': 'relative'}\" class=\"kt-notification\">\n\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t<i class=\"flaticon2-line-chart kt-font-success\"></i>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\tNew order has been received\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t2 hrs ago\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t<i class=\"flaticon2-box-1 kt-font-brand\"></i>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\tNew customer is registered\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t3 hrs ago\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t<i class=\"flaticon2-chart2 kt-font-danger\"></i>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\tApplication has been approved\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t3 hrs ago\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t<i class=\"flaticon2-image-file kt-font-warning\"></i>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\tNew file has been uploaded\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t5 hrs ago\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t<i class=\"flaticon2-drop kt-font-info\"></i>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\tNew user feedback received\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t8 hrs ago\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t<i class=\"flaticon2-pie-chart-2 kt-font-success\"></i>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\tSystem reboot has been successfully completed\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t12 hrs ago\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t<i class=\"flaticon2-favourite kt-font-focus\"></i>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\tNew order has been placed\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t15 hrs ago\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item kt-notification__item--read\">\n\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t<i class=\"flaticon2-safe kt-font-primary\"></i>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\tCompany meeting canceled\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t19 hrs ago\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t<i class=\"flaticon2-psd kt-font-success\"></i>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\tNew report has been received\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t23 hrs ago\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t<i class=\"flaticon-download-1 kt-font-danger\"></i>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\tFinance report has been generated\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t25 hrs ago\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t<i class=\"flaticon-security kt-font-warning\"></i>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\tNew customer comment recieved\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t2 days ago\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t<i class=\"flaticon2-pie-chart kt-font-focus\"></i>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\tNew customer is registered\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t3 days ago\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</div>\n\t\t\t\t</ng-template>\n\t\t\t</ngb-tab>\n\t\t\t<ngb-tab id=\"tab-id-2\">\n\t\t\t\t<ng-template ngbTabContent>\n\t\t\t\t\t<div perfectScrollbar [ngStyle]=\"{'max-height': '85vh', 'position': 'relative'}\" class=\"kt-notification-v2\">\n\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification-v2__item\">\n\t\t\t\t\t\t\t<div class=\"kt-notification-v2__item-icon\">\n\t\t\t\t\t\t\t\t<i class=\"flaticon-bell kt-font-brand\"></i>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"kt-notification-v2__itek-wrapper\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification-v2__item-title\">\n\t\t\t\t\t\t\t\t\t5 new user generated report\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification-v2__item-desc\">\n\t\t\t\t\t\t\t\t\tReports based on sales\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</a>\n\n\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification-v2__item\">\n\t\t\t\t\t\t\t<div class=\"kt-notification-v2__item-icon\">\n\t\t\t\t\t\t\t\t<i class=\"flaticon2-box kt-font-danger\"></i>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"kt-notification-v2__itek-wrapper\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification-v2__item-title\">\n\t\t\t\t\t\t\t\t\t2 new items submited\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification-v2__item-desc\">\n\t\t\t\t\t\t\t\t\tby Grog John\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</a>\n\n\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification-v2__item\">\n\t\t\t\t\t\t\t<div class=\"kt-notification-v2__item-icon\">\n\t\t\t\t\t\t\t\t<i class=\"flaticon-psd kt-font-brand\"></i>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"kt-notification-v2__itek-wrapper\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification-v2__item-title\">\n\t\t\t\t\t\t\t\t\t79 PSD files generated\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification-v2__item-desc\">\n\t\t\t\t\t\t\t\t\tReports based on sales\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</a>\n\n\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification-v2__item\">\n\t\t\t\t\t\t\t<div class=\"kt-notification-v2__item-icon\">\n\t\t\t\t\t\t\t\t<i class=\"flaticon2-supermarket kt-font-warning\"></i>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"kt-notification-v2__itek-wrapper\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification-v2__item-title\">\n\t\t\t\t\t\t\t\t\t$2900 worth producucts sold\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification-v2__item-desc\">\n\t\t\t\t\t\t\t\t\tTotal 234 items\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</a>\n\n\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification-v2__item\">\n\t\t\t\t\t\t\t<div class=\"kt-notification-v2__item-icon\">\n\t\t\t\t\t\t\t\t<i class=\"flaticon-paper-plane-1 kt-font-success\"></i>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"kt-notification-v2__itek-wrapper\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification-v2__item-title\">\n\t\t\t\t\t\t\t\t\t4.5h-avarage response time\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification-v2__item-desc\">\n\t\t\t\t\t\t\t\t\tFostest is Barry\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</a>\n\n\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification-v2__item\">\n\t\t\t\t\t\t\t<div class=\"kt-notification-v2__item-icon\">\n\t\t\t\t\t\t\t\t<i class=\"flaticon2-information kt-font-danger\"></i>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"kt-notification-v2__itek-wrapper\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification-v2__item-title\">\n\t\t\t\t\t\t\t\t\tDatabase server is down\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification-v2__item-desc\">\n\t\t\t\t\t\t\t\t\t10 mins ago\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</a>\n\n\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification-v2__item\">\n\t\t\t\t\t\t\t<div class=\"kt-notification-v2__item-icon\">\n\t\t\t\t\t\t\t\t<i class=\"flaticon2-mail-1 kt-font-brand\"></i>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"kt-notification-v2__itek-wrapper\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification-v2__item-title\">\n\t\t\t\t\t\t\t\t\tSystem report has been generated\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification-v2__item-desc\">\n\t\t\t\t\t\t\t\t\tFostest is Barry\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</a>\n\n\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification-v2__item\">\n\t\t\t\t\t\t\t<div class=\"kt-notification-v2__item-icon\">\n\t\t\t\t\t\t\t\t<i class=\"flaticon2-hangouts-logo kt-font-warning\"></i>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"kt-notification-v2__itek-wrapper\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification-v2__item-title\">\n\t\t\t\t\t\t\t\t\t4.5h-avarage response time\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification-v2__item-desc\">\n\t\t\t\t\t\t\t\t\tFostest is Barry\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</div>\n\t\t\t\t</ng-template>\n\t\t\t</ngb-tab>\n\t\t\t<ngb-tab id=\"tab-id-3\">\n\t\t\t\t<ng-template ngbTabContent>\n\t\t\t\t\t<div class=\"kt-quick-panel__content-padding-x\">\n\t\t\t\t\t\t<form class=\"kt-form\">\n\t\t\t\t\t\t\t<div class=\"kt-heading kt-heading--sm kt-heading--space-sm\">Customer Care</div>\n\n\t\t\t\t\t\t\t<div class=\"form-group form-group-xs row\">\n\t\t\t\t\t\t\t\t<label class=\"col-8 col-form-label\">Enable Notifications:</label>\n\t\t\t\t\t\t\t\t<div class=\"col-4 kt-align-right\">\n                            <span class=\"kt-switch kt-switch--success kt-switch--sm\">\n\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" checked=\"checked\" name=\"quick_panel_notifications_1\">\n\t\t\t\t\t\t\t\t\t<span></span>\n                            </label>\n                            </span>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group form-group-xs row\">\n\t\t\t\t\t\t\t\t<label class=\"col-8 col-form-label\">Enable Case Tracking:</label>\n\t\t\t\t\t\t\t\t<div class=\"col-4 kt-align-right\">\n                            <span class=\"kt-switch kt-switch--success kt-switch--sm\">\n\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"quick_panel_notifications_2\">\n\t\t\t\t\t\t\t\t\t<span></span>\n                            </label>\n                            </span>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group form-group-last form-group-xs row\">\n\t\t\t\t\t\t\t\t<label class=\"col-8 col-form-label\">Support Portal:</label>\n\t\t\t\t\t\t\t\t<div class=\"col-4 kt-align-right\">\n                            <span class=\"kt-switch kt-switch--success kt-switch--sm\">\n\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" checked=\"checked\" name=\"quick_panel_notifications_2\">\n\t\t\t\t\t\t\t\t\t<span></span>\n                            </label>\n                            </span>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t<div class=\"kt-separator kt-separator--space-md kt-separator--border-dashed\"></div>\n\n\t\t\t\t\t\t\t<div class=\"kt-heading kt-heading--sm kt-heading--space-sm\">Reports</div>\n\n\t\t\t\t\t\t\t<div class=\"form-group form-group-xs row\">\n\t\t\t\t\t\t\t\t<label class=\"col-8 col-form-label\">Generate Reports:</label>\n\t\t\t\t\t\t\t\t<div class=\"col-4 kt-align-right\">\n                            <span class=\"kt-switch kt-switch--sm kt-switch--danger\">\n\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" checked=\"checked\" name=\"quick_panel_notifications_3\">\n\t\t\t\t\t\t\t\t\t<span></span>\n                            </label>\n                            </span>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group form-group-xs row\">\n\t\t\t\t\t\t\t\t<label class=\"col-8 col-form-label\">Enable Report Export:</label>\n\t\t\t\t\t\t\t\t<div class=\"col-4 kt-align-right\">\n                            <span class=\"kt-switch kt-switch--sm kt-switch--danger\">\n\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"quick_panel_notifications_3\">\n\t\t\t\t\t\t\t\t\t<span></span>\n                            </label>\n                            </span>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group form-group-last form-group-xs row\">\n\t\t\t\t\t\t\t\t<label class=\"col-8 col-form-label\">Allow Data Collection:</label>\n\t\t\t\t\t\t\t\t<div class=\"col-4 kt-align-right\">\n                            <span class=\"kt-switch kt-switch--sm kt-switch--danger\">\n\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" checked=\"checked\" name=\"quick_panel_notifications_4\">\n\t\t\t\t\t\t\t\t\t<span></span>\n                            </label>\n                            </span>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t<div class=\"kt-separator kt-separator--space-md kt-separator--border-dashed\"></div>\n\n\t\t\t\t\t\t\t<div class=\"kt-heading kt-heading--sm kt-heading--space-sm\">Memebers</div>\n\n\t\t\t\t\t\t\t<div class=\"form-group form-group-xs row\">\n\t\t\t\t\t\t\t\t<label class=\"col-8 col-form-label\">Enable Member singup:</label>\n\t\t\t\t\t\t\t\t<div class=\"col-4 kt-align-right\">\n                            <span class=\"kt-switch kt-switch--sm kt-switch--brand\">\n\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" checked=\"checked\" name=\"quick_panel_notifications_5\">\n\t\t\t\t\t\t\t\t\t<span></span>\n                            </label>\n                            </span>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group form-group-xs row\">\n\t\t\t\t\t\t\t\t<label class=\"col-8 col-form-label\">Allow User Feedbacks:</label>\n\t\t\t\t\t\t\t\t<div class=\"col-4 kt-align-right\">\n                            <span class=\"kt-switch kt-switch--sm kt-switch--brand\">\n\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"quick_panel_notifications_5\">\n\t\t\t\t\t\t\t\t\t<span></span>\n                            </label>\n                            </span>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"form-group form-group-last form-group-xs row\">\n\t\t\t\t\t\t\t\t<label class=\"col-8 col-form-label\">Enable Customer Portal:</label>\n\t\t\t\t\t\t\t\t<div class=\"col-4 kt-align-right\">\n                            <span class=\"kt-switch kt-switch--sm kt-switch--brand\">\n\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" checked=\"checked\" name=\"quick_panel_notifications_6\">\n\t\t\t\t\t\t\t\t\t<span></span>\n                            </label>\n                            </span>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</form>\n\t\t\t\t\t</div>\n\t\t\t\t</ng-template>\n\t\t\t</ngb-tab>\n\t\t</ngb-tabset>\n\t</div>\n</div>\n<!-- end::Offcanvas Toolbar Quick Actions -->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/scroll-top/scroll-top.component.html":
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/layout/scroll-top/scroll-top.component.html ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- begin::Scrolltop -->\n<div ktScrollTop [options]=\"scrollTopOptions\" id=\"kt_scrolltop\" class=\"kt-scrolltop\">\n\t<i class=\"la la-arrow-up\"></i>\n</div>\n<!-- end::Scrolltop -->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/search-result/search-result.component.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/layout/search-result/search-result.component.html ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"kt-quick-search__result\">\n\t<ng-container *ngFor=\"let item of data\">\n\t\t<ng-container *ngIf=\"item.type ===  0\" [ngTemplateOutlet]=\"separatorTemplate\" [ngTemplateOutletContext]=\"{item: item}\"></ng-container>\n\t\t<ng-container *ngIf=\"item.type ===  1\" [ngTemplateOutlet]=\"itemTemplate\" [ngTemplateOutletContext]=\"{item: item}\"></ng-container>\n\t</ng-container>\n</div>\n\n<ng-template #itemTemplate let-item=\"item\">\n\t<div class=\"kt-quick-search__item\">\n\t\t<div class=\"\" [innerHTML]=\"item.icon ? item.icon : item.img\" [ngClass]=\"{'kt-quick-search__item-img': item.img, 'kt-quick-search__item-icon': item.icon}\">\n\t\t</div>\n\t\t<div class=\"kt-quick-search__item-wrapper\">\n\t\t\t<a href=\"#\" class=\"kt-quick-search__item-title\">\n\t\t\t\t{{item.text}}\n\t\t\t</a>\n\t\t\t<div class=\"kt-quick-search__item-desc\">\n\t\t\t\t{{item.text}}\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</ng-template>\n\n<ng-template #separatorTemplate let-item=\"item\">\n\t<div class=\"kt-quick-search__category\">{{item.text}}</div>\n</ng-template>\n\n<ng-template #emptyResultTemplate>\n\t<div class=\"kt-quick-search__message kt-hidden\">\n\t\t{{noRecordText||'No record found'}}\n\t</div>\n</ng-template>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/splash-screen/splash-screen.component.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/layout/splash-screen/splash-screen.component.html ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- splash screen -->\n<div #splashScreen class=\"kt-splash-screen\">\n\t<span *ngIf=\"loaderType === 'spinner-message'\">{{loaderMessage}}</span>\n\t<img *ngIf=\"loaderType === 'spinner-logo'\" [attr.src]=\"loaderLogo\" alt=\"Logo\">\n\t<mat-spinner diameter=\"40\"></mat-spinner>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/sticky-toolbar/sticky-toolbar.component.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/layout/sticky-toolbar/sticky-toolbar.component.html ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- begin::Sticky Toolbar -->\n<ul class=\"kt-sticky-toolbar\" style=\"margin-top: 30px;\">\n\t<li class=\"kt-sticky-toolbar__item kt-sticky-toolbar__item--success\" id=\"kt_demo_panel_toggle\" data-placement=\"left\" ngbTooltip=\"Check out more demos\">\n\t\t<a href=\"javascript:;\" class=\"\"><i class=\"flaticon2-drop\"></i></a>\n\t</li>\n\t<li class=\"kt-sticky-toolbar__item kt-sticky-toolbar__item--brand\" data-placement=\"left\" ngbTooltip=\"Layout Builder\">\n\t\t<a href=\"javascript:;\" routerLink=\"/builder\"><i class=\"flaticon2-gear\"></i></a>\n\t</li>\n\t<li class=\"kt-sticky-toolbar__item kt-sticky-toolbar__item--warning\" data-placement=\"left\" ngbTooltip=\"Documentation\">\n\t\t<a href=\"https://keenthemes.com/metronic/?page=docs\" target=\"_blank\"><i class=\"flaticon2-telegram-logo\"></i></a>\n\t</li>\n</ul>\n<!-- end::Sticky Toolbar -->\n\n<!-- begin::Demo Panel -->\n<div ktOffcanvas [options]=\"demoPanelOptions\" id=\"kt_demo_panel\" class=\"kt-demo-panel\">\n\t<div class=\"kt-demo-panel__head\">\n\t\t<h3 class=\"kt-demo-panel__title\">\n\t\t\tSelect A Demo\n\t\t</h3>\n\t\t<a href=\"javascript:;\" class=\"kt-demo-panel__close\" id=\"kt_demo_panel_close\"><i class=\"flaticon2-delete\"></i></a>\n\t</div>\n\t<div class=\"kt-demo-panel__body kt-scroll\">\n\t\t<div class=\"kt-demo-panel__item\" [ngClass]=\"{'kt-demo-panel__item--active': isActiveDemo('demo1')}\">\n\t\t\t<div class=\"kt-demo-panel__item-title\">\n\t\t\t\tDemo1\n\t\t\t</div>\n\t\t\t<div class=\"kt-demo-panel__item-preview\">\n\t\t\t\t<img src=\"./assets/media/demos/preview/demo1.jpg\" alt=\"\">\n\t\t\t\t<div class=\"kt-demo-panel__item-preview-overlay\">\n\t\t\t\t\t<a href=\"{{baseHref}}demo1\" class=\"btn btn-brand btn-elevate\">Preview</a>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"kt-demo-panel__item\" [ngClass]=\"{'kt-demo-panel__item--active': isActiveDemo('demo2')}\">\n\t\t\t<div class=\"kt-demo-panel__item-title\">\n\t\t\t\tDemo 2\n\t\t\t</div>\n\t\t\t<div class=\"kt-demo-panel__item-preview\">\n\t\t\t\t<img src=\"./assets/media/demos/preview/demo2.jpg\" alt=\"\">\n\t\t\t\t<div class=\"kt-demo-panel__item-preview-overlay\">\n\t\t\t\t\t<a href=\"{{baseHref}}demo2\" class=\"btn btn-brand btn-elevate\">Preview</a>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"kt-demo-panel__item\" [ngClass]=\"{'kt-demo-panel__item--active': isActiveDemo('demo3')}\">\n\t\t\t<div class=\"kt-demo-panel__item-title\">\n\t\t\t\tDemo 3\n\t\t\t</div>\n\t\t\t<div class=\"kt-demo-panel__item-preview\">\n\t\t\t\t<img src=\"./assets/media/demos/preview/demo3.jpg\" alt=\"\">\n\t\t\t\t<div class=\"kt-demo-panel__item-preview-overlay\">\n\t\t\t\t\t<a href=\"{{baseHref}}demo3\" class=\"btn btn-brand btn-elevate\">Preview</a>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"kt-demo-panel__item\" [ngClass]=\"{'kt-demo-panel__item--active': isActiveDemo('demo4')}\">\n\t\t\t<div class=\"kt-demo-panel__item-title\">\n\t\t\t\tDemo 4\n\t\t\t</div>\n\t\t\t<div class=\"kt-demo-panel__item-preview\">\n\t\t\t\t<img src=\"./assets/media/demos/preview/demo4.jpg\" alt=\"\">\n\t\t\t\t<div class=\"kt-demo-panel__item-preview-overlay\">\n\t\t\t\t\t<a href=\"{{baseHref}}demo4\" class=\"btn btn-brand btn-elevate\">Preview</a>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"kt-demo-panel__item\" [ngClass]=\"{'kt-demo-panel__item--active': isActiveDemo('demo5')}\">\n\t\t\t<div class=\"kt-demo-panel__item-title\">\n\t\t\t\tDemo 5\n\t\t\t</div>\n\t\t\t<div class=\"kt-demo-panel__item-preview\">\n\t\t\t\t<img src=\"./assets/media/demos/preview/demo5.jpg\" alt=\"\">\n\t\t\t\t<div class=\"kt-demo-panel__item-preview-overlay\">\n\t\t\t\t\t<a href=\"{{baseHref}}demo5\" class=\"btn btn-brand btn-elevate\">Preview</a>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"kt-demo-panel__item\" [ngClass]=\"{'kt-demo-panel__item--active': isActiveDemo('demo6')}\">\n\t\t\t<div class=\"kt-demo-panel__item-title\">\n\t\t\t\tDemo 6\n\t\t\t</div>\n\t\t\t<div class=\"kt-demo-panel__item-preview\">\n\t\t\t\t<img src=\"./assets/media/demos/preview/demo6.jpg\" alt=\"\">\n\t\t\t\t<div class=\"kt-demo-panel__item-preview-overlay\">\n\t\t\t\t\t<a href=\"{{baseHref}}demo6\" class=\"btn btn-brand btn-elevate\">Preview</a>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"kt-demo-panel__item\" [ngClass]=\"{'kt-demo-panel__item--active': isActiveDemo('demo7')}\">\n\t\t\t<div class=\"kt-demo-panel__item-title\">\n\t\t\t\tDemo 7\n\t\t\t</div>\n\t\t\t<div class=\"kt-demo-panel__item-preview\">\n\t\t\t\t<img src=\"./assets/media/demos/preview/demo7.jpg\" alt=\"\">\n\t\t\t\t<div class=\"kt-demo-panel__item-preview-overlay\">\n\t\t\t\t\t<a href=\"{{baseHref}}demo7\" class=\"btn btn-brand btn-elevate\">Preview</a>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"kt-demo-panel__item\" [ngClass]=\"{'kt-demo-panel__item--active': isActiveDemo('demo8')}\">\n\t\t\t<div class=\"kt-demo-panel__item-title\">\n\t\t\t\tDemo 8\n\t\t\t</div>\n\t\t\t<div class=\"kt-demo-panel__item-preview\">\n\t\t\t\t<img src=\"./assets/media/demos/preview/demo8.jpg\" alt=\"\">\n\t\t\t\t<div class=\"kt-demo-panel__item-preview-overlay\">\n\t\t\t\t\t<a href=\"{{baseHref}}demo8\" class=\"btn btn-brand btn-elevate\">Preview</a>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"kt-demo-panel__item\" [ngClass]=\"{'kt-demo-panel__item--active': isActiveDemo('demo9')}\">\n\t\t\t<div class=\"kt-demo-panel__item-title\">\n\t\t\t\tDemo 9\n\t\t\t</div>\n\t\t\t<div class=\"kt-demo-panel__item-preview\">\n\t\t\t\t<img src=\"./assets/media/demos/preview/demo9.jpg\" alt=\"\">\n\t\t\t\t<div class=\"kt-demo-panel__item-preview-overlay\">\n\t\t\t\t\t<a href=\"{{baseHref}}demo9\" class=\"btn btn-brand btn-elevate\">Preview</a>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"kt-demo-panel__item\" [ngClass]=\"{'kt-demo-panel__item--active': isActiveDemo('demo10')}\">\n\t\t\t<div class=\"kt-demo-panel__item-title\">\n\t\t\t\tDemo 10\n\t\t\t</div>\n\t\t\t<div class=\"kt-demo-panel__item-preview\">\n\t\t\t\t<img src=\"./assets/media/demos/preview/demo10.jpg\" alt=\"\">\n\t\t\t\t<div class=\"kt-demo-panel__item-preview-overlay\">\n\t\t\t\t\t<a href=\"{{baseHref}}demo10\" class=\"btn btn-brand btn-elevate\">Preview</a>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"kt-demo-panel__item\" [ngClass]=\"{'kt-demo-panel__item--active': isActiveDemo('demo11')}\">\n\t\t\t<div class=\"kt-demo-panel__item-title\">\n\t\t\t\tDemo 11\n\t\t\t</div>\n\t\t\t<div class=\"kt-demo-panel__item-preview\">\n\t\t\t\t<img src=\"./assets/media/demos/preview/demo11.jpg\" alt=\"\">\n\t\t\t\t<div class=\"kt-demo-panel__item-preview-overlay\">\n\t\t\t\t\t<a href=\"javascript:;\" class=\"btn btn-brand btn-elevate disabled\">Coming soon</a>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"kt-demo-panel__item\" [ngClass]=\"{'kt-demo-panel__item--active': isActiveDemo('demo12')}\">\n\t\t\t<div class=\"kt-demo-panel__item-title\">\n\t\t\t\tDemo 12\n\t\t\t</div>\n\t\t\t<div class=\"kt-demo-panel__item-preview\">\n\t\t\t\t<img src=\"./assets/media/demos/preview/demo12.jpg\" alt=\"\">\n\t\t\t\t<div class=\"kt-demo-panel__item-preview-overlay\">\n\t\t\t\t\t<a href=\"javascript:;\" class=\"btn btn-brand btn-elevate disabled\">Coming soon</a>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"kt-demo-panel__item\" [ngClass]=\"{'kt-demo-panel__item--active': isActiveDemo('demo13')}\">\n\t\t\t<div class=\"kt-demo-panel__item-title\">\n\t\t\t\tDemo 13\n\t\t\t</div>\n\t\t\t<div class=\"kt-demo-panel__item-preview\">\n\t\t\t\t<img src=\"./assets/media/demos/preview/demo13.jpg\" alt=\"\">\n\t\t\t\t<div class=\"kt-demo-panel__item-preview-overlay\">\n\t\t\t\t\t<a href=\"javascript:;\" class=\"btn btn-brand btn-elevate disabled\">Coming soon</a>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"kt-demo-panel__item\" [ngClass]=\"{'kt-demo-panel__item--active': isActiveDemo('demo14')}\">\n\t\t\t<div class=\"kt-demo-panel__item-title\">\n\t\t\t\tDemo 14\n\t\t\t</div>\n\t\t\t<div class=\"kt-demo-panel__item-preview\">\n\t\t\t\t<img src=\"./assets/media/demos/preview/demo14.jpg\" alt=\"\">\n\t\t\t\t<div class=\"kt-demo-panel__item-preview-overlay\">\n\t\t\t\t\t<a href=\"javascript:;\" class=\"btn btn-brand btn-elevate disabled\">Coming soon</a>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<a href=\"https://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes\" class=\"kt-demo-panel__purchase btn btn-brand btn-elevate btn-bold btn-upper\" target=\"_blank\">\n\t\t\tBuy Metronic Now!\n\t\t</a>\n\t</div>\n</div>\n<!-- end::Demo Panel -->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/subheader/subheader-search/subheader-search.component.html":
/*!****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/layout/subheader/subheader-search/subheader-search.component.html ***!
  \****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"kt-subheader-search\">\n\t<div class=\"kt-container\" [ngClass]=\"{'kt-container--fluid': fluid}\">\n\t\t<h3 class=\"kt-subheader-search__title\">\n\t\t\tRecent Bookings\n\t\t\t<span class=\"kt-subheader-search__desc\">Onling Bookings Management</span>\n\t\t</h3>\n\t\t<form class=\"kt-form\">\n\t\t\t<div class=\"kt-grid kt-grid--desktop kt-grid--ver-desktop\">\n\t\t\t\t<div class=\"kt-grid__item kt-grid__item--middle\">\n\t\t\t\t\t<div class=\"row kt-margin-r-10\">\n\t\t\t\t\t\t<div class=\"col-lg-6\">\n\t\t\t\t\t\t\t<div class=\"kt-input-icon kt-input-icon--pill kt-input-icon--right\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-pill\" placeholder=\"Booking Number\">\n\t\t\t\t\t\t\t\t<span class=\"kt-input-icon__icon kt-input-icon__icon--right\"><span><i class=\"la la-puzzle-piece\"></i></span></span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-lg-3\">\n\t\t\t\t\t\t\t<div class=\"kt-input-icon kt-input-icon--pill kt-input-icon--right\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-pill\" placeholder=\"From\">\n\t\t\t\t\t\t\t\t<span class=\"kt-input-icon__icon kt-input-icon__icon--right\"><span><i class=\"la la-calendar-check-o\"></i></span></span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-lg-3\">\n\t\t\t\t\t\t\t<div class=\"kt-input-icon kt-input-icon--pill kt-input-icon--right\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-pill\" placeholder=\"To\">\n\t\t\t\t\t\t\t\t<span class=\"kt-input-icon__icon kt-input-icon__icon--right\"><span><i class=\"la la-calendar-check-o\"></i></span></span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"kt-grid__item kt-grid__item--middle\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-pill btn-upper btn-bold btn-font-sm kt-subheader-search__submit-btn\">Search Bookings</button>\n\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-subheader-search__link kt-link\">Advance Search</a>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</form>\n\t</div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/subheader/subheader1/subheader1.component.html":
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/layout/subheader/subheader1/subheader1.component.html ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- begin:: Content Head -->\n<div class=\"kt-subheader kt-grid__item\" [ngClass]=\"{'kt-container--clear': clear}\" [hidden]=\"subheaderService.disabled$ | async\" id=\"kt_subheader\">\n\t<div class=\"kt-container\" [ngClass]=\"{'kt-container--fluid': fluid}\">\n\t\t<div class=\"kt-subheader__main\">\n<!--\t\t\t<h3 *ngIf=\"title\" class=\"kt-subheader__title\">{{title}}</h3>-->\n<!--\t\t\t<span class=\"kt-subheader__separator kt-subheader__separator&#45;&#45;v\"></span>-->\n\t\t\t<!--<h4 *ngIf=\"desc\" class=\"kt-subheader__desc\">{{desc}}</h4>-->\n<!--\t\t\t<h4 class=\"kt-subheader__desc\">#XRS-45670</h4>-->\n<!--\t\t\t<a href=\"javascript:;\" class=\"btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10\">-->\n<!--\t\t\t\tAdd New-->\n<!--\t\t\t</a>-->\n\t\t</div>\n<!--\t\t<div class=\"kt-subheader__toolbar\">-->\n<!--\t\t\t<div class=\"kt-subheader__wrapper\">-->\n<!--\t\t\t\t<a href=\"javascript:;\" class=\"btn kt-subheader__btn-secondary\">Today</a>-->\n<!--\t\t\t\t<a href=\"javascript:;\" class=\"btn kt-subheader__btn-secondary\">Month</a>-->\n<!--\t\t\t\t<a href=\"javascript:;\" class=\"btn kt-subheader__btn-secondary\">Year</a>-->\n<!--\t\t\t\t<a href=\"javascript:;\" class=\"btn kt-subheader__btn-primary\">-->\n<!--\t\t\t\t\tActions &nbsp;-->\n<!--\t\t\t\t\t<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24px\" height=\"24px\" viewBox=\"0 0 24 24\" version=\"1.1\" class=\"kt-svg-icon kt-svg-icon&#45;&#45;sm\">-->\n<!--\t\t\t\t\t\t<g stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">-->\n<!--\t\t\t\t\t\t\t<rect id=\"bound\" x=\"0\" y=\"0\" width=\"24\" height=\"24\"></rect>-->\n<!--\t\t\t\t\t\t\t<rect id=\"Rectangle-8\" fill=\"#000000\" x=\"4\" y=\"5\" width=\"16\" height=\"3\" rx=\"1.5\"></rect>-->\n<!--\t\t\t\t\t\t\t<path d=\"M7.5,11 L16.5,11 C17.3284271,11 18,11.6715729 18,12.5 C18,13.3284271 17.3284271,14 16.5,14 L7.5,14 C6.67157288,14 6,13.3284271 6,12.5 C6,11.6715729 6.67157288,11 7.5,11 Z M10.5,17 L13.5,17 C14.3284271,17 15,17.6715729 15,18.5 C15,19.3284271 14.3284271,20 13.5,20 L10.5,20 C9.67157288,20 9,19.3284271 9,18.5 C9,17.6715729 9.67157288,17 10.5,17 Z\" id=\"Combined-Shape\" fill=\"#000000\" opacity=\"0.3\"></path>-->\n<!--\t\t\t\t\t\t</g>-->\n<!--\t\t\t\t\t</svg>-->\n<!--\t\t\t\t</a>-->\n\n<!--\t\t\t\t<div ngbDropdown [placement]=\"'bottom-right'\"  class=\"dropdown dropdown-inline\" ngbTooltip=\"Quick actions\">-->\n<!--\t\t\t\t\t<a ngbDropdownToggle href=\"javascript:;\" class=\"btn btn-icon\">-->\n<!--\t\t\t\t\t\t<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24px\" height=\"24px\" viewBox=\"0 0 24 24\" version=\"1.1\" class=\"kt-svg-icon kt-svg-icon&#45;&#45;success kt-svg-icon&#45;&#45;md\">-->\n<!--\t\t\t\t\t\t\t<g stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">-->\n<!--\t\t\t\t\t\t\t\t<polygon id=\"Shape\" points=\"0 0 24 0 24 24 0 24\"></polygon>-->\n<!--\t\t\t\t\t\t\t\t<path d=\"M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z\" id=\"Combined-Shape\" fill=\"#000000\" fill-rule=\"nonzero\" opacity=\"0.3\"></path>-->\n<!--\t\t\t\t\t\t\t\t<path d=\"M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z\" id=\"Combined-Shape\" fill=\"#000000\"></path>-->\n<!--\t\t\t\t\t\t\t</g>-->\n<!--\t\t\t\t\t\t</svg>-->\n<!--\t\t\t\t\t</a>-->\n<!--\t\t\t\t\t<div ngbDropdownMenu class=\"dropdown-menu dropdown-menu-fit dropdown-menu-md\">-->\n<!--\t\t\t\t\t\t&lt;!&ndash;begin::Nav&ndash;&gt;-->\n<!--\t\t\t\t\t\t<ul class=\"kt-nav\">-->\n<!--\t\t\t\t\t\t\t<li class=\"kt-nav__head\">-->\n<!--\t\t\t\t\t\t\t\tAdd anything or jump to:-->\n<!--\t\t\t\t\t\t\t\t<i class=\"flaticon2-information\" data-placement=\"left\" ngbTooltip=\"Click to learn more...\"></i>-->\n<!--\t\t\t\t\t\t\t</li>-->\n<!--\t\t\t\t\t\t\t<li class=\"kt-nav__separator\"></li>-->\n<!--\t\t\t\t\t\t\t<li class=\"kt-nav__item\">-->\n<!--\t\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-nav__link\">-->\n<!--\t\t\t\t\t\t\t\t\t<i class=\"kt-nav__link-icon flaticon2-drop\"></i>-->\n<!--\t\t\t\t\t\t\t\t\t<span class=\"kt-nav__link-text\">Order</span>-->\n<!--\t\t\t\t\t\t\t\t</a>-->\n<!--\t\t\t\t\t\t\t</li>-->\n<!--\t\t\t\t\t\t\t<li class=\"kt-nav__item\">-->\n<!--\t\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-nav__link\">-->\n<!--\t\t\t\t\t\t\t\t\t<i class=\"kt-nav__link-icon flaticon2-calendar-8\"></i>-->\n<!--\t\t\t\t\t\t\t\t\t<span class=\"kt-nav__link-text\">Ticket</span>-->\n<!--\t\t\t\t\t\t\t\t</a>-->\n<!--\t\t\t\t\t\t\t</li>-->\n<!--\t\t\t\t\t\t\t<li class=\"kt-nav__item\">-->\n<!--\t\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-nav__link\">-->\n<!--\t\t\t\t\t\t\t\t\t<i class=\"kt-nav__link-icon flaticon2-telegram-logo\"></i>-->\n<!--\t\t\t\t\t\t\t\t\t<span class=\"kt-nav__link-text\">Goal</span>-->\n<!--\t\t\t\t\t\t\t\t</a>-->\n<!--\t\t\t\t\t\t\t</li>-->\n<!--\t\t\t\t\t\t\t<li class=\"kt-nav__item\">-->\n<!--\t\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-nav__link\">-->\n<!--\t\t\t\t\t\t\t\t\t<i class=\"kt-nav__link-icon flaticon2-new-email\"></i>-->\n<!--\t\t\t\t\t\t\t\t\t<span class=\"kt-nav__link-text\">Support Case</span>-->\n<!--\t\t\t\t\t\t\t\t\t<span class=\"kt-nav__link-badge\">-->\n<!--                                        <span class=\"kt-badge kt-badge&#45;&#45;success\">5</span>-->\n<!--                                    </span>-->\n<!--\t\t\t\t\t\t\t\t</a>-->\n<!--\t\t\t\t\t\t\t</li>-->\n<!--\t\t\t\t\t\t\t<li class=\"kt-nav__separator\"></li>-->\n<!--\t\t\t\t\t\t\t<li class=\"kt-nav__foot\">-->\n<!--\t\t\t\t\t\t\t\t<a class=\"btn btn-label-brand btn-bold btn-sm\" href=\"javascript:;\">Upgrade plan</a>-->\n<!--\t\t\t\t\t\t\t\t<a class=\"btn btn-clean btn-bold btn-sm\" href=\"javascript:;\" data-placement=\"left\" ngbTooltip=\"Click to learn more...\">Learn more</a>-->\n<!--\t\t\t\t\t\t\t</li>-->\n<!--\t\t\t\t\t\t</ul>-->\n<!--\t\t\t\t\t\t&lt;!&ndash;end::Nav&ndash;&gt;-->\n<!--\t\t\t\t\t</div>-->\n<!--\t\t\t\t</div>-->\n<!--\t\t\t</div>-->\n<!--\t\t</div>-->\n\t</div>\n</div>\n<!-- end:: Content Head -->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/subheader/subheader2/subheader2.component.html":
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/layout/subheader/subheader2/subheader2.component.html ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- begin:: Content Head -->\n<div class=\"kt-subheader kt-grid__item\" [ngClass]=\"{'kt-container--clear': clear}\" [hidden]=\"subheaderService.disabled$ | async\" id=\"kt_subheader\">\n\t<div class=\"kt-container\" [ngClass]=\"{'kt-container--fluid': fluid}\">\n\t\t<div class=\"kt-subheader__main\">\n\t\t\t<h3 *ngIf=\"title\" class=\"kt-subheader__title\">{{title}}</h3>\n\t\t\t<span class=\"kt-subheader__separator kt-subheader__separator--v\"></span>\n\t\t\t<span *ngIf=\"desc\" class=\"kt-subheader__desc\">{{desc}}</span>\n\t\t\t<a href=\"javascript:;\" class=\"btn btn-label-primary btn-bold btn-icon-h\">\n\t\t\t\tAdd New\n\t\t\t</a>\n\t\t</div>\n\t\t<div class=\"kt-subheader__toolbar\">\n\t\t\t<div class=\"kt-subheader__wrapper\">\n\t\t\t\t<a href=\"javascript:;\" class=\"btn kt-subheader__btn-primary\">\n\t\t\t\t\tActions &nbsp;\n\t\t\t\t\t<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24px\" height=\"24px\" viewBox=\"0 0 24 24\" version=\"1.1\" class=\"kt-svg-icon kt-svg-icon--sm\">\n\t\t\t\t\t\t<g stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">\n\t\t\t\t\t\t\t<rect id=\"bound\" x=\"0\" y=\"0\" width=\"24\" height=\"24\"></rect>\n\t\t\t\t\t\t\t<rect id=\"Rectangle-8\" fill=\"#000000\" x=\"4\" y=\"5\" width=\"16\" height=\"3\" rx=\"1.5\"></rect>\n\t\t\t\t\t\t\t<path d=\"M7.5,11 L16.5,11 C17.3284271,11 18,11.6715729 18,12.5 C18,13.3284271 17.3284271,14 16.5,14 L7.5,14 C6.67157288,14 6,13.3284271 6,12.5 C6,11.6715729 6.67157288,11 7.5,11 Z M10.5,17 L13.5,17 C14.3284271,17 15,17.6715729 15,18.5 C15,19.3284271 14.3284271,20 13.5,20 L10.5,20 C9.67157288,20 9,19.3284271 9,18.5 C9,17.6715729 9.67157288,17 10.5,17 Z\" id=\"Combined-Shape\" fill=\"#000000\" opacity=\"0.3\"></path>\n\t\t\t\t\t\t</g>\n\t\t\t\t\t</svg>\n\t\t\t\t</a>\n\n\t\t\t\t<a href=\"javascript:;\" class=\"btn kt-subheader__btn-primary btn-icon\">\n\t\t\t\t\t<i class=\"flaticon2-file\"></i>\n\t\t\t\t</a>\n\n\t\t\t\t<a href=\"javascript:;\" class=\"btn kt-subheader__btn-primary btn-icon\">\n\t\t\t\t\t<i class=\"flaticon-download-1\"></i>\n\t\t\t\t</a>\n\n\t\t\t\t<a href=\"javascript:;\" class=\"btn kt-subheader__btn-primary btn-icon\">\n\t\t\t\t\t<i class=\"flaticon2-fax\"></i>\n\t\t\t\t</a>\n\n\t\t\t\t<a href=\"javascript:;\" class=\"btn kt-subheader__btn-primary btn-icon\">\n\t\t\t\t\t<i class=\"flaticon2-settings\"></i>\n\t\t\t\t</a>\n\n\t\t\t\t<div ngbDropdown [placement]=\"'bottom-right'\" class=\"dropdown dropdown-inline\" ngbTooltip=\"Quick actions\">\n\t\t\t\t\t<a ngbDropdownToggle href=\"javascript:;\" class=\"btn btn-icon\">\n\t\t\t\t\t\t<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24px\" height=\"24px\" viewBox=\"0 0 24 24\" version=\"1.1\" class=\"kt-svg-icon kt-svg-icon--success kt-svg-icon--md\">\n\t\t\t\t\t\t\t<g stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">\n\t\t\t\t\t\t\t\t<polygon id=\"Shape\" points=\"0 0 24 0 24 24 0 24\"></polygon>\n\t\t\t\t\t\t\t\t<path d=\"M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z\" id=\"Combined-Shape\" fill=\"#000000\" fill-rule=\"nonzero\" opacity=\"0.3\"></path>\n\t\t\t\t\t\t\t\t<path d=\"M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z\" id=\"Combined-Shape\" fill=\"#000000\"></path>\n\t\t\t\t\t\t\t</g>\n\t\t\t\t\t\t</svg>\n\t\t\t\t\t</a>\n\t\t\t\t\t<div ngbDropdownMenu class=\"dropdown-menu dropdown-menu-fit dropdown-menu-md\">\n\t\t\t\t\t\t<!--begin::Nav-->\n\t\t\t\t\t\t<ul class=\"kt-nav\">\n\t\t\t\t\t\t\t<li class=\"kt-nav__head\">\n\t\t\t\t\t\t\t\tExport Options:\n\t\t\t\t\t\t\t\t<i class=\"flaticon2-correct kt-font-warning\" data-placement=\"left\" ngbTooltip=\"Click to learn more...\"></i>\n\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t<li class=\"kt-nav__separator\"></li>\n\t\t\t\t\t\t\t<li class=\"kt-nav__item\">\n\t\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-nav__link\">\n\t\t\t\t\t\t\t\t\t<i class=\"kt-nav__link-icon flaticon2-drop\"></i>\n\t\t\t\t\t\t\t\t\t<span class=\"kt-nav__link-text\">Orders</span>\n\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t<li class=\"kt-nav__item\">\n\t\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-nav__link\">\n\t\t\t\t\t\t\t\t\t<i class=\"kt-nav__link-icon flaticon2-new-email\"></i>\n\t\t\t\t\t\t\t\t\t<span class=\"kt-nav__link-text\">Members</span>\n\t\t\t\t\t\t\t\t\t<span class=\"kt-nav__link-badge\">\n                                        <span class=\"kt-badge kt-badge--brand kt-badge--rounded\">15</span>\n                                    </span>\n\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t<li class=\"kt-nav__item\">\n\t\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-nav__link\">\n\t\t\t\t\t\t\t\t\t<i class=\"kt-nav__link-icon flaticon2-calendar-8\"></i>\n\t\t\t\t\t\t\t\t\t<span class=\"kt-nav__link-text\">Reports</span>\n\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t<li class=\"kt-nav__item\">\n\t\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-nav__link\">\n\t\t\t\t\t\t\t\t\t<i class=\"kt-nav__link-icon flaticon2-telegram-logo\"></i>\n\t\t\t\t\t\t\t\t\t<span class=\"kt-nav__link-text\">Finance</span>\n\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t<li class=\"kt-nav__separator\"></li>\n\t\t\t\t\t\t\t<li class=\"kt-nav__foot\">\n\t\t\t\t\t\t\t\t<a class=\"btn btn-label-brand btn-bold btn-sm\" href=\"javascript:;\">More options</a>\n\t\t\t\t\t\t\t\t<a class=\"btn btn-clean btn-bold btn-sm kt-hidden\" href=\"javascript:;\" data-placement=\"left\" ngbTooltip=\"Click to learn more...\">Learn more</a>\n\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t<!--end::Nav-->\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<!-- end:: Content Head -->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/subheader/subheader3/subheader3.component.html":
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/layout/subheader/subheader3/subheader3.component.html ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- begin:: Content Head -->\n<div class=\"kt-subheader kt-grid__item\" [ngClass]=\"{'kt-container--clear': clear}\" [hidden]=\"subheaderService.disabled$ | async\" id=\"kt_subheader\">\n\t<div class=\"kt-container\" [ngClass]=\"{'kt-container--fluid': fluid}\">\n\t\t<div class=\"kt-subheader__main\">\n\t\t\t<h3 *ngIf=\"title\" class=\"kt-subheader__title\">{{title}}</h3>\n\t\t\t<h4 *ngIf=\"desc\" class=\"kt-subheader__desc\">{{desc}}</h4>\n\t\t\t<div class=\"kt-subheader__breadcrumbs\" *ngIf=\"breadcrumbs.length > 0\">\n\t\t\t\t<span class=\"kt-subheader__separator\" [hidden]=\"true\"></span>\n\t\t\t\t<a class=\"kt-subheader__breadcrumbs-home\"><i class=\"flaticon2-shelter\"></i></a>\n\t\t\t\t<ng-container *ngFor=\"let item of breadcrumbs\">\n\t\t\t\t\t<span class=\"kt-subheader__breadcrumbs-separator\"></span>\n\t\t\t\t\t<a [routerLink]=\"item.page\" [queryParams]=\"item.queryParams\" class=\"kt-subheader__breadcrumbs-link\">\n\t\t\t\t\t\t{{item.title}}\n\t\t\t\t\t</a>\n\t\t\t\t</ng-container>\n\t\t\t\t<!-- <span class=\"kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active\">Active link</span> -->\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"kt-subheader__toolbar\">\n\t\t\t<div class=\"kt-subheader__wrapper\">\n\t\t\t\t<a href=\"javascript:;\" class=\"btn kt-subheader__btn-secondary\">Today</a>\n\n\t\t\t\t<a href=\"javascript:;\" class=\"btn kt-subheader__btn-secondary\">Month</a>\n\n\t\t\t\t<a href=\"javascript:;\" class=\"btn kt-subheader__btn-secondary\">Year</a>\n\n\t\t\t\t<a href=\"javascript:;\" class=\"btn kt-subheader__btn-primary\">\n\t\t\t\t\tActions &nbsp;\n\t\t\t\t\t<!--<i class=\"flaticon2-calendar-1\"></i>-->\n\t\t\t\t\t<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24px\" height=\"24px\" viewBox=\"0 0 24 24\" version=\"1.1\" class=\"kt-svg-icon kt-svg-icon--sm\">\n\t\t\t\t\t\t<g stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">\n\t\t\t\t\t\t\t<rect id=\"bound\" x=\"0\" y=\"0\" width=\"24\" height=\"24\"></rect>\n\t\t\t\t\t\t\t<rect id=\"Rectangle-8\" fill=\"#000000\" x=\"4\" y=\"5\" width=\"16\" height=\"3\" rx=\"1.5\"></rect>\n\t\t\t\t\t\t\t<path d=\"M7.5,11 L16.5,11 C17.3284271,11 18,11.6715729 18,12.5 C18,13.3284271 17.3284271,14 16.5,14 L7.5,14 C6.67157288,14 6,13.3284271 6,12.5 C6,11.6715729 6.67157288,11 7.5,11 Z M10.5,17 L13.5,17 C14.3284271,17 15,17.6715729 15,18.5 C15,19.3284271 14.3284271,20 13.5,20 L10.5,20 C9.67157288,20 9,19.3284271 9,18.5 C9,17.6715729 9.67157288,17 10.5,17 Z\" id=\"Combined-Shape\" fill=\"#000000\" opacity=\"0.3\"></path>\n\t\t\t\t\t\t</g>\n\t\t\t\t\t</svg>\n\t\t\t\t</a>\n\n\t\t\t\t<div ngbDropdown [placement]=\"'bottom-right'\" class=\"dropdown dropdown-inline\" title=\"Quick actions\">\n\t\t\t\t\t<a ngbDropdownToggle href=\"javascript:;\" class=\"btn btn-icon\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n\t\t\t\t\t\t<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24px\" height=\"24px\" viewBox=\"0 0 24 24\" version=\"1.1\" class=\"kt-svg-icon kt-svg-icon--success kt-svg-icon--md\">\n\t\t\t\t\t\t\t<g stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">\n\t\t\t\t\t\t\t\t<polygon id=\"Shape\" points=\"0 0 24 0 24 24 0 24\"></polygon>\n\t\t\t\t\t\t\t\t<path d=\"M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z\" id=\"Combined-Shape\" fill=\"#000000\" fill-rule=\"nonzero\" opacity=\"0.3\"></path>\n\t\t\t\t\t\t\t\t<path d=\"M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z\" id=\"Combined-Shape\" fill=\"#000000\"></path>\n\t\t\t\t\t\t\t</g>\n\t\t\t\t\t\t</svg>                        <!--<i class=\"flaticon2-plus\"></i>-->\n\t\t\t\t\t</a>\n\t\t\t\t\t<div ngbDropdownMenu class=\"dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right\">\n\t\t\t\t\t\t<!--begin::Nav-->\n\t\t\t\t\t\t<ul class=\"kt-nav\">\n\t\t\t\t\t\t\t<li class=\"kt-nav__head\">\n\t\t\t\t\t\t\t\tAdd anything or jump to:\n\t\t\t\t\t\t\t\t<i class=\"flaticon2-information\" data-placement=\"left\" ngbTooltip=\"Click to learn more...\"></i>\n\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t<li class=\"kt-nav__separator\"></li>\n\t\t\t\t\t\t\t<li class=\"kt-nav__item\">\n\t\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-nav__link\">\n\t\t\t\t\t\t\t\t\t<i class=\"kt-nav__link-icon flaticon2-drop\"></i>\n\t\t\t\t\t\t\t\t\t<span class=\"kt-nav__link-text\">Order</span>\n\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t<li class=\"kt-nav__item\">\n\t\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-nav__link\">\n\t\t\t\t\t\t\t\t\t<i class=\"kt-nav__link-icon flaticon2-calendar-8\"></i>\n\t\t\t\t\t\t\t\t\t<span class=\"kt-nav__link-text\">Ticket</span>\n\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t<li class=\"kt-nav__item\">\n\t\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-nav__link\">\n\t\t\t\t\t\t\t\t\t<i class=\"kt-nav__link-icon flaticon2-telegram-logo\"></i>\n\t\t\t\t\t\t\t\t\t<span class=\"kt-nav__link-text\">Goal</span>\n\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t<li class=\"kt-nav__item\">\n\t\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-nav__link\">\n\t\t\t\t\t\t\t\t\t<i class=\"kt-nav__link-icon flaticon2-new-email\"></i>\n\t\t\t\t\t\t\t\t\t<span class=\"kt-nav__link-text\">Support Case</span>\n\t\t\t\t\t\t\t\t\t<span class=\"kt-nav__link-badge\">\n                                        <span class=\"kt-badge kt-badge--brand kt-badge--rounded\">5</span>\n                                    </span>\n\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t<li class=\"kt-nav__separator\"></li>\n\t\t\t\t\t\t\t<li class=\"kt-nav__foot\">\n\t\t\t\t\t\t\t\t<a class=\"btn btn-label-brand btn-bold btn-sm\" href=\"javascript:;\">Upgrade plan</a>\n\t\t\t\t\t\t\t\t<a class=\"btn btn-clean btn-bold btn-sm kt-hidden\" href=\"javascript:;\" data-placement=\"left\" ngbTooltip=\"Click to learn more...\">Learn more</a>\n\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t<!--end::Nav-->\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<!-- end:: Content Head -->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/subheader/subheader4/subheader4.component.html":
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/layout/subheader/subheader4/subheader4.component.html ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- begin:: Content Head -->\n<div class=\"kt-subheader kt-grid__item\" [ngClass]=\"{'kt-container--clear': clear}\" [hidden]=\"subheaderService.disabled$ | async\" id=\"kt_subheader\">\n\t<div class=\"kt-container\" [ngClass]=\"{'kt-container--fluid': fluid}\">\n\t\t<div class=\"kt-subheader__main\">\n\t\t\t<h3 *ngIf=\"title\" class=\"kt-subheader__title\">{{title}}</h3>\n\t\t\t<h4 *ngIf=\"desc\" class=\"kt-subheader__desc\">{{desc}}</h4>\n\t\t\t<div class=\"kt-subheader__breadcrumbs\" *ngIf=\"breadcrumbs.length > 0\">\n\t\t\t\t<span class=\"kt-subheader__separator\" [hidden]=\"true\"></span>\n\t\t\t\t<a class=\"kt-subheader__breadcrumbs-home\"><i class=\"flaticon2-shelter\"></i></a>\n\t\t\t\t<ng-container *ngFor=\"let item of breadcrumbs\">\n\t\t\t\t\t<span class=\"kt-subheader__breadcrumbs-separator\"></span>\n\t\t\t\t\t<a [routerLink]=\"item.page\" [queryParams]=\"item.queryParams\" class=\"kt-subheader__breadcrumbs-link\">\n\t\t\t\t\t\t{{item.title}}\n\t\t\t\t\t</a>\n\t\t\t\t</ng-container>\n\t\t\t\t<!-- <span class=\"kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active\">Active link</span> -->\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"kt-subheader__toolbar\">\n\t\t\t<div class=\"kt-subheader__wrapper\">\n\t\t\t\t<a href=\"javascript:;\" class=\"btn kt-subheader__btn-secondary\">\n\t\t\t\t\tReports\n\t\t\t\t</a>\n\n\t\t\t\t<div ngbDropdown [placement]=\"'bottom-right'\" class=\"dropdown dropdown-inline\">\n\t\t\t\t\t<a ngbDropdownToggle href=\"javascript:;\" class=\"btn btn-danger kt-subheader__btn-options\">\n\t\t\t\t\t\tProducts\n\t\t\t\t\t</a>\n\t\t\t\t\t<div ngbDropdownMenu class=\"dropdown-menu dropdown-menu-right\">\n\t\t\t\t\t\t<a class=\"dropdown-item\" href=\"javascript:;\"><i class=\"la la-plus\"></i> New Product</a>\n\t\t\t\t\t\t<a class=\"dropdown-item\" href=\"javascript:;\"><i class=\"la la-user\"></i> New Order</a>\n\t\t\t\t\t\t<a class=\"dropdown-item\" href=\"javascript:;\"><i class=\"la la-cloud-download\"></i> New Download</a>\n\t\t\t\t\t\t<div class=\"dropdown-divider\"></div>\n\t\t\t\t\t\t<a class=\"dropdown-item\" href=\"javascript:;\"><i class=\"la la-cog\"></i> Settings</a>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<!-- end:: Content Head -->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/subheader/subheader5/subheader5.component.html":
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/layout/subheader/subheader5/subheader5.component.html ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- begin:: Content Head -->\n<div class=\"kt-subheader kt-grid__item\" [ngClass]=\"{'kt-container--clear': clear}\" [hidden]=\"subheaderService.disabled$ | async\" id=\"kt_subheader\">\n\t<div class=\"kt-container\" [ngClass]=\"{'kt-container--fluid': fluid}\">\n\t\t<h3 *ngIf=\"title\" class=\"kt-subheader__title\">{{title}}</h3>\n\t\t<h4 *ngIf=\"desc\" class=\"kt-subheader__desc\">{{desc}}</h4>\n\t\t<div class=\"kt-subheader__breadcrumbs\" *ngIf=\"breadcrumbs.length > 0\">\n\t\t\t<span class=\"kt-subheader__separator\" [hidden]=\"true\"></span>\n\t\t\t<a class=\"kt-subheader__breadcrumbs-home\"><i class=\"flaticon2-shelter\"></i></a>\n\t\t\t<ng-container *ngFor=\"let item of breadcrumbs\">\n\t\t\t\t<span class=\"kt-subheader__breadcrumbs-separator\"></span>\n\t\t\t\t<a [routerLink]=\"item.page\" [queryParams]=\"item.queryParams\" class=\"kt-subheader__breadcrumbs-link\">\n\t\t\t\t\t{{item.title}}\n\t\t\t\t</a>\n\t\t\t</ng-container>\n\t\t\t<!-- <span class=\"kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active\">Active link</span> -->\n\t\t</div>\n\t</div>\n\t<div class=\"kt-subheader__toolbar\">\n\t\t<div class=\"kt-subheader__wrapper\">\n\t\t\t<a class=\"btn kt-subheader__btn-daterange\" id=\"kt_dashboard_daterangepicker\" data-placement=\"left\" ngbTooltip=\"Select dashboard daterange\">\n\t\t\t\t<span class=\"kt-subheader__btn-daterange-title\" id=\"kt_dashboard_daterangepicker_title\">Today</span>&nbsp;\n\t\t\t\t<span class=\"kt-subheader__btn-daterange-date\" id=\"kt_dashboard_daterangepicker_date\">{{today | date:'MMM dd'}}</span>\n\t\t\t\t<span class=\"kt-svg-icon kt-svg-icon--sm\"\n\t\t\t\t\t[inlineSVG]=\"'./assets/media/icons/svg/Communication/Chat-check.svg'\"></span>\n\t\t\t</a>\n\t\t\n\t\t\t<div ngbDropdown [placement]=\"'bottom-right'\" class=\"dropdown dropdown-inline\" data-placement=\"left\" ngbTooltip=\"Quick actions\">\n\t\t\t\t<a ngbDropdownToggle href=\"javascript:;\" class=\"btn btn-icon\">\n\t\t\t\t\t<span class=\"kt-svg-icon kt-svg-icon--success kt-svg-icon--md\" [inlineSVG]=\"'./assets/media/icons/svg/Files/File-plus.svg'\"></span>\n\t\t\t\t</a>\n\t\t\t\t<div ngbDropdownMenu class=\"dropdown-menu\">\n\t\t\t\t\t<a class=\"dropdown-item\"><i class=\"la la-plus\"></i> New Report</a>\n\t\t\t\t\t<a class=\"dropdown-item\"><i class=\"la la-user\"></i> Add Customer</a>\n\t\t\t\t\t<a class=\"dropdown-item\"><i class=\"la la-cloud-download\"></i> New Download</a>\n\t\t\t\t\t<div class=\"dropdown-divider\"></div>\n\t\t\t\t\t<a class=\"dropdown-item\"><i class=\"la la-cog\"></i> Settings</a>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<!-- end:: Content Head -->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/topbar/cart/cart.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/layout/topbar/cart/cart.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--begin: My Cart -->\n<div ngbDropdown placement=\"bottom\" autoClose=\"outside\" class=\"kt-header__topbar-item\">\n\t<div ngbDropdownToggle class=\"kt-header__topbar-wrapper\">\n\t\t<span class=\"kt-header__topbar-icon\"\n\t\t\t[ngClass]=\"{'kt-header__topbar-icon--brand':  iconType === 'brand'}\">\n\t\t\t<i *ngIf=\"!useSVG\" [ngClass]=\"icon\"></i>\n\t\t\t<span *ngIf=\"useSVG\"\n\t\t\t\tclass=\"kt-svg-icon\"\n\t\t\t\t[inlineSVG]=\"icon\"\n\t\t\t\t[ngClass]=\"{'kt-svg-icon--brand':  iconType === 'brand'}\"></span>\n\t\t</span>\n\t</div>\n\t<div ngbDropdownMenu class=\"dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl\">\n\t\t<form>\n\t\t\t<!-- begin:: Mycart -->\n\t\t\t<div class=\"kt-mycart\">\n\t\t\t\t<div class=\"kt-mycart__head kt-head\" [ngStyle]=\"{'background-image': 'url('+bgImage+')'}\">\n\t\t\t\t\t<div class=\"kt-mycart__info\">\n\t\t\t\t\t\t<span class=\"kt-mycart__icon\"><i class=\"flaticon2-shopping-cart-1 kt-font-success\"></i></span>\n\t\t\t\t\t\t<h3 class=\"kt-mycart__title\">My Cart</h3>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"kt-mycart__button\">\n\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-success btn-sm\">2 Items</button>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<div [perfectScrollbar]=\"{wheelPropagation: false}\" [ngStyle]=\"{'max-height': '28vh', 'position': 'relative'}\" class=\"kt-mycart__body\">\n\t\t\t\t\t<div class=\"kt-mycart__item\">\n\t\t\t\t\t\t<div class=\"kt-mycart__container\">\n\t\t\t\t\t\t\t<div class=\"kt-mycart__info\">\n\t\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-mycart__title\">\n\t\t\t\t\t\t\t\t\tSamsung\n\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t\t<span class=\"kt-mycart__desc\">\n\t\t\t                        Profile info, Timeline etc\n\t\t\t                    </span>\n\n\t\t\t\t\t\t\t\t<div class=\"kt-mycart__action\">\n\t\t\t\t\t\t\t\t\t<span class=\"kt-mycart__price\">$ 450</span>\n\t\t\t\t\t\t\t\t\t<span class=\"kt-mycart__text\">for</span>\n\t\t\t\t\t\t\t\t\t<span class=\"kt-mycart__quantity\">7</span>\n\t\t\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"btn btn-label-success btn-icon\">−</a>\n\t\t\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"btn btn-label-success btn-icon\">+</a>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-mycart__pic\">\n\t\t\t\t\t\t\t\t<img src=\"./assets/media/products/product9.jpg\" title=\"\">\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\n\t\t\t\t\t<div class=\"kt-mycart__item\">\n\t\t\t\t\t\t<div class=\"kt-mycart__container\">\n\t\t\t\t\t\t\t<div class=\"kt-mycart__info\">\n\t\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-mycart__title\">\n\t\t\t\t\t\t\t\t\tPanasonic\n\t\t\t\t\t\t\t\t</a>\n\n\t\t\t\t\t\t\t\t<span class=\"kt-mycart__desc\">\n\t\t\t                        For PHoto &amp; Others\n\t\t\t                    </span>\n\n\t\t\t\t\t\t\t\t<div class=\"kt-mycart__action\">\n\t\t\t\t\t\t\t\t\t<span class=\"kt-mycart__price\">$ 329</span>\n\t\t\t\t\t\t\t\t\t<span class=\"kt-mycart__text\">for</span>\n\t\t\t\t\t\t\t\t\t<span class=\"kt-mycart__quantity\">1</span>\n\t\t\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"btn btn-label-success btn-icon\">−</a>\n\t\t\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"btn btn-label-success btn-icon\">+</a>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-mycart__pic\">\n\t\t\t\t\t\t\t\t<img src=\"./assets/media/products/product13.jpg\" title=\"\">\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\n\t\t\t\t\t<div class=\"kt-mycart__item\">\n\t\t\t\t\t\t<div class=\"kt-mycart__container\">\n\t\t\t\t\t\t\t<div class=\"kt-mycart__info\">\n\t\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-mycart__title\">\n\t\t\t\t\t\t\t\t\tFujifilm\n\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t\t<span class=\"kt-mycart__desc\">\n\t\t\t                        Profile info, Timeline etc\n\t\t\t                    </span>\n\n\t\t\t\t\t\t\t\t<div class=\"kt-mycart__action\">\n\t\t\t\t\t\t\t\t\t<span class=\"kt-mycart__price\">$ 520</span>\n\t\t\t\t\t\t\t\t\t<span class=\"kt-mycart__text\">for</span>\n\t\t\t\t\t\t\t\t\t<span class=\"kt-mycart__quantity\">6</span>\n\t\t\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"btn btn-label-success btn-icon\">−</a>\n\t\t\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"btn btn-label-success btn-icon\">+</a>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-mycart__pic\">\n\t\t\t\t\t\t\t\t<img src=\"./assets/media/products/product16.jpg\" title=\"\">\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\n\t\t\t\t\t<div class=\"kt-mycart__item\">\n\t\t\t\t\t\t<div class=\"kt-mycart__container\">\n\t\t\t\t\t\t\t<div class=\"kt-mycart__info\">\n\t\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-mycart__title\">\n\t\t\t\t\t\t\t\t\tCandy Machine\n\t\t\t\t\t\t\t\t</a>\n\n\t\t\t\t\t\t\t\t<span class=\"kt-mycart__desc\">\n\t\t\t                        For PHoto &amp; Others\n\t\t\t                    </span>\n\n\t\t\t\t\t\t\t\t<div class=\"kt-mycart__action\">\n\t\t\t\t\t\t\t\t\t<span class=\"kt-mycart__price\">$ 784</span>\n\t\t\t\t\t\t\t\t\t<span class=\"kt-mycart__text\">for</span>\n\t\t\t\t\t\t\t\t\t<span class=\"kt-mycart__quantity\">4</span>\n\t\t\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"btn btn-label-success btn-icon\">−</a>\n\t\t\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"btn btn-label-success btn-icon\">+</a>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-mycart__pic\">\n\t\t\t\t\t\t\t\t<img src=\"./assets/media/products/product15.jpg\" title=\"\" alt=\"\">\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"kt-mycart__footer\">\n\t\t\t\t\t<div class=\"kt-mycart__section\">\n\t\t\t\t\t\t<div class=\"kt-mycart__subtitel\">\n\t\t\t\t\t\t\t<span>Sub Total</span>\n\t\t\t\t\t\t\t<span>Taxes</span>\n\t\t\t\t\t\t\t<span>Total</span>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"kt-mycart__prices\">\n\t\t\t\t\t\t\t<span>$ 840.00</span>\n\t\t\t\t\t\t\t<span>$ 72.00</span>\n\t\t\t\t\t\t\t<span class=\"kt-font-brand\">$ 912.00</span>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"kt-mycart__button kt-align-right\">\n\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary btn-sm\">Place Order</button>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<!-- end:: Mycart -->\n\t\t</form>\n\t</div>\n</div>\n<!--end: My Cart -->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/topbar/language-selector/language-selector.component.html":
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/layout/topbar/language-selector/language-selector.component.html ***!
  \***************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div ngbDropdown placement=\"bottom\" class=\"kt-header__topbar-item kt-header__topbar-item--langs\">\n\t<div ngbDropdownToggle class=\"kt-header__topbar-wrapper\">\n\t\t<span class=\"kt-header__topbar-icon\"\n\t\t\t[ngClass]=\"{ 'kt-header__topbar-icon--brand' : iconType === 'brand' }\">\n\t\t\t<img class=\"\" src=\"{{language?.flag}}\" alt=\"\"/>\n\t\t</span>\n\t</div>\n\t<div ngbDropdownMenu aria-labelledby=\"dropdownBasic1\" class=\"dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround\">\n\t\t<ul class=\"kt-nav kt-margin-t-10 kt-margin-b-10\">\n\t\t\t<ng-container *ngFor=\"let language of languages\">\n\t\t\t\t<li class=\"kt-nav__item\" [ngClass]=\"{'kt-nav__item--active': language.active}\">\n\t\t\t\t\t<a href=\"javascript:;\" (click)=\"setLanguage(language.lang)\" [ngClass]=\"{'kt-nav__link--active': language.active}\" class=\"kt-nav__link\">\n\t\t\t\t\t\t<span class=\"kt-nav__link-icon\">\n\t\t\t\t\t\t\t<img src=\"{{language.flag}}\">\n\t\t\t\t\t\t</span>\n\t\t\t\t\t\t<span class=\"kt-nav__link-text\">{{language.name}}</span>\n\t\t\t\t\t</a>\n\t\t\t\t</li>\n\t\t\t</ng-container>\n\t\t</ul>\n\t</div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/topbar/notification/notification.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/layout/topbar/notification/notification.component.html ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div ngbDropdown placement=\"bottom\" autoClose=\"outside\" class=\"kt-header__topbar-item\">\n\t<div ngbDropdownToggle class=\"kt-header__topbar-wrapper\">\n\t\t<span class=\"kt-header__topbar-icon\" [ngClass]=\"{'kt-pulse kt-pulse--brand': pulse, 'kt-pulse--light' : pulseLight, 'kt-header__topbar-icon--success' : iconType === 'success'\t}\">\n\t\t\t<i *ngIf=\"!useSVG\" [ngClass]=\"icon\"></i>\n\t\t\t<span *ngIf=\"useSVG\" class=\"kt-svg-icon\" [ngClass]=\"{'kt-svg-icon--success' : iconType === 'success'}\" [inlineSVG]=\"icon\"></span>\n\t\t\t<span class=\"kt-pulse__ring\" [hidden]=\"!pulse\"></span>\n\t\t</span>\n\t\t<span class=\"kt-badge kt-badge--dot kt-badge--notify kt-badge--sm kt-badge--brand\" [hidden]=\"!dot\"></span>\n\t</div>\n\n\t<div ngbDropdownMenu class=\"dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-lg\">\n\t\t<form>\n\t\t\t<!--begin: Head -->\n\t\t\t<div class=\"kt-head kt-head--skin-{{skin}} kt-head--fit-x kt-head--fit-b\" [ngStyle]=\"{'background-image': backGroundStyle() }\">\n\t\t\t\t<h3 class=\"kt-head__title\">\n\t\t\t\t\tUser Notifications&nbsp;\n\t\t\t\t\t<span class=\"btn btn-{{ type }} btn-sm btn-bold btn-font-md\">23 new</span>\n\t\t\t\t</h3>\n\t\t\t\t<ul ktTabClickEvent class=\"nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-{{ type }} kt-notification-item-padding-x\" role=\"tablist\">\n\t\t\t\t\t<li class=\"nav-item\">\n\t\t\t\t\t\t<a (click)=\"tab.select('tab-id-1')\" class=\"nav-link active show\" data-toggle=\"tab\" href=\"javascript:;\" role=\"tab\" aria-selected=\"true\">Alerts</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li class=\"nav-item\">\n\t\t\t\t\t\t<a (click)=\"tab.select('tab-id-2')\" class=\"nav-link\" data-toggle=\"tab\" href=\"javascript:;\" role=\"tab\" aria-selected=\"false\">Events</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li class=\"nav-item\">\n\t\t\t\t\t\t<a (click)=\"tab.select('tab-id-3')\" class=\"nav-link\" data-toggle=\"tab\" href=\"javascript:;\" role=\"tab\" aria-selected=\"false\">Logs</a>\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t\t<!--end: Head -->\n\n\t\t\t<ngb-tabset #tab=\"ngbTabset\">\n\t\t\t\t<ngb-tab id=\"tab-id-1\">\n\t\t\t\t\t<ng-template ngbTabContent>\n\t\t\t\t\t\t<div [perfectScrollbar]=\"{wheelPropagation: false}\" [ngStyle]=\"{'max-height': '40vh', 'position': 'relative'}\" class=\"kt-notification kt-margin-t-10 kt-margin-b-10\">\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t\t<i class=\"flaticon2-line-chart kt-font-success\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\t\tNew order has been received\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t\t2 hrs ago\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t\t<i class=\"flaticon2-box-1 kt-font-brand\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\t\tNew customer is registered\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t\t3 hrs ago\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t\t<i class=\"flaticon2-chart2 kt-font-danger\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\t\tApplication has been approved\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t\t3 hrs ago\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t\t<i class=\"flaticon2-image-file kt-font-warning\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\t\tNew file has been uploaded\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t\t5 hrs ago\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t\t<i class=\"flaticon2-drop kt-font-info\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\t\tNew user feedback received\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t\t8 hrs ago\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t\t<i class=\"flaticon2-pie-chart-2 kt-font-success\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\t\tSystem reboot has been successfully completed\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t\t12 hrs ago\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t\t<i class=\"flaticon2-favourite kt-font-focus\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\t\tNew order has been placed\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t\t15 hrs ago\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item kt-notification__item--read\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t\t<i class=\"flaticon2-safe kt-font-primary\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\t\tCompany meeting canceled\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t\t19 hrs ago\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t\t<i class=\"flaticon2-psd kt-font-success\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\t\tNew report has been received\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t\t23 hrs ago\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t\t<i class=\"flaticon-download-1 kt-font-danger\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\t\tFinance report has been generated\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t\t25 hrs ago\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t\t<i class=\"flaticon-security kt-font-warning\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\t\tNew customer comment recieved\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t\t2 days ago\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t\t<i class=\"flaticon2-pie-chart kt-font-focus\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\t\tNew customer is registered\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t\t3 days ago\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<div class=\"ps__rail-x\" style=\"left: 0px; bottom: 0px;\">\n\t\t\t\t\t\t\t\t<div class=\"ps__thumb-x\" tabindex=\"0\" style=\"left: 0px; width: 0px;\"></div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"ps__rail-y\" style=\"top: 0px; right: 0px;\">\n\t\t\t\t\t\t\t\t<div class=\"ps__thumb-y\" tabindex=\"0\" style=\"top: 0px; height: 0px;\"></div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</ng-template>\n\t\t\t\t</ngb-tab>\n\t\t\t\t<ngb-tab id=\"tab-id-2\">\n\t\t\t\t\t<ng-template ngbTabContent>\n\t\t\t\t\t\t<div [perfectScrollbar]=\"{wheelPropagation: false}\" [ngStyle]=\"{'max-height': '40vh', 'position': 'relative'}\" class=\"kt-notification kt-margin-t-10 kt-margin-b-10\">\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t\t<i class=\"flaticon2-psd kt-font-success\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\t\tNew report has been received\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t\t23 hrs ago\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t\t<i class=\"flaticon-download-1 kt-font-danger\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\t\tFinance report has been generated\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t\t25 hrs ago\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t\t<i class=\"flaticon2-line-chart kt-font-success\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\t\tNew order has been received\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t\t2 hrs ago\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t\t<i class=\"flaticon2-box-1 kt-font-brand\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\t\tNew customer is registered\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t\t3 hrs ago\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t\t<i class=\"flaticon2-chart2 kt-font-danger\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\t\tApplication has been approved\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t\t3 hrs ago\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t\t<i class=\"flaticon2-image-file kt-font-warning\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\t\tNew file has been uploaded\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t\t5 hrs ago\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t\t<i class=\"flaticon2-drop kt-font-info\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\t\tNew user feedback received\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t\t8 hrs ago\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t\t<i class=\"flaticon2-pie-chart-2 kt-font-success\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\t\tSystem reboot has been successfully completed\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t\t12 hrs ago\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t\t<i class=\"flaticon2-favourite kt-font-focus\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\t\tNew order has been placed\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t\t15 hrs ago\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item kt-notification__item--read\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t\t<i class=\"flaticon2-safe kt-font-primary\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\t\tCompany meeting canceled\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t\t19 hrs ago\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t\t<i class=\"flaticon2-psd kt-font-success\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\t\tNew report has been received\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t\t23 hrs ago\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t\t<i class=\"flaticon-download-1 kt-font-danger\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\t\tFinance report has been generated\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t\t25 hrs ago\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t\t<i class=\"flaticon-security kt-font-warning\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\t\tNew customer comment recieved\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t\t2 days ago\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t\t\t\t\t<i class=\"flaticon2-pie-chart kt-font-focus\"></i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-title\">\n\t\t\t\t\t\t\t\t\t\tNew customer is registered\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\t\t\t\t\t3 days ago\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<div class=\"ps__rail-x\" style=\"left: 0px; bottom: 0px;\">\n\t\t\t\t\t\t\t\t<div class=\"ps__thumb-x\" tabindex=\"0\" style=\"left: 0px; width: 0px;\"></div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"ps__rail-y\" style=\"top: 0px; right: 0px;\">\n\t\t\t\t\t\t\t\t<div class=\"ps__thumb-y\" tabindex=\"0\" style=\"top: 0px; height: 0px;\"></div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</ng-template>\n\t\t\t\t</ngb-tab>\n\t\t\t\t<ngb-tab id=\"tab-id-3\">\n\t\t\t\t\t<ng-template ngbTabContent>\n\t\t\t\t\t\t<div class=\"kt-grid kt-grid--ver\" style=\"min-height: 200px;\">\n\t\t\t\t\t\t\t<div class=\"kt-grid kt-grid--hor kt-grid__item kt-grid__item--fluid kt-grid__item--middle\">\n\t\t\t\t\t\t\t\t<div class=\"kt-grid__item kt-grid__item--middle kt-align-center\">\n\t\t\t\t\t\t\t\t\tAll caught up!\n\t\t\t\t\t\t\t\t\t<br>No new notifications.\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</ng-template>\n\t\t\t\t</ngb-tab>\n\t\t\t</ngb-tabset>\n\t\t</form>\n\t</div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/topbar/quick-action/quick-action.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/layout/topbar/quick-action/quick-action.component.html ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--begin: Quick actions -->\n<div ngbDropdown placement=\"bottom\" autoClose=\"outside\" class=\"kt-header__topbar-item\">\n\t<div ngbDropdownToggle class=\"kt-header__topbar-wrapper\">\n\t\t<span class=\"kt-header__topbar-icon\" [ngClass]=\"{ 'kt-header__topbar-icon--warning': iconType === 'warning' }\">\n\t\t\t<i *ngIf=\"!useSVG\" [ngClass]=\"icon\"></i>\n\t\t\t<span *ngIf=\"useSVG\" class=\"kt-svg-icon\" [inlineSVG]=\"icon\" [ngClass]=\"{ 'kt-svg-icon--warning': iconType === 'warning' }\"></span>\n\t\t</span>\n\t</div>\n\t<div ngbDropdownMenu class=\"dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl\">\n\t\t<form>\n\t\t\t<!--begin: Head -->\n\t\t\t<div class=\"kt-head kt-head--skin-{{skin}}\" [ngStyle]=\"{'background-image': 'url('+bgImage+')'}\">\n\t\t\t\t<h3 class=\"kt-head__title\">\n\t\t\t\t\tUser Quick Actions\n\t\t\t\t\t<span class=\"kt-space-15\"></span>\n\t\t\t\t\t<span class=\"btn btn-success btn-sm btn-bold btn-font-md\">23 tasks pending</span>\n\t\t\t\t</h3>\n\t\t\t</div>\n\t\t\t<!--end: Head -->\n\t\t\t<!--begin: Grid Nav -->\n\t\t\t<div class=\"kt-grid-nav kt-grid-nav--skin-{{ gridNavSkin }}\">\n\t\t\t\t<div class=\"kt-grid-nav__row\">\n\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-grid-nav__item\">\n\t\t\t\t\t\t<span class=\"kt-grid-nav__icon\" [inlineSVG]=\"'./assets/media/icons/svg/Shopping/Euro.svg'\" (onSVGInserted)=\"onSVGInserted($event)\"></span>\n\t\t\t\t\t\t<span class=\"kt-grid-nav__title\">Accounting</span>\n\t\t\t\t\t\t<span class=\"kt-grid-nav__desc\">eCommerce</span>\n\t\t\t\t\t</a>\n\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-grid-nav__item\">\n\t\t\t\t\t\t<span class=\"kt-grid-nav__icon\" [inlineSVG]=\"'./assets/media/icons/svg/Communication/Mail-attachment.svg'\" (onSVGInserted)=\"onSVGInserted($event)\"></span>\n\t\t\t\t\t\t<span class=\"kt-grid-nav__title\">Administration</span>\n\t\t\t\t\t\t<span class=\"kt-grid-nav__desc\">Console</span>\n\t\t\t\t\t</a>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"kt-grid-nav__row\">\n\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-grid-nav__item\">\n\t\t\t\t\t\t<span class=\"kt-grid-nav__icon\" [inlineSVG]=\"'./assets/media/icons/svg/Shopping/Box%232.svg'\" (onSVGInserted)=\"onSVGInserted($event)\"></span>\n\t\t\t\t\t\t<span class=\"kt-grid-nav__title\">Projects</span>\n\t\t\t\t\t\t<span class=\"kt-grid-nav__desc\">Pending Tasks</span>\n\t\t\t\t\t</a>\n\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-grid-nav__item\">\n\t\t\t\t\t\t<span class=\"kt-grid-nav__icon\" [inlineSVG]=\"'./assets/media/icons/svg/Communication/Group.svg'\" (onSVGInserted)=\"onSVGInserted($event)\"></span>\n\t\t\t\t\t\t<span class=\"kt-grid-nav__title\">Customers</span>\n\t\t\t\t\t\t<span class=\"kt-grid-nav__desc\">Latest cases</span>\n\t\t\t\t\t</a>\n\t\t\t\t</div>\n\t\t\t\t<!--end: Grid Nav -->\n\t\t\t</div>\n\t\t</form>\n\t</div>\n</div>\n<!--end: Quick actions -->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/topbar/search-default/search-default.component.html":
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/layout/topbar/search-default/search-default.component.html ***!
  \*********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--begin: Search -->\n<div class=\"kt-header__topbar-item kt-header__topbar-item--search\">\n\t<div class=\"kt-header__topbar-wrapper\">\n\t\t<div class=\"kt-header-toolbar\">\n\t\t\t<div ngbDropdown placement=\"bottom\" autoClose=\"outside\" [ngClass]=\"{'kt-quick-search--has-result': data?.length}\" class=\"kt-quick-search kt-quick-search--inline kt-quick-search--result-compact\" id=\"kt_quick_search_default\">\n\t\t\t\t<form method=\"get\" class=\"kt-quick-search__form\">\n\t\t\t\t\t<div ngbDropdownToggle class=\"input-group\" [ngClass]=\"{'kt-spinner kt-spinner--input kt-spinner--sm kt-spinner--brand kt-spinner--right': loading}\">\n\t\t\t\t\t\t<div class=\"input-group-prepend\">\n\t\t\t\t\t\t\t<span class=\"input-group-text\">\n\t\t\t\t\t\t\t\t<i *ngIf=\"!useSVG\" [ngClass]=\"icon\"></i>\n\t\t\t\t\t\t\t\t<span class=\"kt-svg-icon\" *ngIf=\"useSVG\" [inlineSVG]=\"icon\"></span>\n\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<input #searchInput (keyup)=\"search($event)\" type=\"text\" class=\"form-control kt-quick-search__input\" placeholder=\"Search...\" />\n\t\t\t\t\t\t<div class=\"input-group-append\">\n\t\t\t\t\t\t\t<span class=\"input-group-text\"><i (click)=\"clear($event)\" [ngStyle]=\"{'display': 'flex'}\" [hidden]=\"!data || !data?.length\" class=\"la la-close kt-quick-search__close\"></i></span>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</form>\n\t\t\t\t<div ngbDropdownMenu *ngIf=\"data?.length\" class=\"dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-lg\">\n\t\t\t\t\t<div [perfectScrollbar]=\"{wheelPropagation: false}\" [ngStyle]=\"{'max-height': '40vh', 'position': 'relative'}\" class=\"kt-quick-search__wrapper\">\n\t\t\t\t\t\t<kt-search-result [data]=\"data\"></kt-search-result>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<!--end: Search -->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/topbar/search-dropdown/search-dropdown.component.html":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/layout/topbar/search-dropdown/search-dropdown.component.html ***!
  \***********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--begin: Search -->\n<div ngbDropdown (openChange)=\"openChange()\" placement=\"bottom\" autoClose=\"outside\" class=\"kt-header__topbar-item kt-header__topbar-item--search dropdown\">\n\t<div ngbDropdownToggle class=\"kt-header__topbar-wrapper\">\n\t\t<span class=\"kt-header__topbar-icon\" [ngClass]=\"type ? 'kt-header__topbar-icon--'+type : ''\">\n\t\t\t<i *ngIf=\"!useSVG\" [ngClass]=\"icon\"></i>\n\t\t\t<span class=\"kt-svg-icon\" *ngIf=\"useSVG\" [inlineSVG]=\"icon\"></span>\n\t\t</span>\n\t</div>\n\t<div ngbDropdownMenu class=\"dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-lg\">\n\t\t<div [ngClass]=\"{'kt-quick-search--has-result': data?.length}\" class=\"kt-quick-search kt-quick-search--dropdown kt-quick-search--result-compact\" id=\"kt_quick_search_dropdown\">\n\t\t\t<form method=\"get\" class=\"kt-quick-search__form\">\n\t\t\t\t<div class=\"input-group\" [ngClass]=\"{'kt-spinner kt-spinner--input kt-spinner--sm kt-spinner--brand kt-spinner--right': loading}\">\n\t\t\t\t\t<div class=\"input-group-prepend\"><span class=\"input-group-text\"><i class=\"flaticon2-search-1\"></i></span></div>\n\t\t\t\t\t<input #searchInput (keyup)=\"search($event)\" type=\"text\" class=\"form-control kt-quick-search__input\" placeholder=\"Search...\" />\n\t\t\t\t\t<div class=\"input-group-append\"><span class=\"input-group-text\"><i (click)=\"clear($event)\" [ngStyle]=\"{'display': 'flex'}\" [hidden]=\"!data || !data?.length\" class=\"la la-close kt-quick-search__close\"></i></span></div>\n\t\t\t\t</div>\n\t\t\t</form>\n\t\t\t<div [perfectScrollbar]=\"{wheelPropagation: false}\" [ngStyle]=\"{'max-height': '40vh', 'position': 'relative'}\" class=\"kt-quick-search__wrapper\">\n\t\t\t\t<kt-search-result [data]=\"data\"></kt-search-result>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<!--end: Search -->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/topbar/user-profile/user-profile.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/layout/topbar/user-profile/user-profile.component.html ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div ngbDropdown placement=\"bottom-right\"\n\t class=\"kt-header__topbar-item kt-header__topbar-item--user\">\n\t<div ngbDropdownToggle class=\"kt-header__topbar-wrapper\">\n\t\t<div class=\"kt-header__topbar-user\" [ngClass]=\"{'kt-header__topbar-icon': icon}\">\n\t\t\t<span style=\"margin-right: 15px\">\n\t\t\t\t<img alt=\"Pic\" [attr.src]=\"'./assets/media/users/default.jpg'\">\n\t\t\t</span>\n\n\t\t\t<button (click)=\"logout()\" class=\"btn btn-primary \">Sign Out</button>\n\t\t</div>\n\t</div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/topbar/user-profile2/user-profile2.component.html":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/layout/topbar/user-profile2/user-profile2.component.html ***!
  \*******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div ngbDropdown placement=\"bottom-right\" *ngIf=\"user$ | async as _user\" class=\"kt-header__topbar-item kt-header__topbar-item--user\">\n\t<div ngbDropdownToggle class=\"kt-header__topbar-wrapper\">\n\t\t<span class=\"kt-header__topbar-welcome\" *ngIf=\"greeting\">Hi,</span>\n\t\t<span class=\"kt-header__topbar-username\" *ngIf=\"greeting\">{{_user.fullname}}</span>\n\t\t<span *ngIf=\"badge\" class=\"kt-header__topbar-icon\"><b>{{_user.fullname|firstLetter}}</b></span>\n\t\t<img *ngIf=\"avatar\" alt=\"Pic\" [attr.src]=\"_user.pic\">\n\t</div>\n\t<div ngbDropdownMenu class=\"dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl\">\n\t\t<!--begin: Head -->\n\t\t<div class=\"kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x\" style=\"background-image: url(./assets/media/misc/bg-1.jpg)\">\n\t\t\t<div class=\"kt-user-card__avatar\">\n\t\t\t\t<img alt=\"Pic\" [attr.src]=\"_user.pic\"/>\n\t\t\t\t<span [hidden]=\"true\" class=\"kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success\">\n\t\t\t\t\t{{_user.fullname|firstLetter}}\n\t\t\t\t</span>\n\t\t\t</div>\n\t\t\t<div class=\"kt-user-card__name\">\n\t\t\t\t{{_user.fullname}}\n\t\t\t</div>\n\t\t\t<div class=\"kt-user-card__badge\">\n\t\t\t\t<span class=\"btn btn-success btn-sm btn-bold btn-font-md\">23 messages</span>\n\t\t\t</div>\n\t\t</div>\n\t\t<!--end: Head -->\n\n\t\t<!--begin: Navigation -->\n\t\t<div class=\"kt-notification\">\n\t\t\t<a ngbDropdownItem routerLink=\"profile\" href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t<i class=\"flaticon2-calendar-3 kt-font-success\"></i>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t<div class=\"kt-notification__item-title kt-font-bold\">\n\t\t\t\t\t\tMy Profile\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\tAccount settings and more\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</a>\n\t\t\t<a ngbDropdownItem routerLink=\"profile\" href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t<i class=\"flaticon2-mail kt-font-warning\"></i>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t<div class=\"kt-notification__item-title kt-font-bold\">\n\t\t\t\t\t\tMy Messages\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\tInbox and tasks\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</a>\n\t\t\t<a ngbDropdownItem routerLink=\"profile\" href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t<i class=\"flaticon2-rocket-1 kt-font-danger\"></i>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t<div class=\"kt-notification__item-title kt-font-bold\">\n\t\t\t\t\t\tMy Activities\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\tLogs and notifications\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</a>\n\t\t\t<a ngbDropdownItem routerLink=\"profile\" href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t<i class=\"flaticon2-hourglass kt-font-brand\"></i>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t<div class=\"kt-notification__item-title kt-font-bold\">\n\t\t\t\t\t\tMy Tasks\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\tlatest tasks and projects\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</a>\n\t\t\t<div class=\"kt-notification__custom\">\n\t\t\t\t<a href=\"javascript:;\" (click)=\"logout()\" class=\"btn btn-outline-brand btn-upper btn-sm btn-bold\">Sign Out</a>\n\t\t\t</div>\n\t\t</div>\n\t\t<!--end: Navigation -->\n\n\t\t<!--begin: Navigation(alternative) -->\n\t\t<ul class=\"kt-nav kt-margin-b-10 kt-hidden\">\n\t\t\t<li class=\"kt-nav__item\">\n\t\t\t\t<a ngbDropdownItem routerLink=\"profile\" class=\"kt-nav__link\">\n\t\t\t\t\t<span class=\"kt-nav__link-icon\"><i class=\"flaticon2-calendar-3\"></i></span>\n\t\t\t\t\t<span class=\"kt-nav__link-text\">My Profile</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li class=\"kt-nav__item\">\n\t\t\t\t<a ngbDropdownItem routerLink=\"profile\" class=\"kt-nav__link\">\n\t\t\t\t\t<span class=\"kt-nav__link-icon\"><i class=\"flaticon2-browser-2\"></i></span>\n\t\t\t\t\t<span class=\"kt-nav__link-text\">My Tasks</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li class=\"kt-nav__item\">\n\t\t\t\t<a ngbDropdownItem routerLink=\"profile\" class=\"kt-nav__link\">\n\t\t\t\t\t<span class=\"kt-nav__link-icon\"><i class=\"flaticon2-mail\"></i></span>\n\t\t\t\t\t<span class=\"kt-nav__link-text\">Messages</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li class=\"kt-nav__item\">\n\t\t\t\t<a ngbDropdownItem routerLink=\"profile\" class=\"kt-nav__link\">\n\t\t\t\t\t<span class=\"kt-nav__link-icon\"><i class=\"flaticon2-gear\"></i></span>\n\t\t\t\t\t<span class=\"kt-nav__link-text\">Settings</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li class=\"kt-nav__item kt-nav__item--custom kt-margin-t-15\">\n\t\t\t\t<a (click)=\"logout()\" class=\"btn btn-label-brand btn-sm btn-bold\">Sign Out</a>\n\t\t\t</li>\n\t\t</ul>\n\t\t<!--end: Navigation(alternative) -->\n\t</div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/topbar/user-profile3/user-profile3.component.html":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/partials/layout/topbar/user-profile3/user-profile3.component.html ***!
  \*******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div ngbDropdown placement=\"bottom-right\" *ngIf=\"user$ | async as _user\" class=\"kt-header__topbar-item kt-header__topbar-item--user\">\n\t<div ngbDropdownToggle class=\"kt-header__topbar-wrapper\">\n\t\t<span class=\"kt-header__topbar-welcome\">Hi,</span>\n\t\t<span class=\"kt-header__topbar-username\">{{_user.fullname}}</span>\n\t\t<span class=\"kt-header__topbar-icon\"><b>{{_user.fullname|firstLetter}}</b></span>\n\t</div>\n\t<div ngbDropdownMenu class=\"dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl\">\n\t\t<!--begin: Head -->\n\t\t<div class=\"kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x\" style=\"background-image: url(./assets/media/misc/bg-1.jpg)\">\n\t\t\t<div class=\"kt-user-card__avatar\">\n\t\t\t\t<img alt=\"Pic\" [attr.src]=\"_user.pic\"/>\n\t\t\t\t<span [hidden]=\"true\" class=\"kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success\">\n\t\t\t\t\t{{_user.fullname|firstLetter}}\n\t\t\t\t</span>\n\t\t\t</div>\n\t\t\t<div class=\"kt-user-card__name\">\n\t\t\t\t{{_user.fullname}}\n\t\t\t</div>\n\t\t\t<div class=\"kt-user-card__badge\">\n\t\t\t\t<span class=\"btn btn-success btn-sm btn-bold btn-font-md\">23 messages</span>\n\t\t\t</div>\n\t\t</div>\n\t\t<!--end: Head -->\n\n\t\t<!--begin: Navigation -->\n\t\t<div class=\"kt-notification\">\n\t\t\t<a ngbDropdownItem routerLink=\"profile\" href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t<i class=\"flaticon2-calendar-3 kt-font-success\"></i>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t<div class=\"kt-notification__item-title kt-font-bold\">\n\t\t\t\t\t\tMy Profile\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\tAccount settings and more\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</a>\n\t\t\t<a ngbDropdownItem routerLink=\"profile\" href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t<i class=\"flaticon2-mail kt-font-warning\"></i>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t<div class=\"kt-notification__item-title kt-font-bold\">\n\t\t\t\t\t\tMy Messages\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\tInbox and tasks\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</a>\n\t\t\t<a ngbDropdownItem routerLink=\"profile\" href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t<i class=\"flaticon2-rocket-1 kt-font-danger\"></i>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t<div class=\"kt-notification__item-title kt-font-bold\">\n\t\t\t\t\t\tMy Activities\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\tLogs and notifications\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</a>\n\t\t\t<a ngbDropdownItem routerLink=\"profile\" href=\"javascript:;\" class=\"kt-notification__item\">\n\t\t\t\t<div class=\"kt-notification__item-icon\">\n\t\t\t\t\t<i class=\"flaticon2-hourglass kt-font-brand\"></i>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"kt-notification__item-details\">\n\t\t\t\t\t<div class=\"kt-notification__item-title kt-font-bold\">\n\t\t\t\t\t\tMy Tasks\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"kt-notification__item-time\">\n\t\t\t\t\t\tlatest tasks and projects\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</a>\n\t\t\t<div class=\"kt-notification__custom\">\n\t\t\t\t<a href=\"javascript:;\" (click)=\"logout()\" class=\"btn btn-label-brand btn-sm btn-bold\">Sign Out</a>\n\t\t\t</div>\n\t\t</div>\n\t\t<!--end: Navigation -->\n\n\t\t<!--begin: Navigation(alternative) -->\n\t\t<ul class=\"kt-nav kt-margin-b-10 kt-hidden\">\n\t\t\t<li class=\"kt-nav__item\">\n\t\t\t\t<a ngbDropdownItem routerLink=\"profile\" class=\"kt-nav__link\">\n\t\t\t\t\t<span class=\"kt-nav__link-icon\"><i class=\"flaticon2-calendar-3\"></i></span>\n\t\t\t\t\t<span class=\"kt-nav__link-text\">My Profile</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li class=\"kt-nav__item\">\n\t\t\t\t<a ngbDropdownItem routerLink=\"profile\" class=\"kt-nav__link\">\n\t\t\t\t\t<span class=\"kt-nav__link-icon\"><i class=\"flaticon2-browser-2\"></i></span>\n\t\t\t\t\t<span class=\"kt-nav__link-text\">My Tasks</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li class=\"kt-nav__item\">\n\t\t\t\t<a ngbDropdownItem routerLink=\"profile\" class=\"kt-nav__link\">\n\t\t\t\t\t<span class=\"kt-nav__link-icon\"><i class=\"flaticon2-mail\"></i></span>\n\t\t\t\t\t<span class=\"kt-nav__link-text\">Messages</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li class=\"kt-nav__item\">\n\t\t\t\t<a ngbDropdownItem routerLink=\"profile\" class=\"kt-nav__link\">\n\t\t\t\t\t<span class=\"kt-nav__link-icon\"><i class=\"flaticon2-gear\"></i></span>\n\t\t\t\t\t<span class=\"kt-nav__link-text\">Settings</span>\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li class=\"kt-nav__item kt-nav__item--custom kt-margin-t-15\">\n\t\t\t\t<a (click)=\"logout()\" class=\"btn btn-label-brand btn-sm btn-bold\">Sign Out</a>\n\t\t\t</li>\n\t\t</ul>\n\t\t<!--end: Navigation(alternative) -->\n\t</div>\n</div>\n\n\n<!-- <a (click)=\"logout()\" class=\"btn btn-label-brand btn-sm btn-bold\">Sign Out</a> -->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/public-page/public-page.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/public-page/public-page.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n\t<div class=\"col-xl-12\">\n\t\t<kt-portlet [class]=\"'kt-portlet--height-fluid' \">\n\t\t\t<div class=\"kt-portlet__head kt-portlet__head--lg\">\n\t\t\t\t<div class=\"kt-portlet__head-label\">\n\t\t\t\t\t\t\t<span class=\"kt-portlet__head-icon\">\n\t\t\t\t\t\t\t\t<i class=\"kt-font-brand flaticon2-line-chart\"></i>\n\t\t\t\t\t\t\t</span>\n\t\t\t\t\t<h3 class=\"kt-portlet__head-title\">\n\t\t\t\t\t\tPRS Works\n\t\t\t\t\t\t<small>the highest scored works</small>\n\t\t\t\t\t</h3>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"kt-portlet__head-toolbar \">\n\t\t\t\t\t<button mat-raised-button\n\t\t\t\t\t\t\tcolor=\"accent\"\n\t\t\t\t\t\t\tclass=\"pulsingButton\"\n\t\t\t\t\t\t\tstyle=\"margin-right: 1.3rem;\"\n\t\t\t\t\t\t\t[routerLink]=\"['/author/submission']\">Submit Work\n\t\t\t\t\t</button>\n\n\n\t\t\t\t\t<button mat-raised-button\n\t\t\t\t\t\t\tcolor=\"primary\"\n\t\t\t\t\t\t\tstyle=\"background-color: #6B39B6;\"\n\t\t\t\t\t\t\t[routerLink]=\"['/auth/login']\">Login\n\t\t\t\t\t</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<kt-portlet-body [class]=\"'kt-portlet__body--fit'\" style=\"height:100%; max-height: 100%\">\n\n\t\t\t\t<!--begin: Datatable -->\n\t\t\t\t<!--\t\t\t\t\t<kt-work-list></kt-work-list>-->\n\t\t\t\t<kt-portlet>\n\t\t\t\t\t<kt-portlet-body>\n\n\t\t\t\t\t\t<!-- start::FILTERS & GROUP ACTIONS -->\n\t\t\t\t\t\t<div class=\"kt-form kt-margin-b-30\">\n\t\t\t\t\t\t\t<!-- start::FILTERS -->\n\t\t\t\t\t\t\t<div class=\"row align-items-center\">\n\t\t\t\t\t\t\t\t<div class=\"col-md-2 kt-margin-bottom-10-mobile\">\n\n\t\t\t\t\t\t\t\t\t<mat-form-field>\n\t\t\t\t\t\t\t\t\t\t<input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Search\">\n\t\t\t\t\t\t\t\t\t</mat-form-field>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<!-- end::FILTERS -->\n\n\t\t\t\t\t\t<!--        <div class=\"mat-elevation-z8\">-->\n\t\t\t\t\t\t<table mat-table [dataSource]=\"dataSource\" matSort\n\t\t\t\t\t\t\t   matSortDirection=\"desc\"\n\t\t\t\t\t\t\t   matSortActive=\"Score\"\n\t\t\t\t\t\t\t   class=\"w-100\"\n\t\t\t\t\t\t>\n\n\t\t\t\t\t\t\t<!-- Tag Column -->\n\t\t\t\t\t\t\t<ng-container matColumnDef=\"Tags\">\n\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef mat-sort-header> Category</th>\n\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let row\"> {{row.Tags[0] }}</td>\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- Title -->\n\t\t\t\t\t\t\t<ng-container matColumnDef=\"Title\">\n\n\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef mat-sort-header> Title</th>\n\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let row\">\n\t\t\t\t\t\t\t\t\t<!--\t\t\t\t\t\t\t\t\t\t<a href=\"javascript:;\">{{row.Title}}</a>-->\n\t\t\t\t\t\t\t\t\t<a href=\"{{row.URL}}\" target=\"_blank\">{{row.Title}}</a>\n\n\t\t\t\t\t\t\t\t\t<span class=\"mdc-chip-set\">\n\t\t\t\t\t\t\t<span *ngFor=\"let tag of row.Tags | slice:1\"\n\t\t\t\t\t\t\t\t  class=\"mdc-chip demo-chip-shaped\"\n\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t<span class=\"mdc-chip__text\">{{tag}}</span></span>\n\t\t\t\t\t\t</span>\n\n\t\t\t\t\t\t\t\t</td>\n\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- AuthorName Column -->\n\t\t\t\t\t\t\t<ng-container matColumnDef=\"AuthorName\">\n\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef mat-sort-header> Author Name(s)</th>\n\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let row\"> {{row.AuthorName}} </td>\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- Score Column -->\n\t\t\t\t\t\t\t<ng-container matColumnDef=\"Score\">\n\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef mat-sort-header\n\t\t\t\t\t\t\t\t> Score\n\t\t\t\t\t\t\t\t</th>\n\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let row\"> {{row.Score}} </td>\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n\t\t\t\t\t\t\t<tr mat-row *matRowDef=\"let row; columns: displayedColumns;\">\n\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t</table>\n\n\n\t\t\t\t\t\t<mat-paginator [pageSizeOptions]=\"[5, 10, 25, 100]\"></mat-paginator>\n\t\t\t\t\t</kt-portlet-body>\n\t\t\t\t</kt-portlet>\n\n\n\t\t\t\t<!--end: Datatable -->\n\t\t\t</kt-portlet-body>\n\t\t</kt-portlet>\n\t</div>\n</div>\n\n"

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");

// Angular


// Admin Parts
var routes = [
    // Public user
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', loadChildren: function () { return Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! app/views/main/main.module */ "./src/app/views/main/main.module.ts")).then(function (m) { return m.MainModule; }); } },
    { path: 'auth', loadChildren: function () { return Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! app/views/auth/auth.module */ "./src/app/views/auth/auth.module.ts")).then(function (m) { return m.AuthModule; }); } },
    { path: 'author', loadChildren: function () { return __webpack_require__.e(/*! import() | app-views-author-author-module */ "app-views-author-author-module").then(__webpack_require__.bind(null, /*! app/views/author/author.module */ "./src/app/views/author/author.module.ts")).then(function (m) { return m.AuthorModule; }); } },
    { path: 'reviewer', loadChildren: function () { return Promise.all(/*! import() | app-views-reviewer-pages-r_pages-module */[__webpack_require__.e("default~app-views-pages-pages-module~app-views-reviewer-pages-r_pages-module"), __webpack_require__.e("app-views-reviewer-pages-r_pages-module")]).then(__webpack_require__.bind(null, /*! app/views/reviewer-pages/r_pages.module */ "./src/app/views/reviewer-pages/r_pages.module.ts")).then(function (m) { return m.ReviewerPagesModule; }); } },
    { path: 'admin', loadChildren: function () { return Promise.all(/*! import() | app-views-pages-pages-module */[__webpack_require__.e("default~app-views-pages-pages-module~app-views-reviewer-pages-r_pages-module"), __webpack_require__.e("app-views-pages-pages-module")]).then(__webpack_require__.bind(null, /*! app/views/pages/pages.module */ "./src/app/views/pages/pages.module.ts")).then(function (m) { return m.PagesModule; }); } },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  height: 100%;\n  margin: 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2thbWlsL1BSU19ORVdfVmVyc2lvbi9zcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLFlBQVk7RUFDWixTQUFTLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9hcHAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG5cdGhlaWdodDogMTAwJTtcblx0bWFyZ2luOiAwO1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_base_layout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./core/_base/layout */ "./src/app/core/_base/layout/index.ts");
/* harmony import */ var _core_config_i18n_en__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./core/_config/i18n/en */ "./src/app/core/_config/i18n/en.ts");
/* harmony import */ var _core_config_i18n_ch__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./core/_config/i18n/ch */ "./src/app/core/_config/i18n/ch.ts");
/* harmony import */ var _core_config_i18n_es__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./core/_config/i18n/es */ "./src/app/core/_config/i18n/es.ts");
/* harmony import */ var _core_config_i18n_jp__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./core/_config/i18n/jp */ "./src/app/core/_config/i18n/jp.ts");
/* harmony import */ var _core_config_i18n_de__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./core/_config/i18n/de */ "./src/app/core/_config/i18n/de.ts");
/* harmony import */ var _core_config_i18n_fr__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./core/_config/i18n/fr */ "./src/app/core/_config/i18n/fr.ts");

// Angular


// Layout

// language list






var AppComponent = /** @class */ (function () {
    /**
     * Component constructor
     *
     * @param translationService: TranslationService
     * @param router: Router
     * @param layoutConfigService: LayoutCongifService
     * @param splashScreenService: SplashScreenService
     */
    function AppComponent(translationService, router, layoutConfigService) {
        this.translationService = translationService;
        this.router = router;
        this.layoutConfigService = layoutConfigService;
        // Public properties
        this.title = 'PRS';
        this.unsubscribe = []; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/
        // register translations
        this.translationService.loadTranslations(_core_config_i18n_en__WEBPACK_IMPORTED_MODULE_4__["locale"], _core_config_i18n_ch__WEBPACK_IMPORTED_MODULE_5__["locale"], _core_config_i18n_es__WEBPACK_IMPORTED_MODULE_6__["locale"], _core_config_i18n_jp__WEBPACK_IMPORTED_MODULE_7__["locale"], _core_config_i18n_de__WEBPACK_IMPORTED_MODULE_8__["locale"], _core_config_i18n_fr__WEBPACK_IMPORTED_MODULE_9__["locale"]);
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    AppComponent.prototype.ngOnInit = function () {
        // enable/disable loader
        // this.loader = this.layoutConfigService.getConfig('loader.enabled');
        var routerSubscription = this.router.events.subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"]) {
                // hide splash screen
                // this.wsplashScreenService.hide();
                // scroll to top on every route change
                window.scrollTo(0, 0);
                // to display back the body content
                setTimeout(function () {
                    document.body.classList.add('kt-page--loaded');
                }, 500);
            }
        });
        this.unsubscribe.push(routerSubscription);
    };
    /**
     * On Destroy
     */
    AppComponent.prototype.ngOnDestroy = function () {
        this.unsubscribe.forEach(function (sb) { return sb.unsubscribe(); });
    };
    AppComponent.ctorParameters = function () { return [
        { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_3__["TranslationService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_3__["LayoutConfigService"] }
    ]; };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            // tslint:disable-next-line:component-selector
            selector: 'body[kt-root]',
            template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_base_layout__WEBPACK_IMPORTED_MODULE_3__["TranslationService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _core_base_layout__WEBPACK_IMPORTED_MODULE_3__["LayoutConfigService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: initializeLayoutConfig, hljsLanguages, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initializeLayoutConfig", function() { return initializeLayoutConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hljsLanguages", function() { return hljsLanguages; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/cdk/overlay */ "./node_modules/@angular/cdk/esm5/overlay.es5.js");
/* harmony import */ var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-perfect-scrollbar */ "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
/* harmony import */ var ng_inline_svg__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng-inline-svg */ "./node_modules/ng-inline-svg/lib_esmodule/index.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var ngx_permissions__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-permissions */ "./node_modules/ngx-permissions/fesm5/ngx-permissions.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _ngrx_effects__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ngrx/effects */ "./node_modules/@ngrx/effects/fesm5/effects.js");
/* harmony import */ var _ngrx_router_store__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ngrx/router-store */ "./node_modules/@ngrx/router-store/fesm5/router-store.js");
/* harmony import */ var _ngrx_store_devtools__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @ngrx/store-devtools */ "./node_modules/@ngrx/store-devtools/fesm5/store-devtools.js");
/* harmony import */ var _core_reducers__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./core/reducers */ "./src/app/core/reducers/index.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./core/core.module */ "./src/app/core/core.module.ts");
/* harmony import */ var _views_partials_partials_module__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./views/partials/partials.module */ "./src/app/views/partials/partials.module.ts");
/* harmony import */ var _core_base_layout__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./core/_base/layout */ "./src/app/core/_base/layout/index.ts");
/* harmony import */ var _views_auth_auth_module__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./views/auth/auth.module */ "./src/app/views/auth/auth.module.ts");
/* harmony import */ var _core_auth__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./core/auth */ "./src/app/core/auth/index.ts");
/* harmony import */ var _core_base_crud__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./core/_base/crud */ "./src/app/core/_base/crud/index.ts");
/* harmony import */ var _core_config_layout_config__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./core/_config/layout.config */ "./src/app/core/_config/layout.config.ts");
/* harmony import */ var ngx_highlightjs__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ngx-highlightjs */ "./node_modules/ngx-highlightjs/fesm5/ngx-highlightjs.js");
/* harmony import */ var highlight_js_lib_languages_typescript__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! highlight.js/lib/languages/typescript */ "./node_modules/highlight.js/lib/languages/typescript.js");
/* harmony import */ var highlight_js_lib_languages_typescript__WEBPACK_IMPORTED_MODULE_27___default = /*#__PURE__*/__webpack_require__.n(highlight_js_lib_languages_typescript__WEBPACK_IMPORTED_MODULE_27__);
/* harmony import */ var highlight_js_lib_languages_scss__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! highlight.js/lib/languages/scss */ "./node_modules/highlight.js/lib/languages/scss.js");
/* harmony import */ var highlight_js_lib_languages_scss__WEBPACK_IMPORTED_MODULE_28___default = /*#__PURE__*/__webpack_require__.n(highlight_js_lib_languages_scss__WEBPACK_IMPORTED_MODULE_28__);
/* harmony import */ var highlight_js_lib_languages_xml__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! highlight.js/lib/languages/xml */ "./node_modules/highlight.js/lib/languages/xml.js");
/* harmony import */ var highlight_js_lib_languages_xml__WEBPACK_IMPORTED_MODULE_29___default = /*#__PURE__*/__webpack_require__.n(highlight_js_lib_languages_xml__WEBPACK_IMPORTED_MODULE_29__);
/* harmony import */ var highlight_js_lib_languages_json__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! highlight.js/lib/languages/json */ "./node_modules/highlight.js/lib/languages/json.js");
/* harmony import */ var highlight_js_lib_languages_json__WEBPACK_IMPORTED_MODULE_30___default = /*#__PURE__*/__webpack_require__.n(highlight_js_lib_languages_json__WEBPACK_IMPORTED_MODULE_30__);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _views_main_main_module__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./views/main/main.module */ "./src/app/views/main/main.module.ts");

// Angular








// SVG inline

// Hammer JS

// NGX Permissions

// NGRX




// State

// Copmponents

// Modules


// Partials

// Layout Services

// Auth


// CRUD

// Config

// Highlight JS







// tslint:disable-next-line:class-name
var DEFAULT_PERFECT_SCROLLBAR_CONFIG = {
    wheelSpeed: 0.5,
    swipeEasing: true,
    minScrollbarLength: 40,
    maxScrollbarLength: 300,
};
function initializeLayoutConfig(appConfig) {
    // initialize app by loading default demo layout config
    return function () {
        if (appConfig.getConfig() === null) {
            appConfig.loadConfigs(new _core_config_layout_config__WEBPACK_IMPORTED_MODULE_25__["LayoutConfig"]().configs);
        }
    };
}
function hljsLanguages() {
    return [
        { name: 'typescript', func: highlight_js_lib_languages_typescript__WEBPACK_IMPORTED_MODULE_27__ },
        { name: 'scss', func: highlight_js_lib_languages_scss__WEBPACK_IMPORTED_MODULE_28__ },
        { name: 'xml', func: highlight_js_lib_languages_xml__WEBPACK_IMPORTED_MODULE_29__ },
        { name: 'json', func: highlight_js_lib_languages_json__WEBPACK_IMPORTED_MODULE_30__ }
    ];
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_17__["AppComponent"],
            ],
            imports: [
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__["BrowserAnimationsModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_18__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                // environment.isMockEnabled ? HttpClientInMemoryWebApiModule.forRoot(FakeApiService, {
                // 	passThruUnknownUrl: true,
                // 	dataEncapsulation: false
                // }) : [],
                ngx_permissions__WEBPACK_IMPORTED_MODULE_11__["NgxPermissionsModule"].forRoot(),
                _views_partials_partials_module__WEBPACK_IMPORTED_MODULE_20__["PartialsModule"],
                _core_core_module__WEBPACK_IMPORTED_MODULE_19__["CoreModule"],
                _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_7__["OverlayModule"],
                _ngrx_store__WEBPACK_IMPORTED_MODULE_12__["StoreModule"].forRoot(_core_reducers__WEBPACK_IMPORTED_MODULE_16__["reducers"], { metaReducers: _core_reducers__WEBPACK_IMPORTED_MODULE_16__["metaReducers"] }),
                _ngrx_effects__WEBPACK_IMPORTED_MODULE_13__["EffectsModule"].forRoot([]),
                _ngrx_router_store__WEBPACK_IMPORTED_MODULE_14__["StoreRouterConnectingModule"].forRoot({ stateKey: 'router' }),
                _ngrx_store_devtools__WEBPACK_IMPORTED_MODULE_15__["StoreDevtoolsModule"].instrument(),
                // AuthModule.forRoot(),
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateModule"].forRoot(),
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatProgressSpinnerModule"],
                ng_inline_svg__WEBPACK_IMPORTED_MODULE_9__["InlineSVGModule"].forRoot(),
                // ThemeModule,
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatFormFieldModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_31__["ReactiveFormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatCardModule"],
                _views_auth_auth_module__WEBPACK_IMPORTED_MODULE_22__["AuthModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatInputModule"],
                // PublicPageModule ////////////////
                _views_main_main_module__WEBPACK_IMPORTED_MODULE_32__["MainModule"]
            ],
            exports: [],
            providers: [
                _core_auth__WEBPACK_IMPORTED_MODULE_23__["AuthService"],
                _core_base_layout__WEBPACK_IMPORTED_MODULE_21__["LayoutConfigService"],
                _core_base_layout__WEBPACK_IMPORTED_MODULE_21__["LayoutRefService"],
                _core_base_layout__WEBPACK_IMPORTED_MODULE_21__["MenuConfigService"],
                _core_base_layout__WEBPACK_IMPORTED_MODULE_21__["PageConfigService"],
                _core_base_layout__WEBPACK_IMPORTED_MODULE_21__["KtDialogService"],
                _core_base_layout__WEBPACK_IMPORTED_MODULE_21__["DataTableService"],
                _core_base_layout__WEBPACK_IMPORTED_MODULE_21__["DataTableWorkService"],
                {
                    provide: ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_8__["PERFECT_SCROLLBAR_CONFIG"],
                    useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
                },
                {
                    provide: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["HAMMER_GESTURE_CONFIG"],
                    useClass: _angular_material__WEBPACK_IMPORTED_MODULE_6__["GestureConfig"]
                },
                {
                    // layout config initializer
                    provide: _angular_core__WEBPACK_IMPORTED_MODULE_2__["APP_INITIALIZER"],
                    useFactory: initializeLayoutConfig,
                    deps: [_core_base_layout__WEBPACK_IMPORTED_MODULE_21__["LayoutConfigService"]], multi: true
                },
                {
                    provide: ngx_highlightjs__WEBPACK_IMPORTED_MODULE_26__["HIGHLIGHT_OPTIONS"],
                    useValue: { languages: hljsLanguages }
                },
                _core_base_crud__WEBPACK_IMPORTED_MODULE_24__["HttpUtilsService"],
                _core_base_crud__WEBPACK_IMPORTED_MODULE_24__["TypesUtilsService"],
                _core_base_crud__WEBPACK_IMPORTED_MODULE_24__["LayoutUtilsService"],
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_17__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/core/_base/crud/index.ts":
/*!******************************************!*\
  !*** ./src/app/core/_base/crud/index.ts ***!
  \******************************************/
/*! exports provided: BaseModel, BaseDataSource, QueryParamsModel, QueryResultsModel, HttpExtenstionsModel, HttpUtilsService, TypesUtilsService, InterceptService, LayoutUtilsService, MessageType */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _models_base_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./models/_base.model */ "./src/app/core/_base/crud/models/_base.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "BaseModel", function() { return _models_base_model__WEBPACK_IMPORTED_MODULE_0__["BaseModel"]; });

/* harmony import */ var _models_base_datasource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./models/_base.datasource */ "./src/app/core/_base/crud/models/_base.datasource.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "BaseDataSource", function() { return _models_base_datasource__WEBPACK_IMPORTED_MODULE_1__["BaseDataSource"]; });

/* harmony import */ var _models_query_models_query_params_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./models/query-models/query-params.model */ "./src/app/core/_base/crud/models/query-models/query-params.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "QueryParamsModel", function() { return _models_query_models_query_params_model__WEBPACK_IMPORTED_MODULE_2__["QueryParamsModel"]; });

/* harmony import */ var _models_query_models_query_results_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./models/query-models/query-results.model */ "./src/app/core/_base/crud/models/query-models/query-results.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "QueryResultsModel", function() { return _models_query_models_query_results_model__WEBPACK_IMPORTED_MODULE_3__["QueryResultsModel"]; });

/* harmony import */ var _models_http_extentsions_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./models/http-extentsions-model */ "./src/app/core/_base/crud/models/http-extentsions-model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "HttpExtenstionsModel", function() { return _models_http_extentsions_model__WEBPACK_IMPORTED_MODULE_4__["HttpExtenstionsModel"]; });

/* harmony import */ var _utils_http_utils_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./utils/http-utils.service */ "./src/app/core/_base/crud/utils/http-utils.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "HttpUtilsService", function() { return _utils_http_utils_service__WEBPACK_IMPORTED_MODULE_5__["HttpUtilsService"]; });

/* harmony import */ var _utils_types_utils_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./utils/types-utils.service */ "./src/app/core/_base/crud/utils/types-utils.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TypesUtilsService", function() { return _utils_types_utils_service__WEBPACK_IMPORTED_MODULE_6__["TypesUtilsService"]; });

/* harmony import */ var _utils_intercept_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./utils/intercept.service */ "./src/app/core/_base/crud/utils/intercept.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "InterceptService", function() { return _utils_intercept_service__WEBPACK_IMPORTED_MODULE_7__["InterceptService"]; });

/* harmony import */ var _utils_layout_utils_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./utils/layout-utils.service */ "./src/app/core/_base/crud/utils/layout-utils.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LayoutUtilsService", function() { return _utils_layout_utils_service__WEBPACK_IMPORTED_MODULE_8__["LayoutUtilsService"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "MessageType", function() { return _utils_layout_utils_service__WEBPACK_IMPORTED_MODULE_8__["MessageType"]; });

// Models





// Utils






/***/ }),

/***/ "./src/app/core/_base/crud/models/_base.datasource.ts":
/*!************************************************************!*\
  !*** ./src/app/core/_base/crud/models/_base.datasource.ts ***!
  \************************************************************/
/*! exports provided: BaseDataSource */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseDataSource", function() { return BaseDataSource; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _http_extentsions_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./http-extentsions-model */ "./src/app/core/_base/crud/models/http-extentsions-model.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
// RxJS

// CRUD


// Why not use MatTableDataSource?
/*  In this example, we will not be using the built-in MatTableDataSource because its designed for filtering,
    sorting and pagination of a client - side data array.
    Read the article: 'https://blog.angular-university.io/angular-material-data-table/'
**/
var BaseDataSource = /** @class */ (function () {
    function BaseDataSource() {
        var _this = this;
        this.entitySubject = new rxjs__WEBPACK_IMPORTED_MODULE_0__["BehaviorSubject"]([]);
        this.hasItems = true; // Need to show message: 'No records found'
        this.isPreloadTextViewed$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(true);
        // Paginator | Paginators count
        this.paginatorTotalSubject = new rxjs__WEBPACK_IMPORTED_MODULE_0__["BehaviorSubject"](0);
        this.subscriptions = [];
        this.paginatorTotal$ = this.paginatorTotalSubject.asObservable();
        // subscribe hasItems to (entitySubject.length==0)
        var hasItemsSubscription = this.paginatorTotal$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["distinctUntilChanged"])(), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["skip"])(1)).subscribe(function (res) { return _this.hasItems = res > 0; });
        this.subscriptions.push(hasItemsSubscription);
    }
    BaseDataSource.prototype.connect = function (collectionViewer) {
        // Connecting data source
        return this.entitySubject.asObservable();
    };
    BaseDataSource.prototype.disconnect = function (collectionViewer) {
        // Disonnecting data source
        this.entitySubject.complete();
        this.paginatorTotalSubject.complete();
        this.subscriptions.forEach(function (sb) { return sb.unsubscribe(); });
    };
    BaseDataSource.prototype.baseFilter = function (_entities, _queryParams, _filtrationFields) {
        if (_filtrationFields === void 0) { _filtrationFields = []; }
        var httpExtention = new _http_extentsions_model__WEBPACK_IMPORTED_MODULE_1__["HttpExtenstionsModel"]();
        return httpExtention.baseFilter(_entities, _queryParams, _filtrationFields);
    };
    BaseDataSource.prototype.sortArray = function (_incomingArray, _sortField, _sortOrder) {
        if (_sortField === void 0) { _sortField = ''; }
        if (_sortOrder === void 0) { _sortOrder = 'asc'; }
        var httpExtention = new _http_extentsions_model__WEBPACK_IMPORTED_MODULE_1__["HttpExtenstionsModel"]();
        return httpExtention.sortArray(_incomingArray, _sortField, _sortOrder);
    };
    BaseDataSource.prototype.searchInArray = function (_incomingArray, _queryObj, _filtrationFields) {
        if (_filtrationFields === void 0) { _filtrationFields = []; }
        var httpExtention = new _http_extentsions_model__WEBPACK_IMPORTED_MODULE_1__["HttpExtenstionsModel"]();
        return httpExtention.searchInArray(_incomingArray, _queryObj, _filtrationFields);
    };
    return BaseDataSource;
}());



/***/ }),

/***/ "./src/app/core/_base/crud/models/_base.model.ts":
/*!*******************************************************!*\
  !*** ./src/app/core/_base/crud/models/_base.model.ts ***!
  \*******************************************************/
/*! exports provided: BaseModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseModel", function() { return BaseModel; });
var BaseModel = /** @class */ (function () {
    function BaseModel() {
        // Edit
        this._isEditMode = false;
        // Log
        this._userId = 0; // Admin
    }
    return BaseModel;
}());



/***/ }),

/***/ "./src/app/core/_base/crud/models/http-extentsions-model.ts":
/*!******************************************************************!*\
  !*** ./src/app/core/_base/crud/models/http-extentsions-model.ts ***!
  \******************************************************************/
/*! exports provided: HttpExtenstionsModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpExtenstionsModel", function() { return HttpExtenstionsModel; });
/* harmony import */ var _query_models_query_results_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./query-models/query-results.model */ "./src/app/core/_base/crud/models/query-models/query-results.model.ts");

var HttpExtenstionsModel = /** @class */ (function () {
    function HttpExtenstionsModel() {
    }
    /**
     * Filtration with sorting
     * First do Sort then filter
     *
     * @param _entities: any[]
     * @param _queryParams: QueryParamsModel
     * @param _filtrationFields: string[]
     */
    HttpExtenstionsModel.prototype.baseFilter = function (_entities, _queryParams, _filtrationFields) {
        if (_filtrationFields === void 0) { _filtrationFields = []; }
        // Filtration
        var entitiesResult = this.searchInArray(_entities, _queryParams.filter, _filtrationFields);
        // Sorting
        // start
        if (_queryParams.sortField) {
            entitiesResult = this.sortArray(entitiesResult, _queryParams.sortField, _queryParams.sortOrder);
        }
        // end
        // Paginator
        // start
        var totalCount = entitiesResult.length;
        var initialPos = _queryParams.pageNumber * _queryParams.pageSize;
        entitiesResult = entitiesResult.slice(initialPos, initialPos + _queryParams.pageSize);
        // end
        var queryResults = new _query_models_query_results_model__WEBPACK_IMPORTED_MODULE_0__["QueryResultsModel"]();
        queryResults.items = entitiesResult;
        queryResults.totalCount = totalCount;
        return queryResults;
    };
    /**
     * Sort array by field name and order-type
     * @param _incomingArray: any[]
     * @param _sortField: string
     * @param _sortOrder: string
     */
    HttpExtenstionsModel.prototype.sortArray = function (_incomingArray, _sortField, _sortOrder) {
        if (_sortField === void 0) { _sortField = ''; }
        if (_sortOrder === void 0) { _sortOrder = 'asc'; }
        if (!_sortField) {
            return _incomingArray;
        }
        var result = [];
        result = _incomingArray.sort(function (a, b) {
            if (a[_sortField] < b[_sortField]) {
                return _sortOrder === 'asc' ? -1 : 1;
            }
            if (a[_sortField] > b[_sortField]) {
                return _sortOrder === 'asc' ? 1 : -1;
            }
            return 0;
        });
        return result;
    };
    /**
     * Filter array by some fields
     *
     * @param _incomingArray: any[]
     * @param _queryObj: any
     * @param _filtrationFields: string[]
     */
    HttpExtenstionsModel.prototype.searchInArray = function (_incomingArray, _queryObj, _filtrationFields) {
        if (_filtrationFields === void 0) { _filtrationFields = []; }
        var result = [];
        var resultBuffer = [];
        var indexes = [];
        var firstIndexes = [];
        var doSearch = false;
        _filtrationFields.forEach(function (item) {
            if (item in _queryObj) {
                _incomingArray.forEach(function (element, index) {
                    if (element[item] === _queryObj[item]) {
                        firstIndexes.push(index);
                    }
                });
                firstIndexes.forEach(function (element) {
                    resultBuffer.push(_incomingArray[element]);
                });
                _incomingArray = resultBuffer.slice(0);
                resultBuffer = [].slice(0);
                firstIndexes = [].slice(0);
            }
        });
        Object.keys(_queryObj).forEach(function (key) {
            var searchText = _queryObj[key].toString().trim().toLowerCase();
            if (key && !_filtrationFields.some(function (e) { return e === key; }) && searchText) {
                doSearch = true;
                try {
                    _incomingArray.forEach(function (element, index) {
                        if (element[key]) {
                            var _val = element[key].toString().trim().toLowerCase();
                            if (_val.indexOf(searchText) > -1 && indexes.indexOf(index) === -1) {
                                indexes.push(index);
                            }
                        }
                    });
                }
                catch (ex) {
                    console.log(ex, key, searchText);
                }
            }
        });
        if (!doSearch) {
            return _incomingArray;
        }
        indexes.forEach(function (re) {
            result.push(_incomingArray[re]);
        });
        return result;
    };
    return HttpExtenstionsModel;
}());



/***/ }),

/***/ "./src/app/core/_base/crud/models/query-models/query-params.model.ts":
/*!***************************************************************************!*\
  !*** ./src/app/core/_base/crud/models/query-models/query-params.model.ts ***!
  \***************************************************************************/
/*! exports provided: QueryParamsModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QueryParamsModel", function() { return QueryParamsModel; });
var QueryParamsModel = /** @class */ (function () {
    // constructor overrides
    function QueryParamsModel(_filter, _sortOrder, _sortField, _pageNumber, _pageSize) {
        if (_sortOrder === void 0) { _sortOrder = 'asc'; }
        if (_sortField === void 0) { _sortField = ''; }
        if (_pageNumber === void 0) { _pageNumber = 0; }
        if (_pageSize === void 0) { _pageSize = 10; }
        this.filter = _filter;
        this.sortOrder = _sortOrder;
        this.sortField = _sortField;
        this.pageNumber = _pageNumber;
        this.pageSize = _pageSize;
    }
    QueryParamsModel.ctorParameters = function () { return [
        { type: undefined },
        { type: String },
        { type: String },
        { type: Number },
        { type: Number }
    ]; };
    return QueryParamsModel;
}());



/***/ }),

/***/ "./src/app/core/_base/crud/models/query-models/query-results.model.ts":
/*!****************************************************************************!*\
  !*** ./src/app/core/_base/crud/models/query-models/query-results.model.ts ***!
  \****************************************************************************/
/*! exports provided: QueryResultsModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QueryResultsModel", function() { return QueryResultsModel; });
var QueryResultsModel = /** @class */ (function () {
    function QueryResultsModel(_items, _totalCount, _errorMessage) {
        if (_items === void 0) { _items = []; }
        if (_totalCount === void 0) { _totalCount = 0; }
        if (_errorMessage === void 0) { _errorMessage = ''; }
        this.items = _items;
        this.totalCount = _totalCount;
    }
    QueryResultsModel.ctorParameters = function () { return [
        { type: Array },
        { type: Number },
        { type: String }
    ]; };
    return QueryResultsModel;
}());



/***/ }),

/***/ "./src/app/core/_base/crud/utils/http-utils.service.ts":
/*!*************************************************************!*\
  !*** ./src/app/core/_base/crud/utils/http-utils.service.ts ***!
  \*************************************************************/
/*! exports provided: HttpUtilsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpUtilsService", function() { return HttpUtilsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _crud_models_http_extentsions_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../crud/models/http-extentsions-model */ "./src/app/core/_base/crud/models/http-extentsions-model.ts");

// Angular



var HttpUtilsService = /** @class */ (function () {
    function HttpUtilsService() {
    }
    /**
     * Prepare query http params
     * @param queryParams: QueryParamsModel
     */
    HttpUtilsService.prototype.getFindHTTPParams = function (queryParams) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('lastNamefilter', queryParams.filter)
            .set('sortOrder', queryParams.sortOrder)
            .set('sortField', queryParams.sortField)
            .set('pageNumber', queryParams.pageNumber.toString())
            .set('pageSize', queryParams.pageSize.toString());
        return params;
    };
    /**
     * get standard content-type
     */
    HttpUtilsService.prototype.getHTTPHeaders = function () {
        var result = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        result.set('Content-Type', 'application/json');
        return result;
    };
    HttpUtilsService.prototype.baseFilter = function (_entities, _queryParams, _filtrationFields) {
        if (_filtrationFields === void 0) { _filtrationFields = []; }
        var httpExtention = new _crud_models_http_extentsions_model__WEBPACK_IMPORTED_MODULE_3__["HttpExtenstionsModel"]();
        return httpExtention.baseFilter(_entities, _queryParams, _filtrationFields);
    };
    HttpUtilsService.prototype.sortArray = function (_incomingArray, _sortField, _sortOrder) {
        if (_sortField === void 0) { _sortField = ''; }
        if (_sortOrder === void 0) { _sortOrder = 'asc'; }
        var httpExtention = new _crud_models_http_extentsions_model__WEBPACK_IMPORTED_MODULE_3__["HttpExtenstionsModel"]();
        return httpExtention.sortArray(_incomingArray, _sortField, _sortOrder);
    };
    HttpUtilsService.prototype.searchInArray = function (_incomingArray, _queryObj, _filtrationFields) {
        if (_filtrationFields === void 0) { _filtrationFields = []; }
        var httpExtention = new _crud_models_http_extentsions_model__WEBPACK_IMPORTED_MODULE_3__["HttpExtenstionsModel"]();
        return httpExtention.searchInArray(_incomingArray, _queryObj, _filtrationFields);
    };
    HttpUtilsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], HttpUtilsService);
    return HttpUtilsService;
}());



/***/ }),

/***/ "./src/app/core/_base/crud/utils/intercept.service.ts":
/*!************************************************************!*\
  !*** ./src/app/core/_base/crud/utils/intercept.service.ts ***!
  \************************************************************/
/*! exports provided: InterceptService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InterceptService", function() { return InterceptService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");

// Angular



/**
 * More information there => https://medium.com/@MetonymyQT/angular-http-interceptors-what-are-they-and-how-to-use-them-52e060321088
 */
var InterceptService = /** @class */ (function () {
    function InterceptService() {
    }
    // intercept request and add token
    InterceptService.prototype.intercept = function (request, next) {
        // tslint:disable-next-line:no-debugger
        // modify request
        // request = request.clone({
        // 	setHeaders: {
        // 		Authorization: `Bearer ${localStorage.getItem('accessToken')}`
        // 	}
        // });
        // console.log('----request----');
        // console.log(request);
        // console.log('--- end of request---');
        return next.handle(request).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (event) {
            if (event instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpResponse"]) {
                // console.log('all looks good');
                // http response status code
                // console.log(event.status);
            }
        }, function (error) {
            // http response status code
            // console.log('----response----');
            // console.error('status code:');
            // tslint:disable-next-line:no-debugger
            console.error(error.status);
            console.error(error.message);
            // console.log('--- end of response---');
        }));
    };
    InterceptService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], InterceptService);
    return InterceptService;
}());



/***/ }),

/***/ "./src/app/core/_base/crud/utils/layout-utils.service.ts":
/*!***************************************************************!*\
  !*** ./src/app/core/_base/crud/utils/layout-utils.service.ts ***!
  \***************************************************************/
/*! exports provided: MessageType, LayoutUtilsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MessageType", function() { return MessageType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutUtilsService", function() { return LayoutUtilsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _views_partials_content_crud__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../views/partials/content/crud */ "./src/app/views/partials/content/crud/index.ts");

// Angular


// Partials for CRUD

var MessageType;
(function (MessageType) {
    MessageType[MessageType["Create"] = 0] = "Create";
    MessageType[MessageType["Read"] = 1] = "Read";
    MessageType[MessageType["Update"] = 2] = "Update";
    MessageType[MessageType["Delete"] = 3] = "Delete";
})(MessageType || (MessageType = {}));
var LayoutUtilsService = /** @class */ (function () {
    /**
     * Service constructor
     *
     * @param snackBar: MatSnackBar
     * @param dialog: MatDialog
     */
    function LayoutUtilsService(snackBar, dialog) {
        this.snackBar = snackBar;
        this.dialog = dialog;
    }
    /**
     * Showing (Mat-Snackbar) Notification
     *
     * @param message: string
     * @param type: MessageType
     * @param duration: number
     * @param showCloseButton: boolean
     * @param showUndoButton: boolean
     * @param undoButtonDuration: number
     * @param verticalPosition: 'top' | 'bottom' = 'top'
     */
    LayoutUtilsService.prototype.showActionNotification = function (_message, _type, _duration, _showCloseButton, _showUndoButton, _undoButtonDuration, _verticalPosition) {
        if (_type === void 0) { _type = MessageType.Create; }
        if (_duration === void 0) { _duration = 10000; }
        if (_showCloseButton === void 0) { _showCloseButton = true; }
        if (_showUndoButton === void 0) { _showUndoButton = true; }
        if (_undoButtonDuration === void 0) { _undoButtonDuration = 3000; }
        if (_verticalPosition === void 0) { _verticalPosition = 'bottom'; }
        var _data = {
            message: _message,
            snackBar: this.snackBar,
            showCloseButton: _showCloseButton,
            showUndoButton: _showUndoButton,
            undoButtonDuration: _undoButtonDuration,
            verticalPosition: _verticalPosition,
            type: _type,
            action: 'Undo'
        };
        return this.snackBar.openFromComponent(_views_partials_content_crud__WEBPACK_IMPORTED_MODULE_3__["ActionNotificationComponent"], {
            duration: _duration,
            data: _data,
            verticalPosition: _verticalPosition
        });
    };
    /**
     * Showing (Mat-Snackbar) Notification
     *
     * @param message: string
     * @param type: MessageType
     * @param duration: number
     * @param showCloseButton: boolean
     * @param showUndoButton: boolean
     * @param undoButtonDuration: number
     * @param verticalPosition: 'top' | 'bottom' = 'top'
     */
    LayoutUtilsService.prototype.showActionNotification_ = function (_message, _type, _duration, _showCloseButton, _showUndoButton, _undoButtonDuration, _verticalPosition) {
        if (_type === void 0) { _type = MessageType.Create; }
        if (_duration === void 0) { _duration = 10000; }
        if (_showCloseButton === void 0) { _showCloseButton = true; }
        if (_showUndoButton === void 0) { _showUndoButton = false; }
        if (_undoButtonDuration === void 0) { _undoButtonDuration = 3000; }
        if (_verticalPosition === void 0) { _verticalPosition = 'bottom'; }
        var _data = {
            message: _message,
            snackBar: this.snackBar,
            showCloseButton: _showCloseButton,
            showUndoButton: _showUndoButton,
            undoButtonDuration: _undoButtonDuration,
            verticalPosition: _verticalPosition,
            type: _type,
            action: 'Undo'
        };
        return this.snackBar.openFromComponent(_views_partials_content_crud__WEBPACK_IMPORTED_MODULE_3__["ActionNotificationComponent"], {
            duration: _duration,
            data: _data,
            verticalPosition: _verticalPosition
        });
    };
    /**
     * Showing Confirmation (Mat-Dialog) before Entity Removing
     *
     * @param title: stirng
     * @param description: stirng
     * @param waitDesciption: string
     */
    LayoutUtilsService.prototype.deleteElement = function (title, description, waitDesciption) {
        if (title === void 0) { title = ''; }
        if (description === void 0) { description = ''; }
        if (waitDesciption === void 0) { waitDesciption = ''; }
        return this.dialog.open(_views_partials_content_crud__WEBPACK_IMPORTED_MODULE_3__["DeleteEntityDialogComponent"], {
            data: { title: title, description: description, waitDesciption: waitDesciption },
            width: '440px'
        });
    };
    /**
     * Showing Fetching Window(Mat-Dialog)
     *
     * @param _data: any
     */
    LayoutUtilsService.prototype.fetchElements = function (_data) {
        return this.dialog.open(_views_partials_content_crud__WEBPACK_IMPORTED_MODULE_3__["FetchEntityDialogComponent"], {
            data: _data,
            width: '400px'
        });
    };
    /**
     * Showing Update Status for Entites Window
     *
     * @param title: string
     * @param statuses: string[]
     * @param messages: string[]
     */
    LayoutUtilsService.prototype.updateStatusForEntities = function (title, statuses, messages) {
        return this.dialog.open(_views_partials_content_crud__WEBPACK_IMPORTED_MODULE_3__["UpdateStatusDialogComponent"], {
            data: { title: title, statuses: statuses, messages: messages },
            width: '480px'
        });
    };
    LayoutUtilsService.ctorParameters = function () { return [
        { type: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"] },
        { type: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] }
    ]; };
    LayoutUtilsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"]])
    ], LayoutUtilsService);
    return LayoutUtilsService;
}());



/***/ }),

/***/ "./src/app/core/_base/crud/utils/types-utils.service.ts":
/*!**************************************************************!*\
  !*** ./src/app/core/_base/crud/utils/types-utils.service.ts ***!
  \**************************************************************/
/*! exports provided: TypesUtilsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TypesUtilsService", function() { return TypesUtilsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

/** Angular */

var TypesUtilsService = /** @class */ (function () {
    function TypesUtilsService() {
    }
    /**
     * Convert number to string and addinng '0' before
     *
     * @param value: number
     */
    TypesUtilsService.prototype.padNumber = function (value) {
        if (this.isNumber(value)) {
            return ("0" + value).slice(-2);
        }
        else {
            return '';
        }
    };
    /**
     * Checking value type equals to Number
     *
     * @param value: any
     */
    TypesUtilsService.prototype.isNumber = function (value) {
        return !isNaN(this.toInteger(value));
    };
    /**
     * Covert value to number
     *
     * @param value: any
     */
    TypesUtilsService.prototype.toInteger = function (value) {
        return parseInt("" + value, 10);
    };
    /**
     * Convert date to string with 'MM/dd/yyyy' format
     *
     * @param date: Date
     */
    TypesUtilsService.prototype.dateFormat = function (date) {
        var month = date.getMonth() + 1;
        var day = date.getDate();
        var year = date.getFullYear();
        if (date) {
            return month + "/" + day + "/" + year;
        }
        return '';
    };
    /**
     * Convert Date to string with custom format 'MM/dd/yyyy'
     *
     * @param date: any
     */
    TypesUtilsService.prototype.dateCustomFormat = function (date) {
        var stringDate = '';
        if (date) {
            stringDate += this.isNumber(date.month) ? this.padNumber(date.month) + '/' : '';
            stringDate += this.isNumber(date.day) ? this.padNumber(date.day) + '/' : '';
            stringDate += date.year;
        }
        return stringDate;
    };
    /**
     * Convert string to DateFormatter (For Reactive Forms Validators)
     *
     * @param dateInStr: string (format => 'MM/dd/yyyy')
     */
    TypesUtilsService.prototype.getDateFormatterFromString = function (dateInStr) {
        if (dateInStr && dateInStr.length > 0) {
            var dateParts = dateInStr.trim().split('/');
            return [
                {
                    year: this.toInteger(dateParts[2]),
                    month: this.toInteger(dateParts[0]),
                    day: this.toInteger(dateParts[1])
                }
            ];
        }
        var _date = new Date();
        return [
            {
                year: _date.getFullYear(),
                month: _date.getMonth() + 1,
                day: _date.getDay()
            }
        ];
    };
    /**
     * Convert string to Date
     *
     * @param dateInStr: string (format => 'MM/dd/yyyy')
     */
    TypesUtilsService.prototype.getDateFromString = function (dateInStr) {
        if (dateInStr === void 0) { dateInStr = ''; }
        if (dateInStr && dateInStr.length > 0) {
            var dateParts = dateInStr.trim().split('/');
            var year = this.toInteger(dateParts[2]);
            var month = this.toInteger(dateParts[0]);
            var day = this.toInteger(dateParts[1]);
            // tslint:disable-next-line:prefer-const
            var result = new Date();
            result.setDate(day);
            result.setMonth(month - 1);
            result.setFullYear(year);
            return result;
        }
        return new Date();
    };
    /**
     * Convert Date to string with format 'MM/dd/yyyy'
     * @param _date: Date?
     */
    TypesUtilsService.prototype.getDateStringFromDate = function (_date) {
        if (_date === void 0) { _date = new Date(); }
        var month = _date.getMonth() + 1;
        var year = _date.getFullYear();
        var date = _date.getDate();
        return month + "/" + date + "/" + year;
    };
    TypesUtilsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], TypesUtilsService);
    return TypesUtilsService;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/directives/content-animate.directive.ts":
/*!***************************************************************************!*\
  !*** ./src/app/core/_base/layout/directives/content-animate.directive.ts ***!
  \***************************************************************************/
/*! exports provided: ContentAnimateDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContentAnimateDirective", function() { return ContentAnimateDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");

// Angular



/**
 * Page load animation
 *
 */
var ContentAnimateDirective = /** @class */ (function () {
    /**
     * Directive Consturctor
     * @param el: ElementRef
     * @param router: Router
     * @param animationBuilder: AnimationBuilder
     */
    function ContentAnimateDirective(el, router, animationBuilder) {
        this.el = el;
        this.router = router;
        this.animationBuilder = animationBuilder;
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    ContentAnimateDirective.prototype.ngOnInit = function () {
        var _this = this;
        // animate the content
        this.initAnimate();
        // animate page load
        this.events = this.router.events.subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_3__["NavigationEnd"]) {
                _this.player.play();
            }
        });
    };
    /**
     * On destroy
     */
    ContentAnimateDirective.prototype.ngOnDestroy = function () {
        this.events.unsubscribe();
        this.player.destroy();
    };
    /**
     * Animate page load
     */
    ContentAnimateDirective.prototype.initAnimate = function () {
        this.player = this.animationBuilder
            .build([
            // style({opacity: 0, transform: 'translateY(15px)'}),
            // animate(500, style({opacity: 1, transform: 'translateY(0)'})),
            // style({transform: 'none'}),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({
                transform: 'translateY(-3%)',
                opacity: 0,
                position: 'static'
            }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ transform: 'translateY(0%)', opacity: 1 }))
        ])
            .create(this.el.nativeElement);
    };
    ContentAnimateDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _angular_animations__WEBPACK_IMPORTED_MODULE_2__["AnimationBuilder"] }
    ]; };
    ContentAnimateDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: '[ktContentAnimate]'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_animations__WEBPACK_IMPORTED_MODULE_2__["AnimationBuilder"]])
    ], ContentAnimateDirective);
    return ContentAnimateDirective;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/directives/header.directive.ts":
/*!******************************************************************!*\
  !*** ./src/app/core/_base/layout/directives/header.directive.ts ***!
  \******************************************************************/
/*! exports provided: HeaderDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderDirective", function() { return HeaderDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var object_path__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! object-path */ "./node_modules/object-path/index.js");
/* harmony import */ var object_path__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(object_path__WEBPACK_IMPORTED_MODULE_2__);

// Angular

// ObjectPath

/**
 * Configure Header
 */
var HeaderDirective = /** @class */ (function () {
    /**
     * Directive Constructor
     * @param el: ElementRef
     */
    function HeaderDirective(el) {
        this.el = el;
        this.options = {};
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * After view init
     */
    HeaderDirective.prototype.ngAfterViewInit = function () {
        this.setupOptions();
        var header = new KTHeader(this.el.nativeElement, this.options);
    };
    /**
     * Setup options to header
     */
    HeaderDirective.prototype.setupOptions = function () {
        this.options = {
            classic: {
                desktop: true,
                mobile: false
            },
        };
        if (this.el.nativeElement.getAttribute('data-ktheader-minimize') == '1') {
            object_path__WEBPACK_IMPORTED_MODULE_2__["set"](this.options, 'minimize', {
                desktop: {
                    on: 'kt-header--minimize'
                },
                mobile: {
                    on: 'kt-header--minimize'
                }
            });
            object_path__WEBPACK_IMPORTED_MODULE_2__["set"](this.options, 'offset', {
                desktop: 200,
                mobile: 150
            });
        }
    };
    HeaderDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], HeaderDirective.prototype, "options", void 0);
    HeaderDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: '[ktHeader]',
            exportAs: 'ktHeader',
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]])
    ], HeaderDirective);
    return HeaderDirective;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/directives/menu.directive.ts":
/*!****************************************************************!*\
  !*** ./src/app/core/_base/layout/directives/menu.directive.ts ***!
  \****************************************************************/
/*! exports provided: MenuDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuDirective", function() { return MenuDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var object_path__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! object-path */ "./node_modules/object-path/index.js");
/* harmony import */ var object_path__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(object_path__WEBPACK_IMPORTED_MODULE_2__);

// Angular

// Object-Path

/**
 * Configure menu
 */
var MenuDirective = /** @class */ (function () {
    /**
     * Directive Constructor
     * @param el: ElementRef
     */
    function MenuDirective(el) {
        this.el = el;
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * After view init
     */
    MenuDirective.prototype.ngAfterViewInit = function () {
        this.setupOptions();
        this.menu = new KTMenu(this.el.nativeElement, this.options);
    };
    /**
     * Return the menu
     */
    MenuDirective.prototype.getMenu = function () {
        return this.menu;
    };
    /**
     * Setup menu options
     */
    MenuDirective.prototype.setupOptions = function () {
        // init aside menu
        var menuDesktopMode = 'accordion';
        if (this.el.nativeElement.getAttribute('data-ktmenu-dropdown') === '1') {
            menuDesktopMode = 'dropdown';
        }
        if (typeof object_path__WEBPACK_IMPORTED_MODULE_2__["get"](this.options, 'submenu.desktop') === 'object') {
            object_path__WEBPACK_IMPORTED_MODULE_2__["set"](this.options, 'submenu.desktop', menuDesktopMode);
        }
    };
    MenuDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], MenuDirective.prototype, "options", void 0);
    MenuDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: '[ktMenu]',
            exportAs: 'ktMenu',
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]])
    ], MenuDirective);
    return MenuDirective;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/directives/offcanvas.directive.ts":
/*!*********************************************************************!*\
  !*** ./src/app/core/_base/layout/directives/offcanvas.directive.ts ***!
  \*********************************************************************/
/*! exports provided: OffcanvasDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OffcanvasDirective", function() { return OffcanvasDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

// Angular

/**
 * Setup off Convas
 */
var OffcanvasDirective = /** @class */ (function () {
    /**
     * Directive Constructor
     * @param el: ElementRef
     */
    function OffcanvasDirective(el) {
        this.el = el;
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * After view init
     */
    OffcanvasDirective.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () {
            _this.offcanvas = new KTOffcanvas(_this.el.nativeElement, _this.options);
        });
    };
    /**
     * Returns the offCanvas
     */
    OffcanvasDirective.prototype.getOffcanvas = function () {
        return this.offcanvas;
    };
    OffcanvasDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], OffcanvasDirective.prototype, "options", void 0);
    OffcanvasDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: '[ktOffcanvas]',
            exportAs: 'ktOffcanvas',
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]])
    ], OffcanvasDirective);
    return OffcanvasDirective;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/directives/scroll-top.directive.ts":
/*!**********************************************************************!*\
  !*** ./src/app/core/_base/layout/directives/scroll-top.directive.ts ***!
  \**********************************************************************/
/*! exports provided: ScrollTopDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ScrollTopDirective", function() { return ScrollTopDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

// Angular

/**
 * Scroll to top
 */
var ScrollTopDirective = /** @class */ (function () {
    /**
     * Directive Conctructor
     * @param el: ElementRef
     */
    function ScrollTopDirective(el) {
        this.el = el;
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * After view init
     */
    ScrollTopDirective.prototype.ngAfterViewInit = function () {
        this.scrollTop = new KTScrolltop(this.el.nativeElement, this.options);
    };
    /**
     * Returns ScrollTop
     */
    ScrollTopDirective.prototype.getScrollTop = function () {
        return this.scrollTop;
    };
    ScrollTopDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ScrollTopDirective.prototype, "options", void 0);
    ScrollTopDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: '[ktScrollTop]'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]])
    ], ScrollTopDirective);
    return ScrollTopDirective;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/directives/sparkline-chart.directive.ts":
/*!***************************************************************************!*\
  !*** ./src/app/core/_base/layout/directives/sparkline-chart.directive.ts ***!
  \***************************************************************************/
/*! exports provided: SparklineChartDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SparklineChartDirective", function() { return SparklineChartDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var chart_js_dist_Chart_min_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! chart.js/dist/Chart.min.js */ "./node_modules/chart.js/dist/Chart.min.js");
/* harmony import */ var chart_js_dist_Chart_min_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(chart_js_dist_Chart_min_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _layout_services_layout_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../layout/services/layout-config.service */ "./src/app/core/_base/layout/services/layout-config.service.ts");

// Angular

// Chart

// LayoutConfig

/**
 * Configure sparkline chart
 */
var SparklineChartDirective = /** @class */ (function () {
    /**
     * Directive Constructor
     *
     * @param el: ElementRef
     * @param layoutConfigService: LayoutConfigService
     */
    function SparklineChartDirective(el, layoutConfigService) {
        this.el = el;
        this.layoutConfigService = layoutConfigService;
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * After view init
     */
    SparklineChartDirective.prototype.ngAfterViewInit = function () {
        this.initChart(this.el.nativeElement, this.options.data, this.options.color, this.options.border, this.options.fill, this.options.tooltip);
    };
    /**
     * Init chart
     * @param src: any
     * @param data: any
     * @param color: any
     * @param border: any
     * @param fill: any
     * @param tooltip: any
     */
    SparklineChartDirective.prototype.initChart = function (src, data, color, border, fill, tooltip) {
        if (src.length === 0) {
            return;
        }
        // set default values
        fill = (typeof fill !== 'undefined') ? fill : false;
        tooltip = (typeof tooltip !== 'undefined') ? tooltip : false;
        var config = {
            type: 'line',
            data: {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October'],
                datasets: [{
                        label: '',
                        borderColor: color,
                        borderWidth: border,
                        pointHoverRadius: 4,
                        pointHoverBorderWidth: 12,
                        pointBackgroundColor: chart_js_dist_Chart_min_js__WEBPACK_IMPORTED_MODULE_2__["Chart"].helpers.color('#000000').alpha(0).rgbString(),
                        pointBorderColor: chart_js_dist_Chart_min_js__WEBPACK_IMPORTED_MODULE_2__["Chart"].helpers.color('#000000').alpha(0).rgbString(),
                        pointHoverBackgroundColor: this.layoutConfigService.getConfig('colors.state.danger'),
                        pointHoverBorderColor: chart_js_dist_Chart_min_js__WEBPACK_IMPORTED_MODULE_2__["Chart"].helpers.color('#000000').alpha(0.1).rgbString(),
                        fill: false,
                        data: data,
                    }]
            },
            options: {
                title: {
                    display: false,
                },
                tooltips: {
                    enabled: false,
                    intersect: false,
                    mode: 'nearest',
                    xPadding: 10,
                    yPadding: 10,
                    caretPadding: 10
                },
                legend: {
                    display: false,
                    labels: {
                        usePointStyle: false
                    }
                },
                responsive: true,
                maintainAspectRatio: true,
                hover: {
                    mode: 'index'
                },
                scales: {
                    xAxes: [{
                            display: false,
                            gridLines: false,
                            scaleLabel: {
                                display: true,
                                labelString: 'Month'
                            }
                        }],
                    yAxes: [{
                            display: false,
                            gridLines: false,
                            scaleLabel: {
                                display: true,
                                labelString: 'Value'
                            },
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                },
                elements: {
                    point: {
                        radius: 4,
                        borderWidth: 12
                    },
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 10,
                        top: 5,
                        bottom: 0
                    }
                }
            }
        };
        this.chart = new chart_js_dist_Chart_min_js__WEBPACK_IMPORTED_MODULE_2__["Chart"](src, config);
    };
    /**
     * Returns the chart
     */
    SparklineChartDirective.prototype.getChart = function () {
        return this.chart;
    };
    SparklineChartDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
        { type: _layout_services_layout_config_service__WEBPACK_IMPORTED_MODULE_3__["LayoutConfigService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], SparklineChartDirective.prototype, "options", void 0);
    SparklineChartDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: '[ktSparklineChart]',
            exportAs: 'ktSparklineChart'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _layout_services_layout_config_service__WEBPACK_IMPORTED_MODULE_3__["LayoutConfigService"]])
    ], SparklineChartDirective);
    return SparklineChartDirective;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/directives/sticky.directive.ts":
/*!******************************************************************!*\
  !*** ./src/app/core/_base/layout/directives/sticky.directive.ts ***!
  \******************************************************************/
/*! exports provided: StickyDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StickyDirective", function() { return StickyDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_internal_scheduler_animationFrame__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/internal/scheduler/animationFrame */ "./node_modules/rxjs/internal/scheduler/animationFrame.js");
/* harmony import */ var rxjs_internal_scheduler_animationFrame__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(rxjs_internal_scheduler_animationFrame__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");






var StickyDirective = /** @class */ (function () {
    function StickyDirective(stickyElement, platformId) {
        var _this = this;
        this.stickyElement = stickyElement;
        this.platformId = platformId;
        this.filterGate = false;
        this.marginTop$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](0);
        this.marginBottom$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](0);
        this.enable$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](true);
        this.sticky = false;
        this.boundaryReached = false;
        /**
         * The field represents some position values in normal (not sticky) mode.
         * If the browser size or the content of the page changes, this value must be recalculated.
         */
        this.scroll$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.resize$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.extraordinaryChange$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](undefined);
        this.componentDestroyed = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.listener = function (e) {
            var upperScreenEdgeAt = e.target.scrollTop || window.pageYOffset;
            _this.scroll$.next(upperScreenEdgeAt);
        };
        /** Throttle the scroll to animation frame (around 16.67ms) */
        this.scrollThrottled$ = this.scroll$
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["throttleTime"])(0, rxjs_internal_scheduler_animationFrame__WEBPACK_IMPORTED_MODULE_4__["animationFrame"]), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["share"])());
        /** Throttle the resize to animation frame (around 16.67ms) */
        this.resizeThrottled$ = this.resize$
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["throttleTime"])(0, rxjs_internal_scheduler_animationFrame__WEBPACK_IMPORTED_MODULE_4__["animationFrame"]), 
        // emit once since we are currently using combineLatest
        Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["startWith"])(null), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["share"])());
        this.status$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["combineLatest"])(this.enable$, this.scrollThrottled$, this.marginTop$, this.marginBottom$, this.extraordinaryChange$, this.resizeThrottled$)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["filter"])(function (_a) {
            var enabled = _a[0];
            return _this.checkEnabled(enabled);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (_a) {
            var enabled = _a[0], pageYOffset = _a[1], marginTop = _a[2], marginBottom = _a[3];
            return _this.determineStatus(_this.determineElementOffsets(), pageYOffset, marginTop, marginBottom, enabled);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["share"])());
    }
    Object.defineProperty(StickyDirective.prototype, "marginTop", {
        set: function (value) {
            this.marginTop$.next(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StickyDirective.prototype, "marginBottom", {
        set: function (value) {
            this.marginBottom$.next(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StickyDirective.prototype, "enable", {
        set: function (value) {
            this.enable$.next(value);
        },
        enumerable: true,
        configurable: true
    });
    StickyDirective.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.status$
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["takeUntil"])(this.componentDestroyed))
            .subscribe(function (status) { return _this.setSticky(status); });
    };
    StickyDirective.prototype.recalculate = function () {
        var _this = this;
        if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["isPlatformBrowser"])(this.platformId)) {
            // Make sure to be in the next tick by using timeout
            setTimeout(function () {
                _this.extraordinaryChange$.next(undefined);
            }, 0);
        }
    };
    /**
     * This is nasty code that should be refactored at some point.
     *
     * The Problem is, we filter for enabled. So that the code doesn't run
     * if @Input enabled = false. But if the user disables, we need exactly 1
     * emit in order to reset and call removeSticky. So this method basically
     * turns the filter in "filter, but let the first pass".
     */
    StickyDirective.prototype.checkEnabled = function (enabled) {
        if (!Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["isPlatformBrowser"])(this.platformId)) {
            return false;
        }
        if (enabled) {
            // reset the gate
            this.filterGate = false;
            return true;
        }
        else {
            if (this.filterGate) {
                // gate closed, first emit has happened
                return false;
            }
            else {
                // this is the first emit for enabled = false,
                // let it pass, and activate the gate
                // so the next wont pass.
                this.filterGate = true;
                return true;
            }
        }
    };
    StickyDirective.prototype.onWindowResize = function () {
        if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["isPlatformBrowser"])(this.platformId)) {
            this.resize$.next();
        }
    };
    StickyDirective.prototype.setupListener = function () {
        if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["isPlatformBrowser"])(this.platformId)) {
            var target = this.getScrollTarget();
            target.addEventListener('scroll', this.listener);
        }
    };
    StickyDirective.prototype.removeListener = function () {
        if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["isPlatformBrowser"])(this.platformId)) {
            var target = this.getScrollTarget();
            target.removeEventListener('scroll', this.listener);
        }
    };
    StickyDirective.prototype.ngOnInit = function () {
        // this.checkSetup();
        this.setupListener();
    };
    StickyDirective.prototype.ngOnDestroy = function () {
        this.componentDestroyed.next();
        this.removeListener();
    };
    StickyDirective.prototype.getComputedStyle = function (el) {
        return el.getBoundingClientRect();
    };
    StickyDirective.prototype.getScrollTarget = function () {
        var target;
        if (this.scrollContainer && typeof this.scrollContainer === 'string') {
            target = document.querySelector(this.scrollContainer);
        }
        else if (this.scrollContainer && this.scrollContainer instanceof HTMLElement) {
            target = this.scrollContainer;
        }
        else {
            target = window;
        }
        return target;
    };
    StickyDirective.prototype.determineStatus = function (originalVals, pageYOffset, marginTop, marginBottom, enabled) {
        var stickyElementHeight = this.getComputedStyle(this.stickyElement.nativeElement).height;
        var reachedLowerEdge = this.boundaryElement && window.pageYOffset + stickyElementHeight + marginBottom >= (originalVals.bottomBoundary - marginTop);
        return {
            isSticky: enabled && pageYOffset > originalVals.offsetY,
            reachedLowerEdge: reachedLowerEdge,
            marginBottom: marginBottom,
            marginTop: marginTop,
        };
    };
    /**
     * Gets the offset for element. If the element
     * currently is sticky, it will get removed
     * to access the original position. Other
     * wise this would just be 0 for fixed elements.
     */
    StickyDirective.prototype.determineElementOffsets = function () {
        if (this.sticky) {
            this.removeSticky();
        }
        var bottomBoundary = null;
        if (this.boundaryElement) {
            var boundaryElementHeight = this.getComputedStyle(this.boundaryElement).height;
            var boundaryElementOffset = getPosition(this.boundaryElement).y;
            bottomBoundary = boundaryElementHeight + boundaryElementOffset;
        }
        return { offsetY: (getPosition(this.stickyElement.nativeElement).y - this.marginTop$.value), bottomBoundary: bottomBoundary };
    };
    StickyDirective.prototype.makeSticky = function (boundaryReached, marginTop, marginBottom) {
        if (boundaryReached === void 0) { boundaryReached = false; }
        this.boundaryReached = boundaryReached;
        // do this before setting it to pos:fixed
        var _a = this.getComputedStyle(this.stickyElement.nativeElement), width = _a.width, height = _a.height, left = _a.left;
        var offSet = boundaryReached ? (this.getComputedStyle(this.boundaryElement).bottom - height - this.marginBottom$.value) : this.marginTop$.value;
        this.sticky = true;
        this.stickyElement.nativeElement.style.position = 'sticky';
        this.stickyElement.nativeElement.style.backgroundColor = '#fff';
        this.stickyElement.nativeElement.style.top = this.marginTop$.value + 'px';
        // this.stickyElement.nativeElement.style.left = left + 'px';
        this.stickyElement.nativeElement.style.width = width + "px";
        this.stickyElement.nativeElement.style.zIndex = "2";
        if (this.spacerElement) {
            var spacerHeight = marginBottom + height + marginTop;
            this.spacerElement.style.height = spacerHeight + "px";
        }
    };
    StickyDirective.prototype.checkSetup = function () {
        if (Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["isDevMode"])() && !this.spacerElement) {
            console.warn("******There might be an issue with your sticky directive!******\n\nYou haven't specified a spacer element. This will cause the page to jump.\n\nBest practise is to provide a spacer element (e.g. a div) right before/after the sticky element.\nThen pass the spacer element as input:\n\n<div #spacer></div>\n\n<div stickyThing=\"\" [spacer]=\"spacer\">\n    I am sticky!\n</div>");
        }
    };
    StickyDirective.prototype.setSticky = function (status) {
        if (status.isSticky) {
            this.makeSticky(status.reachedLowerEdge, status.marginTop, status.marginBottom);
        }
        else {
            this.removeSticky();
        }
    };
    StickyDirective.prototype.removeSticky = function () {
        this.boundaryReached = false;
        this.sticky = false;
        this.stickyElement.nativeElement.style.position = '';
        this.stickyElement.nativeElement.style.width = 'auto';
        this.stickyElement.nativeElement.style.left = 'auto';
        this.stickyElement.nativeElement.style.top = 'auto';
        if (this.spacerElement) {
            this.spacerElement.style.height = '0';
        }
    };
    StickyDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
        { type: String, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["PLATFORM_ID"],] }] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], StickyDirective.prototype, "scrollContainer", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('spacerElement'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", HTMLElement)
    ], StickyDirective.prototype, "spacerElement", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('boundaryElement'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", HTMLElement)
    ], StickyDirective.prototype, "boundaryElement", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('class.is-sticky'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], StickyDirective.prototype, "sticky", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('class.boundary-reached'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], StickyDirective.prototype, "boundaryReached", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Number])
    ], StickyDirective.prototype, "marginTop", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Number])
    ], StickyDirective.prototype, "marginBottom", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Boolean])
    ], StickyDirective.prototype, "enable", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:resize', []),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", []),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], StickyDirective.prototype, "onWindowResize", null);
    StickyDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: '[ktSticky]'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_core__WEBPACK_IMPORTED_MODULE_1__["PLATFORM_ID"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], String])
    ], StickyDirective);
    return StickyDirective;
}());

// Thanks to https://stanko.github.io/javascript-get-element-offset/
function getPosition(el) {
    var top = 0;
    var left = 0;
    var element = el;
    // Loop through the DOM tree
    // and add it's parent's offset to get page offset
    do {
        top += element.offsetTop || 0;
        left += element.offsetLeft || 0;
        element = element.offsetParent;
    } while (element);
    return {
        y: top,
        x: left,
    };
}


/***/ }),

/***/ "./src/app/core/_base/layout/directives/tab-click-event.directive.ts":
/*!***************************************************************************!*\
  !*** ./src/app/core/_base/layout/directives/tab-click-event.directive.ts ***!
  \***************************************************************************/
/*! exports provided: TabClickEventDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabClickEventDirective", function() { return TabClickEventDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


/**
 * Listen Tab click
 */
var TabClickEventDirective = /** @class */ (function () {
    /**
     * Directive Constructor
     * @param el: ElementRef
     * @param render: Renderer2
     */
    function TabClickEventDirective(el, render) {
        this.el = el;
        this.render = render;
    }
    /**
     * A directive handler the tab click event for active tab
     * @param target
     */
    TabClickEventDirective.prototype.onClick = function (target) {
        // remove previous active tab
        var parent = target.closest('[role="tablist"]');
        var activeLink = parent.querySelector('[role="tab"].active');
        if (activeLink) {
            this.render.removeClass(activeLink, 'active');
        }
        // set active tab
        var link = target.closest('[role="tab"]');
        if (link) {
            this.render.addClass(link, 'active');
        }
    };
    TabClickEventDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('click', ['$event.target']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [HTMLElement]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], TabClickEventDirective.prototype, "onClick", null);
    TabClickEventDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: '[ktTabClickEvent]'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"]])
    ], TabClickEventDirective);
    return TabClickEventDirective;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/directives/toggle.directive.ts":
/*!******************************************************************!*\
  !*** ./src/app/core/_base/layout/directives/toggle.directive.ts ***!
  \******************************************************************/
/*! exports provided: ToggleDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToggleDirective", function() { return ToggleDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

// Angular

/**
 * Toggle
 */
var ToggleDirective = /** @class */ (function () {
    /**
     * Directive constructor
     * @param el: ElementRef
     */
    function ToggleDirective(el) {
        this.el = el;
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * After view init
     */
    ToggleDirective.prototype.ngAfterViewInit = function () {
        this.toggle = new KTToggle(this.el.nativeElement, this.options);
    };
    ToggleDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ToggleDirective.prototype, "options", void 0);
    ToggleDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: '[ktToggle]',
            exportAs: 'ktToggle'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]])
    ], ToggleDirective);
    return ToggleDirective;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/index.ts":
/*!********************************************!*\
  !*** ./src/app/core/_base/layout/index.ts ***!
  \********************************************/
/*! exports provided: SparklineChartDirective, OffcanvasDirective, ScrollTopDirective, TabClickEventDirective, ToggleDirective, ContentAnimateDirective, HeaderDirective, MenuDirective, StickyDirective, DataTableItemModel, ExternalCodeExample, DataTableWorkModel, FirstLetterPipe, GetObjectPipe, JoinPipe, SafePipe, TimeElapsedPipe, CustomSortPipe, DataTableService, TranslationService, LayoutConfigService, LayoutRefService, MenuAsideService, MenuConfigService, MenuHorizontalService, PageConfigService, SubheaderService, KtDialogService, DataTableWorkService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _directives_sparkline_chart_directive__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./directives/sparkline-chart.directive */ "./src/app/core/_base/layout/directives/sparkline-chart.directive.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SparklineChartDirective", function() { return _directives_sparkline_chart_directive__WEBPACK_IMPORTED_MODULE_0__["SparklineChartDirective"]; });

/* harmony import */ var _directives_offcanvas_directive__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./directives/offcanvas.directive */ "./src/app/core/_base/layout/directives/offcanvas.directive.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OffcanvasDirective", function() { return _directives_offcanvas_directive__WEBPACK_IMPORTED_MODULE_1__["OffcanvasDirective"]; });

/* harmony import */ var _directives_scroll_top_directive__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./directives/scroll-top.directive */ "./src/app/core/_base/layout/directives/scroll-top.directive.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ScrollTopDirective", function() { return _directives_scroll_top_directive__WEBPACK_IMPORTED_MODULE_2__["ScrollTopDirective"]; });

/* harmony import */ var _directives_tab_click_event_directive__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./directives/tab-click-event.directive */ "./src/app/core/_base/layout/directives/tab-click-event.directive.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TabClickEventDirective", function() { return _directives_tab_click_event_directive__WEBPACK_IMPORTED_MODULE_3__["TabClickEventDirective"]; });

/* harmony import */ var _directives_toggle_directive__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./directives/toggle.directive */ "./src/app/core/_base/layout/directives/toggle.directive.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ToggleDirective", function() { return _directives_toggle_directive__WEBPACK_IMPORTED_MODULE_4__["ToggleDirective"]; });

/* harmony import */ var _directives_content_animate_directive__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./directives/content-animate.directive */ "./src/app/core/_base/layout/directives/content-animate.directive.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ContentAnimateDirective", function() { return _directives_content_animate_directive__WEBPACK_IMPORTED_MODULE_5__["ContentAnimateDirective"]; });

/* harmony import */ var _directives_header_directive__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./directives/header.directive */ "./src/app/core/_base/layout/directives/header.directive.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "HeaderDirective", function() { return _directives_header_directive__WEBPACK_IMPORTED_MODULE_6__["HeaderDirective"]; });

/* harmony import */ var _directives_menu_directive__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./directives/menu.directive */ "./src/app/core/_base/layout/directives/menu.directive.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "MenuDirective", function() { return _directives_menu_directive__WEBPACK_IMPORTED_MODULE_7__["MenuDirective"]; });

/* harmony import */ var _directives_sticky_directive__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./directives/sticky.directive */ "./src/app/core/_base/layout/directives/sticky.directive.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "StickyDirective", function() { return _directives_sticky_directive__WEBPACK_IMPORTED_MODULE_8__["StickyDirective"]; });

/* harmony import */ var _models_datatable_item_model__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./models/datatable-item.model */ "./src/app/core/_base/layout/models/datatable-item.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DataTableItemModel", function() { return _models_datatable_item_model__WEBPACK_IMPORTED_MODULE_9__["DataTableItemModel"]; });

/* harmony import */ var _models_external_code_example__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./models/external-code-example */ "./src/app/core/_base/layout/models/external-code-example.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ExternalCodeExample", function() { return _models_external_code_example__WEBPACK_IMPORTED_MODULE_10__["ExternalCodeExample"]; });

/* harmony import */ var _models_datatable_work_model__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./models/datatable-work.model */ "./src/app/core/_base/layout/models/datatable-work.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DataTableWorkModel", function() { return _models_datatable_work_model__WEBPACK_IMPORTED_MODULE_11__["DataTableWorkModel"]; });

/* harmony import */ var _pipes_first_letter_pipe__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./pipes/first-letter.pipe */ "./src/app/core/_base/layout/pipes/first-letter.pipe.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FirstLetterPipe", function() { return _pipes_first_letter_pipe__WEBPACK_IMPORTED_MODULE_12__["FirstLetterPipe"]; });

/* harmony import */ var _pipes_get_object_pipe__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./pipes/get-object.pipe */ "./src/app/core/_base/layout/pipes/get-object.pipe.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "GetObjectPipe", function() { return _pipes_get_object_pipe__WEBPACK_IMPORTED_MODULE_13__["GetObjectPipe"]; });

/* harmony import */ var _pipes_join_pipe__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./pipes/join.pipe */ "./src/app/core/_base/layout/pipes/join.pipe.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "JoinPipe", function() { return _pipes_join_pipe__WEBPACK_IMPORTED_MODULE_14__["JoinPipe"]; });

/* harmony import */ var _pipes_safe_pipe__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./pipes/safe.pipe */ "./src/app/core/_base/layout/pipes/safe.pipe.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SafePipe", function() { return _pipes_safe_pipe__WEBPACK_IMPORTED_MODULE_15__["SafePipe"]; });

/* harmony import */ var _pipes_time_elapsed_pipe__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./pipes/time-elapsed.pipe */ "./src/app/core/_base/layout/pipes/time-elapsed.pipe.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TimeElapsedPipe", function() { return _pipes_time_elapsed_pipe__WEBPACK_IMPORTED_MODULE_16__["TimeElapsedPipe"]; });

/* harmony import */ var _pipes_custom_sort_pipe__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./pipes/custom-sort.pipe */ "./src/app/core/_base/layout/pipes/custom-sort.pipe.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "CustomSortPipe", function() { return _pipes_custom_sort_pipe__WEBPACK_IMPORTED_MODULE_17__["CustomSortPipe"]; });

/* harmony import */ var _services_datatable_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./services/datatable.service */ "./src/app/core/_base/layout/services/datatable.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DataTableService", function() { return _services_datatable_service__WEBPACK_IMPORTED_MODULE_18__["DataTableService"]; });

/* harmony import */ var _services_translation_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./services/translation.service */ "./src/app/core/_base/layout/services/translation.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TranslationService", function() { return _services_translation_service__WEBPACK_IMPORTED_MODULE_19__["TranslationService"]; });

/* harmony import */ var _services_layout_config_service__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./services/layout-config.service */ "./src/app/core/_base/layout/services/layout-config.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LayoutConfigService", function() { return _services_layout_config_service__WEBPACK_IMPORTED_MODULE_20__["LayoutConfigService"]; });

/* harmony import */ var _services_layout_ref_service__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./services/layout-ref.service */ "./src/app/core/_base/layout/services/layout-ref.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LayoutRefService", function() { return _services_layout_ref_service__WEBPACK_IMPORTED_MODULE_21__["LayoutRefService"]; });

/* harmony import */ var _services_menu_aside_service__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./services/menu-aside.service */ "./src/app/core/_base/layout/services/menu-aside.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "MenuAsideService", function() { return _services_menu_aside_service__WEBPACK_IMPORTED_MODULE_22__["MenuAsideService"]; });

/* harmony import */ var _services_menu_config_service__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./services/menu-config.service */ "./src/app/core/_base/layout/services/menu-config.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "MenuConfigService", function() { return _services_menu_config_service__WEBPACK_IMPORTED_MODULE_23__["MenuConfigService"]; });

/* harmony import */ var _services_menu_horizontal_service__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./services/menu-horizontal.service */ "./src/app/core/_base/layout/services/menu-horizontal.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "MenuHorizontalService", function() { return _services_menu_horizontal_service__WEBPACK_IMPORTED_MODULE_24__["MenuHorizontalService"]; });

/* harmony import */ var _services_page_config_service__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./services/page-config.service */ "./src/app/core/_base/layout/services/page-config.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PageConfigService", function() { return _services_page_config_service__WEBPACK_IMPORTED_MODULE_25__["PageConfigService"]; });

/* harmony import */ var _services_subheader_service__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./services/subheader.service */ "./src/app/core/_base/layout/services/subheader.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SubheaderService", function() { return _services_subheader_service__WEBPACK_IMPORTED_MODULE_26__["SubheaderService"]; });

/* harmony import */ var _services_kt_dialog_service__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./services/kt-dialog.service */ "./src/app/core/_base/layout/services/kt-dialog.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "KtDialogService", function() { return _services_kt_dialog_service__WEBPACK_IMPORTED_MODULE_27__["KtDialogService"]; });

/* harmony import */ var _services_datatable_work_service__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./services/datatable-work.service */ "./src/app/core/_base/layout/services/datatable-work.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DataTableWorkService", function() { return _services_datatable_work_service__WEBPACK_IMPORTED_MODULE_28__["DataTableWorkService"]; });

// Directives









// Models


// Work Model

// Pipes






// Services










// work service

// Server
// export { FakeApiService } from './server/fake-api/fake-api.service';


/***/ }),

/***/ "./src/app/core/_base/layout/models/datatable-item.model.ts":
/*!******************************************************************!*\
  !*** ./src/app/core/_base/layout/models/datatable-item.model.ts ***!
  \******************************************************************/
/*! exports provided: DataTableItemModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataTableItemModel", function() { return DataTableItemModel; });
var DataTableItemModel = /** @class */ (function () {
    function DataTableItemModel() {
    }
    return DataTableItemModel;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/models/datatable-work.model.ts":
/*!******************************************************************!*\
  !*** ./src/app/core/_base/layout/models/datatable-work.model.ts ***!
  \******************************************************************/
/*! exports provided: DataTableWorkModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataTableWorkModel", function() { return DataTableWorkModel; });
var DataTableWorkModel = /** @class */ (function () {
    function DataTableWorkModel() {
    }
    return DataTableWorkModel;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/models/external-code-example.ts":
/*!*******************************************************************!*\
  !*** ./src/app/core/_base/layout/models/external-code-example.ts ***!
  \*******************************************************************/
/*! exports provided: ExternalCodeExample */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExternalCodeExample", function() { return ExternalCodeExample; });
var ExternalCodeExample = /** @class */ (function () {
    function ExternalCodeExample() {
        this.isCodeVisible = true;
    }
    return ExternalCodeExample;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/pipes/custom-sort.pipe.ts":
/*!*************************************************************!*\
  !*** ./src/app/core/_base/layout/pipes/custom-sort.pipe.ts ***!
  \*************************************************************/
/*! exports provided: CustomSortPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomSortPipe", function() { return CustomSortPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var CustomSortPipe = /** @class */ (function () {
    function CustomSortPipe() {
    }
    CustomSortPipe.prototype.transform = function (messages) {
        var _this = this;
        if (messages.length > 0) {
            var copy = Object.assign([], messages);
            copy.sort(function (a, b) { return _this.compare(a.DTime, b.DTime); });
            return copy;
        }
    };
    CustomSortPipe.prototype.compare = function (lfh, rhs) {
        if (lfh > rhs) {
            return 1;
        }
        else if (lfh < rhs) {
            return -1;
        }
        else {
            return 0;
        }
    };
    CustomSortPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'sortbyDate'
        })
    ], CustomSortPipe);
    return CustomSortPipe;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/pipes/first-letter.pipe.ts":
/*!**************************************************************!*\
  !*** ./src/app/core/_base/layout/pipes/first-letter.pipe.ts ***!
  \**************************************************************/
/*! exports provided: FirstLetterPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirstLetterPipe", function() { return FirstLetterPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

// Angular

/**
 * Returns only first letter of string
 */
var FirstLetterPipe = /** @class */ (function () {
    function FirstLetterPipe() {
    }
    /**
     * Transform
     *
     * @param value: any
     * @param args: any
     */
    FirstLetterPipe.prototype.transform = function (value, args) {
        return value.split(' ').map(function (n) { return n[0]; }).join('');
    };
    FirstLetterPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'firstLetter'
        })
    ], FirstLetterPipe);
    return FirstLetterPipe;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/pipes/get-object.pipe.ts":
/*!************************************************************!*\
  !*** ./src/app/core/_base/layout/pipes/get-object.pipe.ts ***!
  \************************************************************/
/*! exports provided: GetObjectPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetObjectPipe", function() { return GetObjectPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var object_path__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! object-path */ "./node_modules/object-path/index.js");
/* harmony import */ var object_path__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(object_path__WEBPACK_IMPORTED_MODULE_2__);

// Angular

// Object-Path

/**
 * Returns object from parent object
 */
var GetObjectPipe = /** @class */ (function () {
    function GetObjectPipe() {
    }
    /**
     * Transform
     *
     * @param value: any
     * @param args: any
     */
    GetObjectPipe.prototype.transform = function (value, args) {
        return object_path__WEBPACK_IMPORTED_MODULE_2__["get"](value, args);
    };
    GetObjectPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'getObject'
        })
    ], GetObjectPipe);
    return GetObjectPipe;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/pipes/join.pipe.ts":
/*!******************************************************!*\
  !*** ./src/app/core/_base/layout/pipes/join.pipe.ts ***!
  \******************************************************/
/*! exports provided: JoinPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JoinPipe", function() { return JoinPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

// Angular

/**
 * Returns string from Array
 */
var JoinPipe = /** @class */ (function () {
    function JoinPipe() {
    }
    /**
     * Transform
     *
     * @param value: any
     * @param args: any
     */
    JoinPipe.prototype.transform = function (value, args) {
        if (Array.isArray(value)) {
            return value.join(' ');
        }
        return value;
    };
    JoinPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'join'
        })
    ], JoinPipe);
    return JoinPipe;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/pipes/safe.pipe.ts":
/*!******************************************************!*\
  !*** ./src/app/core/_base/layout/pipes/safe.pipe.ts ***!
  \******************************************************/
/*! exports provided: SafePipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SafePipe", function() { return SafePipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");

// Angular


/**
 * Sanitize HTML
 */
var SafePipe = /** @class */ (function () {
    /**
     * Pipe Constructor
     *
     * @param _sanitizer: DomSanitezer
     */
    function SafePipe(_sanitizer) {
        this._sanitizer = _sanitizer;
    }
    /**
     * Transform
     *
     * @param value: string
     * @param type: string
     */
    SafePipe.prototype.transform = function (value, type) {
        switch (type) {
            case 'html':
                return this._sanitizer.bypassSecurityTrustHtml(value);
            case 'style':
                return this._sanitizer.bypassSecurityTrustStyle(value);
            case 'script':
                return this._sanitizer.bypassSecurityTrustScript(value);
            case 'url':
                return this._sanitizer.bypassSecurityTrustUrl(value);
            case 'resourceUrl':
                return this._sanitizer.bypassSecurityTrustResourceUrl(value);
            default:
                return this._sanitizer.bypassSecurityTrustHtml(value);
        }
    };
    SafePipe.ctorParameters = function () { return [
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"] }
    ]; };
    SafePipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'safe'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"]])
    ], SafePipe);
    return SafePipe;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/pipes/time-elapsed.pipe.ts":
/*!**************************************************************!*\
  !*** ./src/app/core/_base/layout/pipes/time-elapsed.pipe.ts ***!
  \**************************************************************/
/*! exports provided: TimeElapsedPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimeElapsedPipe", function() { return TimeElapsedPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

// Angular

/**
 * https://github.com/AndrewPoyntz/time-ago-pipe
 * An Angular pipe for converting a date string into a time ago
 */
var TimeElapsedPipe = /** @class */ (function () {
    /**
     * Pipe Constructor
     *
     * @param changeDetectorRef: ChangeDetectorRef
     * @param ngZone: NgZone
     */
    function TimeElapsedPipe(changeDetectorRef, ngZone) {
        this.changeDetectorRef = changeDetectorRef;
        this.ngZone = ngZone;
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On destroy
     */
    TimeElapsedPipe.prototype.ngOnDestroy = function () {
        this.removeTimer();
    };
    /**
     * Transform
     *
     * @param value: string
     */
    TimeElapsedPipe.prototype.transform = function (value) {
        var _this = this;
        this.removeTimer();
        var d = new Date(value);
        var now = new Date();
        var seconds = Math.round(Math.abs((now.getTime() - d.getTime()) / 1000));
        var timeToUpdate = this.getSecondsUntilUpdate(seconds) * 1000;
        this.timer = this.ngZone.runOutsideAngular(function () {
            if (typeof window !== 'undefined') {
                return window.setTimeout(function () {
                    _this.ngZone.run(function () {
                        return _this.changeDetectorRef.markForCheck();
                    });
                }, timeToUpdate);
            }
            return null;
        });
        var minutes = Math.round(Math.abs(seconds / 60));
        var hours = Math.round(Math.abs(minutes / 60));
        var days = Math.round(Math.abs(hours / 24));
        var months = Math.round(Math.abs(days / 30.416));
        var years = Math.round(Math.abs(days / 365));
        if (seconds <= 45) {
            return 'just now';
        }
        else if (seconds <= 90) {
            return '1 min';
        }
        else if (minutes <= 45) {
            return minutes + ' mins';
        }
        else if (minutes <= 90) {
            return '1 hr';
        }
        else if (hours <= 22) {
            return hours + ' hrs';
        }
        else if (hours <= 36) {
            return '1 day';
        }
        else if (days <= 25) {
            return days + ' days';
        }
        else if (days <= 45) {
            return '1 month';
        }
        else if (days <= 345) {
            return months + ' months';
        }
        else if (days <= 545) {
            return '1 year';
        }
        else {
            // (days > 545)
            return years + ' years';
        }
    };
    /**
     * Remove timer
     */
    TimeElapsedPipe.prototype.removeTimer = function () {
        if (this.timer) {
            window.clearTimeout(this.timer);
            this.timer = null;
        }
    };
    /**
     * Returns Seconds Until Update
     * @param seconds: number
     */
    TimeElapsedPipe.prototype.getSecondsUntilUpdate = function (seconds) {
        var min = 60;
        var hr = min * 60;
        var day = hr * 24;
        if (seconds < min) {
            // less than 1 min, update ever 2 secs
            return 2;
        }
        else if (seconds < hr) {
            // less than an hour, update every 30 secs
            return 30;
        }
        else if (seconds < day) {
            // less then a day, update every 5 mins
            return 300;
        }
        else {
            // update every hour
            return 3600;
        }
    };
    TimeElapsedPipe.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] }
    ]; };
    TimeElapsedPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'kTimeElapsed'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]])
    ], TimeElapsedPipe);
    return TimeElapsedPipe;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/services/datatable-work.service.ts":
/*!**********************************************************************!*\
  !*** ./src/app/core/_base/layout/services/datatable-work.service.ts ***!
  \**********************************************************************/
/*! exports provided: DataTableWorkService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataTableWorkService", function() { return DataTableWorkService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");




// const API_DATATABLE_URL = 'api/works'
var API_DATATABLE_URL = 'http://3.95.8.94/example/index.php';
var WORK_URL = 'http://3.95.8.94/example/work_request.php';
var DataTableWorkService = /** @class */ (function () {
    function DataTableWorkService(http) {
        this.http = http;
    }
    DataTableWorkService.prototype.getAllWorks = function () {
        var _this = this;
        return this.http.get(API_DATATABLE_URL, { params: { scoredWorks: 'scoredWorks' } }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (works) {
            works.map(function (dtw) {
                dtw.Tags = _this.convertToArray(dtw.Tags);
            });
            return works;
        }));
    };
    DataTableWorkService.prototype.getIncomingWorks = function () {
        var _this = this;
        return this.http.get(WORK_URL, { params: { incommingWorks: 'any' } }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (works) {
            works.map(function (eachWork) {
                eachWork.Tags = _this.convertToArray(eachWork.Tags);
            });
            return works;
        }));
    };
    DataTableWorkService.prototype.convertToArray = function (str) {
        var ls = Object.assign(str);
        str = ls.split(',');
        return str;
    };
    DataTableWorkService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    DataTableWorkService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], DataTableWorkService);
    return DataTableWorkService;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/services/datatable.service.ts":
/*!*****************************************************************!*\
  !*** ./src/app/core/_base/layout/services/datatable.service.ts ***!
  \*****************************************************************/
/*! exports provided: DataTableService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataTableService", function() { return DataTableService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");

// Angular


var API_DATATABLE_URL = 'api/cars';
var DataTableService = /** @class */ (function () {
    /**
     * Service Constructor
     *
     * @param http: HttpClient
     */
    function DataTableService(http) {
        this.http = http;
    }
    /**
     * Returns data from fake server
     */
    DataTableService.prototype.getAllItems = function () {
        return this.http.get(API_DATATABLE_URL);
    };
    DataTableService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    DataTableService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], DataTableService);
    return DataTableService;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/services/kt-dialog.service.ts":
/*!*****************************************************************!*\
  !*** ./src/app/core/_base/layout/services/kt-dialog.service.ts ***!
  \*****************************************************************/
/*! exports provided: KtDialogService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KtDialogService", function() { return KtDialogService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");

// Angular

// RxJS

var KtDialogService = /** @class */ (function () {
    // Public properties
    function KtDialogService() {
        this.currentState = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](false);
        this.ktDialog = new KTDialog({ 'type': 'loader', 'placement': 'top center', 'message': 'Loading ...' });
    }
    KtDialogService.prototype.show = function () {
        this.currentState.next(true);
        this.ktDialog.show();
    };
    KtDialogService.prototype.hide = function () {
        this.currentState.next(false);
        this.ktDialog.hide();
    };
    KtDialogService.prototype.checkIsShown = function () {
        return this.currentState.value;
    };
    KtDialogService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], KtDialogService);
    return KtDialogService;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/services/layout-config.service.ts":
/*!*********************************************************************!*\
  !*** ./src/app/core/_base/layout/services/layout-config.service.ts ***!
  \*********************************************************************/
/*! exports provided: LayoutConfigService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutConfigService", function() { return LayoutConfigService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var object_path__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! object-path */ "./node_modules/object-path/index.js");
/* harmony import */ var object_path__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(object_path__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_4__);

// Angular

// RxJS

// Object-Path

// Lodash

var LayoutConfigService = /** @class */ (function () {
    /**
     * Servcie constructor
     */
    function LayoutConfigService() {
        // register on config changed event and set default config
        this.onConfigUpdated$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
    }
    /**
     * Save layout config to the local storage
     * @param layoutConfig
     */
    LayoutConfigService.prototype.saveConfig = function (layoutConfig) {
        if (layoutConfig) {
            localStorage.setItem('layoutConfig', JSON.stringify(layoutConfig));
        }
    };
    /**
     * Get layout config from local storage
     */
    LayoutConfigService.prototype.getSavedConfig = function () {
        var config = localStorage.getItem('layoutConfig');
        try {
            return JSON.parse(config);
        }
        catch (e) {
        }
    };
    /**
     * Remove saved layout config and revert back to default
     */
    LayoutConfigService.prototype.resetConfig = function () {
        localStorage.removeItem('layoutConfig');
    };
    /**
     * Get all config or by object path
     * @param path | object path separated by dot
     */
    LayoutConfigService.prototype.getConfig = function (path) {
        // merge default layout config with the saved config from layout storage
        // @todo; known issue; viewing 2 or more demos at the time in different browser's tabs, can cause conflict to the layout config
        this.layoutConfig = this.getSavedConfig();
        if (path) {
            // if path is specified, get the value within object
            return object_path__WEBPACK_IMPORTED_MODULE_3__["get"](this.layoutConfig, path);
        }
        return this.layoutConfig;
    };
    /**
     * Set existing config with a new value
     * @param value
     * @param save
     */
    LayoutConfigService.prototype.setConfig = function (value, save) {
        this.layoutConfig = Object(lodash__WEBPACK_IMPORTED_MODULE_4__["merge"])(this.layoutConfig, value);
        if (save) {
            this.saveConfig(this.layoutConfig);
        }
        // fire off an event that all subscribers will listen
        this.onConfigUpdated$.next(this.layoutConfig);
    };
    /**
     * Get brand logo
     */
    LayoutConfigService.prototype.getLogo = function () {
        var menuAsideLeftSkin = object_path__WEBPACK_IMPORTED_MODULE_3__["get"](this.layoutConfig, 'brand.self.skin');
        // set brand logo
        var logoObject = object_path__WEBPACK_IMPORTED_MODULE_3__["get"](this.layoutConfig, 'self.logo');
        var logo;
        if (typeof logoObject === 'string') {
            logo = logoObject;
        }
        if (typeof logoObject === 'object') {
            logo = object_path__WEBPACK_IMPORTED_MODULE_3__["get"](logoObject, menuAsideLeftSkin + '');
        }
        if (typeof logo === 'undefined') {
            try {
                var logos = object_path__WEBPACK_IMPORTED_MODULE_3__["get"](this.layoutConfig, 'self.logo');
                logo = logos[Object.keys(logos)[0]];
            }
            catch (e) {
            }
        }
        return logo;
    };
    /**
     * Returns sticky logo
     */
    LayoutConfigService.prototype.getStickyLogo = function () {
        var logo = object_path__WEBPACK_IMPORTED_MODULE_3__["get"](this.layoutConfig, 'self.logo.sticky');
        if (typeof logo === 'undefined') {
            logo = this.getLogo();
        }
        return logo + '';
    };
    /**
     * Initialize layout config
     * @param config
     */
    LayoutConfigService.prototype.loadConfigs = function (config) {
        this.layoutConfig = this.getSavedConfig();
        // use saved config as priority, or load new config if demo does not matched
        if (!this.layoutConfig || object_path__WEBPACK_IMPORTED_MODULE_3__["get"](this.layoutConfig, 'demo') !== config.demo) {
            this.layoutConfig = config;
        }
        this.saveConfig(this.layoutConfig);
    };
    /**
     * Reload current layout config to the state of latest saved config
     */
    LayoutConfigService.prototype.reloadConfigs = function () {
        this.layoutConfig = this.getSavedConfig();
        this.onConfigUpdated$.next(this.layoutConfig);
        return this.layoutConfig;
    };
    LayoutConfigService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], LayoutConfigService);
    return LayoutConfigService;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/services/layout-ref.service.ts":
/*!******************************************************************!*\
  !*** ./src/app/core/_base/layout/services/layout-ref.service.ts ***!
  \******************************************************************/
/*! exports provided: LayoutRefService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutRefService", function() { return LayoutRefService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");

// Angular

// RxJS

var LayoutRefService = /** @class */ (function () {
    function LayoutRefService() {
        // Public properties
        this.layoutRefs$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({});
        this.layoutRefs = {};
    }
    /**
     * Add element to Ref
     *
     * @param name: any
     * @param element: any
     */
    LayoutRefService.prototype.addElement = function (name, element) {
        var obj = {};
        obj[name] = element;
        this.layoutRefs = Object.assign({}, this.layoutRefs, obj);
        this.layoutRefs$.next(this.layoutRefs);
    };
    LayoutRefService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], LayoutRefService);
    return LayoutRefService;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/services/menu-aside.service.ts":
/*!******************************************************************!*\
  !*** ./src/app/core/_base/layout/services/menu-aside.service.ts ***!
  \******************************************************************/
/*! exports provided: MenuAsideService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuAsideService", function() { return MenuAsideService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var object_path__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! object-path */ "./node_modules/object-path/index.js");
/* harmony import */ var object_path__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(object_path__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _menu_config_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./menu-config.service */ "./src/app/core/_base/layout/services/menu-config.service.ts");

// Angular

// RxJS

// Object path

// Services

var MenuAsideService = /** @class */ (function () {
    /**
     * Service constructor
     *
     * @param menuConfigService: MenuConfigService
     */
    function MenuAsideService(menuConfigService) {
        this.menuConfigService = menuConfigService;
        // Public properties
        this.menuList$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]([]);
        this.loadMenu();
    }
    /**
     * Load menu list
     */
    MenuAsideService.prototype.loadMenu = function () {
        // get menu list
        var menuItems = object_path__WEBPACK_IMPORTED_MODULE_3__["get"](this.menuConfigService.getMenus(), 'aside.items');
        this.menuList$.next(menuItems);
    };
    MenuAsideService.ctorParameters = function () { return [
        { type: _menu_config_service__WEBPACK_IMPORTED_MODULE_4__["MenuConfigService"] }
    ]; };
    MenuAsideService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_menu_config_service__WEBPACK_IMPORTED_MODULE_4__["MenuConfigService"]])
    ], MenuAsideService);
    return MenuAsideService;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/services/menu-config.service.ts":
/*!*******************************************************************!*\
  !*** ./src/app/core/_base/layout/services/menu-config.service.ts ***!
  \*******************************************************************/
/*! exports provided: MenuConfigService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuConfigService", function() { return MenuConfigService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");

// Angular

// RxJS

var MenuConfigService = /** @class */ (function () {
    /**
     * Service Constructor
     */
    function MenuConfigService() {
        // register on config changed event and set default config
        this.onConfigUpdated$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.onConfigUpdated1$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
    }
    /**
     * Returns the menuConfig
     */
    MenuConfigService.prototype.getMenus = function () {
        return this.menuConfig;
    };
    /**
     * Load config
     *
     * @param config: any
     */
    MenuConfigService.prototype.loadConfigs = function (config) {
        this.menuConfig = config;
        this.onConfigUpdated$.next(this.menuConfig);
    };
    /**
     * Update config
     *
     * @param config: any
     */
    MenuConfigService.prototype.updateConfigs1 = function (config) {
        this.onConfigUpdated1$.next("admin");
    };
    MenuConfigService.prototype.getConfig = function () {
        return this.onConfigUpdated1$.asObservable();
    };
    MenuConfigService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MenuConfigService);
    return MenuConfigService;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/services/menu-horizontal.service.ts":
/*!***********************************************************************!*\
  !*** ./src/app/core/_base/layout/services/menu-horizontal.service.ts ***!
  \***********************************************************************/
/*! exports provided: MenuHorizontalService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuHorizontalService", function() { return MenuHorizontalService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var object_path__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! object-path */ "./node_modules/object-path/index.js");
/* harmony import */ var object_path__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(object_path__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _menu_config_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./menu-config.service */ "./src/app/core/_base/layout/services/menu-config.service.ts");

// Angular

// RxJS

// Object path

// Services

var MenuHorizontalService = /** @class */ (function () {
    /**
     * Service constructor
     *
     * @param menuConfigService: MenuConfigService
     */
    function MenuHorizontalService(menuConfigService) {
        this.menuConfigService = menuConfigService;
        // Public properties
        this.menuList$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]([]);
        this.loadMenu();
    }
    /**
     * Load menu list
     */
    MenuHorizontalService.prototype.loadMenu = function () {
        // get menu list
        var menuItems = object_path__WEBPACK_IMPORTED_MODULE_3__["get"](this.menuConfigService.getMenus(), 'header.items');
        this.menuList$.next(menuItems);
    };
    MenuHorizontalService.ctorParameters = function () { return [
        { type: _menu_config_service__WEBPACK_IMPORTED_MODULE_4__["MenuConfigService"] }
    ]; };
    MenuHorizontalService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_menu_config_service__WEBPACK_IMPORTED_MODULE_4__["MenuConfigService"]])
    ], MenuHorizontalService);
    return MenuHorizontalService;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/services/page-config.service.ts":
/*!*******************************************************************!*\
  !*** ./src/app/core/_base/layout/services/page-config.service.ts ***!
  \*******************************************************************/
/*! exports provided: PageConfigService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageConfigService", function() { return PageConfigService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var object_path__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! object-path */ "./node_modules/object-path/index.js");
/* harmony import */ var object_path__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(object_path__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_5__);

// Angular


// RxJS

// Object-Path

// Lodash

var PageConfigService = /** @class */ (function () {
    /**
     * Service Constructor
     *
     * @param router: Router
     */
    function PageConfigService(router) {
        this.router = router;
        // register on config changed event and set default config
        this.onConfigUpdated$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.onConfigUpdated1$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
    }
    /**
     * Get current page config based on route
     */
    PageConfigService.prototype.getCurrentPageConfig = function (path) {
        var configPath = this.cleanUrl(this.router.url);
        if (path) {
            configPath += '.' + path;
        }
        // get page config by path
        return object_path__WEBPACK_IMPORTED_MODULE_4__["get"](this.pageConfig, configPath);
    };
    /**
     * Set existing config with a new value
     * @param value: any
     * @param sav: boolean?
     */
    PageConfigService.prototype.setConfig = function (value, save) {
        this.pageConfig = Object(lodash__WEBPACK_IMPORTED_MODULE_5__["merge"])(this.pageConfig, value);
        if (save) {
            // not implemented
        }
        // fire off an event that all subscribers will listen
        this.onConfigUpdated$.next(this.pageConfig);
    };
    /**
     * Load confgis
     *
     * @param config: any
     */
    PageConfigService.prototype.loadConfigs = function (config) {
        this.pageConfig = config;
        this.onConfigUpdated$.next(this.pageConfig);
    };
    /**
     * Remove unnecessary params from URL
     * @param url
     */
    PageConfigService.prototype.cleanUrl = function (url) {
        // remove first route (demo name) from url router
        if (new RegExp(/^\/demo/).test(url)) {
            var urls = url.split('/');
            urls.splice(0, 2);
            url = urls.join('/');
        }
        if (url.charAt(0) == '/') {
            url = url.substr(1);
        }
        // we get the page title from config, using url path.
        // we need to remove query from url ?id=1 before we use the path to search in array config.
        var finalUrl = url.replace(/\//g, '.');
        if (finalUrl.indexOf('?') !== -1) {
            finalUrl = finalUrl.substring(0, finalUrl.indexOf('?'));
        }
        return finalUrl;
    };
    /**
     * Update confgis
     *
     * @param config: any
     */
    PageConfigService.prototype.updateConfigs1 = function (config) {
        this.pageConfig1 = config;
        this.onConfigUpdated1$.next(this.pageConfig1);
    };
    PageConfigService.prototype.getConfig = function () {
        return this.onConfigUpdated1$.asObservable();
    };
    PageConfigService.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    PageConfigService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], PageConfigService);
    return PageConfigService;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/services/subheader.service.ts":
/*!*****************************************************************!*\
  !*** ./src/app/core/_base/layout/services/subheader.service.ts ***!
  \*****************************************************************/
/*! exports provided: SubheaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubheaderService", function() { return SubheaderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var object_path__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! object-path */ "./node_modules/object-path/index.js");
/* harmony import */ var object_path__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(object_path__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _page_config_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./page-config.service */ "./src/app/core/_base/layout/services/page-config.service.ts");
/* harmony import */ var _menu_config_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./menu-config.service */ "./src/app/core/_base/layout/services/menu-config.service.ts");

// Angular


// RxJS


// Object-Path

// Services


var SubheaderService = /** @class */ (function () {
    /**
     * Service Constructor
     *
     * @param router: Router
     * @param pageConfigService: PageConfigServie
     * @param menuConfigService: MenuConfigService
     */
    function SubheaderService(router, pageConfigService, menuConfigService) {
        var _this = this;
        this.router = router;
        this.pageConfigService = pageConfigService;
        this.menuConfigService = menuConfigService;
        // Public properties
        this.title$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"]({ title: '', desc: '' });
        this.breadcrumbs$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"]([]);
        this.disabled$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        // Private properties
        this.manualBreadcrumbs = {};
        this.appendingBreadcrumbs = {};
        this.manualTitle = {};
        var initBreadcrumb = function () {
            // get updated title current page config
            _this.pageConfig = _this.pageConfigService.getCurrentPageConfig();
            _this.headerMenus = object_path__WEBPACK_IMPORTED_MODULE_5__["get"](_this.menuConfigService.getMenus(), 'header');
            _this.asideMenus = object_path__WEBPACK_IMPORTED_MODULE_5__["get"](_this.menuConfigService.getMenus(), 'aside');
            // update breadcrumb on initial page load
            _this.updateBreadcrumbs();
            if (object_path__WEBPACK_IMPORTED_MODULE_5__["get"](_this.manualTitle, _this.router.url)) {
                _this.setTitle(_this.manualTitle[_this.router.url]);
            }
            else {
                // get updated page title on every route changed
                _this.title$.next(object_path__WEBPACK_IMPORTED_MODULE_5__["get"](_this.pageConfig, 'page'));
                // subheader enable/disable
                var hideSubheader = object_path__WEBPACK_IMPORTED_MODULE_5__["get"](_this.pageConfig, 'page.subheader');
                _this.disabled$.next(typeof hideSubheader !== 'undefined' && !hideSubheader);
                if (object_path__WEBPACK_IMPORTED_MODULE_5__["get"](_this.manualBreadcrumbs, _this.router.url)) {
                    // breadcrumbs was set manually
                    _this.setBreadcrumbs(_this.manualBreadcrumbs[_this.router.url]);
                }
                else {
                    // get updated breadcrumbs on every route changed
                    _this.updateBreadcrumbs();
                    // breadcrumbs was appended before, reuse it for this page
                    if (object_path__WEBPACK_IMPORTED_MODULE_5__["get"](_this.appendingBreadcrumbs, _this.router.url)) {
                        _this.appendBreadcrumbs(_this.appendingBreadcrumbs[_this.router.url]);
                    }
                }
            }
        };
        initBreadcrumb();
        // subscribe to router events
        this.router.events
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["filter"])(function (event) { return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"]; }))
            .subscribe(initBreadcrumb);
    }
    /**
     * Update breadCrumbs
     */
    SubheaderService.prototype.updateBreadcrumbs = function () {
        // get breadcrumbs from header menu
        var breadcrumbs = this.getBreadcrumbs(this.headerMenus);
        // if breadcrumbs empty from header menu
        if (breadcrumbs.length === 0) {
            // get breadcrumbs from aside menu
            breadcrumbs = this.getBreadcrumbs(this.asideMenus);
        }
        if (
        // if breadcrumb has only 1 item
        breadcrumbs.length === 1 &&
            // and breadcrumb title is same as current page title
            breadcrumbs[0].title.indexOf(object_path__WEBPACK_IMPORTED_MODULE_5__["get"](this.pageConfig, 'page.title')) !== -1) {
            // no need to display on frontend
            breadcrumbs = [];
        }
        this.breadcrumbs$.next(breadcrumbs);
    };
    /**
     * Manually set full breadcrumb paths
     */
    SubheaderService.prototype.setBreadcrumbs = function (breadcrumbs) {
        this.manualBreadcrumbs[this.router.url] = breadcrumbs;
        this.breadcrumbs$.next(breadcrumbs);
    };
    /**
     * Append breadcrumb to the last existing breadcrumbs
     * @param breadcrumbs
     */
    SubheaderService.prototype.appendBreadcrumbs = function (breadcrumbs) {
        this.appendingBreadcrumbs[this.router.url] = breadcrumbs;
        var prev = this.breadcrumbs$.getValue();
        this.breadcrumbs$.next(prev.concat(breadcrumbs));
    };
    /**
     * Get breadcrumbs from menu items
     * @param menus
     */
    SubheaderService.prototype.getBreadcrumbs = function (menus) {
        var url = this.pageConfigService.cleanUrl(this.router.url);
        url = url.replace(new RegExp(/\./, 'g'), '/');
        var breadcrumbs = [];
        var menuPath = this.getPath(menus, url) || [];
        menuPath.forEach(function (key) {
            menus = menus[key];
            if (typeof menus !== 'undefined' && menus.title) {
                breadcrumbs.push(menus);
            }
        });
        return breadcrumbs;
    };
    /**
     * Set title
     *
     * @param title: string
     */
    SubheaderService.prototype.setTitle = function (title) {
        this.manualTitle[this.router.url] = title;
        this.title$.next({ title: title });
    };
    /**
     * Get object path by value
     * @param obj
     * @param value
     */
    SubheaderService.prototype.getPath = function (obj, value) {
        if (typeof obj !== 'object') {
            return;
        }
        var path = [];
        var found = false;
        var search = function (haystack) {
            // tslint:disable-next-line:forin
            for (var key in haystack) {
                path.push(key);
                if (haystack[key] === value) {
                    found = true;
                    break;
                }
                if (typeof haystack[key] === 'object') {
                    search(haystack[key]);
                    if (found) {
                        break;
                    }
                }
                path.pop();
            }
        };
        search(obj);
        return path;
    };
    SubheaderService.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _page_config_service__WEBPACK_IMPORTED_MODULE_6__["PageConfigService"] },
        { type: _menu_config_service__WEBPACK_IMPORTED_MODULE_7__["MenuConfigService"] }
    ]; };
    SubheaderService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _page_config_service__WEBPACK_IMPORTED_MODULE_6__["PageConfigService"],
            _menu_config_service__WEBPACK_IMPORTED_MODULE_7__["MenuConfigService"]])
    ], SubheaderService);
    return SubheaderService;
}());



/***/ }),

/***/ "./src/app/core/_base/layout/services/translation.service.ts":
/*!*******************************************************************!*\
  !*** ./src/app/core/_base/layout/services/translation.service.ts ***!
  \*******************************************************************/
/*! exports provided: TranslationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TranslationService", function() { return TranslationService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");

// Angular

// Tranlsation

var TranslationService = /** @class */ (function () {
    /**
     * Service Constructor
     *
     * @param translate: TranslateService
     */
    function TranslationService(translate) {
        this.translate = translate;
        // Private properties
        this.langIds = [];
        // add new langIds to the list
        this.translate.addLangs(['en']);
        // this language will be used as a fallback when a translation isn't found in the current language
        this.translate.setDefaultLang('en');
    }
    /**
     * Load Translation
     *
     * @param args: Locale[]
     */
    TranslationService.prototype.loadTranslations = function () {
        var _this = this;
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        var locales = args.slice();
        locales.forEach(function (locale) {
            // use setTranslation() with the third argument set to true
            // to append translations instead of replacing them
            _this.translate.setTranslation(locale.lang, locale.data, true);
            _this.langIds.push(locale.lang);
        });
        // add new languages to the list
        this.translate.addLangs(this.langIds);
    };
    /**
     * Setup language
     *
     * @param lang: any
     */
    TranslationService.prototype.setLanguage = function (lang) {
        if (lang) {
            this.translate.use(this.translate.getDefaultLang());
            this.translate.use(lang);
            localStorage.setItem('language', lang);
        }
    };
    /**
     * Returns selected language
     */
    TranslationService.prototype.getSelectedLanguage = function () {
        return localStorage.getItem('language') || this.translate.getDefaultLang();
    };
    TranslationService.ctorParameters = function () { return [
        { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"] }
    ]; };
    TranslationService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"]])
    ], TranslationService);
    return TranslationService;
}());



/***/ }),

/***/ "./src/app/core/_config/i18n/ch.ts":
/*!*****************************************!*\
  !*** ./src/app/core/_config/i18n/ch.ts ***!
  \*****************************************/
/*! exports provided: locale */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "locale", function() { return locale; });
// China
var locale = {
    lang: 'ch',
    data: {
        TRANSLATOR: {
            SELECT: '选择你的语言',
        },
        MENU: {
            NEW: '新',
            ACTIONS: '行动',
            CREATE_POST: '创建新帖子',
            PAGES: 'Pages',
            FEATURES: '特征',
            APPS: '应用',
            DASHBOARD: '仪表板',
        },
        AUTH: {
            GENERAL: {
                OR: '要么',
                SUBMIT_BUTTON: '提交',
                NO_ACCOUNT: '没有账号？',
                SIGNUP_BUTTON: '注册',
                FORGOT_BUTTON: '忘记密码',
                BACK_BUTTON: '背部',
                PRIVACY: '隐私',
                LEGAL: '法律',
                CONTACT: '联系',
            },
            LOGIN: {
                TITLE: '创建帐号',
                BUTTON: '签到',
            },
            FORGOT: {
                TITLE: 'Forgotten Password?',
                DESC: 'Enter your email to reset your password',
                SUCCESS: 'Your account has been successfully reset.'
            },
            REGISTER: {
                TITLE: 'Sign Up',
                DESC: 'Enter your details to create your account',
                SUCCESS: 'Your account has been successfuly registered.'
            },
            INPUT: {
                EMAIL: 'Email',
                FULLNAME: 'Fullname',
                PASSWORD: 'Password',
                CONFIRM_PASSWORD: 'Confirm Password',
                USERNAME: '用戶名'
            },
            VALIDATION: {
                INVALID: '{{name}} is not valid',
                REQUIRED: '{{name}} is required',
                MIN_LENGTH: '{{name}} minimum length is {{min}}',
                AGREEMENT_REQUIRED: 'Accepting terms & conditions are required',
                NOT_FOUND: 'The requested {{name}} is not found',
                INVALID_LOGIN: 'The login detail is incorrect',
                REQUIRED_FIELD: 'Required field',
                MIN_LENGTH_FIELD: 'Minimum field length:',
                MAX_LENGTH_FIELD: 'Maximum field length:',
                INVALID_FIELD: 'Field is not valid',
            }
        },
        ECOMMERCE: {
            COMMON: {
                SELECTED_RECORDS_COUNT: 'Selected records count: ',
                ALL: 'All',
                SUSPENDED: 'Suspended',
                ACTIVE: 'Active',
                FILTER: 'Filter',
                BY_STATUS: 'by Status',
                BY_TYPE: 'by Type',
                BUSINESS: 'Business',
                INDIVIDUAL: 'Individual',
                SEARCH: 'Search',
                IN_ALL_FIELDS: 'in all fields'
            },
            ECOMMERCE: 'eCommerce',
            CUSTOMERS: {
                CUSTOMERS: '顾客',
                CUSTOMERS_LIST: '客户名单',
                NEW_CUSTOMER: 'New Customer',
                DELETE_CUSTOMER_SIMPLE: {
                    TITLE: 'Customer Delete',
                    DESCRIPTION: 'Are you sure to permanently delete this customer?',
                    WAIT_DESCRIPTION: 'Customer is deleting...',
                    MESSAGE: 'Customer has been deleted'
                },
                DELETE_CUSTOMER_MULTY: {
                    TITLE: 'Customers Delete',
                    DESCRIPTION: 'Are you sure to permanently delete selected customers?',
                    WAIT_DESCRIPTION: 'Customers are deleting...',
                    MESSAGE: 'Selected customers have been deleted'
                },
                UPDATE_STATUS: {
                    TITLE: 'Status has been updated for selected customers',
                    MESSAGE: 'Selected customers status have successfully been updated'
                },
                EDIT: {
                    UPDATE_MESSAGE: 'Customer has been updated',
                    ADD_MESSAGE: 'Customer has been created'
                }
            }
        }
    }
};


/***/ }),

/***/ "./src/app/core/_config/i18n/de.ts":
/*!*****************************************!*\
  !*** ./src/app/core/_config/i18n/de.ts ***!
  \*****************************************/
/*! exports provided: locale */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "locale", function() { return locale; });
// Germany
var locale = {
    lang: 'de',
    data: {
        TRANSLATOR: {
            SELECT: 'Wähle deine Sprache',
        },
        MENU: {
            NEW: 'Neu',
            ACTIONS: 'Aktionen',
            CREATE_POST: 'Erstellen Sie einen neuen Beitrag',
            PAGES: 'Pages',
            FEATURES: 'Eigenschaften',
            APPS: 'Apps',
            DASHBOARD: 'Instrumententafel'
        },
        AUTH: {
            GENERAL: {
                OR: 'Oder',
                SUBMIT_BUTTON: 'einreichen',
                NO_ACCOUNT: 'Hast du kein Konto?',
                SIGNUP_BUTTON: 'Anmelden',
                FORGOT_BUTTON: 'Passwort vergessen',
                BACK_BUTTON: 'Zurück',
                PRIVACY: 'Privatsphäre',
                LEGAL: 'Legal',
                CONTACT: 'Kontakt',
            },
            LOGIN: {
                TITLE: 'Create Account',
                BUTTON: 'Sign In',
            },
            FORGOT: {
                TITLE: 'Forgotten Password?',
                DESC: 'Enter your email to reset your password',
                SUCCESS: 'Your account has been successfully reset.'
            },
            REGISTER: {
                TITLE: 'Sign Up',
                DESC: 'Enter your details to create your account',
                SUCCESS: 'Your account has been successfuly registered.'
            },
            INPUT: {
                EMAIL: 'Email',
                FULLNAME: 'Fullname',
                PASSWORD: 'Password',
                CONFIRM_PASSWORD: 'Confirm Password',
                USERNAME: 'Nutzername'
            },
            VALIDATION: {
                INVALID: '{{name}} is not valid',
                REQUIRED: '{{name}} is required',
                MIN_LENGTH: '{{name}} minimum length is {{min}}',
                AGREEMENT_REQUIRED: 'Accepting terms & conditions are required',
                NOT_FOUND: 'The requested {{name}} is not found',
                INVALID_LOGIN: 'The login detail is incorrect',
                REQUIRED_FIELD: 'Required field',
                MIN_LENGTH_FIELD: 'Minimum field length:',
                MAX_LENGTH_FIELD: 'Maximum field length:',
                INVALID_FIELD: 'Field is not valid',
            }
        },
        ECOMMERCE: {
            COMMON: {
                SELECTED_RECORDS_COUNT: 'Selected records count: ',
                ALL: 'All',
                SUSPENDED: 'Suspended',
                ACTIVE: 'Active',
                FILTER: 'Filter',
                BY_STATUS: 'by Status',
                BY_TYPE: 'by Type',
                BUSINESS: 'Business',
                INDIVIDUAL: 'Individual',
                SEARCH: 'Search',
                IN_ALL_FIELDS: 'in all fields'
            },
            ECOMMERCE: 'eCommerce',
            CUSTOMERS: {
                CUSTOMERS: 'Customers',
                CUSTOMERS_LIST: 'Customers list',
                NEW_CUSTOMER: 'New Customer',
                DELETE_CUSTOMER_SIMPLE: {
                    TITLE: 'Customer Delete',
                    DESCRIPTION: 'Are you sure to permanently delete this customer?',
                    WAIT_DESCRIPTION: 'Customer is deleting...',
                    MESSAGE: 'Customer has been deleted'
                },
                DELETE_CUSTOMER_MULTY: {
                    TITLE: 'Customers Delete',
                    DESCRIPTION: 'Are you sure to permanently delete selected customers?',
                    WAIT_DESCRIPTION: 'Customers are deleting...',
                    MESSAGE: 'Selected customers have been deleted'
                },
                UPDATE_STATUS: {
                    TITLE: 'Status has been updated for selected customers',
                    MESSAGE: 'Selected customers status have successfully been updated'
                },
                EDIT: {
                    UPDATE_MESSAGE: 'Customer has been updated',
                    ADD_MESSAGE: 'Customer has been created'
                }
            }
        }
    }
};


/***/ }),

/***/ "./src/app/core/_config/i18n/en.ts":
/*!*****************************************!*\
  !*** ./src/app/core/_config/i18n/en.ts ***!
  \*****************************************/
/*! exports provided: locale */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "locale", function() { return locale; });
// USA
var locale = {
    lang: 'en',
    data: {
        TRANSLATOR: {
            SELECT: 'Select your language',
        },
        MENU: {
            NEW: 'new',
            ACTIONS: 'Actions',
            CREATE_POST: 'Create New Post',
            PAGES: 'Pages',
            FEATURES: 'Features',
            APPS: 'Apps',
            DASHBOARD: 'Dashboard',
        },
        AUTH: {
            GENERAL: {
                OR: 'Or',
                SUBMIT_BUTTON: 'Submit',
                NO_ACCOUNT: 'Don\'t have an account?',
                SIGNUP_BUTTON: 'Sign Up',
                FORGOT_BUTTON: 'Forgot Password',
                BACK_BUTTON: 'Back',
                PRIVACY: 'Privacy',
                LEGAL: 'Legal',
                CONTACT: 'Contact',
            },
            LOGIN: {
                TITLE: 'Login Account',
                BUTTON: 'Sign In',
            },
            FORGOT: {
                TITLE: 'Forgotten Password?',
                DESC: 'Enter your email to reset your password',
                SUCCESS: 'Your account has been successfully reset.'
            },
            REGISTER: {
                TITLE: 'Sign Up',
                DESC: 'Enter your details to create your account',
                SUCCESS: 'Your account has been successfuly registered.'
            },
            INPUT: {
                EMAIL: 'Email',
                FULLNAME: 'Fullname',
                PASSWORD: 'Password',
                CONFIRM_PASSWORD: 'Confirm Password',
                USERNAME: 'Username'
            },
            VALIDATION: {
                INVALID: '{{name}} is not valid',
                REQUIRED: '{{name}} is required',
                MIN_LENGTH: '{{name}} minimum length is {{min}}',
                AGREEMENT_REQUIRED: 'Accepting terms & conditions are required',
                NOT_FOUND: 'The requested {{name}} is not found',
                INVALID_LOGIN: 'The login detail is incorrect',
                REQUIRED_FIELD: 'Required field',
                MIN_LENGTH_FIELD: 'Minimum field length:',
                MAX_LENGTH_FIELD: 'Maximum field length:',
                INVALID_FIELD: 'Field is not valid',
            }
        },
        ECOMMERCE: {
            COMMON: {
                SELECTED_RECORDS_COUNT: 'Selected records count: ',
                ALL: 'All',
                SUSPENDED: 'Suspended',
                ACTIVE: 'Active',
                FILTER: 'Filter',
                BY_STATUS: 'by Status',
                BY_TYPE: 'by Type',
                BUSINESS: 'Business',
                INDIVIDUAL: 'Individual',
                SEARCH: 'Search',
                IN_ALL_FIELDS: 'in all fields'
            },
            ECOMMERCE: 'eCommerce',
            CUSTOMERS: {
                CUSTOMERS: 'Customers',
                CUSTOMERS_LIST: 'Customers list',
                NEW_CUSTOMER: 'New Customer',
                DELETE_CUSTOMER_SIMPLE: {
                    TITLE: 'Customer Delete',
                    DESCRIPTION: 'Are you sure to permanently delete this customer?',
                    WAIT_DESCRIPTION: 'Customer is deleting...',
                    MESSAGE: 'Customer has been deleted'
                },
                DELETE_CUSTOMER_MULTY: {
                    TITLE: 'Customers Delete',
                    DESCRIPTION: 'Are you sure to permanently delete selected customers?',
                    WAIT_DESCRIPTION: 'Customers are deleting...',
                    MESSAGE: 'Selected customers have been deleted'
                },
                UPDATE_STATUS: {
                    TITLE: 'Status has been updated for selected customers',
                    MESSAGE: 'Selected customers status have successfully been updated'
                },
                EDIT: {
                    UPDATE_MESSAGE: 'Customer has been updated',
                    ADD_MESSAGE: 'Customer has been created'
                }
            }
        }
    }
};


/***/ }),

/***/ "./src/app/core/_config/i18n/es.ts":
/*!*****************************************!*\
  !*** ./src/app/core/_config/i18n/es.ts ***!
  \*****************************************/
/*! exports provided: locale */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "locale", function() { return locale; });
// Spain
var locale = {
    lang: 'es',
    data: {
        TRANSLATOR: {
            SELECT: 'Elige tu idioma',
        },
        MENU: {
            NEW: 'nuevo',
            ACTIONS: 'Comportamiento',
            CREATE_POST: 'Crear nueva publicación',
            PAGES: 'Pages',
            FEATURES: 'Caracteristicas',
            APPS: 'Aplicaciones',
            DASHBOARD: 'Tablero'
        },
        AUTH: {
            GENERAL: {
                OR: 'O',
                SUBMIT_BUTTON: 'Enviar',
                NO_ACCOUNT: 'No tienes una cuenta?',
                SIGNUP_BUTTON: 'Regístrate',
                FORGOT_BUTTON: 'Se te olvidó tu contraseña',
                BACK_BUTTON: 'Espalda',
                PRIVACY: 'Intimidad',
                LEGAL: 'Legal',
                CONTACT: 'Contacto',
            },
            LOGIN: {
                TITLE: 'Crear una cuenta',
                BUTTON: 'Registrarse',
            },
            FORGOT: {
                TITLE: 'Contraseña olvidada?',
                DESC: 'Ingrese su correo electrónico para restablecer su contraseña',
                SUCCESS: 'Your account has been successfully reset.'
            },
            REGISTER: {
                TITLE: 'Sign Up',
                DESC: 'Enter your details to create your account',
                SUCCESS: 'Your account has been successfuly registered.'
            },
            INPUT: {
                EMAIL: 'Email',
                FULLNAME: 'Fullname',
                PASSWORD: 'Password',
                CONFIRM_PASSWORD: 'Confirm Password',
                USERNAME: 'Usuario'
            },
            VALIDATION: {
                INVALID: '{{name}} is not valid',
                REQUIRED: '{{name}} is required',
                MIN_LENGTH: '{{name}} minimum length is {{min}}',
                AGREEMENT_REQUIRED: 'Accepting terms & conditions are required',
                NOT_FOUND: 'The requested {{name}} is not found',
                INVALID_LOGIN: 'The login detail is incorrect',
                REQUIRED_FIELD: 'Required field',
                MIN_LENGTH_FIELD: 'Minimum field length:',
                MAX_LENGTH_FIELD: 'Maximum field length:',
                INVALID_FIELD: 'Field is not valid',
            }
        },
        ECOMMERCE: {
            COMMON: {
                SELECTED_RECORDS_COUNT: 'Selected records count: ',
                ALL: 'All',
                SUSPENDED: 'Suspended',
                ACTIVE: 'Active',
                FILTER: 'Filter',
                BY_STATUS: 'by Status',
                BY_TYPE: 'by Type',
                BUSINESS: 'Business',
                INDIVIDUAL: 'Individual',
                SEARCH: 'Search',
                IN_ALL_FIELDS: 'in all fields'
            },
            ECOMMERCE: 'eCommerce',
            CUSTOMERS: {
                CUSTOMERS: 'Customers',
                CUSTOMERS_LIST: 'Customers list',
                NEW_CUSTOMER: 'New Customer',
                DELETE_CUSTOMER_SIMPLE: {
                    TITLE: 'Customer Delete',
                    DESCRIPTION: 'Are you sure to permanently delete this customer?',
                    WAIT_DESCRIPTION: 'Customer is deleting...',
                    MESSAGE: 'Customer has been deleted'
                },
                DELETE_CUSTOMER_MULTY: {
                    TITLE: 'Customers Delete',
                    DESCRIPTION: 'Are you sure to permanently delete selected customers?',
                    WAIT_DESCRIPTION: 'Customers are deleting...',
                    MESSAGE: 'Selected customers have been deleted'
                },
                UPDATE_STATUS: {
                    TITLE: 'Status has been updated for selected customers',
                    MESSAGE: 'Selected customers status have successfully been updated'
                },
                EDIT: {
                    UPDATE_MESSAGE: 'Customer has been updated',
                    ADD_MESSAGE: 'Customer has been created'
                }
            }
        }
    }
};


/***/ }),

/***/ "./src/app/core/_config/i18n/fr.ts":
/*!*****************************************!*\
  !*** ./src/app/core/_config/i18n/fr.ts ***!
  \*****************************************/
/*! exports provided: locale */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "locale", function() { return locale; });
// France
var locale = {
    lang: 'fr',
    data: {
        TRANSLATOR: {
            SELECT: 'choisissez votre langue',
        },
        MENU: {
            NEW: 'Nouveau',
            ACTIONS: 'Actes',
            CREATE_POST: 'Créer un nouveau Post',
            PAGES: 'Pages',
            FEATURES: 'Fonctionnalités',
            APPS: 'Applications',
            DASHBOARD: 'Tableau de Bord',
        },
        AUTH: {
            GENERAL: {
                OR: 'Ou',
                SUBMIT_BUTTON: 'Soumettre',
                NO_ACCOUNT: 'Ne pas avoir de compte?',
                SIGNUP_BUTTON: 'Registre',
                FORGOT_BUTTON: 'Mot de passe oublié',
                BACK_BUTTON: 'Back',
                PRIVACY: 'Privacy',
                LEGAL: 'Legal',
                CONTACT: 'Contact',
            },
            LOGIN: {
                TITLE: 'Créer un compte',
                BUTTON: 'Sign In',
            },
            FORGOT: {
                TITLE: 'Forgotten Password?',
                DESC: 'Enter your email to reset your password',
                SUCCESS: 'Your account has been successfully reset.'
            },
            REGISTER: {
                TITLE: 'Sign Up',
                DESC: 'Enter your details to create your account',
                SUCCESS: 'Your account has been successfuly registered.'
            },
            INPUT: {
                EMAIL: 'Email',
                FULLNAME: 'Fullname',
                PASSWORD: 'Mot de passe',
                CONFIRM_PASSWORD: 'Confirm Password',
                USERNAME: 'Nom d\'utilisateur'
            },
            VALIDATION: {
                INVALID: '{{name}} n\'est pas valide',
                REQUIRED: '{{name}} est requis',
                MIN_LENGTH: '{{name}} minimum length is {{min}}',
                AGREEMENT_REQUIRED: 'Accepting terms & conditions are required',
                NOT_FOUND: 'The requested {{name}} is not found',
                INVALID_LOGIN: 'The login detail is incorrect',
                REQUIRED_FIELD: 'Required field',
                MIN_LENGTH_FIELD: 'Minimum field length:',
                MAX_LENGTH_FIELD: 'Maximum field length:',
                INVALID_FIELD: 'Field is not valid',
            }
        },
        ECOMMERCE: {
            COMMON: {
                SELECTED_RECORDS_COUNT: 'Nombre d\'enregistrements sélectionnés: ',
                ALL: 'All',
                SUSPENDED: 'Suspended',
                ACTIVE: 'Active',
                FILTER: 'Filter',
                BY_STATUS: 'by Status',
                BY_TYPE: 'by Type',
                BUSINESS: 'Business',
                INDIVIDUAL: 'Individual',
                SEARCH: 'Search',
                IN_ALL_FIELDS: 'in all fields'
            },
            ECOMMERCE: 'éCommerce',
            CUSTOMERS: {
                CUSTOMERS: 'Les clients',
                CUSTOMERS_LIST: 'Liste des clients',
                NEW_CUSTOMER: 'Nouveau client',
                DELETE_CUSTOMER_SIMPLE: {
                    TITLE: 'Suppression du client',
                    DESCRIPTION: 'Êtes-vous sûr de supprimer définitivement ce client?',
                    WAIT_DESCRIPTION: 'Le client est en train de supprimer ...',
                    MESSAGE: 'Le client a été supprimé'
                },
                DELETE_CUSTOMER_MULTY: {
                    TITLE: 'Supprimer les clients',
                    DESCRIPTION: 'Êtes-vous sûr de supprimer définitivement les clients sélectionnés?',
                    WAIT_DESCRIPTION: 'Les clients suppriment ...',
                    MESSAGE: 'Les clients sélectionnés ont été supprimés'
                },
                UPDATE_STATUS: {
                    TITLE: 'Le statut a été mis à jour pour les clients sélectionnés',
                    MESSAGE: 'Le statut des clients sélectionnés a été mis à jour avec succès'
                },
                EDIT: {
                    UPDATE_MESSAGE: 'Le client a été mis à jour',
                    ADD_MESSAGE: 'Le client a été créé'
                }
            }
        }
    }
};


/***/ }),

/***/ "./src/app/core/_config/i18n/jp.ts":
/*!*****************************************!*\
  !*** ./src/app/core/_config/i18n/jp.ts ***!
  \*****************************************/
/*! exports provided: locale */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "locale", function() { return locale; });
// Japan
var locale = {
    lang: 'jp',
    data: {
        TRANSLATOR: {
            SELECT: 'あなたが使う言語を選んでください',
        },
        MENU: {
            NEW: '新しい',
            ACTIONS: '行動',
            CREATE_POST: '新しい投稿を作成',
            PAGES: 'Pages',
            FEATURES: '特徴',
            APPS: 'アプリ',
            DASHBOARD: 'ダッシュボード',
        },
        AUTH: {
            GENERAL: {
                OR: 'または',
                SUBMIT_BUTTON: '提出する',
                NO_ACCOUNT: 'アカウントを持っていない？',
                SIGNUP_BUTTON: 'サインアップ',
                FORGOT_BUTTON: 'パスワードをお忘れですか',
                BACK_BUTTON: 'バック',
                PRIVACY: 'プライバシー',
                LEGAL: '法的',
                CONTACT: '接触',
            },
            LOGIN: {
                TITLE: 'Create Account',
                BUTTON: 'Sign In',
            },
            FORGOT: {
                TITLE: 'Forgotten Password?',
                DESC: 'Enter your email to reset your password',
                SUCCESS: 'Your account has been successfully reset.'
            },
            REGISTER: {
                TITLE: 'Sign Up',
                DESC: 'Enter your details to create your account',
                SUCCESS: 'Your account has been successfuly registered.'
            },
            INPUT: {
                EMAIL: 'Email',
                FULLNAME: 'Fullname',
                PASSWORD: 'Password',
                CONFIRM_PASSWORD: 'Confirm Password',
                USERNAME: 'ユーザー名'
            },
            VALIDATION: {
                INVALID: '{{name}} is not valid',
                REQUIRED: '{{name}} is required',
                MIN_LENGTH: '{{name}} minimum length is {{min}}',
                AGREEMENT_REQUIRED: 'Accepting terms & conditions are required',
                NOT_FOUND: 'The requested {{name}} is not found',
                INVALID_LOGIN: 'The login detail is incorrect',
                REQUIRED_FIELD: 'Required field',
                MIN_LENGTH_FIELD: 'Minimum field length:',
                MAX_LENGTH_FIELD: 'Maximum field length:',
                INVALID_FIELD: 'Field is not valid',
            }
        },
        ECOMMERCE: {
            COMMON: {
                SELECTED_RECORDS_COUNT: 'Selected records count: ',
                ALL: 'All',
                SUSPENDED: 'Suspended',
                ACTIVE: 'Active',
                FILTER: 'Filter',
                BY_STATUS: 'by Status',
                BY_TYPE: 'by Type',
                BUSINESS: 'Business',
                INDIVIDUAL: 'Individual',
                SEARCH: 'Search',
                IN_ALL_FIELDS: 'in all fields'
            },
            ECOMMERCE: 'eCommerce',
            CUSTOMERS: {
                CUSTOMERS: 'Customers',
                CUSTOMERS_LIST: 'Customers list',
                NEW_CUSTOMER: 'New Customer',
                DELETE_CUSTOMER_SIMPLE: {
                    TITLE: 'Customer Delete',
                    DESCRIPTION: 'Are you sure to permanently delete this customer?',
                    WAIT_DESCRIPTION: 'Customer is deleting...',
                    MESSAGE: 'Customer has been deleted'
                },
                DELETE_CUSTOMER_MULTY: {
                    TITLE: 'Customers Delete',
                    DESCRIPTION: 'Are you sure to permanently delete selected customers?',
                    WAIT_DESCRIPTION: 'Customers are deleting...',
                    MESSAGE: 'Selected customers have been deleted'
                },
                UPDATE_STATUS: {
                    TITLE: 'Status has been updated for selected customers',
                    MESSAGE: 'Selected customers status have successfully been updated'
                },
                EDIT: {
                    UPDATE_MESSAGE: 'Customer has been updated',
                    ADD_MESSAGE: 'Customer has been created'
                }
            }
        }
    }
};


/***/ }),

/***/ "./src/app/core/_config/layout.config.ts":
/*!***********************************************!*\
  !*** ./src/app/core/_config/layout.config.ts ***!
  \***********************************************/
/*! exports provided: LayoutConfig */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutConfig", function() { return LayoutConfig; });
var LayoutConfig = /** @class */ (function () {
    function LayoutConfig() {
        this.defaults = {
            "demo": "demo1",
            "self": {
                "layout": "fluid",
                "body": {
                    "background-image": "./assets/media/misc/bg-1.jpg"
                },
                "logo": {
                    "dark": "./assets/media/logos/logo-light.png",
                    "light": "./assets/media/logos/logo-dark.png",
                    "brand": "./assets/media/logos/logo-light.png",
                    "green": "./assets/media/logos/logo-light.png"
                }
            },
            "loader": {
                "enabled": true,
                "type": "",
                "logo": "./assets/media/logos/logo-mini-md.png",
                "message": "Please wait..."
            },
            "colors": {
                "state": {
                    "brand": "#5d78ff",
                    "dark": "#282a3c",
                    "light": "#ffffff",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": [
                        "#c5cbe3",
                        "#a1a8c3",
                        "#3d4465",
                        "#3e4466"
                    ],
                    "shape": [
                        "#f0f3ff",
                        "#d9dffa",
                        "#afb4d4",
                        "#646c9a"
                    ]
                }
            },
            "header": {
                "self": {
                    "skin": "light",
                    "fixed": {
                        "desktop": true,
                        "mobile": true
                    }
                },
                "menu": {
                    "self": {
                        "display": true,
                        "layout": "default",
                        "root-arrow": false
                    },
                    "desktop": {
                        "arrow": true,
                        "toggle": "click",
                        "submenu": {
                            "skin": "light",
                            "arrow": true
                        }
                    },
                    "mobile": {
                        "submenu": {
                            "skin": "dark",
                            "accordion": true
                        }
                    }
                }
            },
            "subheader": {
                "display": true,
                "layout": "subheader-v1",
                "fixed": true,
                "width": "fluid",
                "style": "solid"
            },
            "content": {
                "width": "fluid"
            },
            "brand": {
                "self": {
                    "skin": "dark"
                }
            },
            "aside": {
                "self": {
                    "skin": "dark",
                    "display": true,
                    "fixed": true,
                    "minimize": {
                        "toggle": true,
                        "default": false
                    }
                },
                "footer": {
                    "self": {
                        "display": true
                    }
                },
                "menu": {
                    "dropdown": false,
                    "scroll": false,
                    "submenu": {
                        "accordion": true,
                        "dropdown": {
                            "arrow": true,
                            "hover-timeout": 500
                        }
                    }
                }
            },
            "footer": {
                "self": {
                    "width": "fluid"
                }
            }
        };
    }
    Object.defineProperty(LayoutConfig.prototype, "configs", {
        /**
         * Good place for getting the remote config
         */
        get: function () {
            return this.defaults;
        },
        enumerable: true,
        configurable: true
    });
    return LayoutConfig;
}());



/***/ }),

/***/ "./src/app/core/admin/_services/admin.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/core/admin/_services/admin.service.ts ***!
  \*******************************************************/
/*! exports provided: AdminService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminService", function() { return AdminService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");






// const ADMIN_REQUEST = 'http://3.95.8.94/example/admin_request.php';
var ADMIN_REQUEST = _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].baseUrl + 'admin_request.php';
// const url = 'http://3.95.8.94/example/groupOfReviewers.php';
var url = _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].baseUrl + 'groupOfReviewers.php';
// const scorecardsUrls = 'http://3.95.8.94/example/scorecard.php?getReviewersScorecard=getReviewersScorecard';
var scorecardsUrls = _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].baseUrl + 'scorecard.php?getReviewersScorecard=getReviewersScorecard';
var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/x-www-form-urlencoded' });
var AdminService = /** @class */ (function () {
    function AdminService(http) {
        this.http = http;
    }
    AdminService.prototype.submitPreReview = function (preReview) {
        var body = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]()
            .set("adminReview", '1')
            .set("AdminID", preReview.AdminID.toString())
            .set("WorkID", preReview.WorkID.toString())
            .set("DateReviewed", preReview.DateReviewed)
            .set("Decision", preReview.Decision)
            .set("RejectNote", preReview.RejectNote);
        return this.http.post(ADMIN_REQUEST, body, { headers: headers });
    };
    AdminService.prototype.getAllAssignedWorks = function () {
        var _this = this;
        return this.http.get(ADMIN_REQUEST, { params: { getAssignedWorks: 'getAssignedWorks' } })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (works) {
            return _this.groupReviewers(works);
        }));
    };
    AdminService.prototype.getReviewersForAssignment = function (workID) {
        // console.log('aaaa', workID);
        if (workID > 0) {
            return this.http.get(ADMIN_REQUEST, {
                params: {
                    reviewersToAssign: 'reviewersToAssign',
                    workID: workID.toString()
                }
            })
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (reviewers) {
                return reviewers;
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) { return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err); }));
        }
        else {
            return null;
        }
    };
    AdminService.prototype.deactivateUser = function (_user) {
        // console.log(_user);
        var body = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]()
            .set("deactivateUser", 'deactivateUser')
            .set("id", _user.id.toString())
            .set("activeStatus", _user.isActive.toString())
            .set("roleId", _user.roleId.toString());
        return this.http.post(ADMIN_REQUEST, body, { headers: headers });
    };
    AdminService.prototype.assignReviewer = function (assignment) {
        var body = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]()
            .set("assignReviewer", '1')
            .set("adminID", assignment.adminID.toString())
            .set("reviewerID", assignment.reviewerID.toString())
            .set("workID", assignment.workID.toString())
            .set("dueDate", assignment.dueDate.toString())
            .set("dateAssigned", assignment.dateAssigned.toString());
        return this.http.post(ADMIN_REQUEST, body, { headers: headers });
    };
    AdminService.prototype.assignGroupReviewers = function (assignment) {
        return this.http.post(url, assignment, { headers: headers });
    };
    AdminService.prototype.deactivateUserFromAssignment = function (reviewerID, workID) {
        var body = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]()
            .set("deactivateFromAssignment", '1')
            .set("ReviewerID", reviewerID.toString())
            .set("WorkID", workID.toString());
        return this.http.post(ADMIN_REQUEST, body, { headers: headers });
    };
    AdminService.prototype.getUnassignedWorks = function () {
        return this.http.get(ADMIN_REQUEST, { params: { unassignedWorks: 'unassignedWorks' } });
    };
    AdminService.prototype.groupReviewers = function (works) {
        var reviewerDueDate = [];
        var assignedWorks = [];
        var group = [];
        works.forEach(function (work) {
            (reviewerDueDate[work.WID] = reviewerDueDate[work.WID] || []).push({
                ReviewerID: work.ReviewerID,
                ReviewerName: work.ReviewerName,
                Role: work.Role,
                DueDate: work.DueDate
            });
            if (assignedWorks[work.WID] !== null) {
                assignedWorks[work.WID] = {
                    WID: work.WID,
                    Title: work.Title,
                    URL: work.URL,
                    AuthorName: work.AuthorName,
                };
            }
        });
        assignedWorks.forEach(function (work) {
            work.Reviewers = reviewerDueDate[work.WID];
            group.push(work);
        });
        // console.log(reviewers);
        // console.log("Ass ",assignedWorks);
        return group;
    };
    AdminService.prototype.getReviewersScorecard = function (wid) {
        if (wid === void 0) { wid = ''; }
        return this.http.get(scorecardsUrls, { params: { WID: wid } });
    };
    AdminService.prototype.getAdminReview = function (wid) {
        return this.http.get(ADMIN_REQUEST, {
            params: {
                getAllAdminReviews: 'any',
                WID: wid.toString()
            }
        });
    };
    AdminService.prototype.convertToArray = function (str) {
        var ls = Object.assign(str);
        str = ls.split(',');
        return str;
    };
    AdminService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
    ]; };
    AdminService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], AdminService);
    return AdminService;
}());



/***/ }),

/***/ "./src/app/core/admin/directives/title-checker.directive.ts":
/*!******************************************************************!*\
  !*** ./src/app/core/admin/directives/title-checker.directive.ts ***!
  \******************************************************************/
/*! exports provided: TitleCheckerDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TitleCheckerDirective", function() { return TitleCheckerDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var TitleCheckerDirective = /** @class */ (function () {
    function TitleCheckerDirective(el) {
        this.el = el;
        console.log(el);
        console.log("Oldtitle ", this.oldWID);
        console.log("NewTITLE ", this.newWID);
    }
    TitleCheckerDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('title-changer'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], TitleCheckerDirective.prototype, "oldWID", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], TitleCheckerDirective.prototype, "newWID", void 0);
    TitleCheckerDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: '[title-changer]'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]])
    ], TitleCheckerDirective);
    return TitleCheckerDirective;
}());



/***/ }),

/***/ "./src/app/core/auth/_actions/auth.actions.ts":
/*!****************************************************!*\
  !*** ./src/app/core/auth/_actions/auth.actions.ts ***!
  \****************************************************/
/*! exports provided: AuthActionTypes, Login, Logout, UserRequested, UserLoaded */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthActionTypes", function() { return AuthActionTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Login", function() { return Login; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Logout", function() { return Logout; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserRequested", function() { return UserRequested; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserLoaded", function() { return UserLoaded; });
/* harmony import */ var _models_user1_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../_models/user1.model */ "./src/app/core/auth/_models/user1.model.ts");

var AuthActionTypes;
(function (AuthActionTypes) {
    AuthActionTypes["Login"] = "[Login] Action";
    AuthActionTypes["Logout"] = "[Logout] Action";
    AuthActionTypes["UserRequested"] = "[Request User] Action";
    AuthActionTypes["UserLoaded"] = "[Load User] Auth API";
})(AuthActionTypes || (AuthActionTypes = {}));
var Login = /** @class */ (function () {
    function Login(username, password) {
        this.username = username;
        this.password = password;
        this.type = AuthActionTypes.Login;
    }
    Login.ctorParameters = function () { return [
        { type: String },
        { type: String }
    ]; };
    return Login;
}());

var Logout = /** @class */ (function () {
    function Logout() {
        this.type = AuthActionTypes.Logout;
    }
    return Logout;
}());

var UserRequested = /** @class */ (function () {
    function UserRequested() {
        this.type = AuthActionTypes.UserRequested;
    }
    return UserRequested;
}());

var UserLoaded = /** @class */ (function () {
    function UserLoaded(user) {
        this.user = user;
        this.type = AuthActionTypes.UserLoaded;
    }
    UserLoaded.ctorParameters = function () { return [
        { type: _models_user1_model__WEBPACK_IMPORTED_MODULE_0__["User1"] }
    ]; };
    return UserLoaded;
}());



/***/ }),

/***/ "./src/app/core/auth/_actions/user.actions.ts":
/*!****************************************************!*\
  !*** ./src/app/core/auth/_actions/user.actions.ts ***!
  \****************************************************/
/*! exports provided: UserActionTypes, UserOnServerCreated, UserCreated, UserUpdated, UserDeleted, UsersPageRequested, UsersPageLoaded, UsersPageCancelled, UsersPageToggleLoading, UsersActionToggleLoading */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserActionTypes", function() { return UserActionTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserOnServerCreated", function() { return UserOnServerCreated; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserCreated", function() { return UserCreated; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserUpdated", function() { return UserUpdated; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserDeleted", function() { return UserDeleted; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersPageRequested", function() { return UsersPageRequested; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersPageLoaded", function() { return UsersPageLoaded; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersPageCancelled", function() { return UsersPageCancelled; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersPageToggleLoading", function() { return UsersPageToggleLoading; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersActionToggleLoading", function() { return UsersActionToggleLoading; });
var UserActionTypes;
(function (UserActionTypes) {
    UserActionTypes["AllUsersRequested"] = "[Users Module] All Users Requested";
    UserActionTypes["AllUsersLoaded"] = "[Users API] All Users Loaded";
    UserActionTypes["UserOnServerCreated"] = "[Edit User Component] User On Server Created";
    UserActionTypes["UserCreated"] = "[Edit User Dialog] User Created";
    UserActionTypes["UserUpdated"] = "[Edit User Dialog] User Updated";
    UserActionTypes["UserDeleted"] = "[Users List Page] User Deleted";
    UserActionTypes["UsersPageRequested"] = "[Users List Page] Users Page Requested";
    UserActionTypes["UsersPageLoaded"] = "[Users API] Users Page Loaded";
    UserActionTypes["UsersPageCancelled"] = "[Users API] Users Page Cancelled";
    UserActionTypes["UsersPageToggleLoading"] = "[Users] Users Page Toggle Loading";
    UserActionTypes["UsersActionToggleLoading"] = "[Users] Users Action Toggle Loading";
})(UserActionTypes || (UserActionTypes = {}));
var UserOnServerCreated = /** @class */ (function () {
    function UserOnServerCreated(payload) {
        this.payload = payload;
        this.type = UserActionTypes.UserOnServerCreated;
    }
    UserOnServerCreated.ctorParameters = function () { return [
        { type: undefined }
    ]; };
    return UserOnServerCreated;
}());

var UserCreated = /** @class */ (function () {
    function UserCreated(payload) {
        this.payload = payload;
        this.type = UserActionTypes.UserCreated;
    }
    UserCreated.ctorParameters = function () { return [
        { type: undefined }
    ]; };
    return UserCreated;
}());

var UserUpdated = /** @class */ (function () {
    function UserUpdated(payload) {
        this.payload = payload;
        this.type = UserActionTypes.UserUpdated;
    }
    UserUpdated.ctorParameters = function () { return [
        { type: undefined }
    ]; };
    return UserUpdated;
}());

var UserDeleted = /** @class */ (function () {
    function UserDeleted(payload) {
        this.payload = payload;
        this.type = UserActionTypes.UserDeleted;
    }
    UserDeleted.ctorParameters = function () { return [
        { type: undefined }
    ]; };
    return UserDeleted;
}());

var UsersPageRequested = /** @class */ (function () {
    function UsersPageRequested(payload) {
        this.payload = payload;
        this.type = UserActionTypes.UsersPageRequested;
    }
    UsersPageRequested.ctorParameters = function () { return [
        { type: undefined }
    ]; };
    return UsersPageRequested;
}());

var UsersPageLoaded = /** @class */ (function () {
    function UsersPageLoaded(payload) {
        this.payload = payload;
        this.type = UserActionTypes.UsersPageLoaded;
    }
    UsersPageLoaded.ctorParameters = function () { return [
        { type: undefined }
    ]; };
    return UsersPageLoaded;
}());

var UsersPageCancelled = /** @class */ (function () {
    function UsersPageCancelled() {
        this.type = UserActionTypes.UsersPageCancelled;
    }
    return UsersPageCancelled;
}());

var UsersPageToggleLoading = /** @class */ (function () {
    function UsersPageToggleLoading(payload) {
        this.payload = payload;
        this.type = UserActionTypes.UsersPageToggleLoading;
    }
    UsersPageToggleLoading.ctorParameters = function () { return [
        { type: undefined }
    ]; };
    return UsersPageToggleLoading;
}());

var UsersActionToggleLoading = /** @class */ (function () {
    function UsersActionToggleLoading(payload) {
        this.payload = payload;
        this.type = UserActionTypes.UsersActionToggleLoading;
    }
    UsersActionToggleLoading.ctorParameters = function () { return [
        { type: undefined }
    ]; };
    return UsersActionToggleLoading;
}());



/***/ }),

/***/ "./src/app/core/auth/_effects/auth.effects.ts":
/*!****************************************************!*\
  !*** ./src/app/core/auth/_effects/auth.effects.ts ***!
  \****************************************************/
/*! exports provided: AuthEffects */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthEffects", function() { return AuthEffects; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _ngrx_effects__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngrx/effects */ "./node_modules/@ngrx/effects/fesm5/effects.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _actions_auth_actions__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../_actions/auth.actions */ "./src/app/core/auth/_actions/auth.actions.ts");
/* harmony import */ var _services_index__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../_services/index */ "./src/app/core/auth/_services/index.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");

// Angular


// RxJS

// NGRX


// Auth actions



var AuthEffects = /** @class */ (function () {
    function AuthEffects(actions$, router, auth, store) {
        var _this = this;
        this.actions$ = actions$;
        this.router = router;
        this.auth = auth;
        this.store = store;
        this.login$ = this.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_4__["ofType"])(_actions_auth_actions__WEBPACK_IMPORTED_MODULE_6__["AuthActionTypes"].Login), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["mergeMap"])(function (user) {
            return _this.auth.login(user.username, user.password)
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (user) {
                _this.store.dispatch(new _actions_auth_actions__WEBPACK_IMPORTED_MODULE_6__["UserLoaded"](user));
            })
            // localStorage.setItem(environment.authTokenKey, action.payload.authToken);
            // this.store.dispatch(new UserRequested());
            );
        }));
        this.logout$ = this.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_4__["ofType"])(_actions_auth_actions__WEBPACK_IMPORTED_MODULE_6__["AuthActionTypes"].Logout), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function () {
            localStorage.removeItem(_environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].authTokenKey);
            _this.router.navigate(['/auth/login'], { queryParams: { returnUrl: _this.returnUrl } });
        }));
        this.router.events.subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"]) {
                _this.returnUrl = event.url;
            }
        });
    }
    AuthEffects.ctorParameters = function () { return [
        { type: _ngrx_effects__WEBPACK_IMPORTED_MODULE_4__["Actions"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _services_index__WEBPACK_IMPORTED_MODULE_7__["AuthService"] },
        { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_5__["Store"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_4__["Effect"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], AuthEffects.prototype, "login$", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_4__["Effect"])({ dispatch: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], AuthEffects.prototype, "logout$", void 0);
    AuthEffects = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngrx_effects__WEBPACK_IMPORTED_MODULE_4__["Actions"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services_index__WEBPACK_IMPORTED_MODULE_7__["AuthService"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_5__["Store"]])
    ], AuthEffects);
    return AuthEffects;
}());



/***/ }),

/***/ "./src/app/core/auth/_effects/user.effects.ts":
/*!****************************************************!*\
  !*** ./src/app/core/auth/_effects/user.effects.ts ***!
  \****************************************************/
/*! exports provided: UserEffects */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserEffects", function() { return UserEffects; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _ngrx_effects__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngrx/effects */ "./node_modules/@ngrx/effects/fesm5/effects.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _core_auth_services__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../core/auth/_services */ "./src/app/core/auth/_services/index.ts");
/* harmony import */ var _actions_user_actions__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../_actions/user.actions */ "./src/app/core/auth/_actions/user.actions.ts");

// Angular

// RxJS


// NGRX


// Services


var UserEffects = /** @class */ (function () {
    function UserEffects(actions$, auth, store) {
        var _this = this;
        this.actions$ = actions$;
        this.auth = auth;
        this.store = store;
        this.showPageLoadingDistpatcher = new _actions_user_actions__WEBPACK_IMPORTED_MODULE_7__["UsersPageToggleLoading"]({ isLoading: true });
        this.hidePageLoadingDistpatcher = new _actions_user_actions__WEBPACK_IMPORTED_MODULE_7__["UsersPageToggleLoading"]({ isLoading: false });
        this.showActionLoadingDistpatcher = new _actions_user_actions__WEBPACK_IMPORTED_MODULE_7__["UsersActionToggleLoading"]({ isLoading: true });
        this.hideActionLoadingDistpatcher = new _actions_user_actions__WEBPACK_IMPORTED_MODULE_7__["UsersActionToggleLoading"]({ isLoading: false });
        this.loadUsersPage$ = this.actions$
            .pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_4__["ofType"])(_actions_user_actions__WEBPACK_IMPORTED_MODULE_7__["UserActionTypes"].UsersPageRequested), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["mergeMap"])(function (_a) {
            var payload = _a.payload;
            _this.store.dispatch(_this.showPageLoadingDistpatcher);
            var requestToServer = _this.auth.findUsers(payload.page);
            var lastQuery = Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(payload.page);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["forkJoin"])(requestToServer, lastQuery);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (response) {
            var result = response[0];
            var lastQuery = response[1];
            return new _actions_user_actions__WEBPACK_IMPORTED_MODULE_7__["UsersPageLoaded"]({
                users: result.items,
                totalCount: result.totalCount,
                page: lastQuery
            });
        }));
        this.deleteUser$ = this.actions$
            .pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_4__["ofType"])(_actions_user_actions__WEBPACK_IMPORTED_MODULE_7__["UserActionTypes"].UserDeleted), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["mergeMap"])(function (_a) {
            var payload = _a.payload;
            _this.store.dispatch(_this.showActionLoadingDistpatcher);
            return _this.auth.deleteUser(payload.id);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function () {
            return _this.hideActionLoadingDistpatcher;
        }));
        this.updateUser$ = this.actions$
            .pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_4__["ofType"])(_actions_user_actions__WEBPACK_IMPORTED_MODULE_7__["UserActionTypes"].UserUpdated), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["mergeMap"])(function (_a) {
            var payload = _a.payload;
            _this.store.dispatch(_this.showActionLoadingDistpatcher);
            return _this.auth.updateUser(payload.user);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function () {
            return _this.hideActionLoadingDistpatcher;
        }));
        this.createUser$ = this.actions$
            .pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_4__["ofType"])(_actions_user_actions__WEBPACK_IMPORTED_MODULE_7__["UserActionTypes"].UserOnServerCreated), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["mergeMap"])(function (_a) {
            var payload = _a.payload;
            _this.store.dispatch(_this.showActionLoadingDistpatcher);
            return _this.auth.createUser(payload.user).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])(function (res) {
                _this.store.dispatch(new _actions_user_actions__WEBPACK_IMPORTED_MODULE_7__["UserCreated"]({ user: res }));
            }));
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function () {
            return _this.hideActionLoadingDistpatcher;
        }));
    }
    UserEffects.ctorParameters = function () { return [
        { type: _ngrx_effects__WEBPACK_IMPORTED_MODULE_4__["Actions"] },
        { type: _core_auth_services__WEBPACK_IMPORTED_MODULE_6__["AuthService"] },
        { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_5__["Store"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_4__["Effect"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], UserEffects.prototype, "loadUsersPage$", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_4__["Effect"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], UserEffects.prototype, "deleteUser$", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_4__["Effect"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], UserEffects.prototype, "updateUser$", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_4__["Effect"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], UserEffects.prototype, "createUser$", void 0);
    UserEffects = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngrx_effects__WEBPACK_IMPORTED_MODULE_4__["Actions"], _core_auth_services__WEBPACK_IMPORTED_MODULE_6__["AuthService"], _ngrx_store__WEBPACK_IMPORTED_MODULE_5__["Store"]])
    ], UserEffects);
    return UserEffects;
}());



/***/ }),

/***/ "./src/app/core/auth/_guards/auth.guard.ts":
/*!*************************************************!*\
  !*** ./src/app/core/auth/_guards/auth.guard.ts ***!
  \*************************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");

// Angular


var AuthGuard = /** @class */ (function () {
    function AuthGuard(router) {
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        var user = JSON.parse(sessionStorage.getItem('user'));
        if (user) {
            if (route.data.roles && route.data.roles.indexOf(user.roleType) === -1) {
                sessionStorage.removeItem('user');
                this.router.navigate(['auth/login']);
                return false;
            }
        }
        else {
            this.router.navigate(['/home/public']);
            return false;
        }
        return true;
    };
    AuthGuard.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/core/auth/_guards/module.guard.ts":
/*!***************************************************!*\
  !*** ./src/app/core/auth/_guards/module.guard.ts ***!
  \***************************************************/
/*! exports provided: ModuleGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModuleGuard", function() { return ModuleGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");

// Angular


// RxJS

// NGRX

var ModuleGuard = /** @class */ (function () {
    function ModuleGuard(store, router) {
        this.store = store;
        this.router = router;
    }
    ModuleGuard.prototype.canActivate = function (route, state) {
        var moduleName = route.data['moduleName'];
        if (!moduleName) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(false);
        }
        // return this.store
        // .pipe(
        // select(currentUserPermissions),
        // map((permissions: Permission[]) => {
        //     const _perm = find(permissions, (elem: Permission) => {
        //         return elem.title.toLocaleLowerCase() === moduleName.toLocaleLowerCase();
        //     });
        //     return _perm ? true : false;
        // }),
        // tap(hasAccess => {
        //     if (!hasAccess) {
        //         this.router.navigateByUrl('/error/403');
        //     }
        // })
        // );
    };
    ModuleGuard.ctorParameters = function () { return [
        { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_4__["Store"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    ModuleGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngrx_store__WEBPACK_IMPORTED_MODULE_4__["Store"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], ModuleGuard);
    return ModuleGuard;
}());



/***/ }),

/***/ "./src/app/core/auth/_models/role.ts":
/*!*******************************************!*\
  !*** ./src/app/core/auth/_models/role.ts ***!
  \*******************************************/
/*! exports provided: Role */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Role", function() { return Role; });
var Role;
(function (Role) {
    Role["Reviewer"] = "Reviewer";
    Role["LeadReviewer"] = "Lead Reviewer";
    Role["Admin"] = "Admin";
})(Role || (Role = {}));


/***/ }),

/***/ "./src/app/core/auth/_models/user.model.ts":
/*!*************************************************!*\
  !*** ./src/app/core/auth/_models/user.model.ts ***!
  \*************************************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _base_crud__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../_base/crud */ "./src/app/core/_base/crud/index.ts");


var User = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](User, _super);
    function User() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    User.prototype.clear = function () {
        this.id = undefined;
        this.username = '';
        this.password = '';
        this.email = '';
        this.roles = [];
        this.fullname = '';
        this.accessToken = 'access-token-' + Math.random();
        this.refreshToken = 'access-token-' + Math.random();
        this.pic = './assets/media/users/default.jpg';
        this.occupation = '';
        this.companyName = '';
        this.phone = '';
    };
    return User;
}(_base_crud__WEBPACK_IMPORTED_MODULE_1__["BaseModel"]));



/***/ }),

/***/ "./src/app/core/auth/_models/user1.model.ts":
/*!**************************************************!*\
  !*** ./src/app/core/auth/_models/user1.model.ts ***!
  \**************************************************/
/*! exports provided: User1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User1", function() { return User1; });
var User1 = /** @class */ (function () {
    function User1() {
    }
    User1.prototype.clear = function () {
        this.id = undefined;
        this.username = '';
        this.password = '';
        this.email = '';
        this.roleId = undefined;
        this.fullname = '';
        this.credentialID = undefined;
        this.roleType = '';
        this.accessToken = '';
        this.refreshToken = '';
    };
    return User1;
}());



/***/ }),

/***/ "./src/app/core/auth/_reducers/auth.reducers.ts":
/*!******************************************************!*\
  !*** ./src/app/core/auth/_reducers/auth.reducers.ts ***!
  \******************************************************/
/*! exports provided: initialAuthState, authReducer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initialAuthState", function() { return initialAuthState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "authReducer", function() { return authReducer; });
/* harmony import */ var _actions_auth_actions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../_actions/auth.actions */ "./src/app/core/auth/_actions/auth.actions.ts");
// Actions

var initialAuthState = {
    loggedIn: false,
    user: undefined,
    isUserLoaded: false
};
function authReducer(state, action) {
    if (state === void 0) { state = initialAuthState; }
    switch (action.type) {
        case _actions_auth_actions__WEBPACK_IMPORTED_MODULE_0__["AuthActionTypes"].Login: {
            return {
                loggedIn: false,
                user: undefined,
                isUserLoaded: false
            };
        }
        case _actions_auth_actions__WEBPACK_IMPORTED_MODULE_0__["AuthActionTypes"].UserLoaded: {
            var _user = action.user;
            return {
                loggedIn: true,
                user: _user,
                isUserLoaded: true
            };
        }
        case _actions_auth_actions__WEBPACK_IMPORTED_MODULE_0__["AuthActionTypes"].Logout:
            return initialAuthState;
        default:
            return state;
    }
}


/***/ }),

/***/ "./src/app/core/auth/_reducers/user.reducers.ts":
/*!******************************************************!*\
  !*** ./src/app/core/auth/_reducers/user.reducers.ts ***!
  \******************************************************/
/*! exports provided: adapter, initialUsersState, usersReducer, getUserState, selectAll, selectEntities, selectIds, selectTotal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "adapter", function() { return adapter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initialUsersState", function() { return initialUsersState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "usersReducer", function() { return usersReducer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getUserState", function() { return getUserState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "selectAll", function() { return selectAll; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "selectEntities", function() { return selectEntities; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "selectIds", function() { return selectIds; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "selectTotal", function() { return selectTotal; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _ngrx_entity__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngrx/entity */ "./node_modules/@ngrx/entity/fesm5/entity.js");
/* harmony import */ var _actions_user_actions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_actions/user.actions */ "./src/app/core/auth/_actions/user.actions.ts");
/* harmony import */ var _base_crud__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../_base/crud */ "./src/app/core/_base/crud/index.ts");

var _a;
// NGRX


// Actions

// CRUD

var adapter = Object(_ngrx_entity__WEBPACK_IMPORTED_MODULE_2__["createEntityAdapter"])();
var initialUsersState = adapter.getInitialState({
    listLoading: false,
    actionsloading: false,
    totalCount: 0,
    lastQuery: new _base_crud__WEBPACK_IMPORTED_MODULE_4__["QueryParamsModel"]({}),
    lastCreatedUserId: undefined,
    showInitWaitingMessage: true
});
function usersReducer(state, action) {
    if (state === void 0) { state = initialUsersState; }
    switch (action.type) {
        case _actions_user_actions__WEBPACK_IMPORTED_MODULE_3__["UserActionTypes"].UsersPageToggleLoading: return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, state, { listLoading: action.payload.isLoading, lastCreatedUserId: undefined });
        case _actions_user_actions__WEBPACK_IMPORTED_MODULE_3__["UserActionTypes"].UsersActionToggleLoading: return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, state, { actionsloading: action.payload.isLoading });
        case _actions_user_actions__WEBPACK_IMPORTED_MODULE_3__["UserActionTypes"].UserOnServerCreated: return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, state);
        case _actions_user_actions__WEBPACK_IMPORTED_MODULE_3__["UserActionTypes"].UserCreated: return adapter.addOne(action.payload.user, tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, state, { lastCreatedUserId: action.payload.user.id }));
        case _actions_user_actions__WEBPACK_IMPORTED_MODULE_3__["UserActionTypes"].UserUpdated: return adapter.updateOne(action.payload.partialUser, state);
        case _actions_user_actions__WEBPACK_IMPORTED_MODULE_3__["UserActionTypes"].UserDeleted: return adapter.removeOne(action.payload.id, state);
        case _actions_user_actions__WEBPACK_IMPORTED_MODULE_3__["UserActionTypes"].UsersPageCancelled: return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, state, { listLoading: false, lastQuery: new _base_crud__WEBPACK_IMPORTED_MODULE_4__["QueryParamsModel"]({}) });
        case _actions_user_actions__WEBPACK_IMPORTED_MODULE_3__["UserActionTypes"].UsersPageLoaded: {
            return adapter.addMany(action.payload.users, tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, initialUsersState, { totalCount: action.payload.totalCount, lastQuery: action.payload.page, listLoading: false, showInitWaitingMessage: false }));
        }
        default: return state;
    }
}
var getUserState = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_1__["createFeatureSelector"])('users');
var selectAll = (_a = adapter.getSelectors(), _a.selectAll), selectEntities = _a.selectEntities, selectIds = _a.selectIds, selectTotal = _a.selectTotal;


/***/ }),

/***/ "./src/app/core/auth/_selectors/auth.selectors.ts":
/*!********************************************************!*\
  !*** ./src/app/core/auth/_selectors/auth.selectors.ts ***!
  \********************************************************/
/*! exports provided: selectAuthState, isLoggedIn, isLoggedOut, isUserLoaded, currentUser */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "selectAuthState", function() { return selectAuthState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isLoggedIn", function() { return isLoggedIn; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isLoggedOut", function() { return isLoggedOut; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isUserLoaded", function() { return isUserLoaded; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "currentUser", function() { return currentUser; });
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
// NGRX

var selectAuthState = function (state) { return state.auth; };
var isLoggedIn = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(selectAuthState, function (auth) { return auth.loggedIn; });
var isLoggedOut = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(isLoggedIn, function (loggedIn) { return !loggedIn; });
var isUserLoaded = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(selectAuthState, function (auth) { return auth.isUserLoaded; });
var currentUser = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(selectAuthState, function (auth) { return auth.user; });


/***/ }),

/***/ "./src/app/core/auth/_selectors/user.selectors.ts":
/*!********************************************************!*\
  !*** ./src/app/core/auth/_selectors/user.selectors.ts ***!
  \********************************************************/
/*! exports provided: selectUsersState, selectUserById, selectUsersPageLoading, selectWorksPageLoading, selectUsersActionLoading, selectLastCreatedUserId, selectUsersPageLastQuery, selectUsersInStore, selectUsersShowInitWaitingMessage, selectHasUsersInStore */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "selectUsersState", function() { return selectUsersState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "selectUserById", function() { return selectUserById; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "selectUsersPageLoading", function() { return selectUsersPageLoading; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "selectWorksPageLoading", function() { return selectWorksPageLoading; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "selectUsersActionLoading", function() { return selectUsersActionLoading; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "selectLastCreatedUserId", function() { return selectLastCreatedUserId; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "selectUsersPageLastQuery", function() { return selectUsersPageLastQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "selectUsersInStore", function() { return selectUsersInStore; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "selectUsersShowInitWaitingMessage", function() { return selectUsersShowInitWaitingMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "selectHasUsersInStore", function() { return selectHasUsersInStore; });
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _base_crud__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../_base/crud */ "./src/app/core/_base/crud/index.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);
// NGRX

// CRUD


var selectUsersState = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createFeatureSelector"])('users');
var selectUserById = function (userId) { return Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(selectUsersState, function (usersState) { return usersState.entities[userId]; }); };
var selectUsersPageLoading = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(selectUsersState, function (usersState) {
    return usersState.listLoading;
});
var selectWorksPageLoading = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(selectUsersState, function (usersState) {
    return usersState.listLoading;
});
var selectUsersActionLoading = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(selectUsersState, function (usersState) { return usersState.actionsloading; });
var selectLastCreatedUserId = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(selectUsersState, function (usersState) { return usersState.lastCreatedUserId; });
var selectUsersPageLastQuery = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(selectUsersState, function (usersState) { return usersState.lastQuery; });
var selectUsersInStore = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(selectUsersState, function (usersState) {
    var items = [];
    Object(lodash__WEBPACK_IMPORTED_MODULE_2__["each"])(usersState.entities, function (element) {
        items.push(element);
    });
    var httpExtension = new _base_crud__WEBPACK_IMPORTED_MODULE_1__["HttpExtenstionsModel"]();
    var result = httpExtension.sortArray(items, usersState.lastQuery.sortField, usersState.lastQuery.sortOrder);
    return new _base_crud__WEBPACK_IMPORTED_MODULE_1__["QueryResultsModel"](result, usersState.totalCount, '');
});
var selectUsersShowInitWaitingMessage = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(selectUsersState, function (usersState) { return usersState.showInitWaitingMessage; });
var selectHasUsersInStore = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(selectUsersState, function (queryResult) {
    if (!queryResult.totalCount) {
        return false;
    }
    return true;
});


/***/ }),

/***/ "./src/app/core/auth/_services/auth.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/core/auth/_services/auth.service.ts ***!
  \*****************************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _base_crud__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../_base/crud */ "./src/app/core/_base/crud/index.ts");

// Angular


// RxJS


// Environment

// CRUD

// const API_USER_URL = 'http://3.95.8.94/example/index.php';
var API_USER_URL = _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].baseUrl + 'index.php';
var API_USERS_URL = "";
var AuthService = /** @class */ (function () {
    function AuthService(http, httpUtils) {
        this.http = http;
        this.httpUtils = httpUtils;
        this.subject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
    }
    AuthService.prototype.login = function (username, password) {
        var body = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set("userLogin", 'userLogin').set("username", username).set("password", password);
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        // return this.http.post<User1>(API_USER_URL, body.toString(), {headers});
        return this.http.post(API_USER_URL, body.toString(), { headers: headers, observe: 'response' })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (response) {
            return response.body[0];
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(error);
        }));
    };
    AuthService.prototype.extractData = function (res) {
        var body = res.json();
        return body || {};
    };
    // CREATE =>  POST: add a new user to the server
    AuthService.prototype.createUser = function (user) {
        var httpHeaders = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        // Note: Add headers if needed (tokens/bearer)
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.post(API_USER_URL, user, { headers: httpHeaders });
    };
    // READ
    AuthService.prototype.getAllUsers = function () {
        return this.http.get(API_USER_URL);
    };
    AuthService.prototype.getUser = function (username, password) {
        var body = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set("userLogin", 'userLogin').set("username", username).set("password", password);
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this.http.post(API_USER_URL, body.toString(), { headers: headers, observe: 'response' });
    };
    AuthService.prototype.getAllUsers1 = function () {
        return this.http.get(API_USER_URL);
    };
    AuthService.prototype.getUserById = function (userId) {
        if (!userId) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(null);
        }
        return this.http.get(API_USER_URL + ("/" + userId));
    };
    // DELETE => delete the user from the server
    AuthService.prototype.deleteUser = function (userId) {
        var url = API_USERS_URL + "/" + userId;
        return this.http.delete(url);
    };
    // UPDATE => PUT: update the user on the server
    AuthService.prototype.updateUser = function (_user) {
        var httpHeaders = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.put(API_USERS_URL, _user, { headers: httpHeaders }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(null);
        }));
    };
    // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
    // items => filtered/sorted result
    AuthService.prototype.findUsers = function (queryParams) {
        var _this = this;
        // This code imitates server calls
        return this.getAllUsers().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["mergeMap"])(function (response) {
            var result = _this.httpUtils.baseFilter(response, queryParams, []);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(result);
        }));
    };
    AuthService.prototype.getUserMenuConfig = function () {
        return this.subject.asObservable();
    };
    AuthService.prototype.handleError = function (operation, result) {
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            // Let the app keep running by returning an empty result.
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(result);
        };
    };
    AuthService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
        { type: _base_crud__WEBPACK_IMPORTED_MODULE_6__["HttpUtilsService"] }
    ]; };
    AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
            _base_crud__WEBPACK_IMPORTED_MODULE_6__["HttpUtilsService"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/core/auth/_services/index.ts":
/*!**********************************************!*\
  !*** ./src/app/core/auth/_services/index.ts ***!
  \**********************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./auth.service */ "./src/app/core/auth/_services/auth.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return _auth_service__WEBPACK_IMPORTED_MODULE_0__["AuthService"]; });

 // You have to comment this, when your real back-end is done
// export { AuthService } from './auth.service'; // You have to uncomment this, when your real back-end is done


/***/ }),

/***/ "./src/app/core/auth/auth-notice/auth-notice.service.ts":
/*!**************************************************************!*\
  !*** ./src/app/core/auth/auth-notice/auth-notice.service.ts ***!
  \**************************************************************/
/*! exports provided: AuthNoticeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthNoticeService", function() { return AuthNoticeService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");



var AuthNoticeService = /** @class */ (function () {
    function AuthNoticeService() {
        this.onNoticeChanged$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](null);
    }
    AuthNoticeService.prototype.setNotice = function (message, type) {
        var notice = {
            message: message,
            type: type
        };
        this.onNoticeChanged$.next(notice);
    };
    AuthNoticeService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AuthNoticeService);
    return AuthNoticeService;
}());



/***/ }),

/***/ "./src/app/core/auth/index.ts":
/*!************************************!*\
  !*** ./src/app/core/auth/index.ts ***!
  \************************************/
/*! exports provided: AuthService, AuthNoticeService, Login, Logout, AuthActionTypes, UserCreated, UserUpdated, UserDeleted, UserOnServerCreated, UsersPageLoaded, UsersPageCancelled, UsersPageToggleLoading, UsersPageRequested, UsersActionToggleLoading, AuthEffects, UserEffects, authReducer, usersReducer, isLoggedIn, isLoggedOut, isUserLoaded, currentUser, selectUserById, selectUsersPageLoading, selectLastCreatedUserId, selectUsersInStore, selectHasUsersInStore, selectUsersPageLastQuery, selectUsersActionLoading, selectUsersShowInitWaitingMessage, AuthGuard, ModuleGuard, User, Role */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./_services */ "./src/app/core/auth/_services/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return _services__WEBPACK_IMPORTED_MODULE_0__["AuthService"]; });

/* harmony import */ var _auth_notice_auth_notice_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./auth-notice/auth-notice.service */ "./src/app/core/auth/auth-notice/auth-notice.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AuthNoticeService", function() { return _auth_notice_auth_notice_service__WEBPACK_IMPORTED_MODULE_1__["AuthNoticeService"]; });

/* harmony import */ var _actions_auth_actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./_actions/auth.actions */ "./src/app/core/auth/_actions/auth.actions.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Login", function() { return _actions_auth_actions__WEBPACK_IMPORTED_MODULE_2__["Login"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Logout", function() { return _actions_auth_actions__WEBPACK_IMPORTED_MODULE_2__["Logout"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AuthActionTypes", function() { return _actions_auth_actions__WEBPACK_IMPORTED_MODULE_2__["AuthActionTypes"]; });

/* harmony import */ var _actions_user_actions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./_actions/user.actions */ "./src/app/core/auth/_actions/user.actions.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UserCreated", function() { return _actions_user_actions__WEBPACK_IMPORTED_MODULE_3__["UserCreated"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UserUpdated", function() { return _actions_user_actions__WEBPACK_IMPORTED_MODULE_3__["UserUpdated"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UserDeleted", function() { return _actions_user_actions__WEBPACK_IMPORTED_MODULE_3__["UserDeleted"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UserOnServerCreated", function() { return _actions_user_actions__WEBPACK_IMPORTED_MODULE_3__["UserOnServerCreated"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UsersPageLoaded", function() { return _actions_user_actions__WEBPACK_IMPORTED_MODULE_3__["UsersPageLoaded"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UsersPageCancelled", function() { return _actions_user_actions__WEBPACK_IMPORTED_MODULE_3__["UsersPageCancelled"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UsersPageToggleLoading", function() { return _actions_user_actions__WEBPACK_IMPORTED_MODULE_3__["UsersPageToggleLoading"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UsersPageRequested", function() { return _actions_user_actions__WEBPACK_IMPORTED_MODULE_3__["UsersPageRequested"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UsersActionToggleLoading", function() { return _actions_user_actions__WEBPACK_IMPORTED_MODULE_3__["UsersActionToggleLoading"]; });

/* harmony import */ var _effects_auth_effects__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./_effects/auth.effects */ "./src/app/core/auth/_effects/auth.effects.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AuthEffects", function() { return _effects_auth_effects__WEBPACK_IMPORTED_MODULE_4__["AuthEffects"]; });

/* harmony import */ var _effects_user_effects__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./_effects/user.effects */ "./src/app/core/auth/_effects/user.effects.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UserEffects", function() { return _effects_user_effects__WEBPACK_IMPORTED_MODULE_5__["UserEffects"]; });

/* harmony import */ var _reducers_auth_reducers__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./_reducers/auth.reducers */ "./src/app/core/auth/_reducers/auth.reducers.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "authReducer", function() { return _reducers_auth_reducers__WEBPACK_IMPORTED_MODULE_6__["authReducer"]; });

/* harmony import */ var _reducers_user_reducers__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./_reducers/user.reducers */ "./src/app/core/auth/_reducers/user.reducers.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "usersReducer", function() { return _reducers_user_reducers__WEBPACK_IMPORTED_MODULE_7__["usersReducer"]; });

/* harmony import */ var _selectors_auth_selectors__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./_selectors/auth.selectors */ "./src/app/core/auth/_selectors/auth.selectors.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isLoggedIn", function() { return _selectors_auth_selectors__WEBPACK_IMPORTED_MODULE_8__["isLoggedIn"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isLoggedOut", function() { return _selectors_auth_selectors__WEBPACK_IMPORTED_MODULE_8__["isLoggedOut"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isUserLoaded", function() { return _selectors_auth_selectors__WEBPACK_IMPORTED_MODULE_8__["isUserLoaded"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "currentUser", function() { return _selectors_auth_selectors__WEBPACK_IMPORTED_MODULE_8__["currentUser"]; });

/* harmony import */ var _selectors_user_selectors__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./_selectors/user.selectors */ "./src/app/core/auth/_selectors/user.selectors.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "selectUserById", function() { return _selectors_user_selectors__WEBPACK_IMPORTED_MODULE_9__["selectUserById"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "selectUsersPageLoading", function() { return _selectors_user_selectors__WEBPACK_IMPORTED_MODULE_9__["selectUsersPageLoading"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "selectLastCreatedUserId", function() { return _selectors_user_selectors__WEBPACK_IMPORTED_MODULE_9__["selectLastCreatedUserId"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "selectUsersInStore", function() { return _selectors_user_selectors__WEBPACK_IMPORTED_MODULE_9__["selectUsersInStore"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "selectHasUsersInStore", function() { return _selectors_user_selectors__WEBPACK_IMPORTED_MODULE_9__["selectHasUsersInStore"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "selectUsersPageLastQuery", function() { return _selectors_user_selectors__WEBPACK_IMPORTED_MODULE_9__["selectUsersPageLastQuery"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "selectUsersActionLoading", function() { return _selectors_user_selectors__WEBPACK_IMPORTED_MODULE_9__["selectUsersActionLoading"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "selectUsersShowInitWaitingMessage", function() { return _selectors_user_selectors__WEBPACK_IMPORTED_MODULE_9__["selectUsersShowInitWaitingMessage"]; });

/* harmony import */ var _guards_auth_guard__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./_guards/auth.guard */ "./src/app/core/auth/_guards/auth.guard.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return _guards_auth_guard__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]; });

/* harmony import */ var _guards_module_guard__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./_guards/module.guard */ "./src/app/core/auth/_guards/module.guard.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ModuleGuard", function() { return _guards_module_guard__WEBPACK_IMPORTED_MODULE_11__["ModuleGuard"]; });

/* harmony import */ var _models_user_model__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./_models/user.model */ "./src/app/core/auth/_models/user.model.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "User", function() { return _models_user_model__WEBPACK_IMPORTED_MODULE_12__["User"]; });

/* harmony import */ var _models_role__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./_models/role */ "./src/app/core/auth/_models/role.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Role", function() { return _models_role__WEBPACK_IMPORTED_MODULE_13__["Role"]; });

// SERVICES


// ACTIONS


// EFFECTS


// REDUCERS


// SELECTORS


// GUARDS


// MODELS




/***/ }),

/***/ "./src/app/core/core.module.ts":
/*!*************************************!*\
  !*** ./src/app/core/core.module.ts ***!
  \*************************************/
/*! exports provided: CoreModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CoreModule", function() { return CoreModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _base_layout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./_base/layout */ "./src/app/core/_base/layout/index.ts");
/* harmony import */ var _admin_services_admin_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./admin/_services/admin.service */ "./src/app/core/admin/_services/admin.service.ts");
/* harmony import */ var _admin_directives_title_checker_directive__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./admin/directives/title-checker.directive */ "./src/app/core/admin/directives/title-checker.directive.ts");
/* harmony import */ var _email_notification_services_email_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./email-notification/_services/email.service */ "./src/app/core/email-notification/_services/email.service.ts");
/* harmony import */ var _file_service_file_backup_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./file-service/file-backup.service */ "./src/app/core/file-service/file-backup.service.ts");
/* harmony import */ var _rubric_service_rubric_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./rubric/_service/rubric.service */ "./src/app/core/rubric/_service/rubric.service.ts");

// Anglar


// Layout Directives
// Services






var CoreModule = /** @class */ (function () {
    function CoreModule() {
    }
    CoreModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]],
            declarations: [
                // directives
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["ScrollTopDirective"],
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["HeaderDirective"],
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["OffcanvasDirective"],
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["ToggleDirective"],
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["MenuDirective"],
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["TabClickEventDirective"],
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["SparklineChartDirective"],
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["ContentAnimateDirective"],
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["StickyDirective"],
                // pipes
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["TimeElapsedPipe"],
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["JoinPipe"],
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["GetObjectPipe"],
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["SafePipe"],
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["FirstLetterPipe"],
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["CustomSortPipe"],
                _admin_directives_title_checker_directive__WEBPACK_IMPORTED_MODULE_5__["TitleCheckerDirective"]
            ],
            exports: [
                // directives
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["ScrollTopDirective"],
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["HeaderDirective"],
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["OffcanvasDirective"],
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["ToggleDirective"],
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["MenuDirective"],
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["TabClickEventDirective"],
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["SparklineChartDirective"],
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["ContentAnimateDirective"],
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["StickyDirective"],
                // pipes
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["TimeElapsedPipe"],
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["JoinPipe"],
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["GetObjectPipe"],
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["SafePipe"],
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["FirstLetterPipe"],
                _base_layout__WEBPACK_IMPORTED_MODULE_3__["CustomSortPipe"],
                _admin_directives_title_checker_directive__WEBPACK_IMPORTED_MODULE_5__["TitleCheckerDirective"]
            ],
            providers: [_admin_services_admin_service__WEBPACK_IMPORTED_MODULE_4__["AdminService"], _email_notification_services_email_service__WEBPACK_IMPORTED_MODULE_6__["EmailService"], _file_service_file_backup_service__WEBPACK_IMPORTED_MODULE_7__["FileBackupService"], _rubric_service_rubric_service__WEBPACK_IMPORTED_MODULE_8__["RubricService"]]
        })
    ], CoreModule);
    return CoreModule;
}());



/***/ }),

/***/ "./src/app/core/email-notification/_services/email.service.ts":
/*!********************************************************************!*\
  !*** ./src/app/core/email-notification/_services/email.service.ts ***!
  \********************************************************************/
/*! exports provided: EmailService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailService", function() { return EmailService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");






// const url = 'http://3.95.8.94/example/mail_request.php';
var url = _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].baseUrl + 'mail_request.php';
var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/x-www-form-urlencoded' });
var EmailService = /** @class */ (function () {
    function EmailService(http) {
        this.http = http;
    }
    /*
    *
    * senderName: 'no-reply',
            senderEmail: this.admin.email,
            recepientName: this.work.AuthorName,
            recepientEmail: this.work.AuthorEmail,
            subject: 'Work Status Notification',
            message: msg,
    *
    * */
    EmailService.prototype.sendEmail = function (email) {
        var body = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set("contactMessage", 'contactMessage')
            .set("senderName", email.senderName)
            .set("senderEmail", email.senderEmail)
            .set("subject", email.subject)
            .set("message", email.message)
            .set("canReply", email.canReply.toString());
        return this.http.post(url, body, { headers: headers })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    };
    // send email to author about any progress of the submitted work
    EmailService.prototype.sendWorkStatusEmail = function (email) {
        var body = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set("workStatusReport", 'contactMessage')
            .set("senderName", email.senderName)
            .set("senderEmail", email.senderEmail)
            .set("recepientName", email.recepientName)
            .set("recepientEmail", email.recepientEmail)
            .set("subject", email.subject)
            .set("canReply", email.canReply.toString())
            .set("message", email.message);
        return this.http.post(url, body, { headers: headers })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    };
    EmailService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    EmailService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], EmailService);
    return EmailService;
}());



/***/ }),

/***/ "./src/app/core/file-service/file-backup.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/core/file-service/file-backup.service.ts ***!
  \**********************************************************/
/*! exports provided: FileBackupService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileBackupService", function() { return FileBackupService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");






// const downloadUrl = 'http://3.95.8.94/example/download_request.php';
var downloadUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].baseUrl + 'download_request.php';
// const uploadUrl = 'http://3.95.8.94/example/upload_request.php';
var uploadUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].baseUrl + 'upload_request.php';
var FileBackupService = /** @class */ (function () {
    function FileBackupService(http) {
        this.http = http;
    }
    // upload(formData: FormData):Observable<any> {
    // 	return this.http.post(uploadUrl, formData);
    // }
    FileBackupService.prototype.upload = function (formData) {
        return this.http.post(uploadUrl, formData)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(error);
        }));
        /*return this.http.post(uploadUrl, formData, {
            reportProgress: true,
            observe: 'events'
        }).pipe(
            map(event => {
                switch (event.type) {

                    case HttpEventType.UploadProgress:
                        const progress = Math.round(100 * event.loaded / event.total);
                        return {status: 'progress', message: progress};

                    case HttpEventType.Response:
                        return event.body;

                    default:
                        return `Unhandled event: ${event.type}`;

                }
            })
        );*/
    };
    FileBackupService.prototype.download = function () {
        return this.http.get(downloadUrl, { responseType: 'blob' });
    };
    FileBackupService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    FileBackupService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], FileBackupService);
    return FileBackupService;
}());



/***/ }),

/***/ "./src/app/core/reducers/index.ts":
/*!****************************************!*\
  !*** ./src/app/core/reducers/index.ts ***!
  \****************************************/
/*! exports provided: reducers, metaReducers */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reducers", function() { return reducers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "metaReducers", function() { return metaReducers; });
/* harmony import */ var ngrx_store_freeze__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ngrx-store-freeze */ "./node_modules/ngrx-store-freeze/index.js");
/* harmony import */ var ngrx_store_freeze__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(ngrx_store_freeze__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ngrx_router_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ngrx/router-store */ "./node_modules/@ngrx/router-store/fesm5/router-store.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");



var reducers = { router: _ngrx_router_store__WEBPACK_IMPORTED_MODULE_1__["routerReducer"] };
var metaReducers = !_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].production ? [ngrx_store_freeze__WEBPACK_IMPORTED_MODULE_0__["storeFreeze"]] : [];


/***/ }),

/***/ "./src/app/core/rubric/_service/rubric.service.ts":
/*!********************************************************!*\
  !*** ./src/app/core/rubric/_service/rubric.service.ts ***!
  \********************************************************/
/*! exports provided: RubricService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RubricService", function() { return RubricService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");





var url = 'http://3.95.8.94/example/rubric.php';
var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/x-www-form-urlencoded' });
var RubricService = /** @class */ (function () {
    function RubricService(http) {
        this.http = http;
    }
    RubricService.prototype.getScorecardRubric = function () {
        return this.http.get(url, { responseType: 'json' })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    };
    RubricService.prototype.submitUpdatedRubric = function (rubric) {
        return this.http.put(url, rubric);
    };
    RubricService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    RubricService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], RubricService);
    return RubricService;
}());



/***/ }),

/***/ "./src/app/views/auth/auth-notice/auth-notice.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/views/auth/auth-notice/auth-notice.component.ts ***!
  \*****************************************************************/
/*! exports provided: AuthNoticeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthNoticeComponent", function() { return AuthNoticeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../core/auth */ "./src/app/core/auth/index.ts");

// Angular

// Auth

var AuthNoticeComponent = /** @class */ (function () {
    /**
     * Component Constructure
     *
     * @param authNoticeService
     * @param cdr
     */
    function AuthNoticeComponent(authNoticeService, cdr) {
        this.authNoticeService = authNoticeService;
        this.cdr = cdr;
        this.message = '';
        // Private properties
        this.subscriptions = [];
    }
    /*
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
    */
    /**
     * On init
     */
    AuthNoticeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscriptions.push(this.authNoticeService.onNoticeChanged$.subscribe(function (notice) {
            notice = Object.assign({}, { message: '', type: '' }, notice);
            _this.message = notice.message;
            _this.type = notice.type;
            _this.cdr.markForCheck();
        }));
    };
    /**
     * On destroy
     */
    AuthNoticeComponent.prototype.ngOnDestroy = function () {
        this.subscriptions.forEach(function (sb) { return sb.unsubscribe(); });
    };
    AuthNoticeComponent.ctorParameters = function () { return [
        { type: _core_auth__WEBPACK_IMPORTED_MODULE_2__["AuthNoticeService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], AuthNoticeComponent.prototype, "type", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], AuthNoticeComponent.prototype, "message", void 0);
    AuthNoticeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-auth-notice',
            template: __webpack_require__(/*! raw-loader!./auth-notice.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/auth/auth-notice/auth-notice.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_auth__WEBPACK_IMPORTED_MODULE_2__["AuthNoticeService"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]])
    ], AuthNoticeComponent);
    return AuthNoticeComponent;
}());



/***/ }),

/***/ "./src/app/views/auth/auth.component.scss":
/*!************************************************!*\
  !*** ./src/app/views/auth/auth.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".kt-login.kt-login--v1 .kt-login__aside {\n  width: 605px;\n  padding: 3rem 3.5rem;\n  background-repeat: no-repeat;\n  background-size: cover; }\n  .kt-login.kt-login--v1 .kt-login__aside .kt-login__logo {\n    display: flex; }\n  .kt-login.kt-login--v1 .kt-login__aside .kt-login__title {\n    color: #ffffff;\n    font-size: 2rem;\n    font-weight: 500; }\n  .kt-login.kt-login--v1 .kt-login__aside .kt-login__subtitle {\n    font-size: 1.2rem;\n    font-weight: 200;\n    margin: 2.5rem 0 3.5rem 0;\n    color: rgba(255, 255, 255, 0.7); }\n  .kt-login.kt-login--v1 .kt-login__aside .kt-login__info {\n    display: flex;\n    justify-content: space-between; }\n  .kt-login.kt-login--v1 .kt-login__aside .kt-login__info .kt-login__menu > a {\n      text-decoration: none;\n      color: #fff;\n      margin-right: 2rem;\n      display: inline-block;\n      color: rgba(255, 255, 255, 0.7); }\n  .kt-login.kt-login--v1 .kt-login__aside .kt-login__info .kt-login__menu > a:hover {\n        color: #fff; }\n  .kt-login.kt-login--v1 .kt-login__aside .kt-login__info .kt-login__menu > a:hover:after {\n          border-bottom: 1px solid #fff;\n          opacity: 0.3; }\n  .kt-login.kt-login--v1 .kt-login__aside .kt-login__info .kt-login__menu > a:last-child {\n        margin-right: 0; }\n  .kt-login.kt-login--v1 .kt-login__aside .kt-login__info .kt-login__copyright {\n      color: rgba(255, 255, 255, 0.4); }\n  .kt-login.kt-login--v1 .kt-login__wrapper {\n  padding: 3rem 3rem;\n  background: #fff; }\n  .kt-login.kt-login--v1 .kt-login__wrapper .kt-login__head {\n    font-size: 1rem;\n    font-weight: 500;\n    text-align: right; }\n  .kt-login.kt-login--v1 .kt-login__wrapper .kt-login__head .kt-login__signup-label {\n      color: #74788d; }\n  .kt-login.kt-login--v1 .kt-login__wrapper .kt-login__head .kt-login__signup-link {\n      color: #5d78ff; }\n  .kt-login.kt-login--v1 .kt-login__wrapper .kt-login__body {\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    height: 100%; }\n  .kt-login.kt-login--v1 .kt-login__wrapper .kt-login__body .kt-login__form {\n      width: 100%;\n      max-width: 450px; }\n  .kt-login.kt-login--v1 .kt-login__wrapper .kt-login__body .kt-login__form .kt-login__title {\n        display: block;\n        text-align: center;\n        margin-bottom: 5rem;\n        text-decoration: none; }\n  .kt-login.kt-login--v1 .kt-login__wrapper .kt-login__body .kt-login__form .kt-login__title > h3 {\n          font-size: 2rem;\n          color: #67666e; }\n  .kt-login.kt-login--v1 .kt-login__wrapper .kt-login__body .kt-login__form .kt-form {\n        margin: 4rem auto; }\n  .kt-login.kt-login--v1 .kt-login__wrapper .kt-login__body .kt-login__form .kt-form .form-group {\n          margin: 0;\n          padding: 0;\n          margin: 0 auto; }\n  .kt-login.kt-login--v1 .kt-login__wrapper .kt-login__body .kt-login__form .kt-form .form-group .form-control {\n            border: none;\n            height: 50px;\n            margin-top: 1.25rem;\n            background-color: rgba(247, 247, 249, 0.7);\n            padding-left: 1.25rem;\n            padding-right: 1.25rem;\n            transition: background-color 0.3s ease; }\n  .kt-login.kt-login--v1 .kt-login__wrapper .kt-login__body .kt-login__form .kt-form .form-group .form-control:focus {\n              transition: background-color 0.3s ease;\n              background-color: #f7f7f9; }\n  .kt-login.kt-login--v1 .kt-login__wrapper .kt-login__body .kt-login__form .kt-login__actions {\n        display: flex;\n        justify-content: space-between;\n        align-items: center;\n        margin: 3rem 0; }\n  .kt-login.kt-login--v1 .kt-login__wrapper .kt-login__body .kt-login__form .kt-login__actions .kt-login__link-forgot {\n          font-weight: 400;\n          color: #74788d; }\n  .kt-login.kt-login--v1 .kt-login__wrapper .kt-login__body .kt-login__form .kt-login__actions .kt-login__link-forgot:hover {\n            color: #5d78ff; }\n  .kt-login.kt-login--v1 .kt-login__wrapper .kt-login__body .kt-login__form .kt-login__actions .kt-login__link-forgot:hover:after {\n              border-bottom: 1px solid #5d78ff;\n              opacity: 0.3; }\n  .kt-login.kt-login--v1 .kt-login__wrapper .kt-login__body .kt-login__form .kt-login__actions .kt-login__btn-secondary,\n        .kt-login.kt-login--v1 .kt-login__wrapper .kt-login__body .kt-login__form .kt-login__actions .kt-login__btn-primary {\n          font-weight: 500;\n          font-size: 1rem;\n          height: 50px;\n          padding-left: 2.75rem;\n          padding-right: 2.75rem; }\n  .kt-login.kt-login--v1 .kt-login__wrapper .kt-login__body .kt-login__form .kt-login__divider {\n        margin: 1rem 0 2rem 0; }\n  .kt-login.kt-login--v1 .kt-login__wrapper .kt-login__body .kt-login__form .kt-login__divider:not(:first-child):not(:last-child) {\n          font-weight: 400;\n          color: #b5b2c3;\n          font-size: 1rem; }\n  .kt-login.kt-login--v1 .kt-login__wrapper .kt-login__body .kt-login__form .kt-login__options {\n        display: flex;\n        justify-content: center;\n        justify-content: space-between;\n        max-width: 100%; }\n  .kt-login.kt-login--v1 .kt-login__wrapper .kt-login__body .kt-login__form .kt-login__options > a {\n          text-decoration: none;\n          flex: 1;\n          justify-content: center;\n          align-items: center;\n          display: flex; }\n  .kt-login.kt-login--v1 .kt-login__wrapper .kt-login__body .kt-login__form .kt-login__options > a:not(:last-child) {\n            margin: 0 1.5rem 0 0; }\n  @media (min-width: 1025px) {\n  .kt-login.kt-login--v1 .kt-login__aside {\n    flex: 1; } }\n  @media (max-width: 1024px) {\n  .kt-login.kt-login--v1 .kt-login__aside {\n    width: 100%;\n    height: auto;\n    padding: 2rem 1.5rem; }\n    .kt-login.kt-login--v1 .kt-login__aside .kt-login__logo {\n      margin-bottom: 2.5rem; }\n    .kt-login.kt-login--v1 .kt-login__aside .kt-login__info {\n      margin-top: 2rem; }\n    .kt-login.kt-login--v1 .kt-login__aside .kt-login__subtitle {\n      margin: 2rem 0; }\n  .kt-login.kt-login--v1 .kt-login__wrapper {\n    padding: 3rem 1.5rem; }\n    .kt-login.kt-login--v1 .kt-login__wrapper .kt-login__head {\n      padding-left: 2rem;\n      right: 2rem; }\n    .kt-login.kt-login--v1 .kt-login__wrapper .kt-login__body {\n      display: flex;\n      justify-content: center;\n      align-items: center;\n      margin-top: 5rem; }\n      .kt-login.kt-login--v1 .kt-login__wrapper .kt-login__body .kt-login__form .kt-login__options > a:not(:first-child):not(:last-child) {\n        margin: 0 0.8rem; } }\n  kt-auth, .kt-login {\n  height: 100%; }\n  kt-auth .mat-form-field, .kt-login .mat-form-field {\n    width: 100%; }\n  kt-auth .kt-spinner, .kt-login .kt-spinner {\n    padding-right: 3rem !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2thbWlsL1BSU19ORVdfVmVyc2lvbi9zcmMvYXNzZXRzL3Nhc3MvcGFnZXMvbG9naW4vbG9naW4tMS5zY3NzIiwiL2hvbWUva2FtaWwvUFJTX05FV19WZXJzaW9uL3NyYy9hc3NldHMvc2Fzcy9nbG9iYWwvY29tcG9uZW50cy90eXBvZ3JhcGh5L21peGlucy9fbGluay5zY3NzIiwiL2hvbWUva2FtaWwvUFJTX05FV19WZXJzaW9uL3NyYy9hc3NldHMvc2Fzcy9fY29uZmlnLnNjc3MiLCIvaG9tZS9rYW1pbC9QUlNfTkVXX1ZlcnNpb24vc3JjL2Fzc2V0cy9zYXNzL2dsb2JhbC9fbWl4aW5zLnNjc3MiLCIvaG9tZS9rYW1pbC9QUlNfTkVXX1ZlcnNpb24vc3JjL2FwcC92aWV3cy9hdXRoL2F1dGguY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBY0E7RUFHUSxZQUFZO0VBQ1osb0JBQW9CO0VBQ3BCLDRCQUE0QjtFQUM1QixzQkFBc0IsRUFBQTtFQU45QjtJQVNZLGFBQWEsRUFBQTtFQVR6QjtJQWFZLGNBQWM7SUFDZCxlQUFlO0lBQ2YsZ0JBQWdCLEVBQUE7RUFmNUI7SUFtQlksaUJBQWlCO0lBQ2pCLGdCQUFnQjtJQUNoQix5QkFBeUI7SUFDekIsK0JBQWdCLEVBQUE7RUF0QjVCO0lBMEJZLGFBQWE7SUFDYiw4QkFBOEIsRUFBQTtFQTNCMUM7TUErQm9CLHFCQUFxQjtNQUNyQixXQUFXO01BQ1gsa0JBQWtCO01BQ2xCLHFCQUFxQjtNQ3pDckMsK0JEMkNnRCxFQUFBO0VBcENwRDtRQ0pLLFdEd0MyRCxFQUFBO0VBcENoRTtVQ0RNLDZCRHFDMEQ7VUNwQzFELFlBQVksRUFBQTtFREFsQjtRQXVDd0IsZUFBZSxFQUFBO0VBdkN2QztNQTZDZ0IsK0JBQWdCLEVBQUE7RUE3Q2hDO0VBb0RRLGtCQUFrQjtFQUNsQixnQkFBZ0IsRUFBQTtFQXJEeEI7SUF5RFksZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixpQkFBaUIsRUFBQTtFQTNEN0I7TUE4RGdCLGNFeUNFLEVBQUE7RUZ2R2xCO01Ba0VnQixjRUhLLEVBQUE7RUYvRHJCO0lBd0VZLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsbUJBQW1CO0lBQ25CLFlBQVksRUFBQTtFQTNFeEI7TUErRWdCLFdBQVc7TUFDWCxnQkFBZ0IsRUFBQTtFQWhGaEM7UUFtRm9CLGNBQWM7UUFDZCxrQkFBa0I7UUFDbEIsbUJBQW1CO1FBQ25CLHFCQUFxQixFQUFBO0VBdEZ6QztVQXlGd0IsZUFBZTtVQUNmLGNBQWMsRUFBQTtFQTFGdEM7UUFnR29CLGlCQUFpQixFQUFBO0VBaEdyQztVQW1Hd0IsU0FBUztVQUNULFVBQVU7VUFDVixjQUFjLEVBQUE7RUFyR3RDO1lBd0c0QixZQUFZO1lBQ1osWUFBWTtZQUNaLG1CQUFtQjtZQUNuQiwwQ0FBOEI7WUFDOUIscUJBQXFCO1lBQ3JCLHNCQUFzQjtZQUN0QixzQ0FBc0MsRUFBQTtFQTlHbEU7Y0FpSGdDLHNDQUFzQztjQUN0Qyx5QkFBeUIsRUFBQTtFQWxIekQ7UUEwSG9CLGFBQWE7UUFDYiw4QkFBOEI7UUFDOUIsbUJBQW1CO1FBQ25CLGNBQWMsRUFBQTtFQTdIbEM7VUFnSXdCLGdCQUFnQjtVQ3ZJcEMsY0M4R2MsRUFBQTtFRnZHbEI7WUNKSyxjQ21FZ0IsRUFBQTtFRi9EckI7Y0NETSxnQ0NnRWU7Y0QvRGYsWUFBWSxFQUFBO0VEQWxCOztVQXNJd0IsZ0JBQWdCO1VBQ2hCLGVBQWU7VUFDZixZQUFZO1VBQ1oscUJBQXFCO1VBQ3JCLHNCQUFzQixFQUFBO0VBMUk5QztRQWdKb0IscUJBQXFCLEVBQUE7RUFoSnpDO1VBbUp3QixnQkFBZ0I7VUFDaEIsY0FBYztVQUNkLGVBQWUsRUFBQTtFQXJKdkM7UUEySm9CLGFBQWE7UUFDYix1QkFBdUI7UUFDdkIsOEJBQThCO1FBQzlCLGVBQWUsRUFBQTtFQTlKbkM7VUFpS3dCLHFCQUFxQjtVQUNyQixPQUFPO1VBQ1AsdUJBQXVCO1VBQ3ZCLG1CQUFtQjtVQUNuQixhQUFhLEVBQUE7RUFyS3JDO1lBd0s0QixvQkFBb0IsRUFBQTtFRzRJNUM7RUhwVEo7SUFtTFksT0FBTyxFQUFBLEVBQ1Y7RUc4Skw7RUhsVko7SUEwTFksV0FBVztJQUNYLFlBQVk7SUFDWixvQkFBb0IsRUFBQTtJQTVMaEM7TUErTGdCLHFCQUFxQixFQUFBO0lBL0xyQztNQW1NZ0IsZ0JBQWdCLEVBQUE7SUFuTWhDO01BdU1nQixjQUFjLEVBQUE7RUF2TTlCO0lBNk1ZLG9CQUFvQixFQUFBO0lBN01oQztNQWdOZ0Isa0JBQWtCO01BQ2xCLFdBQVcsRUFBQTtJQWpOM0I7TUFxTmdCLGFBQWE7TUFDYix1QkFBdUI7TUFDdkIsbUJBQW1CO01BQ25CLGdCQUFnQixFQUFBO01BeE5oQztRQThOZ0MsZ0JBQWdCLEVBQUEsRUFDbkI7RUkxTzdCO0VBQ0MsWUFBWSxFQUFBO0VBRGI7SUFJRSxXQUFXLEVBQUE7RUFKYjtJQVNFLDhCQUE4QixFQUFBIiwiZmlsZSI6InNyYy9hcHAvdmlld3MvYXV0aC9hdXRoLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy9cbi8vIFBhZ2UgLSBVc2VyIExvZ2luIDFcbi8vIFBhZ2VzIFNBU1MgZmlsZXMgYXJlIGNvbXBpbGVkIGludG8gc2VwYXJhdGUgY3NzIGZpbGVzXG4vL1xuXG5cblxuLy8gR2xvYmFsIGNvbmZpZ1xuQGltcG9ydCBcIi4uLy4uL2NvbmZpZ1wiO1xuXG4vLyBMYXlvdXQgY29uZmlnXG5AaW1wb3J0IFwiLi4vLi4vZ2xvYmFsL2xheW91dC9jb25maWcuc2Nzc1wiO1xuXG4vLyBMb2dpbiBCYXNlXG4ua3QtbG9naW4ua3QtbG9naW4tLXYxIHtcbiAgICAvLyBBc2lkZVxuICAgIC5rdC1sb2dpbl9fYXNpZGUge1xuICAgICAgICB3aWR0aDogNjA1cHg7XG4gICAgICAgIHBhZGRpbmc6IDNyZW0gMy41cmVtO1xuICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuXG4gICAgICAgIC5rdC1sb2dpbl9fbG9nbyB7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICB9XG5cbiAgICAgICAgLmt0LWxvZ2luX190aXRsZSB7XG4gICAgICAgICAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMnJlbTtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgICAgIH1cblxuICAgICAgICAua3QtbG9naW5fX3N1YnRpdGxlIHtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMS4ycmVtO1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDIwMDtcbiAgICAgICAgICAgIG1hcmdpbjogMi41cmVtIDAgMy41cmVtIDA7XG4gICAgICAgICAgICBjb2xvcjogcmdiYSgjZmZmLCAwLjcpO1xuICAgICAgICB9XG5cbiAgICAgICAgLmt0LWxvZ2luX19pbmZvIHtcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG5cbiAgICAgICAgICAgIC5rdC1sb2dpbl9fbWVudSB7XG4gICAgICAgICAgICAgICAgPiBhIHtcbiAgICAgICAgICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiAycmVtO1xuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG5cbiAgICAgICAgICAgICAgICAgICAgQGluY2x1ZGUga3QtbGluay1jb2xvcihyZ2JhKCNmZmYsIDAuNyksICNmZmYpO1xuXG4gICAgICAgICAgICAgICAgICAgICY6bGFzdC1jaGlsZCB7XG4gICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDA7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC5rdC1sb2dpbl9fY29weXJpZ2h0IHtcbiAgICAgICAgICAgICAgICBjb2xvcjogcmdiYSgjZmZmLCAwLjQpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLy8gV3JhcHBlclxuICAgIC5rdC1sb2dpbl9fd3JhcHBlciB7XG4gICAgICAgIHBhZGRpbmc6IDNyZW0gM3JlbTtcbiAgICAgICAgYmFja2dyb3VuZDogI2ZmZjtcblxuICAgICAgICAvLyBIZWFkXG4gICAgICAgIC5rdC1sb2dpbl9faGVhZCB7XG4gICAgICAgICAgICBmb250LXNpemU6IDFyZW07XG4gICAgICAgICAgICBmb250LXdlaWdodDogNTAwO1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogcmlnaHQ7XG5cbiAgICAgICAgICAgIC5rdC1sb2dpbl9fc2lnbnVwLWxhYmVsIHtcbiAgICAgICAgICAgICAgICBjb2xvcjoga3QtYmFzZS1jb2xvcihsYWJlbCwgMik7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC5rdC1sb2dpbl9fc2lnbnVwLWxpbmsge1xuICAgICAgICAgICAgICAgIGNvbG9yOiBrdC1icmFuZC1jb2xvcigpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgLy8gQm9keVxuICAgICAgICAua3QtbG9naW5fX2JvZHkge1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgICAgIGhlaWdodDogMTAwJTtcblxuICAgICAgICAgICAgLy8gRm9ybSBXcmFwcGVyXG4gICAgICAgICAgICAua3QtbG9naW5fX2Zvcm0ge1xuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgICAgIG1heC13aWR0aDogNDUwcHg7XG5cbiAgICAgICAgICAgICAgICAua3QtbG9naW5fX3RpdGxlIHtcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogNXJlbTtcbiAgICAgICAgICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuXG4gICAgICAgICAgICAgICAgICAgID4gaDMge1xuICAgICAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAycmVtO1xuICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6ICM2NzY2NmU7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAvLyBGb3JtXG4gICAgICAgICAgICAgICAgLmt0LWZvcm0ge1xuICAgICAgICAgICAgICAgICAgICBtYXJnaW46IDRyZW0gYXV0bztcblxuICAgICAgICAgICAgICAgICAgICAuZm9ybS1ncm91cCB7XG4gICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiAwO1xuICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luOiAwIGF1dG87XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC5mb3JtLWNvbnRyb2wge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlcjogbm9uZTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDUwcHg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogMS4yNXJlbTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKCNmN2Y3ZjksIDAuNyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAxLjI1cmVtO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDEuMjVyZW07XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdHJhbnNpdGlvbjogYmFja2dyb3VuZC1jb2xvciAwLjNzIGVhc2U7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAmOmZvY3VzIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHJhbnNpdGlvbjogYmFja2dyb3VuZC1jb2xvciAwLjNzIGVhc2U7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgLy8gQWN0aW9uXG4gICAgICAgICAgICAgICAgLmt0LWxvZ2luX19hY3Rpb25zIHtcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgICAgICAgICAgICBtYXJnaW46IDNyZW0gMDtcblxuICAgICAgICAgICAgICAgICAgICAua3QtbG9naW5fX2xpbmstZm9yZ290IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICAgICAgICAgICAgICAgICAgICBAaW5jbHVkZSBrdC1saW5rLWNvbG9yKGt0LWJhc2UtY29sb3IobGFiZWwsIDIpLCBrdC1icmFuZC1jb2xvcigpKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIC5rdC1sb2dpbl9fYnRuLXNlY29uZGFyeSxcbiAgICAgICAgICAgICAgICAgICAgLmt0LWxvZ2luX19idG4tcHJpbWFyeSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNTAwO1xuICAgICAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxcmVtO1xuICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiA1MHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAyLjc1cmVtO1xuICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMi43NXJlbTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIC8vIERpdmlkZXJcbiAgICAgICAgICAgICAgICAua3QtbG9naW5fX2RpdmlkZXIge1xuICAgICAgICAgICAgICAgICAgICBtYXJnaW46IDFyZW0gMCAycmVtIDA7XG5cbiAgICAgICAgICAgICAgICAgICAgJjpub3QoOmZpcnN0LWNoaWxkKTpub3QoOmxhc3QtY2hpbGQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb2xvcjogI2I1YjJjMztcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIC8vIE9wdGlvbnNcbiAgICAgICAgICAgICAgICAua3QtbG9naW5fX29wdGlvbnMge1xuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICAgICAgICAgICAgICBtYXgtd2lkdGg6IDEwMCU7XG5cbiAgICAgICAgICAgICAgICAgICAgPiBhIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZsZXg6IDE7XG4gICAgICAgICAgICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgICAgICAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAmOm5vdCg6bGFzdC1jaGlsZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbjogMCAxLjVyZW0gMCAwO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgQGluY2x1ZGUga3QtZGVza3RvcCB7XG4gICAgICAgIC8vIEFzaWRlXG4gICAgICAgIC5rdC1sb2dpbl9fYXNpZGUge1xuICAgICAgICAgICAgZmxleDogMTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIEBpbmNsdWRlIGt0LXRhYmxldC1hbmQtbW9iaWxlKCkge1xuICAgICAgICAvLyBBc2lkZVxuICAgICAgICAua3QtbG9naW5fX2FzaWRlIHtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgaGVpZ2h0OiBhdXRvO1xuICAgICAgICAgICAgcGFkZGluZzogMnJlbSAxLjVyZW07XG5cbiAgICAgICAgICAgIC5rdC1sb2dpbl9fbG9nbyB7XG4gICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMi41cmVtO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAua3QtbG9naW5fX2luZm8ge1xuICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IDJyZW07XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC5rdC1sb2dpbl9fc3VidGl0bGUge1xuICAgICAgICAgICAgICAgIG1hcmdpbjogMnJlbSAwO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgLy8gV3JhcHBlclxuICAgICAgICAua3QtbG9naW5fX3dyYXBwZXIge1xuICAgICAgICAgICAgcGFkZGluZzogM3JlbSAxLjVyZW07XG5cbiAgICAgICAgICAgIC5rdC1sb2dpbl9faGVhZCB7XG4gICAgICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAycmVtO1xuICAgICAgICAgICAgICAgIHJpZ2h0OiAycmVtO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAua3QtbG9naW5fX2JvZHkge1xuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiA1cmVtO1xuXG4gICAgICAgICAgICAgICAgLmt0LWxvZ2luX19mb3JtIHtcbiAgICAgICAgICAgICAgICAgICAgLmt0LWxvZ2luX19vcHRpb25zIHtcbiAgICAgICAgICAgICAgICAgICAgICAgID4gYSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJjpub3QoOmZpcnN0LWNoaWxkKTpub3QoOmxhc3QtY2hpbGQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luOiAwIDAuOHJlbTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59XG4iLCIvL1xuLy8gTGluayBNaXhpbnNcbi8vIFxuXG5cblxuQG1peGluIGt0LWxpbmstY29sb3IoJGRlZmF1bHQsICRob3Zlcikge1xuICAgXHRjb2xvcjogJGRlZmF1bHQ7XG5cbiAgICAmOmhvdmVyIHtcbiAgICBcdGNvbG9yOiAkaG92ZXI7XG5cbiAgICBcdCY6YWZ0ZXIge1xuICAgIFx0XHRib3JkZXItYm90dG9tOiAxcHggc29saWQgJGhvdmVyOyBcbiAgICBcdFx0b3BhY2l0eTogMC4zO1xuICAgIFx0fSBcbiAgICB9ICAgIFxufSIsIi8vXG4vLyBHbG9iYWwgQ29uZmlnXG4vL1xuXG5cblxuLy8gSW5pdCBnbG9iYWwgZnVuY3Rpb25zIGFuZCBtaXhpbnNcbkBpbXBvcnQgXCJnbG9iYWwvaW5pdFwiO1xuXG4vLyBMYXlvdXQgQnJlYWtwb2ludHMoYm9vdHN0cmFwIHJlc3BvbnNpdmUgYnJlYWtwb2ludHMpXG4vLyBEZWZpbmUgdGhlIG1pbmltdW0gYW5kIG1heGltdW0gZGltZW5zaW9ucyBhdCB3aGljaCB5b3VyIGxheW91dCB3aWxsIGNoYW5nZSwgYWRhcHRpbmcgdG8gZGlmZmVyZW50IHNjcmVlbiBzaXplcywgZm9yIHVzZSBpbiBtZWRpYSBxdWVyaWVzLlxuJGt0LW1lZGlhLWJyZWFrcG9pbnRzOiAoXG4gICAgLy8gRXh0cmEgc21hbGwgc2NyZWVuIC8gcGhvbmVcbiAgICB4czogMCxcblxuICAgIC8vIFNtYWxsIHNjcmVlbiAvIHBob25lXG4gICAgc206IDU3NnB4LFxuXG4gICAgLy8gTWVkaXVtIHNjcmVlbiAvIHRhYmxldFxuICAgIG1kOiA3NjhweCxcblxuICAgIC8vIExhcmdlIHNjcmVlbiAvIGRlc2t0b3BcbiAgICBsZzogMTAyNHB4LCAvLzEwMjRweCxcblxuICAgIC8vIEV4dHJhIGxhcmdlIHNjcmVlbiAvIHdpZGUgZGVza3RvcFxuICAgIHhsOiAxMzk5cHhcbikgIWRlZmF1bHQ7XG5cbi8vIEdsb2JhbCByb3VuZGVkIGJvcmRlciBtb2RlXG4ka3Qtcm91bmRlZDogdHJ1ZSAhZGVmYXVsdDtcblxuLy8gQm9yZGVyIFJhZGl1c1xuJGt0LWJvcmRlci1yYWRpdXM6IDRweCAhZGVmYXVsdDtcblxuLy8gQ29yZSBpY29uIGNvZGVzKGxpbmVhd2Vzb21lIGljb25zOiBodHRwczovL2ljb25zOC5jb20vbGluZS1hd2Vzb21lKVxuJGt0LWFjdGlvbi1pY29uczogKFxuICAgIGNsb3NlOiAnXFxmMTkxJyxcbiAgICBkb3duOiAnXFxmMTEwJyxcbiAgICB1cDogJ1xcZjExMycsXG4gICAgbGVmdDogJ1xcZjExMScsXG4gICAgcmlnaHQ6ICdcXGYxMTInLFxuICAgIHBsdXM6ICdcXGYyYzInLFxuICAgIG1pbnVzOiAnXFxmMjhlJ1xuKSAhZGVmYXVsdDtcblxuLy8gQ29yZSBib2xkIGljb24gY29kZXMobGluZWF3ZXNvbWUgaWNvbnM6IGh0dHBzOi8va2VlbnRoZW1lcy5jb20vbWV0cm9uaWMvcHJldmlldy9kZWZhdWx0L2NvbXBvbmVudHMvaWNvbnMvZmxhdGljb24uaHRtbClcbiRrdC1hY3Rpb24tYm9sZC1pY29uczogKFxuICAgIGRvd246ICdcXGYxYTMnLFxuICAgIHVwOiAnXFxmMWE1JyxcbiAgICBsZWZ0OiAnXFxmMWE0JyxcbiAgICByaWdodDogJ1xcZjE5ZCcsXG4gICAgY2xvc2U6ICdcXGYxYjInXG4pICFkZWZhdWx0O1xuXG4vLyBFbGV2YXRlIHNoYWRvd1xuJGt0LWVsZXZhdGUtc2hhZG93OiAwcHggMHB4IDEzcHggMHB4IHJnYmEoODIsNjMsMTA1LDAuMDUpICFkZWZhdWx0O1xuJGt0LWVsZXZhdGUtc2hhZG93LTI6IDBweCAwcHggMTNweCAwcHggcmdiYSg4Miw2MywxMDUsMC4xKSAhZGVmYXVsdDtcblxuLy8gRHJvcGRvd24gc2hhZG93XG4ka3QtZHJvcGRvd24tc2hhZG93OiAwcHggMHB4IDUwcHggMHB4IHJnYmEoODIsNjMsMTA1LCAwLjE1KSAhZGVmYXVsdDtcblxuLy8gQ3VzdG9tIHNjcm9sbGJhciBjb2xvclxuJGt0LXNjcm9sbC1jb2xvcjogZGFya2VuKCNlYmVkZjIsIDYlKSAhZGVmYXVsdDtcblxuLy8gVHJhbnNpdGlvblxuJGt0LXRyYW5zaXRpb246IGFsbCAwLjNzICFkZWZhdWx0O1xuXG4vLyBNb2RhbCBaLWluZGV4XG4ka3QtbW9kYWwtemluZGV4OiAxMDUwICFkZWZhdWx0O1xuXG4vLyBkcm9wZG93biBaLWluZGV4XG4ka3QtZHJvcGRvd24temluZGV4OiA5NSAhZGVmYXVsdDtcblxuLy8gU3RhdGUgQ29sb3JcbiRrdC1zdGF0ZS1jb2xvcnM6IChcbiAgICAvLyBNZXRyb25pYyBzdGF0ZXNcbiAgICBicmFuZDogKFxuICAgICAgICBiYXNlOiAjNWQ3OGZmLFxuICAgICAgICBpbnZlcnNlOiAjZmZmZmZmXG4gICAgKSxcbiAgICBsaWdodDogKFxuICAgICAgICBiYXNlOiAjZmZmZmZmLFxuICAgICAgICBpbnZlcnNlOiAjMjgyYTNjXG4gICAgKSxcbiAgICBkYXJrOiAoXG4gICAgICAgIGJhc2U6ICMyODJhM2MsXG4gICAgICAgIGludmVyc2U6ICNmZmZmZmZcbiAgICApLFxuXG4gICAgLy8gQm9vdHN0cmFwIHN0YXRlc1xuICAgIHByaW1hcnk6IChcbiAgICAgICAgYmFzZTogIzU4NjdkZCxcbiAgICAgICAgaW52ZXJzZTogI2ZmZmZmZlxuICAgICksXG4gICAgc3VjY2VzczogKFxuICAgICAgICBiYXNlOiAjMGFiYjg3LCAvLzFkYzliNyxcbiAgICAgICAgaW52ZXJzZTogI2ZmZmZmZlxuICAgICksXG4gICAgaW5mbzogKFxuICAgICAgICBiYXNlOiAjNTU3OGViLFxuICAgICAgICBpbnZlcnNlOiAjZmZmZmZmXG4gICAgKSxcbiAgICB3YXJuaW5nOiAoXG4gICAgICAgIGJhc2U6ICNmZmI4MjIsXG4gICAgICAgIGludmVyc2U6ICMxMTExMTFcbiAgICApLFxuICAgIGRhbmdlcjogKFxuICAgICAgICBiYXNlOiAjZmQzOTdhLFxuICAgICAgICBpbnZlcnNlOiAjZmZmZmZmXG4gICAgKVxuKSAhZGVmYXVsdDtcblxuXG4vLyBCYXNlIGNvbG9yc1xuJGt0LWJhc2UtY29sb3JzOiAoXG4gICAgbGFiZWw6IChcbiAgICAgICAgMTogI2EyYTViOSxcbiAgICAgICAgMjogIzc0Nzg4ZCxcbiAgICAgICAgMzogIzU5NWQ2ZSxcbiAgICAgICAgNDogIzQ4NDY1YlxuICAgICksXG4gICAgc2hhcGU6IChcbiAgICAgICAgMTogI2YwZjNmZixcbiAgICAgICAgMjogI2U4ZWNmYSxcbiAgICAgICAgMzogIzkzYTJkZCxcbiAgICAgICAgNDogIzY0NmM5YVxuICAgICksXG4gICAgZ3JleTogKFxuICAgICAgICAxOiAjZjdmOGZhLCAvLyNmNGY1ZjhcbiAgICAgICAgMjogI2ViZWRmMixcbiAgICAgICAgMzogZGFya2VuKCNlYmVkZjIsIDMlKSxcbiAgICAgICAgNDogZGFya2VuKCNlYmVkZjIsIDYlKVxuICAgIClcbikgIWRlZmF1bHQ7XG5cbi8vIFNvY2lhbCBuZXR3b3JrIGNvbG9ycyhzZWU6IGh0dHBzOi8vYnJhbmRjb2xvcnMubmV0LylcbiRrdC1zb2NpYWwtY29sb3JzOiAoXG4gICAgZmFjZWJvb2s6IChcbiAgICAgICAgYmFzZTogIzNiNTk5OCxcbiAgICAgICAgaW52ZXJzZTogI2ZmZmZmZlxuICAgICksXG4gICAgZ29vZ2xlOiAoXG4gICAgICAgIGJhc2U6ICNkYzRlNDEsXG4gICAgICAgIGludmVyc2U6ICNmZmZmZmZcbiAgICApLFxuICAgIHR3aXR0ZXI6IChcbiAgICAgICAgYmFzZTogIzRBQjNGNCxcbiAgICAgICAgaW52ZXJzZTogI2ZmZmZmZlxuICAgICksXG4gICAgaW5zdGFncmFtOiAoXG4gICAgICAgIGJhc2U6ICM1MTdmYTQsXG4gICAgICAgIGludmVyc2U6ICNmZmZmZmZcbiAgICApLFxuICAgIHlvdXR1YmU6IChcbiAgICAgICAgYmFzZTogI2IzMTIxNyxcbiAgICAgICAgaW52ZXJzZTogI2ZmZmZmZlxuICAgICksXG4gICAgbGlua2VkaW46IChcbiAgICAgICAgYmFzZTogIzAwNzdiNSxcbiAgICAgICAgaW52ZXJzZTogI2ZmZmZmZlxuICAgICksXG4gICAgc2t5cGU6IChcbiAgICAgICAgYmFzZTogIzAwYWZmMCxcbiAgICAgICAgaW52ZXJzZTogI2ZmZmZmZlxuICAgIClcbikgIWRlZmF1bHQ7XG5cbi8vIFJvb3QgRm9udCBTZXR0aW5nc1xuJGt0LWZvbnQtZmFtaWxpZXM6IChcbiAgICByZWd1bGFyOiB1bnF1b3RlKCdQb3BwaW5zLCBIZWx2ZXRpY2EsIHNhbnMtc2VyaWYnKSxcbiAgICBoZWFkaW5nOiB1bnF1b3RlKCdQb3BwaW5zLCBIZWx2ZXRpY2EsIHNhbnMtc2VyaWYnKVxuKSAhZGVmYXVsdDtcblxuLy8gUm9vdCBGb250IFNldHRpbmdzXG4ka3QtZm9udC1zaXplOiAoXG4gICAgc2l6ZTogKFxuICAgICAgICBkZXNrdG9wOiAxM3B4LFxuICAgICAgICB0YWJsZXQ6IDEycHgsXG4gICAgICAgIG1vYmlsZTogMTJweFxuICAgICksXG4gICAgd2VpZ2h0OiAzMDBcbikgIWRlZmF1bHQ7XG5cbi8vIEdlbmVyYWwgTGluayBTZXR0aW5nc1xuJGt0LWZvbnQtY29sb3I6IChcbiAgICB0ZXh0OiAjNjQ2YzlhLFxuICAgIGxpbms6IChcbiAgICAgICAgZGVmYXVsdDoga3Qtc3RhdGUtY29sb3IoYnJhbmQpLFxuICAgICAgICBob3ZlcjogZGFya2VuKGt0LXN0YXRlLWNvbG9yKGJyYW5kKSwgNiUpXG4gICAgKVxuKSAhZGVmYXVsdDtcblxuLy8gUG9ydGxldCBzZXR0aW5nc1xuJGt0LXBvcnRsZXQ6IChcbiAgICBtaW4taGVpZ2h0OiAoXG4gICAgICAgIGRlZmF1bHQ6IChcbiAgICAgICAgICAgIGRlc2t0b3A6IDYwcHgsXG4gICAgICAgICAgICBtb2JpbGU6IDUwcHhcbiAgICAgICAgKSxcbiAgICAgICAgc206IChcbiAgICAgICAgICAgIGRlc2t0b3A6IDUwcHgsXG4gICAgICAgICAgICBtb2JpbGU6IDQwcHhcbiAgICAgICAgKSxcbiAgICAgICAgbGc6IChcbiAgICAgICAgICAgIGRlc2t0b3A6IDgwcHgsXG4gICAgICAgICAgICBtb2JpbGU6IDYwcHhcbiAgICAgICAgKSxcbiAgICAgICAgeGw6IChcbiAgICAgICAgICAgIGRlc2t0b3A6IDEwMHB4LFxuICAgICAgICAgICAgbW9iaWxlOiA4MHB4XG4gICAgICAgIClcbiAgICApLFxuICAgIHNwYWNlOiAoXG4gICAgICAgIGRlc2t0b3A6IDI1cHgsXG4gICAgICAgIG1vYmlsZTogMTVweFxuICAgICksXG4gICAgYm90dG9tLXNwYWNlOiAoXG4gICAgICAgIGRlc2t0b3A6IDIwcHgsXG4gICAgICAgIG1vYmlsZTogMjBweFxuICAgICksXG4gICAgYm9yZGVyLWNvbG9yOiBrdC1iYXNlLWNvbG9yKGdyZXksIDIpLFxuICAgIGJnLWNvbG9yOiAjZmZmZmZmLFxuICAgIHNoYWRvdzogMHB4IDBweCAzMHB4IDBweCByZ2JhKDgyLDYzLDEwNSwwLjA1KVxuKSAhZGVmYXVsdDtcblxuLy8gUGFnZSBwYWRkaW5nXG4ka3QtcGFnZS1wYWRkaW5nOiAoXG5cdGRlc2t0b3A6IDI1cHgsXG5cdG1vYmlsZTogMTVweFxuKSAhZGVmYXVsdDtcblxuLy8gUGFnZSBjb250YWluZXIgd2lkdGhcbiRrdC1wYWdlLWNvbnRhaW5lci13aWR0aDogMTM4MHB4ICFkZWZhdWx0O1xuXG4vLyBDdXN0b20gU2Nyb2xsKFBlcmZlY3QgU2Nyb2xsYmFyKSBzaXplXG4ka3QtY3VzdG9tLXNjcm9sbC1zaXplOiA0cHggIWRlZmF1bHQ7XG4iLCIvL1xuLy8gR2xvYmFsIE1peGluc1xuLy9cblxuXG5cblxuQG1peGluIGt0LWNsZWFyZml4KCkge1xuXHQmOmJlZm9yZSxcblx0JjphZnRlciB7XG5cdFx0Y29udGVudDogXCIgXCI7IC8vIDFcblx0XHRkaXNwbGF5OiB0YWJsZTsgLy8gMlxuXHR9XG5cdCY6YWZ0ZXIge1xuXHRcdGNsZWFyOiBib3RoO1xuXHR9XG59XG5cbkBtaXhpbiBrdC1idXR0b24tcmVzZXQoKSB7XG4gICAgYXBwZWFyYW5jZTogbm9uZTtcbiAgICBib3gtc2hhZG93OiBub25lO1xuICAgIGJvcmRlci1yYWRpdXM6IG5vbmU7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBiYWNrZ3JvdW5kOiBub25lO1xuICAgIG91dGxpbmU6IG5vbmUgIWltcG9ydGFudDtcbiAgICBtYXJnaW46IDA7XG4gICAgcGFkZGluZzogMDtcbn1cblxuQG1peGluIGt0LWlucHV0LXJlc2V0KCkge1xuXHRib3JkZXI6IDA7XG5cdGJhY2tncm91bmQ6IG5vbmU7XG5cdG91dGxpbmU6IG5vbmUgIWltcG9ydGFudDtcblx0Ym94LXNoYWRvdzogbm9uZTtcbn1cblxuQG1peGluIGt0LWJ0bi1yZXNldCgpIHtcbiAgICBib3JkZXI6IDA7XG4gICAgYmFja2dyb3VuZDogbm9uZTtcbiAgICBvdXRsaW5lOiBub25lICFpbXBvcnRhbnQ7XG4gICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICBvdXRsaW5lOiBub25lICFpbXBvcnRhbnQ7XG59XG5cbkBtaXhpbiBrdC1maXgtZml4ZWQtcG9zaXRpb24tbGFncygpIHtcblx0Ly8gd2Via2l0IGhhY2sgZm9yIHNtb290aCBmb250IHZpZXcgb24gZml4ZWQgcG9zaXRpb25lZCBlbGVtZW50c1xuXHQtd2Via2l0LWJhY2tmYWNlLXZpc2liaWxpdHk6aGlkZGVuO1xuXHRiYWNrZmFjZS12aXNpYmlsaXR5OmhpZGRlbjtcbn1cblxuQG1peGluIGt0LWZpeC1hbmltYXRpb24tbGFncygpIHtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVooMCk7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm0tc3R5bGU6IHByZXNlcnZlLTNkO1xufVxuXG5AbWl4aW4ga3QtYXR0cigkYXR0ciwgJHZhbHVlLCAkaW1wb3J0YW50OiAnJykge1xuICAgIEBpZiAgJHZhbHVlICE9IG51bGwgIHtcbiAgICAgICAgI3skYXR0cn06ICN7JHZhbHVlfSAjeyRpbXBvcnRhbnR9O1xuICAgIH1cbn1cblxuQG1peGluIGt0LWhhY2staWUge1xuXHRAbWVkaWEgc2NyZWVuXFwwIHtcblx0XHRAY29udGVudDtcblx0fVxufVxuXG5AbWl4aW4ga3QtaGFjay1lZGdlLWFsbCB7XG5cdEBzdXBwb3J0cyAoLW1zLWltZS1hbGlnbjphdXRvKSB7XG5cdFx0QGNvbnRlbnQ7XG5cdH1cbn1cblxuQG1peGluIGt0LXJvdW5kZWQge1xuICAgIEBpZiAka3Qtcm91bmRlZCA9PSB0cnVlIHtcbiAgICAgICBAY29udGVudDtcbiAgICB9XG59XG5cbkBtaXhpbiBrdC1ub3Qtcm91bmRlZCB7XG4gICAgQGlmICRrdC1yb3VuZGVkID09IGZhbHNlIHtcbiAgICAgICAgQGNvbnRlbnQ7XG4gICAgfVxufVxuXG4vLyBJbnB1dCBwbGFjZWhvbGRlciBjb2xvclxuQG1peGluIGt0LWlucHV0LXBsYWNlaG9sZGVyKCRjb2xvcikge1xuICAgICY6Oi1tb3otcGxhY2Vob2xkZXIge1xuICAgICAgICBjb2xvcjogJGNvbG9yO1xuICAgICAgICBvcGFjaXR5OiAxO1xuICAgIH1cbiAgICAvLyBTZWUgaHR0cHM6Ly9naXRodWIuY29tL3R3YnMvYm9vdHN0cmFwL3B1bGwvMTE1MjZcbiAgICAmOi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7XG4gICAgICAgIGNvbG9yOiAkY29sb3I7XG4gICAgfVxuICAgIC8vIEludGVybmV0IEV4cGxvcmVyIDEwK1xuICAgICY6Oi13ZWJraXQtaW5wdXQtcGxhY2Vob2xkZXIge1xuICAgICAgICBjb2xvcjogJGNvbG9yO1xuICAgIH1cbiAgICAvLyBTYWZhcmkgYW5kIENocm9tZVxufVxuXG5AbWl4aW4ga3QtaG92ZXItdHJhbnNpdGlvbiB7XG4gICAgdHJhbnNpdGlvbjogJGt0LXRyYW5zaXRpb247XG5cbiAgICAmOmhvdmVyIHtcbiAgICAgICAgdHJhbnNpdGlvbjogJGt0LXRyYW5zaXRpb247XG4gICAgfVxufVxuXG5AbWl4aW4ga3QtdHJhbnNpdGlvbiB7XG4gICAgdHJhbnNpdGlvbjogJGt0LXRyYW5zaXRpb247XG59XG5cbi8vIEljb24gU2l6aW5nXG4vLyBTQVNTIE1hcDogIChsaW5lYXdlc29tZTogMS4xcmVtLCBmb250YXdlc29tZTogMS4ycmVtLCBmbGF0aWNvbjogMS4xcmVtKVxuQG1peGluIGt0LWljb25zLXNpemUoJGNvbmZpZykge1xuICAgIC8vIExpbmVhd2Vzb21lXG4gICAgW2NsYXNzXj1cImxhLVwiXSxcbiAgICBbY2xhc3MqPVwiIGxhLVwiXSB7XG4gICAgICAgIGZvbnQtc2l6ZToga3QtZ2V0KCRjb25maWcsIGxpbmVhd2Vzb21lKTtcbiAgICB9XG5cbiAgICAvLyBGb250YXdlc29tZVxuICAgIFtjbGFzc149XCJmYS1cIl0sXG4gICAgW2NsYXNzKj1cIiBmYS1cIl0ge1xuICAgICAgICBmb250LXNpemU6IGt0LWdldCgkY29uZmlnLCBmb250YXdlc29tZSk7XG4gICAgfVxuXG4gICAgLy8gRmxhdGljb25cbiAgICBbY2xhc3NePVwiZmxhdGljb24tXCJdLFxuICAgIFtjbGFzcyo9XCIgZmxhdGljb24tXCJdLFxuICAgIFtjbGFzc149XCJmbGF0aWNvbjItXCJdLFxuICAgIFtjbGFzcyo9XCIgZmxhdGljb24yLVwiXSB7XG4gICAgICAgIGZvbnQtc2l6ZToga3QtZ2V0KCRjb25maWcsIGZsYXRpY29uKTtcbiAgICB9XG59XG5cbi8vIEljb24gYXR0clxuLy8gU0FTUyBNYXA6ICAobGluZWF3ZXNvbWU6IDEuMXJlbSwgZm9udGF3ZXNvbWU6IDEuMnJlbSwgZmxhdGljb246IDEuMXJlbSlcbkBtaXhpbiBrdC1pY29ucy1zdHlsZSgkYXR0ciwgJGNvbmZpZykge1xuICAgIC8vIGxpbmVhd2Vzb21lXG4gICAgW2NsYXNzXj1cImxhLVwiXSxcbiAgICBbY2xhc3MqPVwiIGxhLVwiXSB7XG4gICAgICAgICN7JGF0dHJ9OiBrdC1nZXQoJGNvbmZpZywgbGluZWF3ZXNvbWUpO1xuICAgIH1cblxuICAgIC8vIEZvbnRhd2Vzb21lXG4gICAgW2NsYXNzXj1cImZhLVwiXSxcbiAgICBbY2xhc3MqPVwiIGZhLVwiXSB7XG4gICAgICAgICN7JGF0dHJ9OiBrdC1nZXQoJGNvbmZpZywgZm9udGF3ZXNvbWUpO1xuICAgIH1cblxuICAgIC8vIEZsYXRpY29uXG4gICAgW2NsYXNzXj1cImZsYXRpY29uLVwiXSxcbiAgICBbY2xhc3MqPVwiIGZsYXRpY29uLVwiXSxcbiAgICBbY2xhc3NePVwiZmxhdGljb24yLVwiXSxcbiAgICBbY2xhc3MqPVwiIGZsYXRpY29uMi1cIl0ge1xuICAgICAgICAjeyRhdHRyfToga3QtZ2V0KCRjb25maWcsIGZsYXRpY29uKTtcbiAgICB9XG59XG5cbi8vIFNBU1MgTWFwOiAgKGxpbmVhd2Vzb21lOiAxLjFyZW0sIGZvbnRhd2Vzb21lOiAxLjJyZW0sIGZsYXRpY29uOiAxLjFyZW0pXG5AbWl4aW4ga3QtaWNvbnMge1xuICAgIFtjbGFzc149XCJsYS1cIl0sXG4gICAgW2NsYXNzKj1cIiBsYS1cIl0sXG4gICAgW2NsYXNzXj1cImZhLVwiXSxcbiAgICBbY2xhc3MqPVwiIGZhLVwiXSxcbiAgICBbY2xhc3NePVwiZmxhdGljb24tXCJdLFxuICAgIFtjbGFzcyo9XCIgZmxhdGljb24tXCJdLFxuICAgIFtjbGFzc149XCJmbGF0aWNvbjItXCJdLFxuICAgIFtjbGFzcyo9XCIgZmxhdGljb24yLVwiXSB7XG4gICAgICAgIEBjb250ZW50O1xuICAgIH1cbn1cblxuLy8gTGluZWF3ZXNvbWUgaWNvblxuQG1peGluIGt0LWxhLWljb24oJGljb24pIHtcbiAgICBmb250LWZhbWlseTogXCJMaW5lQXdlc29tZVwiO1xuICAgIHRleHQtZGVjb3JhdGlvbjogaW5oZXJpdDtcbiAgICB0ZXh0LXJlbmRlcmluZzogb3B0aW1pemVMZWdpYmlsaXR5O1xuICAgIHRleHQtdHJhbnNmb3JtOiBub25lO1xuICAgIC1tb3otb3N4LWZvbnQtc21vb3RoaW5nOiBncmF5c2NhbGU7XG4gICAgLXdlYmtpdC1mb250LXNtb290aGluZzogYW50aWFsaWFzZWQ7XG4gICAgZm9udC1zbW9vdGhpbmc6IGFudGlhbGlhc2VkO1xuXG4gICAgJjpiZWZvcmUge1xuICAgICAgICBjb250ZW50OiBcIiN7JGljb259XCI7XG4gICAgfVxufVxuXG5AbWl4aW4ga3QtbGEtaWNvbi1jaGFuZ2UoJGljb24pIHtcbiAgICAmOmJlZm9yZSB7XG4gICAgICAgIGNvbnRlbnQ6IFwiI3skaWNvbn1cIjtcbiAgICB9XG59XG5cbkBtaXhpbiBrdC1mbGF0aWNvbjItaWNvbigkaWNvbikge1xuICAgIGZvbnQtZmFtaWx5OiBGbGF0aWNvbjI7XG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgZm9udC12YXJpYW50OiBub3JtYWw7XG4gICAgbGluZS1oZWlnaHQ6IDE7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBpbmhlcml0O1xuICAgIHRleHQtcmVuZGVyaW5nOiBvcHRpbWl6ZUxlZ2liaWxpdHk7XG4gICAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XG4gICAgLW1vei1vc3gtZm9udC1zbW9vdGhpbmc6IGdyYXlzY2FsZTtcbiAgICAtd2Via2l0LWZvbnQtc21vb3RoaW5nOiBhbnRpYWxpYXNlZDtcbiAgICBmb250LXNtb290aGluZzogYW50aWFsaWFzZWQ7XG4gICAgY29udGVudDogXCIjeyRpY29ufVwiO1xufVxuXG4vLyBMaW5lYXdlc29tZSBpY29uXG5AbWl4aW4ga3QtbGEtaWNvbi1zZWxmKCRpY29uKSB7XG4gICAgZm9udC1mYW1pbHk6IFwiTGluZUF3ZXNvbWVcIjtcbiAgICB0ZXh0LWRlY29yYXRpb246IGluaGVyaXQ7XG4gICAgdGV4dC1yZW5kZXJpbmc6IG9wdGltaXplTGVnaWJpbGl0eTtcbiAgICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcbiAgICAtbW96LW9zeC1mb250LXNtb290aGluZzogZ3JheXNjYWxlO1xuICAgIC13ZWJraXQtZm9udC1zbW9vdGhpbmc6IGFudGlhbGlhc2VkO1xuICAgIGZvbnQtc21vb3RoaW5nOiBhbnRpYWxpYXNlZDtcbiAgICBjb250ZW50OiBcIiN7JGljb259XCI7XG59XG5cbi8vIENsb3NlIGljb25cbkBtaXhpbiBrdC1jbG9zZS1pY29uKCRzZWxmOm51bGwpIHtcbiAgICBAaWYgJHNlbGYgPT0gdHJ1ZSB7XG4gICAgICAgIEBpbmNsdWRlIGt0LWxhLWljb24tc2VsZigga3QtZ2V0KCRrdC1hY3Rpb24taWNvbnMsIGNsb3NlKSApO1xuICAgIH0gQGVsc2Uge1xuICAgICAgICBAaW5jbHVkZSBrdC1sYS1pY29uKCBrdC1nZXQoJGt0LWFjdGlvbi1pY29ucywgY2xvc2UpICk7XG4gICAgfVxufVxuXG4vLyBBcnJvdyBpY29uXG5AbWl4aW4ga3QtYXJyb3ctaWNvbigkZGlyLCAkc2VsZjpudWxsKSB7XG4gICAgQGlmICRkaXIgPT0gZG93biB7XG4gICAgICAgIEBpZiAkc2VsZiA9PSB0cnVlIHtcbiAgICAgICAgICAgIEBpbmNsdWRlIGt0LWxhLWljb24tc2VsZigga3QtZ2V0KCRrdC1hY3Rpb24taWNvbnMsIGRvd24pICk7XG4gICAgICAgIH0gQGVsc2Uge1xuICAgICAgICAgICAgQGluY2x1ZGUga3QtbGEtaWNvbigga3QtZ2V0KCRrdC1hY3Rpb24taWNvbnMsIGRvd24pICk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBAaWYgJGRpciA9PSB1cCB7XG4gICAgICAgIEBpZiAkc2VsZiA9PSB0cnVlIHtcbiAgICAgICAgICAgIEBpbmNsdWRlIGt0LWxhLWljb24tc2VsZigga3QtZ2V0KCRrdC1hY3Rpb24taWNvbnMsIHVwKSApO1xuICAgICAgICB9IEBlbHNlIHtcbiAgICAgICAgICAgIEBpbmNsdWRlIGt0LWxhLWljb24oIGt0LWdldCgka3QtYWN0aW9uLWljb25zLCB1cCkgKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIEBpZiAkZGlyID09IGxlZnQge1xuICAgICAgICBAaWYgJHNlbGYgPT0gdHJ1ZSB7XG4gICAgICAgICAgICBAaW5jbHVkZSBrdC1sYS1pY29uLXNlbGYoIGt0LWdldCgka3QtYWN0aW9uLWljb25zLCBsZWZ0KSApO1xuICAgICAgICB9IEBlbHNlIHtcbiAgICAgICAgICAgIEBpbmNsdWRlIGt0LWxhLWljb24oIGt0LWdldCgka3QtYWN0aW9uLWljb25zLCBsZWZ0KSApO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgQGlmICRkaXIgPT0gcmlnaHQge1xuICAgICAgICBAaWYgJHNlbGYgPT0gdHJ1ZSB7XG4gICAgICAgICAgICBAaW5jbHVkZSBrdC1sYS1pY29uLXNlbGYoIGt0LWdldCgka3QtYWN0aW9uLWljb25zLCByaWdodCkgKTtcbiAgICAgICAgfSBAZWxzZSB7XG4gICAgICAgICAgICBAaW5jbHVkZSBrdC1sYS1pY29uKCBrdC1nZXQoJGt0LWFjdGlvbi1pY29ucywgcmlnaHQpICk7XG4gICAgICAgIH1cbiAgICB9XG59XG5cbi8vIFN2ZyBpY29uIGNvbG9yXG5AbWl4aW4ga3Qtc3ZnLWljb24tY29sb3IoJGNvbG9yKSB7XG4gICAgZyB7XG4gICAgICAgIFtmaWxsXSB7XG4gICAgICAgICAgICB0cmFuc2l0aW9uOiBmaWxsIDAuM3MgZWFzZTtcbiAgICAgICAgICAgIGZpbGw6ICRjb2xvcjtcbiAgICAgICAgfVxuICAgIH1cblxuICAgICY6aG92ZXIge1xuICAgICAgICBnIHtcbiAgICAgICAgICAgIFtmaWxsXSB7XG4gICAgICAgICAgICAgICAgdHJhbnNpdGlvbjogZmlsbCAwLjNzIGVhc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59XG5cbi8vIEJyZWFrcG9pbnQgbWl4aW5zXG4vLyBMYXlvdXQgQnJlYWtwb2ludHNcbi8vIERlZmluZSB0aGUgbWluaW11bSBhbmQgbWF4aW11bSBkaW1lbnNpb25zIGF0IHdoaWNoIHlvdXIgbGF5b3V0IHdpbGwgY2hhbmdlLCBhZGFwdGluZyB0byBkaWZmZXJlbnQgc2NyZWVuIHNpemVzLCBmb3IgdXNlIGluIG1lZGlhIHF1ZXJpZXMuXG5cbkBtaXhpbiBrdC1tZWRpYS1iZWxvdygkd2lkdGgpIHtcbiAgICBAbWVkaWEgKG1heC13aWR0aDogI3trdC1tZWRpYS1icmVha3BvaW50KCR3aWR0aCl9KSB7XG4gICAgICAgIEBjb250ZW50O1xuICAgIH1cbn1cblxuQG1peGluIGt0LW1lZGlhLWFib3ZlKCR3aWR0aCkge1xuICAgIEBtZWRpYSAobWluLXdpZHRoOiAje2t0LW1lZGlhLWJyZWFrcG9pbnQoJHdpZHRoKSArIDFweH0pIHtcbiAgICAgICAgQGNvbnRlbnQ7XG4gICAgfVxufVxuXG5AbWl4aW4ga3QtbWVkaWEtcmFuZ2UoJGZyb20sICR0bykge1xuICAgIEBtZWRpYSAobWluLXdpZHRoOiAje2t0LW1lZGlhLWJyZWFrcG9pbnQoJGZyb20pICsgMXB4fSkgYW5kIChtYXgtd2lkdGg6ICN7a3QtbWVkaWEtYnJlYWtwb2ludCgkdG8pfSkge1xuICAgICAgICBAY29udGVudDtcbiAgICB9XG59XG5cbkBtaXhpbiBrdC1taW5pbWFsLWRlc2t0b3Age1xuICAgIEBtZWRpYSAobWluLXdpZHRoOiAje2t0LW1lZGlhLWJyZWFrcG9pbnQobGcpICsgMXB4fSkgYW5kIChtYXgtd2lkdGg6ICN7a3QtbWVkaWEtYnJlYWtwb2ludCh4bCl9KSB7XG4gICAgICAgIEBjb250ZW50O1xuICAgIH1cbn1cblxuQG1peGluIGt0LW1pbmltYWwtZGVza3RvcC1hbmQtYmVsb3cge1xuICAgIEBtZWRpYSAobWF4LXdpZHRoOiAje2t0LW1lZGlhLWJyZWFrcG9pbnQoeGwpfSkge1xuICAgICAgICBAY29udGVudDtcbiAgICB9XG59XG5cbkBtaXhpbiBrdC1kZXNrdG9wIHtcbiAgICBAbWVkaWEgKG1pbi13aWR0aDogI3trdC1tZWRpYS1icmVha3BvaW50KGxnKSArIDFweH0pIHtcbiAgICAgICAgQGNvbnRlbnQ7XG4gICAgfVxufVxuXG5AbWl4aW4ga3QtZGVza3RvcC14bCB7XG4gICAgQG1lZGlhIChtaW4td2lkdGg6ICN7a3QtbWVkaWEtYnJlYWtwb2ludCh4bCkgKyAxcHh9KSB7XG4gICAgICAgIEBjb250ZW50O1xuICAgIH1cbn1cblxuQG1peGluIGt0LWRlc2t0b3AteHhsIHtcbiAgICBAbWVkaWEgKG1pbi13aWR0aDogI3trdC1tZWRpYS1icmVha3BvaW50KHh4bCkgKyAxcHh9KSB7XG4gICAgICAgIEBjb250ZW50O1xuICAgIH1cbn1cblxuQG1peGluIGt0LWRlc2t0b3AtYW5kLXRhYmxldCB7XG4gICAgQG1lZGlhIChtaW4td2lkdGg6ICN7a3QtbWVkaWEtYnJlYWtwb2ludChtZCkgKyAxcHh9KSB7XG4gICAgICAgIEBjb250ZW50O1xuICAgIH1cbn1cblxuQG1peGluIGt0LXRhYmxldCB7XG4gICAgQG1lZGlhIChtaW4td2lkdGg6ICN7a3QtbWVkaWEtYnJlYWtwb2ludChtZCkgKyAxcHh9KSBhbmQgKG1heC13aWR0aDogI3trdC1tZWRpYS1icmVha3BvaW50KGxnKX0pIHtcbiAgICAgICAgQGNvbnRlbnQ7XG4gICAgfVxufVxuXG5AbWl4aW4ga3QtdGFibGV0LWFuZC1tb2JpbGUge1xuICAgIEBtZWRpYSAobWF4LXdpZHRoOiAje2t0LW1lZGlhLWJyZWFrcG9pbnQobGcpfSkge1xuICAgICAgICBAY29udGVudDtcbiAgICB9XG59XG5cbkBtaXhpbiBrdC1tb2JpbGUge1xuICAgIEBtZWRpYSAobWF4LXdpZHRoOiAje2t0LW1lZGlhLWJyZWFrcG9pbnQobWQpfSkge1xuICAgICAgICBAY29udGVudDtcbiAgICB9XG59XG5cbkBtaXhpbiBrdC1tb2JpbGUtc20ge1xuICAgIEBtZWRpYSAobWF4LXdpZHRoOiAje2t0LW1lZGlhLWJyZWFrcG9pbnQoc20pfSkge1xuICAgICAgICBAY29udGVudDtcbiAgICB9XG59XG5cbkBtaXhpbiBrdC1yZXNwb25zaXZlLWJlbG93KCR3aWR0aCkge1xuICAgIEBtZWRpYSAobWF4LXdpZHRoOiAjeyR3aWR0aH0pIHtcbiAgICAgIEBjb250ZW50O1xuICAgIH1cbn1cblxuQG1peGluIGt0LXJlc3BvbnNpdmUtYWJvdmUoJHdpZHRoKSB7XG4gICAgQG1lZGlhIChtaW4td2lkdGg6ICN7JHdpZHRofSkge1xuICAgICAgQGNvbnRlbnQ7XG4gICAgfVxufVxuXG5AbWl4aW4ga3QtcmVzcG9uc2l2ZS1yYW5nZSgkZnJvbSwgJHRvKSB7XG4gICAgQG1lZGlhIChtaW4td2lkdGg6ICN7JGZyb219KSBhbmQgKG1heC13aWR0aDogI3skdG99KSB7XG4gICAgICBAY29udGVudDtcbiAgICB9XG59XG4iLCIvLyBpbXBvcnQgbG9naW4gY3NzXG5AaW1wb3J0IFwiLi4vLi4vLi4vYXNzZXRzL3Nhc3MvcGFnZXMvbG9naW4vbG9naW4tMVwiO1xuXG5rdC1hdXRoLCAua3QtbG9naW4ge1xuXHRoZWlnaHQ6IDEwMCU7XG5cblx0Lm1hdC1mb3JtLWZpZWxkIHtcblx0XHR3aWR0aDogMTAwJTtcblx0fVxuXG5cdC8vIGFkZCBleHRyYSByaWdodCBwYWRkaW5nIHdoZW4gZGlzcGxheWluZyBzcGlubmVyIGluIGJ1dHRvblxuXHQua3Qtc3Bpbm5lciB7XG5cdFx0cGFkZGluZy1yaWdodDogM3JlbSAhaW1wb3J0YW50O1xuXHR9XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/views/auth/auth.component.ts":
/*!**********************************************!*\
  !*** ./src/app/views/auth/auth.component.ts ***!
  \**********************************************/
/*! exports provided: AuthComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthComponent", function() { return AuthComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_base_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/_base/layout */ "./src/app/core/_base/layout/index.ts");
/* harmony import */ var _core_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../core/auth */ "./src/app/core/auth/index.ts");

// Angular

// Layout

// Auth

var AuthComponent = /** @class */ (function () {
    /**
     * Component constructor
     *
     * @param el
     * @param render
     * @param layoutConfigService: LayoutConfigService
     * @param authNoticeService: authNoticeService
     * @param translationService: TranslationService
     */
    function AuthComponent(el, render, layoutConfigService, authNoticeService, translationService) {
        this.el = el;
        this.render = render;
        this.layoutConfigService = layoutConfigService;
        this.authNoticeService = authNoticeService;
        this.translationService = translationService;
        // Public properties
        this.today = Date.now();
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    AuthComponent.prototype.ngOnInit = function () {
        // this.translationService.setLanguage(this.translationService.getSelectedLanguage());
        // this.headerLogo = this.layoutConfigService.getLogo();
    };
    /**
     * Load CSS for this specific page only, and destroy when navigate away
     * @param styleUrl
     */
    AuthComponent.prototype.loadCSS = function (styleUrl) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var styleElement = document.createElement('link');
            styleElement.href = styleUrl;
            styleElement.type = 'text/css';
            styleElement.rel = 'stylesheet';
            styleElement.onload = resolve;
            _this.render.appendChild(_this.el.nativeElement, styleElement);
        });
    };
    AuthComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"] },
        { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_2__["LayoutConfigService"] },
        { type: _core_auth__WEBPACK_IMPORTED_MODULE_3__["AuthNoticeService"] },
        { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_2__["TranslationService"] }
    ]; };
    AuthComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-auth',
            template: __webpack_require__(/*! raw-loader!./auth.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/auth/auth.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./auth.component.scss */ "./src/app/views/auth/auth.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"],
            _core_base_layout__WEBPACK_IMPORTED_MODULE_2__["LayoutConfigService"],
            _core_auth__WEBPACK_IMPORTED_MODULE_3__["AuthNoticeService"],
            _core_base_layout__WEBPACK_IMPORTED_MODULE_2__["TranslationService"]])
    ], AuthComponent);
    return AuthComponent;
}());



/***/ }),

/***/ "./src/app/views/auth/auth.module.ts":
/*!*******************************************!*\
  !*** ./src/app/views/auth/auth.module.ts ***!
  \*******************************************/
/*! exports provided: AuthModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthModule", function() { return AuthModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _auth_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./auth.component */ "./src/app/views/auth/auth.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./login/login.component */ "./src/app/views/auth/login/login.component.ts");
/* harmony import */ var _auth_notice_auth_notice_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./auth-notice/auth-notice.component */ "./src/app/views/auth/auth-notice/auth-notice.component.ts");
/* harmony import */ var _core_auth__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../core/auth */ "./src/app/core/auth/index.ts");

// Angular




// Material

// Translate

// CRUD
// import { InterceptService } from '../../../core/_base/crud/';
// Module components




// Auth
// import { AuthEffects, AuthGuard, authReducer, AuthService } from '../../../core/auth';
var routes = [
    {
        path: '',
        component: _auth_component__WEBPACK_IMPORTED_MODULE_7__["AuthComponent"],
        children: [
            {
                path: '',
                redirectTo: 'login',
                pathMatch: 'full'
            },
            {
                path: 'login',
                component: _login_login_component__WEBPACK_IMPORTED_MODULE_8__["LoginComponent"],
            },
        ]
    }
];
var AuthModule = /** @class */ (function () {
    function AuthModule() {
    }
    AuthModule_1 = AuthModule;
    AuthModule.forRoot = function () {
        return {
            ngModule: AuthModule_1,
            providers: [
                _core_auth__WEBPACK_IMPORTED_MODULE_10__["AuthService"],
                _core_auth__WEBPACK_IMPORTED_MODULE_10__["AuthGuard"]
            ]
        };
    };
    var AuthModule_1;
    AuthModule = AuthModule_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatButtonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatCheckboxModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateModule"].forChild(),
            ],
            providers: [
            // InterceptService,
            // {
            // 	provide: HTTP_INTERCEPTORS,
            // useClass: InterceptService,
            // multi: true
            // },
            ],
            exports: [_auth_component__WEBPACK_IMPORTED_MODULE_7__["AuthComponent"], _auth_notice_auth_notice_component__WEBPACK_IMPORTED_MODULE_9__["AuthNoticeComponent"]],
            declarations: [
                _auth_component__WEBPACK_IMPORTED_MODULE_7__["AuthComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_8__["LoginComponent"],
                _auth_notice_auth_notice_component__WEBPACK_IMPORTED_MODULE_9__["AuthNoticeComponent"]
            ]
        })
    ], AuthModule);
    return AuthModule;
}());



/***/ }),

/***/ "./src/app/views/auth/login/login.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/views/auth/login/login.component.ts ***!
  \*****************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _core_auth__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../core/auth */ "./src/app/core/auth/index.ts");
/* harmony import */ var _core_base_layout__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../core/_base/layout */ "./src/app/core/_base/layout/index.ts");

// Angular



// RxJS


// Translate

// Store

// Auth


/**
 * ! Just example => Should be removed in development
 */
var DEMO_PARAMS = {
    USERNAME: 'admin',
    PASSWORD: '1234'
};
var LoginComponent = /** @class */ (function () {
    // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/
    /**
     * Component constructor
     *
     * @param router: Router
     * @param auth: AuthService
     * @param authNoticeService: AuthNoticeService
     * @param translate: TranslateService
     * @param store: Store<AppState>
     * @param fb: FormBuilder
     * @param cdr
     * @param route
     */
    function LoginComponent(router, auth, authNoticeService, translate, 
    // private store: Store<AppState>,
    fb, cdr, menuConfigService, pageConfigService, store
    // private route: ActivatedRoute
    ) {
        this.router = router;
        this.auth = auth;
        this.authNoticeService = authNoticeService;
        this.translate = translate;
        this.fb = fb;
        this.cdr = cdr;
        this.menuConfigService = menuConfigService;
        this.pageConfigService = pageConfigService;
        this.store = store;
        this.loading = false;
        this.errors = [];
        this.wSub = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Subscription"]();
        this.unsubscribe = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Subject"]();
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    LoginComponent.prototype.ngOnInit = function () {
        this.initLoginForm();
        // redirect back to the returnUrl before login
        // this.route.queryParams.subscribe(params => {
        // 	this.returnUrl = params['returnUrl'] || '/';
        // });
    };
    /**
     * On destroy
     */
    LoginComponent.prototype.ngOnDestroy = function () {
        this.authNoticeService.setNotice(null);
        // this.unsubscribe.next();
        // this.unsubscribe.complete();
        this.loading = false;
        // this.wSub.unsubscribe();
    };
    /**
     * Form initalization
     * Default params, validators
     */
    LoginComponent.prototype.initLoginForm = function () {
        // demo message to show
        if (!this.authNoticeService.onNoticeChanged$.getValue()) {
            var initialNotice = "Use account\n\t\t\t<strong>" + DEMO_PARAMS.USERNAME + "</strong> and password\n\t\t\t<strong>" + DEMO_PARAMS.PASSWORD + "</strong> to continue.";
            this.authNoticeService.setNotice(initialNotice, 'info');
        }
        this.loginForm = this.fb.group({
            username: [DEMO_PARAMS.USERNAME, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(3),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(320) // https://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address
                ])
            ],
            password: [DEMO_PARAMS.PASSWORD, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(3),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(100)
                ])
            ]
        });
    };
    LoginComponent.prototype.submit = function () {
        var _this = this;
        var controls = this.loginForm.controls;
        /** check form */
        if (this.loginForm.invalid) {
            Object.keys(controls).forEach(function (controlName) {
                return controls[controlName].markAsTouched();
            });
            return;
        }
        this.loading = true;
        var authData = {
            username: controls['username'].value,
            password: controls['password'].value
        };
        this.wSub = this.auth
            .login(authData.username, authData.password)
            .subscribe(function (res) {
            _this.user = res;
            if (_this.user.isActive == 0) {
                _this.authNoticeService.setNotice('Error, you account has been deactivated', 'danger');
                return;
            }
            if (_this.user.roleId != 3) {
                // this.router.navigateByUrl('reviewer/dashboard'); // Reviewer page
                _this.router.navigate(['reviewer/dashboard']); // Reviewer page
            }
            else {
                _this.router.navigate(['admin/dashboard']); // Reviewer page
            }
            sessionStorage.setItem('user', JSON.stringify(_this.user));
        }, function (error) {
            _this.authNoticeService.setNotice(_this.translate.instant('AUTH.VALIDATION.INVALID_LOGIN'), 'danger');
            console.log('There was an error while retrieving User !!!' + error);
        }),
            Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["takeUntil"])(this.unsubscribe),
            Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () {
                _this.loading = false;
                // this.cdr.markForCheck();
            });
    };
    /**
     * Checking control validation
     *
     * @param controlName: string => Equals to formControlName
     * @param validationType: string => Equals to valitors name
     */
    LoginComponent.prototype.isControlHasError = function (controlName, validationType) {
        var control = this.loginForm.controls[controlName];
        if (!control) {
            return false;
        }
        var result = control.hasError(validationType) && (control.dirty || control.touched);
        return result;
    };
    LoginComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _core_auth__WEBPACK_IMPORTED_MODULE_8__["AuthService"] },
        { type: _core_auth__WEBPACK_IMPORTED_MODULE_8__["AuthNoticeService"] },
        { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_9__["MenuConfigService"] },
        { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_9__["PageConfigService"] },
        { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_7__["Store"] }
    ]; };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-login',
            template: __webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/auth/login/login.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _core_auth__WEBPACK_IMPORTED_MODULE_8__["AuthService"],
            _core_auth__WEBPACK_IMPORTED_MODULE_8__["AuthNoticeService"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _core_base_layout__WEBPACK_IMPORTED_MODULE_9__["MenuConfigService"],
            _core_base_layout__WEBPACK_IMPORTED_MODULE_9__["PageConfigService"],
            _ngrx_store__WEBPACK_IMPORTED_MODULE_7__["Store"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/views/author/service/work.service.ts":
/*!******************************************************!*\
  !*** ./src/app/views/author/service/work.service.ts ***!
  \******************************************************/
/*! exports provided: WorkService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WorkService", function() { return WorkService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");






// const url = 'http://3.95.8.94/example/work_request.php';
var url = _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].baseUrl + 'work_request.php';
var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/x-www-form-urlencoded' });
var WorkService = /** @class */ (function () {
    function WorkService(http) {
        this.http = http;
    }
    WorkService.prototype.submit = function (work) {
        var body = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set("postNewWork", 'postNewWork')
            .set("title", work.Title)
            .set("dateWritten", work.DateWritten)
            .set("dateSubmitted", work.DateSubmission)
            .set("url", work.URL)
            .set("author", work.AuthorName)
            .set("email", work.AuthorEmail)
            .set("selectedTags", work.SelectedTags.toString());
        return this.http.post(url, body, { headers: headers })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    };
    WorkService.prototype.getAllTags = function () {
        var _this = this;
        return this.http.get(url, { params: { getAllTags: 'getAllTags' } })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (tags) {
            tags.forEach(function (tag) {
                // let ls: string = Object.assign(tag.TagList);
                // tag.TagList = ls.split(',');
                tag.TagList = _this.convertToArray(tag.TagList);
            });
            return tags;
        }));
    };
    // getAllWorks
    WorkService.prototype.getAllWorks = function () {
        var _this = this;
        return this.http.get(url, { params: { getAllWorks: 'getAllWorks' } }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (works) {
            works.map(function (dtw) {
                dtw.Tags = _this.convertToArray(dtw.Tags);
            });
            return works;
        }));
    };
    WorkService.prototype.getWorksForPublic = function () {
        var _this = this;
        return this.http.get(url, { params: { getWorksForPublic: 'getWorksForPublic' } }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["retry"])(3), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (works) {
            works.map(function (dtw) {
                dtw.Tags = _this.convertToArray(dtw.Tags);
            });
            return works;
        }));
    };
    WorkService.prototype.getIncomingWorks = function () {
        var _this = this;
        return this.http.get(url, { params: { incommingWorks: 'any' } }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (works) {
            if (works) {
                works.map(function (eachWork) {
                    eachWork.Tags = _this.convertToArray(eachWork.Tags);
                });
                return works;
            }
        }));
    };
    WorkService.prototype.convertToArray = function (str) {
        var ls = Object.assign(str);
        str = ls.split(',');
        return str;
    };
    WorkService.prototype.publishWork = function (WID, checked) {
        var body = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set("publishWork", 'publishWork')
            .set("WID", WID.toString())
            .set("Publish", checked.toString());
        return this.http.post(url, body, { headers: headers })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    };
    WorkService.prototype.updateWorkStatusByID = function (wid, status) {
        var body = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set("updateWorkStatus", 'updateWorkStatus')
            .set("WID", wid.toString())
            .set("Status", status);
        return this.http.post(url, body, { headers: headers })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    };
    WorkService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    WorkService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], WorkService);
    return WorkService;
}());



/***/ }),

/***/ "./src/app/views/main/about-page/about.component.scss":
/*!************************************************************!*\
  !*** ./src/app/views/main/about-page/about.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container {\n  max-width: 1080px; }\n\n.card-header h4 {\n  margin: 0; }\n\n.card-deck {\n  font-size: 15px; }\n\n.img {\n  vertical-align: top;\n  font-size: 40px; }\n\n#upload-icon {\n  color: white;\n  font-size: 15px;\n  margin-left: 20px;\n  margin-top: 30px; }\n\n.bottom {\n  background-color: #646c9a;\n  height: 285px;\n  display: flex;\n  /* padding-top: 37px; */ }\n\n.bottom-card {\n  width: 50%;\n  height: 50%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2thbWlsL1BSU19ORVdfVmVyc2lvbi9zcmMvYXBwL3ZpZXdzL21haW4vYWJvdXQtcGFnZS9hYm91dC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNDLGlCQUFpQixFQUFBOztBQUdsQjtFQUNDLFNBQVMsRUFBQTs7QUFHVjtFQUNDLGVBQWUsRUFBQTs7QUFHaEI7RUFDQyxtQkFBbUI7RUFDbkIsZUFBZSxFQUFBOztBQU1oQjtFQUNDLFlBQVk7RUFDWixlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLGdCQUFnQixFQUFBOztBQUdqQjtFQUNDLHlCQUF5QjtFQUN6QixhQUFhO0VBQ2IsYUFBYTtFQUNiLHVCQUFBLEVBQXdCOztBQUd6QjtFQUNDLFVBQVU7RUFDVixXQUFXLEVBQUEiLCJmaWxlIjoic3JjL2FwcC92aWV3cy9tYWluL2Fib3V0LXBhZ2UvYWJvdXQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbi5jb250YWluZXJ7XG5cdG1heC13aWR0aDogMTA4MHB4O1xufVxuXG4uY2FyZC1oZWFkZXIgaDR7XG5cdG1hcmdpbjogMDtcbn1cblxuLmNhcmQtZGVja3tcblx0Zm9udC1zaXplOiAxNXB4O1xufVxuXG4uaW1nIHtcblx0dmVydGljYWwtYWxpZ246IHRvcDtcblx0Zm9udC1zaXplOiA0MHB4O1xuXHQvL21hcmdpbi10b3A6IDI3cHg7XG5cdC8vYm9yZGVyOiAzcHggc29saWQ7XG59XG5cblxuI3VwbG9hZC1pY29uIHtcblx0Y29sb3I6IHdoaXRlO1xuXHRmb250LXNpemU6IDE1cHg7XG5cdG1hcmdpbi1sZWZ0OiAyMHB4O1xuXHRtYXJnaW4tdG9wOiAzMHB4O1xufVxuXG4uYm90dG9tIHtcblx0YmFja2dyb3VuZC1jb2xvcjogIzY0NmM5YTtcblx0aGVpZ2h0OiAyODVweDtcblx0ZGlzcGxheTogZmxleDtcblx0LyogcGFkZGluZy10b3A6IDM3cHg7ICovXG59XG5cbi5ib3R0b20tY2FyZHtcblx0d2lkdGg6IDUwJTtcblx0aGVpZ2h0OiA1MCU7XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/views/main/about-page/about.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/views/main/about-page/about.component.ts ***!
  \**********************************************************/
/*! exports provided: AboutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutComponent", function() { return AboutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AboutComponent = /** @class */ (function () {
    function AboutComponent() {
        this.title = 'About Peer Review System Application';
    }
    AboutComponent.prototype.ngOnInit = function () {
    };
    AboutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-about',
            template: __webpack_require__(/*! raw-loader!./about.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/main/about-page/about.component.html"),
            styles: [__webpack_require__(/*! ./about.component.scss */ "./src/app/views/main/about-page/about.component.scss")]
        })
    ], AboutComponent);
    return AboutComponent;
}());



/***/ }),

/***/ "./src/app/views/main/contact-page/contact.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/views/main/contact-page/contact.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".contact {\n  width: 50%;\n  padding: 22px;\n  margin: auto;\n  height: 100%;\n  background: white;\n  border-radius: 9px; }\n\nmat-form-field {\n  width: 100%; }\n\n.mat-simple-snackbar {\n  font-size: 16px; }\n\n.snackbar2 {\n  height: 90px;\n  padding: 30px !important;\n  background-color: #5A29A9;\n  color: #ffffff;\n  font-weight: 500;\n  font-family: inherit; }\n\n.snackbar2 .mat-button-wrapper {\n    color: black !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2thbWlsL1BSU19ORVdfVmVyc2lvbi9zcmMvYXBwL3ZpZXdzL21haW4vY29udGFjdC1wYWdlL2NvbnRhY3QuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDQyxVQUFVO0VBQ1YsYUFBYTtFQUNiLFlBQVk7RUFDWixZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLGtCQUFrQixFQUFBOztBQUluQjtFQUNDLFdBQVcsRUFBQTs7QUFJWjtFQUNDLGVBQWUsRUFBQTs7QUFHaEI7RUFDQyxZQUFZO0VBSVosd0JBQXdCO0VBQ3hCLHlCQUF5QjtFQUN6QixjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLG9CQUFvQixFQUFBOztBQVRyQjtJQVlFLHVCQUF1QixFQUFBIiwiZmlsZSI6InNyYy9hcHAvdmlld3MvbWFpbi9jb250YWN0LXBhZ2UvY29udGFjdC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuLmNvbnRhY3Qge1xuXHR3aWR0aDogNTAlO1xuXHRwYWRkaW5nOiAyMnB4O1xuXHRtYXJnaW46IGF1dG87XG5cdGhlaWdodDogMTAwJTtcblx0YmFja2dyb3VuZDogd2hpdGU7XG5cdGJvcmRlci1yYWRpdXM6IDlweDtcbn1cblxuXG5tYXQtZm9ybS1maWVsZCB7XG5cdHdpZHRoOiAxMDAlO1xufVxuXG5cbi5tYXQtc2ltcGxlLXNuYWNrYmFyIHtcblx0Zm9udC1zaXplOiAxNnB4O1xufVxuXG4uc25hY2tiYXIyIHtcblx0aGVpZ2h0OiA5MHB4O1xuXHQvL21hcmdpbi1sZWZ0OiBhdXRvICFpbXBvcnRhbnQ7IC8vIGNlbnRlciBhbGlnbiBmcm9tIGxlZnRcblx0Ly9tYXJnaW4tcmlnaHQ6IGF1dG8gIWltcG9ydGFudDsgLy8gY2VudGVyIGFsaWduIGZyb20gcmlnaHRcblx0Ly9tYXJnaW4tYm90dG9tOiAxcmVtICFpbXBvcnRhbnQ7XG5cdHBhZGRpbmc6IDMwcHggIWltcG9ydGFudDsgLy8gc3BhY2luZyBiZXR3ZWVuIHRoZSB0ZXh0IGFuZCBib3VuZGFyeVxuXHRiYWNrZ3JvdW5kLWNvbG9yOiAjNUEyOUE5O1xuXHRjb2xvcjogI2ZmZmZmZjtcblx0Zm9udC13ZWlnaHQ6IDUwMDtcblx0Zm9udC1mYW1pbHk6IGluaGVyaXQ7XG5cblx0Lm1hdC1idXR0b24td3JhcHBlciB7XG5cdFx0Y29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7IC8vIGFjdGlvbiB0ZXh0IGNvbG9yXG5cdH1cbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/views/main/contact-page/contact.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/views/main/contact-page/contact.component.ts ***!
  \**************************************************************/
/*! exports provided: ContactComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactComponent", function() { return ContactComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_email_notification_services_email_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../core/email-notification/_services/email.service */ "./src/app/core/email-notification/_services/email.service.ts");






var ContactComponent = /** @class */ (function () {
    function ContactComponent(fb, snackBar, router, emailService) {
        this.fb = fb;
        this.snackBar = snackBar;
        this.router = router;
        this.emailService = emailService;
        this.title = 'About Peer Review System Application';
    }
    ContactComponent.prototype.ngOnInit = function () {
        this.initContactForm();
    };
    ContactComponent.prototype.ngOnDestroy = function () {
        if (this.subscription != undefined) {
            this.subscription.unsubscribe();
        }
    };
    ContactComponent.prototype.isControlHasError = function (controlName, validationType) {
        var control = this.contactForm.controls[controlName];
        if (!control) {
            return false;
        }
        var result = control.hasError(validationType) && (control.dirty || control.touched);
        return result;
    };
    ContactComponent.prototype.submit = function () {
        var _this = this;
        var controls = this.contactForm.controls;
        if (this.contactForm.invalid) {
            Object.keys(controls).forEach(function (controlName) {
                return controls[controlName].markAsTouched();
            });
            return;
        }
        var senderName = controls['fullname'].value;
        var senderEmail = controls['email'].value;
        var subject = controls['subject'].value;
        var cmessage = controls['cmessage'].value;
        var email = {
            senderName: senderName,
            senderEmail: senderEmail,
            subject: subject,
            message: cmessage,
            canReply: 1
        };
        this.subscription = this.emailService.sendEmail(email).subscribe(function () {
        }, function (error) {
            _this.displaySnackBar('Unable to submit your message.');
            return;
        });
        this.resetForm();
        this.displaySnackBar('Your message has been submitted successfully.');
        this.router.navigateByUrl('/home');
        return;
    };
    ContactComponent.prototype.initContactForm = function () {
        this.contactForm = this.fb.group({
            fullname: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(3),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(320)
                ])
            ],
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(3),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(100)
                ])
            ],
            subject: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(3),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(320)
                ])
            ],
            cmessage: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(3),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(100)
                ])
            ]
        });
    };
    ContactComponent.prototype.displaySnackBar = function (message) {
        var config = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBarConfig"]();
        config.duration = 5000;
        config.panelClass = ['d-flex', 'justify-content-center', 'snackbar2'];
        this.snackBar.open(message, '', config);
    };
    ContactComponent.prototype.resetForm = function () {
        var _this = this;
        this.contactForm.reset();
        Object.keys(this.contactForm.controls).forEach(function (key) {
            _this.contactForm.get(key).setErrors(null);
        });
    };
    ContactComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
        { type: _core_email_notification_services_email_service__WEBPACK_IMPORTED_MODULE_5__["EmailService"] }
    ]; };
    ContactComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-contact1',
            template: __webpack_require__(/*! raw-loader!./contact.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/main/contact-page/contact.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./contact.component.scss */ "./src/app/views/main/contact-page/contact.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _core_email_notification_services_email_service__WEBPACK_IMPORTED_MODULE_5__["EmailService"]])
    ], ContactComponent);
    return ContactComponent;
}());



/***/ }),

/***/ "./src/app/views/main/main.component.scss":
/*!************************************************!*\
  !*** ./src/app/views/main/main.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header-about {\n  position: absolute;\n  margin: 0;\n  top: 50%;\n  left: 50%;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%); }\n\n.header-company {\n  height: 280px;\n  max-width: 100%; }\n\n.underline {\n  text-decoration: underline; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2thbWlsL1BSU19ORVdfVmVyc2lvbi9zcmMvYXBwL3ZpZXdzL21haW4vbWFpbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUE2R0E7RUFDQyxrQkFBa0I7RUFDbEIsU0FBUztFQUNULFFBQVE7RUFDUixTQUFTO0VBQ1Qsd0NBQWdDO1VBQWhDLGdDQUFnQyxFQUFBOztBQUdqQztFQUNDLGFBQWE7RUFDYixlQUFlLEVBQUE7O0FBSWhCO0VBQ0MsMEJBQTBCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC92aWV3cy9tYWluL21haW4uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLzpob3N0IHtcbi8vICA6Om5nLWRlZXAge1xuLy8gICAgbmdiLXRhYnNldCA+IC5uYXYtdGFicyB7XG4vLyAgICAgIGRpc3BsYXk6IG5vbmU7XG4vLyAgICB9XG4vLyAgfVxuLy99XG4vL1xuLy8uZXhhbXBsZS1idXR0b24tcm93IHtcbi8vICBkaXNwbGF5OiBmbGV4O1xuLy8gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4vLyAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4vL31cbi8vXG4vL1xuLy8ubWF0LXRhYmxle1xuLy9cbi8vXHRtYXgtaGVpZ2h0OiA2MDBweDtcbi8vfVxuLy9cbi8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuLy9cbi8vQGltcG9ydCB1cmwoJ2h0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1Tb3VyY2UrU2FucytQcm8nKTtcbi8vXG4vLyoge1xuLy9cdGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4vL31cbi8vXG4vL2JvZHkge1xuLy9cdGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byBib3R0b20sICNjZmQ5ZGYgMCUsICNlMmViZjAgMTAwJSk7XG4vL1x0Zm9udC1mYW1pbHk6ICdTb3VyY2UgU2FucyBQcm8nLCBzYW5zLXNlcmlmO1xuLy9cdG1hcmdpbjogMDtcbi8vXHRoZWlnaHQ6IDEwMHZoO1xuLy9cdGRpc3BsYXk6IGZsZXg7XG4vL1x0YWxpZ24taXRlbXM6IGNlbnRlcjtcbi8vXHRqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbi8vXHRmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuLy99XG4vL1xuLy8vKiBCYXNpYyBidXR0b24gc3R5bGluZyAqL1xuLy9cbi8vLnd3e1xuLy9cdGJhY2tncm91bmQ6ICM3MjI1ZmY7XG4vL31cbi8vXG4vLy5wdWxzaW5nQnV0dG9uIHtcbi8vXHRib3gtc2hhZG93OiAwIDAgMCAwIHJnYmEoODUsIDE1NywgMjI4LCAwLjcpO1xuLy9cdC13ZWJraXQtYW5pbWF0aW9uOiBwdWxzaW5nIDUuMjVzIGluZmluaXRlIGN1YmljLWJlemllcigwLjY2LCAwLCAwLCAxKTtcbi8vXHQtbW96LWFuaW1hdGlvbjogcHVsc2luZyA1LjI1cyBpbmZpbml0ZSBjdWJpYy1iZXppZXIoMC42NiwgMCwgMCwgMSk7XG4vL1x0LW1zLWFuaW1hdGlvbjogcHVsc2luZyA1LjI1cyBpbmZpbml0ZSBjdWJpYy1iZXppZXIoMC42NiwgMCwgMCwgMSk7XG4vL1x0YW5pbWF0aW9uOiBwdWxzaW5nIDUuMjVzIGluZmluaXRlIGN1YmljLWJlemllcigwLjY2LCAwLCAwLCAxKTtcbi8vXHR0cmFuc2l0aW9uOiBhbGwgODAwbXMgZWFzZS1pbi1vdXQ7XG4vL1xuLy99XG4vL1xuLy9cbi8vLyogQ29tbWVudC1vdXQgdG8gaGF2ZSB0aGUgYnV0dG9uIGNvbnRpbnVlIHRvIHB1bHNlIG9uIG1vdXNlb3ZlciAqL1xuLy9cbi8vYS5wdWxzaW5nQnV0dG9uOmhvdmVyIHtcbi8vXHQtd2Via2l0LWFuaW1hdGlvbjogbm9uZTtcbi8vXHQtbW96LWFuaW1hdGlvbjogbm9uZTtcbi8vXHQtbXMtYW5pbWF0aW9uOiBub25lO1xuLy9cdGFuaW1hdGlvbjogbm9uZTtcbi8vXHRjb2xvcjogI2ZmZmZmZjtcbi8vfVxuLy9cbi8vXG4vLy8qIEFuaW1hdGlvbiAqL1xuLy9cbi8vQC13ZWJraXQta2V5ZnJhbWVzIHB1bHNpbmcge1xuLy9cdHRvIHtcbi8vXHRcdGJveC1zaGFkb3c6IDAgMCAwIDE1cHggcmdiYSgyMzIsIDc2LCA2MSwgMCk7XG4vL1x0fVxuLy99XG4vL1xuLy9ALW1vei1rZXlmcmFtZXMgcHVsc2luZyB7XG4vL1x0dG8ge1xuLy9cdFx0Ym94LXNoYWRvdzogMCAwIDAgMTVweCByZ2JhKDIzMiwgNzYsIDYxLCAwKTtcbi8vXHR9XG4vL31cbi8vXG4vL0AtbXMta2V5ZnJhbWVzIHB1bHNpbmcge1xuLy9cdHRvIHtcbi8vXHRcdGJveC1zaGFkb3c6IDAgMCAwIDE1cHggcmdiYSgyMzIsIDc2LCA2MSwgMCk7XG4vL1x0fVxuLy99XG4vL1xuLy9Aa2V5ZnJhbWVzIHB1bHNpbmcge1xuLy9cdHRvIHtcbi8vXHRcdGJveC1zaGFkb3c6IDAgMCAwIDE1cHggcmdiYSgyMzIsIDc2LCA2MSwgMCk7XG4vL1x0fVxuLy99XG4vL1xuLy8uZmxleC1jb250YWluZXJ7XG4vL1x0ZGlzcGxheTogZmxleDtcbi8vXHRmbGV4LWRpcmVjdGlvbjogcm93O1xuLy9cdG1hcmdpbi10b3A6IDE1MHB4O1xuLy9cdG1hcmdpbi1sZWZ0OiAtNTVweDtcbi8vXG4vL31cbi8vXG4vLy5mbGV4LWNvbnRhaW5lciA+IGRpdiA+IGEge1xuLy9cdHRleHQtYWxpZ246IGxlZnQ7XG4vL1x0Zm9udC1zaXplOiAxNnB4O1xuLy9cdG1hcmdpbi1yaWdodDogMTBweDtcbi8vXG4vL1x0Y29sb3I6d2hpdGU7XG4vL31cblxuLmhlYWRlci1hYm91dCB7XG5cdHBvc2l0aW9uOiBhYnNvbHV0ZTtcblx0bWFyZ2luOiAwO1xuXHR0b3A6IDUwJTtcblx0bGVmdDogNTAlO1xuXHR0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbn1cblxuLmhlYWRlci1jb21wYW55IHtcblx0aGVpZ2h0OiAyODBweDtcblx0bWF4LXdpZHRoOiAxMDAlO1xufVxuXG5cbi51bmRlcmxpbmUge1xuXHR0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcbn1cblxuLy8jbXlOYXZiYXI3IGEuYWN0aXZlIHtcbi8vXHRib3JkZXItYm90dG9tOiAycHggc29saWQgd2hpdGU7XG4vL31cbiJdfQ== */"

/***/ }),

/***/ "./src/app/views/main/main.component.ts":
/*!**********************************************!*\
  !*** ./src/app/views/main/main.component.ts ***!
  \**********************************************/
/*! exports provided: MainComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainComponent", function() { return MainComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var MainComponent = /** @class */ (function () {
    function MainComponent(router) {
        var _this = this;
        this.router = router;
        this.title = 'The Highest Scored Works';
        this.subscription = this.router.events
            .subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"]) {
                switch (event.url) {
                    case '/contact':
                    case '/about':
                        _this.title = 'About Peer Review System Application';
                        break;
                    default:
                        _this.title = 'The Highest Scored Works';
                }
            }
        });
    }
    MainComponent.prototype.ngOnInit = function () {
    };
    MainComponent.prototype.ngOnDestroy = function () {
        if (this.subscription !== undefined) {
            this.subscription.unsubscribe();
        }
    };
    MainComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    MainComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-main',
            template: __webpack_require__(/*! raw-loader!./main.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/main/main.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./main.component.scss */ "./src/app/views/main/main.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], MainComponent);
    return MainComponent;
}());



/***/ }),

/***/ "./src/app/views/main/main.module.ts":
/*!*******************************************!*\
  !*** ./src/app/views/main/main.module.ts ***!
  \*******************************************/
/*! exports provided: MainModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainModule", function() { return MainModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/esm5/paginator.es5.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm5/form-field.es5.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/sort */ "./node_modules/@angular/material/esm5/sort.es5.js");
/* harmony import */ var _main_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./main.component */ "./src/app/views/main/main.component.ts");
/* harmony import */ var _public_page_public_page_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../public-page/public-page.component */ "./src/app/views/public-page/public-page.component.ts");
/* harmony import */ var _about_page_about_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./about-page/about.component */ "./src/app/views/main/about-page/about.component.ts");
/* harmony import */ var _contact_page_contact_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./contact-page/contact.component */ "./src/app/views/main/contact-page/contact.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ngx-perfect-scrollbar */ "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
/* harmony import */ var _partials_content_general_portlet_portlet_module__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../partials/content/general/portlet/portlet.module */ "./src/app/views/partials/content/general/portlet/portlet.module.ts");
















var routes = [
    {
        path: '',
        component: _main_component__WEBPACK_IMPORTED_MODULE_8__["MainComponent"],
        children: [
            {
                path: '',
                redirectTo: 'public',
                pathMatch: 'full'
            },
            {
                path: 'public',
                component: _public_page_public_page_component__WEBPACK_IMPORTED_MODULE_9__["PublicPageComponent"],
            },
            {
                path: 'about',
                component: _about_page_about_component__WEBPACK_IMPORTED_MODULE_10__["AboutComponent"],
            },
            {
                path: 'contact',
                component: _contact_page_contact_component__WEBPACK_IMPORTED_MODULE_11__["ContactComponent"],
            },
        ]
    }
];
var MainModule = /** @class */ (function () {
    function MainModule() {
    }
    MainModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
                _angular_material_paginator__WEBPACK_IMPORTED_MODULE_4__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_13__["MatProgressSpinnerModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__["MatFormFieldModule"],
                _angular_material_table__WEBPACK_IMPORTED_MODULE_6__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_13__["MatIconModule"],
                _angular_material_sort__WEBPACK_IMPORTED_MODULE_7__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_13__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_13__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_13__["MatChipsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_13__["MatButtonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_12__["ReactiveFormsModule"],
                ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_14__["PerfectScrollbarModule"],
                _partials_content_general_portlet_portlet_module__WEBPACK_IMPORTED_MODULE_15__["PortletModule"]
            ],
            providers: [],
            exports: [],
            declarations: [
                _main_component__WEBPACK_IMPORTED_MODULE_8__["MainComponent"],
                _about_page_about_component__WEBPACK_IMPORTED_MODULE_10__["AboutComponent"],
                _contact_page_contact_component__WEBPACK_IMPORTED_MODULE_11__["ContactComponent"],
                _public_page_public_page_component__WEBPACK_IMPORTED_MODULE_9__["PublicPageComponent"],
            ]
        })
    ], MainModule);
    return MainModule;
}());



/***/ }),

/***/ "./src/app/views/partials/content/crud/action-natification/action-notification.component.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/views/partials/content/crud/action-natification/action-notification.component.ts ***!
  \**************************************************************************************************/
/*! exports provided: ActionNotificationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionNotificationComponent", function() { return ActionNotificationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");

// Angular


// RxJS


var ActionNotificationComponent = /** @class */ (function () {
    /**
     * Component constructor
     *
     * @param data: any
     */
    function ActionNotificationComponent(data) {
        this.data = data;
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    ActionNotificationComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (!this.data.showUndoButton || (this.data.undoButtonDuration >= this.data.duration)) {
            return;
        }
        this.delayForUndoButton(this.data.undoButtonDuration).subscribe(function () {
            _this.data.showUndoButton = false;
        });
    };
    /*
     *	Returns delay
     *
     * @param timeToDelay: any
     */
    ActionNotificationComponent.prototype.delayForUndoButton = function (timeToDelay) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["of"])('').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["delay"])(timeToDelay));
    };
    /**
     * Dismiss with Action
     */
    ActionNotificationComponent.prototype.onDismissWithAction = function () {
        this.data.snackBar.dismiss();
    };
    /**
     * Dismiss
     */
    ActionNotificationComponent.prototype.onDismiss = function () {
        this.data.snackBar.dismiss();
    };
    ActionNotificationComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_SNACK_BAR_DATA"],] }] }
    ]; };
    ActionNotificationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-action-natification',
            template: __webpack_require__(/*! raw-loader!./action-notification.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/content/crud/action-natification/action-notification.component.html"),
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].Default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_SNACK_BAR_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object])
    ], ActionNotificationComponent);
    return ActionNotificationComponent;
}());



/***/ }),

/***/ "./src/app/views/partials/content/crud/alert/alert.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/views/partials/content/crud/alert/alert.component.ts ***!
  \**********************************************************************/
/*! exports provided: AlertComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertComponent", function() { return AlertComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

// Angular

var AlertComponent = /** @class */ (function () {
    function AlertComponent() {
        this.duration = 0;
        this.showCloseButton = true;
        this.close = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.alertShowing = true;
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    AlertComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.duration === 0) {
            return;
        }
        setTimeout(function () {
            _this.closeAlert();
        }, this.duration);
    };
    /**
     * close alert
     */
    AlertComponent.prototype.closeAlert = function () {
        this.close.emit();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], AlertComponent.prototype, "type", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], AlertComponent.prototype, "duration", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], AlertComponent.prototype, "showCloseButton", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], AlertComponent.prototype, "close", void 0);
    AlertComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-alert',
            template: __webpack_require__(/*! raw-loader!./alert.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/content/crud/alert/alert.component.html")
        })
    ], AlertComponent);
    return AlertComponent;
}());



/***/ }),

/***/ "./src/app/views/partials/content/crud/delete-entity-dialog/delete-entity-dialog.component.ts":
/*!****************************************************************************************************!*\
  !*** ./src/app/views/partials/content/crud/delete-entity-dialog/delete-entity-dialog.component.ts ***!
  \****************************************************************************************************/
/*! exports provided: DeleteEntityDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeleteEntityDialogComponent", function() { return DeleteEntityDialogComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");

// Angular


var DeleteEntityDialogComponent = /** @class */ (function () {
    /**
     * Component constructor
     *
     * @param dialogRef: MatDialogRef<DeleteEntityDialogComponent>
     * @param data: any
     */
    function DeleteEntityDialogComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        // Public properties
        this.viewLoading = false;
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    DeleteEntityDialogComponent.prototype.ngOnInit = function () {
    };
    /**
     * Close dialog with false result
     */
    DeleteEntityDialogComponent.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    /**
     * Close dialog with true result
     */
    DeleteEntityDialogComponent.prototype.onYesClick = function () {
        var _this = this;
        /* Server loading imitation. Remove this */
        this.viewLoading = true;
        setTimeout(function () {
            _this.dialogRef.close(true); // Keep only this row
        }, 2500);
    };
    DeleteEntityDialogComponent.ctorParameters = function () { return [
        { type: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"] },
        { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"],] }] }
    ]; };
    DeleteEntityDialogComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-delete-entity-dialog',
            template: __webpack_require__(/*! raw-loader!./delete-entity-dialog.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/content/crud/delete-entity-dialog/delete-entity-dialog.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"], Object])
    ], DeleteEntityDialogComponent);
    return DeleteEntityDialogComponent;
}());



/***/ }),

/***/ "./src/app/views/partials/content/crud/fetch-entity-dialog/fetch-entity-dialog.component.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/views/partials/content/crud/fetch-entity-dialog/fetch-entity-dialog.component.ts ***!
  \**************************************************************************************************/
/*! exports provided: FetchEntityDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FetchEntityDialogComponent", function() { return FetchEntityDialogComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");

// Angular


var FetchEntityDialogComponent = /** @class */ (function () {
    /**
     * Component constructor
     *
     * @param dialogRef: MatDialogRef<FetchEntityDialogComponent>,
     * @param data: any
     */
    function FetchEntityDialogComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    /**
     * Close dialog with false result
     */
    FetchEntityDialogComponent.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    /** UI */
    /**
     * Returns CSS Class Name by status type
     * @param status: number
     */
    FetchEntityDialogComponent.prototype.getItemCssClassByStatus = function (status) {
        if (status === void 0) { status = 0; }
        switch (status) {
            case 0: return 'success';
            case 1: return 'metal';
            case 2: return 'danger';
            default: return 'success';
        }
    };
    FetchEntityDialogComponent.ctorParameters = function () { return [
        { type: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"] },
        { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"],] }] }
    ]; };
    FetchEntityDialogComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-fetch-entity-dialog',
            template: __webpack_require__(/*! raw-loader!./fetch-entity-dialog.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/content/crud/fetch-entity-dialog/fetch-entity-dialog.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"], Object])
    ], FetchEntityDialogComponent);
    return FetchEntityDialogComponent;
}());



/***/ }),

/***/ "./src/app/views/partials/content/crud/index.ts":
/*!******************************************************!*\
  !*** ./src/app/views/partials/content/crud/index.ts ***!
  \******************************************************/
/*! exports provided: UpdateStatusDialogComponent, FetchEntityDialogComponent, DeleteEntityDialogComponent, AlertComponent, ActionNotificationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _update_status_dialog_update_status_dialog_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./update-status-dialog/update-status-dialog.component */ "./src/app/views/partials/content/crud/update-status-dialog/update-status-dialog.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UpdateStatusDialogComponent", function() { return _update_status_dialog_update_status_dialog_component__WEBPACK_IMPORTED_MODULE_0__["UpdateStatusDialogComponent"]; });

/* harmony import */ var _fetch_entity_dialog_fetch_entity_dialog_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fetch-entity-dialog/fetch-entity-dialog.component */ "./src/app/views/partials/content/crud/fetch-entity-dialog/fetch-entity-dialog.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FetchEntityDialogComponent", function() { return _fetch_entity_dialog_fetch_entity_dialog_component__WEBPACK_IMPORTED_MODULE_1__["FetchEntityDialogComponent"]; });

/* harmony import */ var _delete_entity_dialog_delete_entity_dialog_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./delete-entity-dialog/delete-entity-dialog.component */ "./src/app/views/partials/content/crud/delete-entity-dialog/delete-entity-dialog.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DeleteEntityDialogComponent", function() { return _delete_entity_dialog_delete_entity_dialog_component__WEBPACK_IMPORTED_MODULE_2__["DeleteEntityDialogComponent"]; });

/* harmony import */ var _alert_alert_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./alert/alert.component */ "./src/app/views/partials/content/crud/alert/alert.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AlertComponent", function() { return _alert_alert_component__WEBPACK_IMPORTED_MODULE_3__["AlertComponent"]; });

/* harmony import */ var _action_natification_action_notification_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./action-natification/action-notification.component */ "./src/app/views/partials/content/crud/action-natification/action-notification.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ActionNotificationComponent", function() { return _action_natification_action_notification_component__WEBPACK_IMPORTED_MODULE_4__["ActionNotificationComponent"]; });

// Components







/***/ }),

/***/ "./src/app/views/partials/content/crud/update-status-dialog/update-status-dialog.component.ts":
/*!****************************************************************************************************!*\
  !*** ./src/app/views/partials/content/crud/update-status-dialog/update-status-dialog.component.ts ***!
  \****************************************************************************************************/
/*! exports provided: UpdateStatusDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateStatusDialogComponent", function() { return UpdateStatusDialogComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");

// Angular



var UpdateStatusDialogComponent = /** @class */ (function () {
    function UpdateStatusDialogComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.selectedStatusForUpdate = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('');
        this.viewLoading = false;
        this.loadingAfterSubmit = false;
    }
    UpdateStatusDialogComponent.prototype.ngOnInit = function () {
        var _this = this;
        /* Server loading imitation. Remove this */
        this.viewLoading = true;
        setTimeout(function () {
            _this.viewLoading = false;
        }, 2500);
    };
    UpdateStatusDialogComponent.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    UpdateStatusDialogComponent.prototype.updateStatus = function () {
        var _this = this;
        if (this.selectedStatusForUpdate.value.length === 0) {
            return;
        }
        /* Server loading imitation. Remove this */
        this.viewLoading = true;
        this.loadingAfterSubmit = true;
        setTimeout(function () {
            _this.dialogRef.close(_this.selectedStatusForUpdate.value); // Keep only this row
        }, 2500);
    };
    UpdateStatusDialogComponent.ctorParameters = function () { return [
        { type: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"] },
        { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"],] }] }
    ]; };
    UpdateStatusDialogComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-update-status-dialog',
            template: __webpack_require__(/*! raw-loader!./update-status-dialog.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/content/crud/update-status-dialog/update-status-dialog.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"], Object])
    ], UpdateStatusDialogComponent);
    return UpdateStatusDialogComponent;
}());



/***/ }),

/***/ "./src/app/views/partials/content/general/error/error.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/views/partials/content/general/error/error.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  height: 100%; }\n\n.kt-error-v403 {\n  background-position: center;\n  background-repeat: no-repeat;\n  background-attachment: fixed;\n  background-size: cover; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2thbWlsL1BSU19ORVdfVmVyc2lvbi9zcmMvYXBwL3ZpZXdzL3BhcnRpYWxzL2NvbnRlbnQvZ2VuZXJhbC9lcnJvci9lcnJvci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLFlBQVksRUFBQTs7QUFHYjtFQUNJLDJCQUEyQjtFQUMzQiw0QkFBNEI7RUFDNUIsNEJBQTRCO0VBQzVCLHNCQUFzQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvdmlld3MvcGFydGlhbHMvY29udGVudC9nZW5lcmFsL2Vycm9yL2Vycm9yLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xuXHRoZWlnaHQ6IDEwMCU7XG59XG5cbi5rdC1lcnJvci12NDAzIHtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59Il19 */"

/***/ }),

/***/ "./src/app/views/partials/content/general/error/error.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/views/partials/content/general/error/error.component.ts ***!
  \*************************************************************************/
/*! exports provided: ErrorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorComponent", function() { return ErrorComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

// Angular

var ErrorComponent = /** @class */ (function () {
    function ErrorComponent() {
        // Public properties
        // type of error template to be used, accepted values; error-v1 | error-v2 | error-v3 | error-v4 | error-v5 | error-v6
        this.type = 'error-v1';
        // error code, some error types template has it
        this.code = '404';
        // error descriptions
        this.desc = 'Oops! Something went wrong!';
        // return back button title
        this.return = 'Return back';
        this.classes = 'kt-grid kt-grid--ver kt-grid--root';
    }
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ErrorComponent.prototype, "type", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], ErrorComponent.prototype, "image", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ErrorComponent.prototype, "code", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], ErrorComponent.prototype, "title", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], ErrorComponent.prototype, "subtitle", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ErrorComponent.prototype, "desc", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ErrorComponent.prototype, "return", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('class'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], ErrorComponent.prototype, "classes", void 0);
    ErrorComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-error',
            template: __webpack_require__(/*! raw-loader!./error.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/content/general/error/error.component.html"),
            styles: [__webpack_require__(/*! ./error.component.scss */ "./src/app/views/partials/content/general/error/error.component.scss")]
        })
    ], ErrorComponent);
    return ErrorComponent;
}());



/***/ }),

/***/ "./src/app/views/partials/content/general/notice/notice.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/views/partials/content/general/notice/notice.component.ts ***!
  \***************************************************************************/
/*! exports provided: NoticeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NoticeComponent", function() { return NoticeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

// Angular

var NoticeComponent = /** @class */ (function () {
    /**
     * Component constructor
     */
    function NoticeComponent() {
        // Public properties
        this.classes = '';
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    NoticeComponent.prototype.ngOnInit = function () {
        if (this.icon) {
            this.classes += ' kt-alert--icon';
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NoticeComponent.prototype, "classes", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NoticeComponent.prototype, "icon", void 0);
    NoticeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-notice',
            template: __webpack_require__(/*! raw-loader!./notice.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/content/general/notice/notice.component.html"),
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], NoticeComponent);
    return NoticeComponent;
}());



/***/ }),

/***/ "./src/app/views/partials/content/general/portlet/portlet-body.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/views/partials/content/general/portlet/portlet-body.component.ts ***!
  \**********************************************************************************/
/*! exports provided: PortletBodyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PortletBodyComponent", function() { return PortletBodyComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

// Angular

var PortletBodyComponent = /** @class */ (function () {
    function PortletBodyComponent() {
        // Public properties
        this.classList = 'kt-portlet__body';
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    PortletBodyComponent.prototype.ngOnInit = function () {
        if (this.class) {
            this.classList += ' ' + this.class;
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('class'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], PortletBodyComponent.prototype, "classList", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], PortletBodyComponent.prototype, "class", void 0);
    PortletBodyComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: '	kt-portlet-body',
            template: "\n\t\t<ng-content></ng-content>"
        })
    ], PortletBodyComponent);
    return PortletBodyComponent;
}());



/***/ }),

/***/ "./src/app/views/partials/content/general/portlet/portlet-footer.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/views/partials/content/general/portlet/portlet-footer.component.ts ***!
  \************************************************************************************/
/*! exports provided: PortletFooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PortletFooterComponent", function() { return PortletFooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

// Angular

var PortletFooterComponent = /** @class */ (function () {
    function PortletFooterComponent() {
        // Public properties
        this.classList = 'kt-portlet__foot';
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    PortletFooterComponent.prototype.ngOnInit = function () {
        if (this.class) {
            this.classList += ' ' + this.class;
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('class'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], PortletFooterComponent.prototype, "classList", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], PortletFooterComponent.prototype, "class", void 0);
    PortletFooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-portlet-footer',
            template: "\n\t\t<ng-content></ng-content>"
        })
    ], PortletFooterComponent);
    return PortletFooterComponent;
}());



/***/ }),

/***/ "./src/app/views/partials/content/general/portlet/portlet-header.component.scss":
/*!**************************************************************************************!*\
  !*** ./src/app/views/partials/content/general/portlet/portlet-header.component.scss ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  z-index: 1; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2thbWlsL1BSU19ORVdfVmVyc2lvbi9zcmMvYXBwL3ZpZXdzL3BhcnRpYWxzL2NvbnRlbnQvZ2VuZXJhbC9wb3J0bGV0L3BvcnRsZXQtaGVhZGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MsVUFBVSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvdmlld3MvcGFydGlhbHMvY29udGVudC9nZW5lcmFsL3BvcnRsZXQvcG9ydGxldC1oZWFkZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG5cdHotaW5kZXg6IDE7XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/views/partials/content/general/portlet/portlet-header.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/views/partials/content/general/portlet/portlet-header.component.ts ***!
  \************************************************************************************/
/*! exports provided: PortletHeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PortletHeaderComponent", function() { return PortletHeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _core_base_layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../core/_base/layout */ "./src/app/core/_base/layout/index.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");


// Angular

// RXJS

var PortletHeaderComponent = /** @class */ (function () {
    function PortletHeaderComponent(el, platformId, ktDialogService) {
        this.el = el;
        this.platformId = platformId;
        this.ktDialogService = ktDialogService;
        this.viewLoading = false;
        this.classes = 'kt-portlet__head';
        this.lastScrollTop = 0;
        this.subscriptions = [];
        this.isScrollDown = false;
        this.stickyDirective = new _core_base_layout__WEBPACK_IMPORTED_MODULE_1__["StickyDirective"](this.el, this.platformId);
    }
    PortletHeaderComponent.prototype.onResize = function () {
        this.updateStickyPosition();
    };
    PortletHeaderComponent.prototype.onScroll = function () {
        this.updateStickyPosition();
        var st = window.pageYOffset || document.documentElement.scrollTop;
        this.isScrollDown = st > this.lastScrollTop;
        this.lastScrollTop = st <= 0 ? 0 : st;
    };
    PortletHeaderComponent.prototype.updateStickyPosition = function () {
        var _this = this;
        if (this.sticky) {
            Promise.resolve(null).then(function () {
                // get boundary top margin for sticky header
                var headerElement = document.querySelector('.kt-header');
                var subheaderElement = document.querySelector('.kt-subheader');
                var headerMobileElement = document.querySelector('.kt-header-mobile');
                var height = 0;
                if (headerElement != null) {
                    // mobile header
                    if (window.getComputedStyle(headerElement).height === '0px') {
                        height += headerMobileElement.offsetHeight;
                    }
                    else {
                        // desktop header
                        if (document.body.classList.contains('kt-header--minimize-topbar')) {
                            // hardcoded minimized header height
                            height = 60;
                        }
                        else {
                            // normal fixed header
                            if (document.body.classList.contains('kt-header--fixed')) {
                                height += headerElement.offsetHeight;
                            }
                            if (document.body.classList.contains('kt-subheader--fixed')) {
                                height += subheaderElement.offsetHeight;
                            }
                        }
                    }
                }
                _this.stickyDirective.marginTop = height;
            });
        }
    };
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    PortletHeaderComponent.prototype.ngOnInit = function () {
        if (this.sticky) {
            this.stickyDirective.ngOnInit();
        }
    };
    PortletHeaderComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        // append custom class
        this.classes += this.class ? ' ' + this.class : '';
        // hide icon's parent node if no icon provided
        this.hideIcon = this.refIcon.nativeElement.children.length === 0;
        // hide tools' parent node if no tools template is provided
        this.hideTools = this.refTools.nativeElement.children.length === 0;
        if (this.sticky) {
            this.updateStickyPosition();
            this.stickyDirective.ngAfterViewInit();
        }
        // initialize loading dialog
        if (this.viewLoading$) {
            var loadingSubscription = this.viewLoading$.subscribe(function (res) { return _this.toggleLoading(res); });
            this.subscriptions.push(loadingSubscription);
        }
    };
    PortletHeaderComponent.prototype.toggleLoading = function (_incomingValue) {
        this.viewLoading = _incomingValue;
        if (_incomingValue && !this.ktDialogService.checkIsShown()) {
            this.ktDialogService.show();
        }
        if (!this.viewLoading && this.ktDialogService.checkIsShown()) {
            this.ktDialogService.hide();
        }
    };
    PortletHeaderComponent.prototype.ngOnDestroy = function () {
        this.subscriptions.forEach(function (sb) { return sb.unsubscribe(); });
        if (this.sticky) {
            this.stickyDirective.ngOnDestroy();
        }
    };
    PortletHeaderComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"] },
        { type: String, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Inject"], args: [_angular_core__WEBPACK_IMPORTED_MODULE_2__["PLATFORM_ID"],] }] },
        { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_1__["KtDialogService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], PortletHeaderComponent.prototype, "class", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], PortletHeaderComponent.prototype, "title", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], PortletHeaderComponent.prototype, "icon", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], PortletHeaderComponent.prototype, "noTitle", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], PortletHeaderComponent.prototype, "sticky", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
    ], PortletHeaderComponent.prototype, "viewLoading$", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["HostBinding"])('class'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], PortletHeaderComponent.prototype, "classes", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["HostBinding"])('attr.ktSticky'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _core_base_layout__WEBPACK_IMPORTED_MODULE_1__["StickyDirective"])
    ], PortletHeaderComponent.prototype, "stickyDirective", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])('refIcon', { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"])
    ], PortletHeaderComponent.prototype, "refIcon", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])('refTools', { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"])
    ], PortletHeaderComponent.prototype, "refTools", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["HostListener"])('window:resize', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", []),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], PortletHeaderComponent.prototype, "onResize", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["HostListener"])('window:scroll', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", []),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], PortletHeaderComponent.prototype, "onScroll", null);
    PortletHeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'kt-portlet-header',
            template: "\n\t\t<div class=\"kt-portlet__head-label\" [hidden]=\"noTitle\">\n\t\t\t<span class=\"kt-portlet__head-icon\" #refIcon [hidden]=\"hideIcon\">\n\t\t\t\t<ng-content *ngIf=\"!icon\" select=\"[ktPortletIcon]\"></ng-content>\n\t\t\t\t<i *ngIf=\"icon\" [ngClass]=\"icon\"></i>\n\t\t\t</span>\n\t\t\t<ng-content *ngIf=\"!title\" select=\"[ktPortletTitle]\"></ng-content>\n\t\t\t<h3 *ngIf=\"title\" class=\"kt-portlet__head-title\" [innerHTML]=\"title\"></h3>\n\t\t</div>\n\t\t<div class=\"kt-portlet__head-toolbar\" #refTools [hidden]=\"hideTools\">\n\t\t\t<ng-content select=\"[ktPortletTools]\"></ng-content>\n\t\t</div>",
            styles: [__webpack_require__(/*! ./portlet-header.component.scss */ "./src/app/views/partials/content/general/portlet/portlet-header.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Inject"])(_angular_core__WEBPACK_IMPORTED_MODULE_2__["PLATFORM_ID"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"], String, _core_base_layout__WEBPACK_IMPORTED_MODULE_1__["KtDialogService"]])
    ], PortletHeaderComponent);
    return PortletHeaderComponent;
}());



/***/ }),

/***/ "./src/app/views/partials/content/general/portlet/portlet.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/views/partials/content/general/portlet/portlet.component.ts ***!
  \*****************************************************************************/
/*! exports provided: PortletComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PortletComponent", function() { return PortletComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-loading-bar/core */ "./node_modules/@ngx-loading-bar/core/fesm5/ngx-loading-bar-core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _portlet_body_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./portlet-body.component */ "./src/app/views/partials/content/general/portlet/portlet-body.component.ts");
/* harmony import */ var _portlet_header_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./portlet-header.component */ "./src/app/views/partials/content/general/portlet/portlet-header.component.ts");
/* harmony import */ var _portlet_footer_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./portlet-footer.component */ "./src/app/views/partials/content/general/portlet/portlet-footer.component.ts");
/* harmony import */ var _core_base_layout__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../core/_base/layout */ "./src/app/core/_base/layout/index.ts");

// Angular

// Loading bar

// RxJS

// Portlet



// Layout

var PortletComponent = /** @class */ (function () {
    /**
     * Component constructor
     *
     * @param el: ElementRef
     * @param loader: LoadingBarService
     * @param layoutConfigService: LayoutConfigService
     */
    function PortletComponent(el, loader, layoutConfigService) {
        this.el = el;
        this.loader = loader;
        this.layoutConfigService = layoutConfigService;
        this.loader.complete();
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    PortletComponent.prototype.ngOnInit = function () {
    };
    /**
     * After view init
     */
    PortletComponent.prototype.ngAfterViewInit = function () {
    };
    PortletComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
        { type: _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_2__["LoadingBarService"] },
        { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_7__["LayoutConfigService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
    ], PortletComponent.prototype, "loading$", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], PortletComponent.prototype, "options", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], PortletComponent.prototype, "class", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('portlet', { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], PortletComponent.prototype, "portlet", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_portlet_header_component__WEBPACK_IMPORTED_MODULE_5__["PortletHeaderComponent"], { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _portlet_header_component__WEBPACK_IMPORTED_MODULE_5__["PortletHeaderComponent"])
    ], PortletComponent.prototype, "header", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_portlet_body_component__WEBPACK_IMPORTED_MODULE_4__["PortletBodyComponent"], { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _portlet_body_component__WEBPACK_IMPORTED_MODULE_4__["PortletBodyComponent"])
    ], PortletComponent.prototype, "body", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_portlet_footer_component__WEBPACK_IMPORTED_MODULE_6__["PortletFooterComponent"], { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _portlet_footer_component__WEBPACK_IMPORTED_MODULE_6__["PortletFooterComponent"])
    ], PortletComponent.prototype, "footer", void 0);
    PortletComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-portlet',
            template: __webpack_require__(/*! raw-loader!./portlet.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/content/general/portlet/portlet.component.html"),
            exportAs: 'ktPortlet'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_2__["LoadingBarService"],
            _core_base_layout__WEBPACK_IMPORTED_MODULE_7__["LayoutConfigService"]])
    ], PortletComponent);
    return PortletComponent;
}());



/***/ }),

/***/ "./src/app/views/partials/content/general/portlet/portlet.module.ts":
/*!**************************************************************************!*\
  !*** ./src/app/views/partials/content/general/portlet/portlet.module.ts ***!
  \**************************************************************************/
/*! exports provided: PortletModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PortletModule", function() { return PortletModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../core/core.module */ "./src/app/core/core.module.ts");
/* harmony import */ var _portlet_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./portlet.component */ "./src/app/views/partials/content/general/portlet/portlet.component.ts");
/* harmony import */ var _portlet_header_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./portlet-header.component */ "./src/app/views/partials/content/general/portlet/portlet-header.component.ts");
/* harmony import */ var _portlet_body_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./portlet-body.component */ "./src/app/views/partials/content/general/portlet/portlet-body.component.ts");
/* harmony import */ var _portlet_footer_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./portlet-footer.component */ "./src/app/views/partials/content/general/portlet/portlet-footer.component.ts");

// Angular



// Module

// Portlet




var PortletModule = /** @class */ (function () {
    function PortletModule() {
    }
    PortletModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _core_core_module__WEBPACK_IMPORTED_MODULE_4__["CoreModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatProgressBarModule"]
            ],
            declarations: [
                _portlet_component__WEBPACK_IMPORTED_MODULE_5__["PortletComponent"],
                _portlet_header_component__WEBPACK_IMPORTED_MODULE_6__["PortletHeaderComponent"],
                _portlet_body_component__WEBPACK_IMPORTED_MODULE_7__["PortletBodyComponent"],
                _portlet_footer_component__WEBPACK_IMPORTED_MODULE_8__["PortletFooterComponent"],
            ],
            exports: [
                _portlet_component__WEBPACK_IMPORTED_MODULE_5__["PortletComponent"],
                _portlet_header_component__WEBPACK_IMPORTED_MODULE_6__["PortletHeaderComponent"],
                _portlet_body_component__WEBPACK_IMPORTED_MODULE_7__["PortletBodyComponent"],
                _portlet_footer_component__WEBPACK_IMPORTED_MODULE_8__["PortletFooterComponent"],
            ]
        })
    ], PortletModule);
    return PortletModule;
}());



/***/ }),

/***/ "./src/app/views/partials/layout/context-menu/context-menu.component.scss":
/*!********************************************************************************!*\
  !*** ./src/app/views/partials/layout/context-menu/context-menu.component.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhcnRpYWxzL2xheW91dC9jb250ZXh0LW1lbnUvY29udGV4dC1tZW51LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/views/partials/layout/context-menu/context-menu.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/views/partials/layout/context-menu/context-menu.component.ts ***!
  \******************************************************************************/
/*! exports provided: ContextMenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContextMenuComponent", function() { return ContextMenuComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

// Angular

/**
 * Sample context menu dropdown
 */
var ContextMenuComponent = /** @class */ (function () {
    function ContextMenuComponent() {
    }
    ContextMenuComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-context-menu',
            template: __webpack_require__(/*! raw-loader!./context-menu.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/context-menu/context-menu.component.html"),
            styles: [__webpack_require__(/*! ./context-menu.component.scss */ "./src/app/views/partials/layout/context-menu/context-menu.component.scss")]
        })
    ], ContextMenuComponent);
    return ContextMenuComponent;
}());



/***/ }),

/***/ "./src/app/views/partials/layout/context-menu2/context-menu2.component.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/views/partials/layout/context-menu2/context-menu2.component.scss ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host [ngbdropdowntoggle]::after {\n  display: inline-block !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2thbWlsL1BSU19ORVdfVmVyc2lvbi9zcmMvYXBwL3ZpZXdzL3BhcnRpYWxzL2xheW91dC9jb250ZXh0LW1lbnUyL2NvbnRleHQtbWVudTIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFHRSxnQ0FBZ0MsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhcnRpYWxzL2xheW91dC9jb250ZXh0LW1lbnUyL2NvbnRleHQtbWVudTIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG5cdC8vIGVuYWJsZSBuZy1ib29zdHJhcCBkcm9wZG93bSBhcnJvd1xuXHRbbmdiZHJvcGRvd250b2dnbGVdOjphZnRlciB7XG5cdFx0ZGlzcGxheTogaW5saW5lLWJsb2NrICFpbXBvcnRhbnQ7XG5cdH1cbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/views/partials/layout/context-menu2/context-menu2.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/views/partials/layout/context-menu2/context-menu2.component.ts ***!
  \********************************************************************************/
/*! exports provided: ContextMenu2Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContextMenu2Component", function() { return ContextMenu2Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

// Angular

/**
 * Sample context menu dropdown
 */
var ContextMenu2Component = /** @class */ (function () {
    function ContextMenu2Component() {
    }
    ContextMenu2Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-context-menu2',
            template: __webpack_require__(/*! raw-loader!./context-menu2.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/context-menu2/context-menu2.component.html"),
            styles: [__webpack_require__(/*! ./context-menu2.component.scss */ "./src/app/views/partials/layout/context-menu2/context-menu2.component.scss")]
        })
    ], ContextMenu2Component);
    return ContextMenu2Component;
}());



/***/ }),

/***/ "./src/app/views/partials/layout/index.ts":
/*!************************************************!*\
  !*** ./src/app/views/partials/layout/index.ts ***!
  \************************************************/
/*! exports provided: ContextMenuComponent, ContextMenu2Component, QuickPanelComponent, ScrollTopComponent, SearchResultComponent, SplashScreenComponent, StickyToolbarComponent, Subheader1Component, Subheader2Component, Subheader3Component, Subheader4Component, Subheader5Component, SubheaderSearchComponent, LanguageSelectorComponent, NotificationComponent, QuickActionComponent, SearchDefaultComponent, SearchDropdownComponent, UserProfileComponent, UserProfile2Component, UserProfile3Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _context_menu_context_menu_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./context-menu/context-menu.component */ "./src/app/views/partials/layout/context-menu/context-menu.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ContextMenuComponent", function() { return _context_menu_context_menu_component__WEBPACK_IMPORTED_MODULE_0__["ContextMenuComponent"]; });

/* harmony import */ var _context_menu2_context_menu2_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./context-menu2/context-menu2.component */ "./src/app/views/partials/layout/context-menu2/context-menu2.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ContextMenu2Component", function() { return _context_menu2_context_menu2_component__WEBPACK_IMPORTED_MODULE_1__["ContextMenu2Component"]; });

/* harmony import */ var _quick_panel_quick_panel_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./quick-panel/quick-panel.component */ "./src/app/views/partials/layout/quick-panel/quick-panel.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "QuickPanelComponent", function() { return _quick_panel_quick_panel_component__WEBPACK_IMPORTED_MODULE_2__["QuickPanelComponent"]; });

/* harmony import */ var _scroll_top_scroll_top_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./scroll-top/scroll-top.component */ "./src/app/views/partials/layout/scroll-top/scroll-top.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ScrollTopComponent", function() { return _scroll_top_scroll_top_component__WEBPACK_IMPORTED_MODULE_3__["ScrollTopComponent"]; });

/* harmony import */ var _search_result_search_result_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./search-result/search-result.component */ "./src/app/views/partials/layout/search-result/search-result.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SearchResultComponent", function() { return _search_result_search_result_component__WEBPACK_IMPORTED_MODULE_4__["SearchResultComponent"]; });

/* harmony import */ var _splash_screen_splash_screen_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./splash-screen/splash-screen.component */ "./src/app/views/partials/layout/splash-screen/splash-screen.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SplashScreenComponent", function() { return _splash_screen_splash_screen_component__WEBPACK_IMPORTED_MODULE_5__["SplashScreenComponent"]; });

/* harmony import */ var _sticky_toolbar_sticky_toolbar_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./sticky-toolbar/sticky-toolbar.component */ "./src/app/views/partials/layout/sticky-toolbar/sticky-toolbar.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "StickyToolbarComponent", function() { return _sticky_toolbar_sticky_toolbar_component__WEBPACK_IMPORTED_MODULE_6__["StickyToolbarComponent"]; });

/* harmony import */ var _subheader_subheader1_subheader1_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./subheader/subheader1/subheader1.component */ "./src/app/views/partials/layout/subheader/subheader1/subheader1.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Subheader1Component", function() { return _subheader_subheader1_subheader1_component__WEBPACK_IMPORTED_MODULE_7__["Subheader1Component"]; });

/* harmony import */ var _subheader_subheader2_subheader2_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./subheader/subheader2/subheader2.component */ "./src/app/views/partials/layout/subheader/subheader2/subheader2.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Subheader2Component", function() { return _subheader_subheader2_subheader2_component__WEBPACK_IMPORTED_MODULE_8__["Subheader2Component"]; });

/* harmony import */ var _subheader_subheader3_subheader3_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./subheader/subheader3/subheader3.component */ "./src/app/views/partials/layout/subheader/subheader3/subheader3.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Subheader3Component", function() { return _subheader_subheader3_subheader3_component__WEBPACK_IMPORTED_MODULE_9__["Subheader3Component"]; });

/* harmony import */ var _subheader_subheader4_subheader4_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./subheader/subheader4/subheader4.component */ "./src/app/views/partials/layout/subheader/subheader4/subheader4.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Subheader4Component", function() { return _subheader_subheader4_subheader4_component__WEBPACK_IMPORTED_MODULE_10__["Subheader4Component"]; });

/* harmony import */ var _subheader_subheader5_subheader5_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./subheader/subheader5/subheader5.component */ "./src/app/views/partials/layout/subheader/subheader5/subheader5.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Subheader5Component", function() { return _subheader_subheader5_subheader5_component__WEBPACK_IMPORTED_MODULE_11__["Subheader5Component"]; });

/* harmony import */ var _subheader_subheader_search_subheader_search_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./subheader/subheader-search/subheader-search.component */ "./src/app/views/partials/layout/subheader/subheader-search/subheader-search.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SubheaderSearchComponent", function() { return _subheader_subheader_search_subheader_search_component__WEBPACK_IMPORTED_MODULE_12__["SubheaderSearchComponent"]; });

/* harmony import */ var _topbar_language_selector_language_selector_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./topbar/language-selector/language-selector.component */ "./src/app/views/partials/layout/topbar/language-selector/language-selector.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LanguageSelectorComponent", function() { return _topbar_language_selector_language_selector_component__WEBPACK_IMPORTED_MODULE_13__["LanguageSelectorComponent"]; });

/* harmony import */ var _topbar_notification_notification_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./topbar/notification/notification.component */ "./src/app/views/partials/layout/topbar/notification/notification.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "NotificationComponent", function() { return _topbar_notification_notification_component__WEBPACK_IMPORTED_MODULE_14__["NotificationComponent"]; });

/* harmony import */ var _topbar_quick_action_quick_action_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./topbar/quick-action/quick-action.component */ "./src/app/views/partials/layout/topbar/quick-action/quick-action.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "QuickActionComponent", function() { return _topbar_quick_action_quick_action_component__WEBPACK_IMPORTED_MODULE_15__["QuickActionComponent"]; });

/* harmony import */ var _topbar_search_default_search_default_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./topbar/search-default/search-default.component */ "./src/app/views/partials/layout/topbar/search-default/search-default.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SearchDefaultComponent", function() { return _topbar_search_default_search_default_component__WEBPACK_IMPORTED_MODULE_16__["SearchDefaultComponent"]; });

/* harmony import */ var _topbar_search_dropdown_search_dropdown_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./topbar/search-dropdown/search-dropdown.component */ "./src/app/views/partials/layout/topbar/search-dropdown/search-dropdown.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SearchDropdownComponent", function() { return _topbar_search_dropdown_search_dropdown_component__WEBPACK_IMPORTED_MODULE_17__["SearchDropdownComponent"]; });

/* harmony import */ var _topbar_user_profile_user_profile_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./topbar/user-profile/user-profile.component */ "./src/app/views/partials/layout/topbar/user-profile/user-profile.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UserProfileComponent", function() { return _topbar_user_profile_user_profile_component__WEBPACK_IMPORTED_MODULE_18__["UserProfileComponent"]; });

/* harmony import */ var _topbar_user_profile2_user_profile2_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./topbar/user-profile2/user-profile2.component */ "./src/app/views/partials/layout/topbar/user-profile2/user-profile2.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UserProfile2Component", function() { return _topbar_user_profile2_user_profile2_component__WEBPACK_IMPORTED_MODULE_19__["UserProfile2Component"]; });

/* harmony import */ var _topbar_user_profile3_user_profile3_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./topbar/user-profile3/user-profile3.component */ "./src/app/views/partials/layout/topbar/user-profile3/user-profile3.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UserProfile3Component", function() { return _topbar_user_profile3_user_profile3_component__WEBPACK_IMPORTED_MODULE_20__["UserProfile3Component"]; });

// Components







// Subheader components






// Topbar components










/***/ }),

/***/ "./src/app/views/partials/layout/quick-panel/quick-panel.component.scss":
/*!******************************************************************************!*\
  !*** ./src/app/views/partials/layout/quick-panel/quick-panel.component.scss ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ::ng-deep ngb-tabset > .nav-tabs {\n  display: none; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2thbWlsL1BSU19ORVdfVmVyc2lvbi9zcmMvYXBwL3ZpZXdzL3BhcnRpYWxzL2xheW91dC9xdWljay1wYW5lbC9xdWljay1wYW5lbC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUdHLGFBQWEsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhcnRpYWxzL2xheW91dC9xdWljay1wYW5lbC9xdWljay1wYW5lbC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcblx0OjpuZy1kZWVwIHtcblx0XHRuZ2ItdGFic2V0ID4gLm5hdi10YWJzIHtcblx0XHRcdGRpc3BsYXk6IG5vbmU7XG5cdFx0fVxuXHR9XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/views/partials/layout/quick-panel/quick-panel.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/views/partials/layout/quick-panel/quick-panel.component.ts ***!
  \****************************************************************************/
/*! exports provided: QuickPanelComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuickPanelComponent", function() { return QuickPanelComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

// Angular

var QuickPanelComponent = /** @class */ (function () {
    function QuickPanelComponent() {
        // Public properties
        this.offcanvasOptions = {
            overlay: true,
            baseClass: 'kt-quick-panel',
            closeBy: 'kt_quick_panel_close_btn',
            toggleBy: 'kt_quick_panel_toggler_btn'
        };
    }
    QuickPanelComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-quick-panel',
            template: __webpack_require__(/*! raw-loader!./quick-panel.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/quick-panel/quick-panel.component.html"),
            styles: [__webpack_require__(/*! ./quick-panel.component.scss */ "./src/app/views/partials/layout/quick-panel/quick-panel.component.scss")]
        })
    ], QuickPanelComponent);
    return QuickPanelComponent;
}());



/***/ }),

/***/ "./src/app/views/partials/layout/scroll-top/scroll-top.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/views/partials/layout/scroll-top/scroll-top.component.ts ***!
  \**************************************************************************/
/*! exports provided: ScrollTopComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ScrollTopComponent", function() { return ScrollTopComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

// Angular

var ScrollTopComponent = /** @class */ (function () {
    function ScrollTopComponent() {
        // Public properties
        this.scrollTopOptions = {
            offset: 300,
            speed: 600
        };
    }
    ScrollTopComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-scroll-top',
            template: __webpack_require__(/*! raw-loader!./scroll-top.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/scroll-top/scroll-top.component.html"),
        })
    ], ScrollTopComponent);
    return ScrollTopComponent;
}());



/***/ }),

/***/ "./src/app/views/partials/layout/search-result/search-result.component.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/views/partials/layout/search-result/search-result.component.scss ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host .kt-quick-search__category:first-child {\n  margin-top: 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2thbWlsL1BSU19ORVdfVmVyc2lvbi9zcmMvYXBwL3ZpZXdzL3BhcnRpYWxzL2xheW91dC9zZWFyY2gtcmVzdWx0L3NlYXJjaC1yZXN1bHQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFRSxhQUFhLEVBQUEiLCJmaWxlIjoic3JjL2FwcC92aWV3cy9wYXJ0aWFscy9sYXlvdXQvc2VhcmNoLXJlc3VsdC9zZWFyY2gtcmVzdWx0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xuXHQua3QtcXVpY2stc2VhcmNoX19jYXRlZ29yeTpmaXJzdC1jaGlsZCB7XG5cdFx0bWFyZ2luLXRvcDogMDtcblx0fVxufVxuIl19 */"

/***/ }),

/***/ "./src/app/views/partials/layout/search-result/search-result.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/views/partials/layout/search-result/search-result.component.ts ***!
  \********************************************************************************/
/*! exports provided: SearchResultComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchResultComponent", function() { return SearchResultComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

// Angular

var SearchResultComponent = /** @class */ (function () {
    function SearchResultComponent() {
    }
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], SearchResultComponent.prototype, "data", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], SearchResultComponent.prototype, "noRecordText", void 0);
    SearchResultComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-search-result',
            template: __webpack_require__(/*! raw-loader!./search-result.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/search-result/search-result.component.html"),
            styles: [__webpack_require__(/*! ./search-result.component.scss */ "./src/app/views/partials/layout/search-result/search-result.component.scss")]
        })
    ], SearchResultComponent);
    return SearchResultComponent;
}());



/***/ }),

/***/ "./src/app/views/partials/layout/splash-screen/splash-screen.component.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/views/partials/layout/splash-screen/splash-screen.component.scss ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host .kt-splash-screen {\n  background-color: #f2f3f8;\n  position: absolute;\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  justify-content: center;\n  height: 100%;\n  width: 100%;\n  z-index: 1000; }\n  :host .kt-splash-screen img {\n    margin-left: calc(100vw - 100%);\n    width: 90px;\n    margin-bottom: 30px; }\n  :host .kt-splash-screen span {\n    margin-left: calc(100vw - 100%);\n    margin-bottom: 30px; }\n  :host .kt-splash-screen ::ng-deep [role=\"progressbar\"] {\n    margin-left: calc(100vw - 100%); }\n  :host .kt-splash-screen ::ng-deep .mat-progress-spinner circle, :host .kt-splash-screen ::ng-deep .mat-spinner circle {\n    stroke: #5d78ff; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2thbWlsL1BSU19ORVdfVmVyc2lvbi9zcmMvYXBwL3ZpZXdzL3BhcnRpYWxzL2xheW91dC9zcGxhc2gtc2NyZWVuL3NwbGFzaC1zY3JlZW4uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFRSx5QkFBeUI7RUFDekIsa0JBQWtCO0VBQ2xCLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixZQUFZO0VBQ1osV0FBVztFQUNYLGFBQWEsRUFBQTtFQVZmO0lBYUcsK0JBQStCO0lBQy9CLFdBQVc7SUFDWCxtQkFBbUIsRUFBQTtFQWZ0QjtJQW1CRywrQkFBK0I7SUFDL0IsbUJBQW1CLEVBQUE7RUFwQnRCO0lBeUJJLCtCQUErQixFQUFBO0VBekJuQztJQTZCSSxlQUFlLEVBQUEiLCJmaWxlIjoic3JjL2FwcC92aWV3cy9wYXJ0aWFscy9sYXlvdXQvc3BsYXNoLXNjcmVlbi9zcGxhc2gtc2NyZWVuLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xuXHQua3Qtc3BsYXNoLXNjcmVlbiB7XG5cdFx0YmFja2dyb3VuZC1jb2xvcjogI2YyZjNmODtcblx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XG5cdFx0ZGlzcGxheTogZmxleDtcblx0XHRmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuXHRcdGFsaWduLWl0ZW1zOiBjZW50ZXI7XG5cdFx0anVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5cdFx0aGVpZ2h0OiAxMDAlO1xuXHRcdHdpZHRoOiAxMDAlO1xuXHRcdHotaW5kZXg6IDEwMDA7XG5cblx0XHRpbWcge1xuXHRcdFx0bWFyZ2luLWxlZnQ6IGNhbGMoMTAwdncgLSAxMDAlKTtcblx0XHRcdHdpZHRoOiA5MHB4O1xuXHRcdFx0bWFyZ2luLWJvdHRvbTogMzBweDtcblx0XHR9XG5cblx0XHRzcGFuIHtcblx0XHRcdG1hcmdpbi1sZWZ0OiBjYWxjKDEwMHZ3IC0gMTAwJSk7XG5cdFx0XHRtYXJnaW4tYm90dG9tOiAzMHB4O1xuXHRcdH1cblxuXHRcdDo6bmctZGVlcCB7XG5cdFx0XHRbcm9sZT1cInByb2dyZXNzYmFyXCJdIHtcblx0XHRcdFx0bWFyZ2luLWxlZnQ6IGNhbGMoMTAwdncgLSAxMDAlKTtcblx0XHRcdH1cblx0XHRcdC5tYXQtcHJvZ3Jlc3Mtc3Bpbm5lciBjaXJjbGUsIC5tYXQtc3Bpbm5lciBjaXJjbGUge1xuXHRcdFx0XHQvLyBicmFuZCBjb2xvclxuXHRcdFx0XHRzdHJva2U6ICM1ZDc4ZmY7XG5cdFx0XHR9XG5cdFx0fVxuXHR9XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/views/partials/layout/splash-screen/splash-screen.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/views/partials/layout/splash-screen/splash-screen.component.ts ***!
  \********************************************************************************/
/*! exports provided: SplashScreenComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SplashScreenComponent", function() { return SplashScreenComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var object_path__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! object-path */ "./node_modules/object-path/index.js");
/* harmony import */ var object_path__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(object_path__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _core_base_layout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../core/_base/layout */ "./src/app/core/_base/layout/index.ts");

// Angular

// Object-Path

// Layout

var SplashScreenComponent = /** @class */ (function () {
    /**
     * Component constructor
     *
     * @param el: ElementRef
     * @param layoutConfigService: LayoutConfigService
     * @param splashScreenService: SplachScreenService
     */
    function SplashScreenComponent(el, layoutConfigService) {
        this.el = el;
        this.layoutConfigService = layoutConfigService;
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    SplashScreenComponent.prototype.ngOnInit = function () {
        // init splash screen, see loader option in layout.config.ts
        var loaderConfig = this.layoutConfigService.getConfig('loader');
        this.loaderLogo = object_path__WEBPACK_IMPORTED_MODULE_2__["get"](loaderConfig, 'logo');
        this.loaderType = object_path__WEBPACK_IMPORTED_MODULE_2__["get"](loaderConfig, 'type');
        this.loaderMessage = object_path__WEBPACK_IMPORTED_MODULE_2__["get"](loaderConfig, 'message');
    };
    SplashScreenComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
        { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_3__["LayoutConfigService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('splashScreen', { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], SplashScreenComponent.prototype, "splashScreen", void 0);
    SplashScreenComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-splash-screen',
            template: __webpack_require__(/*! raw-loader!./splash-screen.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/splash-screen/splash-screen.component.html"),
            styles: [__webpack_require__(/*! ./splash-screen.component.scss */ "./src/app/views/partials/layout/splash-screen/splash-screen.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"],
            _core_base_layout__WEBPACK_IMPORTED_MODULE_3__["LayoutConfigService"]])
    ], SplashScreenComponent);
    return SplashScreenComponent;
}());



/***/ }),

/***/ "./src/app/views/partials/layout/sticky-toolbar/sticky-toolbar.component.scss":
/*!************************************************************************************!*\
  !*** ./src/app/views/partials/layout/sticky-toolbar/sticky-toolbar.component.scss ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ::ng-deep .tooltip {\n  white-space: nowrap; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2thbWlsL1BSU19ORVdfVmVyc2lvbi9zcmMvYXBwL3ZpZXdzL3BhcnRpYWxzL2xheW91dC9zdGlja3ktdG9vbGJhci9zdGlja3ktdG9vbGJhci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUdHLG1CQUFtQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvdmlld3MvcGFydGlhbHMvbGF5b3V0L3N0aWNreS10b29sYmFyL3N0aWNreS10b29sYmFyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xuXHQ6Om5nLWRlZXAge1xuXHRcdC50b29sdGlwIHtcblx0XHRcdHdoaXRlLXNwYWNlOiBub3dyYXA7XG5cdFx0fVxuXHR9XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/views/partials/layout/sticky-toolbar/sticky-toolbar.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/views/partials/layout/sticky-toolbar/sticky-toolbar.component.ts ***!
  \**********************************************************************************/
/*! exports provided: StickyToolbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StickyToolbarComponent", function() { return StickyToolbarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_base_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../core/_base/layout */ "./src/app/core/_base/layout/index.ts");

// Angular

// Layout

var StickyToolbarComponent = /** @class */ (function () {
    function StickyToolbarComponent(layoutConfigService) {
        this.layoutConfigService = layoutConfigService;
        // Public properties
        this.demoPanelOptions = {
            overlay: true,
            baseClass: 'kt-demo-panel',
            closeBy: 'kt_demo_panel_close',
            toggleBy: 'kt_demo_panel_toggle',
        };
        this.baseHref = 'https://keenthemes.com/metronic/preview/angular/';
    }
    StickyToolbarComponent.prototype.isActiveDemo = function (demo) {
        return demo === this.layoutConfigService.getConfig('demo');
    };
    StickyToolbarComponent.ctorParameters = function () { return [
        { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_2__["LayoutConfigService"] }
    ]; };
    StickyToolbarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-sticky-toolbar',
            template: __webpack_require__(/*! raw-loader!./sticky-toolbar.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/sticky-toolbar/sticky-toolbar.component.html"),
            styles: [__webpack_require__(/*! ./sticky-toolbar.component.scss */ "./src/app/views/partials/layout/sticky-toolbar/sticky-toolbar.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_base_layout__WEBPACK_IMPORTED_MODULE_2__["LayoutConfigService"]])
    ], StickyToolbarComponent);
    return StickyToolbarComponent;
}());



/***/ }),

/***/ "./src/app/views/partials/layout/subheader/subheader-search/subheader-search.component.scss":
/*!**************************************************************************************************!*\
  !*** ./src/app/views/partials/layout/subheader/subheader-search/subheader-search.component.scss ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host .kt-subheader__desc {\n  margin: 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2thbWlsL1BSU19ORVdfVmVyc2lvbi9zcmMvYXBwL3ZpZXdzL3BhcnRpYWxzL2xheW91dC9zdWJoZWFkZXIvc3ViaGVhZGVyLXNlYXJjaC9zdWJoZWFkZXItc2VhcmNoLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUUsU0FBUyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvdmlld3MvcGFydGlhbHMvbGF5b3V0L3N1YmhlYWRlci9zdWJoZWFkZXItc2VhcmNoL3N1YmhlYWRlci1zZWFyY2guY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG5cdC5rdC1zdWJoZWFkZXJfX2Rlc2Mge1xuXHRcdG1hcmdpbjogMDtcblx0fVxufVxuIl19 */"

/***/ }),

/***/ "./src/app/views/partials/layout/subheader/subheader-search/subheader-search.component.ts":
/*!************************************************************************************************!*\
  !*** ./src/app/views/partials/layout/subheader/subheader-search/subheader-search.component.ts ***!
  \************************************************************************************************/
/*! exports provided: SubheaderSearchComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubheaderSearchComponent", function() { return SubheaderSearchComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_base_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../core/_base/layout */ "./src/app/core/_base/layout/index.ts");

// Angular

// Layout

var SubheaderSearchComponent = /** @class */ (function () {
    /**
     * Component constructor
     *
     * @param subheaderService: SubheaderService
     */
    function SubheaderSearchComponent(subheaderService) {
        this.subheaderService = subheaderService;
        this.today = Date.now();
        this.title = '';
        this.desc = '';
        this.breadcrumbs = [];
        // Private properties
        this.subscriptions = [];
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    SubheaderSearchComponent.prototype.ngOnInit = function () {
    };
    /**
     * After view init
     */
    SubheaderSearchComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.subscriptions.push(this.subheaderService.title$.subscribe(function (bt) {
            // breadcrumbs title sometimes can be undefined
            if (bt) {
                Promise.resolve(null).then(function () {
                    _this.title = bt.title;
                    _this.desc = bt.desc;
                });
            }
        }));
        this.subscriptions.push(this.subheaderService.breadcrumbs$.subscribe(function (bc) {
            Promise.resolve(null).then(function () {
                _this.breadcrumbs = bc;
            });
        }));
    };
    /**
     * On destroy
     */
    SubheaderSearchComponent.prototype.ngOnDestroy = function () {
        this.subscriptions.forEach(function (sb) { return sb.unsubscribe(); });
    };
    SubheaderSearchComponent.ctorParameters = function () { return [
        { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_2__["SubheaderService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], SubheaderSearchComponent.prototype, "fluid", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], SubheaderSearchComponent.prototype, "clear", void 0);
    SubheaderSearchComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-subheader-search',
            template: __webpack_require__(/*! raw-loader!./subheader-search.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/subheader/subheader-search/subheader-search.component.html"),
            styles: [__webpack_require__(/*! ./subheader-search.component.scss */ "./src/app/views/partials/layout/subheader/subheader-search/subheader-search.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_base_layout__WEBPACK_IMPORTED_MODULE_2__["SubheaderService"]])
    ], SubheaderSearchComponent);
    return SubheaderSearchComponent;
}());



/***/ }),

/***/ "./src/app/views/partials/layout/subheader/subheader1/subheader1.component.scss":
/*!**************************************************************************************!*\
  !*** ./src/app/views/partials/layout/subheader/subheader1/subheader1.component.scss ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host .kt-subheader__desc {\n  margin: 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2thbWlsL1BSU19ORVdfVmVyc2lvbi9zcmMvYXBwL3ZpZXdzL3BhcnRpYWxzL2xheW91dC9zdWJoZWFkZXIvc3ViaGVhZGVyMS9zdWJoZWFkZXIxLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUUsU0FBUyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvdmlld3MvcGFydGlhbHMvbGF5b3V0L3N1YmhlYWRlci9zdWJoZWFkZXIxL3N1YmhlYWRlcjEuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG5cdC5rdC1zdWJoZWFkZXJfX2Rlc2Mge1xuXHRcdG1hcmdpbjogMDtcblx0fVxufVxuIl19 */"

/***/ }),

/***/ "./src/app/views/partials/layout/subheader/subheader1/subheader1.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/views/partials/layout/subheader/subheader1/subheader1.component.ts ***!
  \************************************************************************************/
/*! exports provided: Subheader1Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Subheader1Component", function() { return Subheader1Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_base_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../core/_base/layout */ "./src/app/core/_base/layout/index.ts");

// Angular

// Layout

var Subheader1Component = /** @class */ (function () {
    /**
     * Component constructor
     *
     * @param subheaderService: SubheaderService
     */
    function Subheader1Component(subheaderService) {
        this.subheaderService = subheaderService;
        this.today = Date.now();
        this.title = '';
        this.desc = '';
        this.breadcrumbs = [];
        // Private properties
        this.subscriptions = [];
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    Subheader1Component.prototype.ngOnInit = function () {
    };
    /**
     * After view init
     */
    Subheader1Component.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.subscriptions.push(this.subheaderService.title$.subscribe(function (bt) {
            // breadcrumbs title sometimes can be undefined
            if (bt) {
                Promise.resolve(null).then(function () {
                    _this.title = bt.title;
                    _this.desc = bt.desc;
                });
            }
        }));
        this.subscriptions.push(this.subheaderService.breadcrumbs$.subscribe(function (bc) {
            Promise.resolve(null).then(function () {
                _this.breadcrumbs = bc;
            });
        }));
    };
    /**
     * On destroy
     */
    Subheader1Component.prototype.ngOnDestroy = function () {
        this.subscriptions.forEach(function (sb) { return sb.unsubscribe(); });
    };
    Subheader1Component.ctorParameters = function () { return [
        { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_2__["SubheaderService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], Subheader1Component.prototype, "fluid", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], Subheader1Component.prototype, "clear", void 0);
    Subheader1Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-subheader1',
            template: __webpack_require__(/*! raw-loader!./subheader1.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/subheader/subheader1/subheader1.component.html"),
            styles: [__webpack_require__(/*! ./subheader1.component.scss */ "./src/app/views/partials/layout/subheader/subheader1/subheader1.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_base_layout__WEBPACK_IMPORTED_MODULE_2__["SubheaderService"]])
    ], Subheader1Component);
    return Subheader1Component;
}());



/***/ }),

/***/ "./src/app/views/partials/layout/subheader/subheader2/subheader2.component.scss":
/*!**************************************************************************************!*\
  !*** ./src/app/views/partials/layout/subheader/subheader2/subheader2.component.scss ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host .kt-subheader__desc {\n  margin: 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2thbWlsL1BSU19ORVdfVmVyc2lvbi9zcmMvYXBwL3ZpZXdzL3BhcnRpYWxzL2xheW91dC9zdWJoZWFkZXIvc3ViaGVhZGVyMi9zdWJoZWFkZXIyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUUsU0FBUyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvdmlld3MvcGFydGlhbHMvbGF5b3V0L3N1YmhlYWRlci9zdWJoZWFkZXIyL3N1YmhlYWRlcjIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG5cdC5rdC1zdWJoZWFkZXJfX2Rlc2Mge1xuXHRcdG1hcmdpbjogMDtcblx0fVxufVxuIl19 */"

/***/ }),

/***/ "./src/app/views/partials/layout/subheader/subheader2/subheader2.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/views/partials/layout/subheader/subheader2/subheader2.component.ts ***!
  \************************************************************************************/
/*! exports provided: Subheader2Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Subheader2Component", function() { return Subheader2Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_base_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../core/_base/layout */ "./src/app/core/_base/layout/index.ts");

// Angular

// Layout

var Subheader2Component = /** @class */ (function () {
    /**
     * Component constructor
     *
     * @param subheaderService: SubheaderService
     */
    function Subheader2Component(subheaderService) {
        this.subheaderService = subheaderService;
        this.today = Date.now();
        this.title = '';
        this.desc = '';
        this.breadcrumbs = [];
        // Private properties
        this.subscriptions = [];
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    Subheader2Component.prototype.ngOnInit = function () {
    };
    /**
     * After view init
     */
    Subheader2Component.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.subscriptions.push(this.subheaderService.title$.subscribe(function (bt) {
            // breadcrumbs title sometimes can be undefined
            if (bt) {
                Promise.resolve(null).then(function () {
                    _this.title = bt.title;
                    _this.desc = bt.desc;
                });
            }
        }));
        this.subscriptions.push(this.subheaderService.breadcrumbs$.subscribe(function (bc) {
            Promise.resolve(null).then(function () {
                _this.breadcrumbs = bc;
            });
        }));
    };
    /**
     * On destroy
     */
    Subheader2Component.prototype.ngOnDestroy = function () {
        this.subscriptions.forEach(function (sb) { return sb.unsubscribe(); });
    };
    Subheader2Component.ctorParameters = function () { return [
        { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_2__["SubheaderService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], Subheader2Component.prototype, "fluid", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], Subheader2Component.prototype, "clear", void 0);
    Subheader2Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-subheader2',
            template: __webpack_require__(/*! raw-loader!./subheader2.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/subheader/subheader2/subheader2.component.html"),
            styles: [__webpack_require__(/*! ./subheader2.component.scss */ "./src/app/views/partials/layout/subheader/subheader2/subheader2.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_base_layout__WEBPACK_IMPORTED_MODULE_2__["SubheaderService"]])
    ], Subheader2Component);
    return Subheader2Component;
}());



/***/ }),

/***/ "./src/app/views/partials/layout/subheader/subheader3/subheader3.component.scss":
/*!**************************************************************************************!*\
  !*** ./src/app/views/partials/layout/subheader/subheader3/subheader3.component.scss ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host .kt-subheader__desc {\n  margin: 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2thbWlsL1BSU19ORVdfVmVyc2lvbi9zcmMvYXBwL3ZpZXdzL3BhcnRpYWxzL2xheW91dC9zdWJoZWFkZXIvc3ViaGVhZGVyMy9zdWJoZWFkZXIzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUUsU0FBUyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvdmlld3MvcGFydGlhbHMvbGF5b3V0L3N1YmhlYWRlci9zdWJoZWFkZXIzL3N1YmhlYWRlcjMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG5cdC5rdC1zdWJoZWFkZXJfX2Rlc2Mge1xuXHRcdG1hcmdpbjogMDtcblx0fVxufVxuIl19 */"

/***/ }),

/***/ "./src/app/views/partials/layout/subheader/subheader3/subheader3.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/views/partials/layout/subheader/subheader3/subheader3.component.ts ***!
  \************************************************************************************/
/*! exports provided: Subheader3Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Subheader3Component", function() { return Subheader3Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_base_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../core/_base/layout */ "./src/app/core/_base/layout/index.ts");

// Angular

// Layout

var Subheader3Component = /** @class */ (function () {
    /**
     * Component constructor
     *
     * @param subheaderService: SubheaderService
     */
    function Subheader3Component(subheaderService) {
        this.subheaderService = subheaderService;
        this.today = Date.now();
        this.title = '';
        this.desc = '';
        this.breadcrumbs = [];
        // Private properties
        this.subscriptions = [];
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    Subheader3Component.prototype.ngOnInit = function () {
    };
    /**
     * After view init
     */
    Subheader3Component.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.subscriptions.push(this.subheaderService.title$.subscribe(function (bt) {
            // breadcrumbs title sometimes can be undefined
            if (bt) {
                Promise.resolve(null).then(function () {
                    _this.title = bt.title;
                    _this.desc = bt.desc;
                });
            }
        }));
        this.subscriptions.push(this.subheaderService.breadcrumbs$.subscribe(function (bc) {
            Promise.resolve(null).then(function () {
                _this.breadcrumbs = bc;
            });
        }));
    };
    /**
     * On destroy
     */
    Subheader3Component.prototype.ngOnDestroy = function () {
        this.subscriptions.forEach(function (sb) { return sb.unsubscribe(); });
    };
    Subheader3Component.ctorParameters = function () { return [
        { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_2__["SubheaderService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], Subheader3Component.prototype, "fluid", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], Subheader3Component.prototype, "clear", void 0);
    Subheader3Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-subheader3',
            template: __webpack_require__(/*! raw-loader!./subheader3.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/subheader/subheader3/subheader3.component.html"),
            styles: [__webpack_require__(/*! ./subheader3.component.scss */ "./src/app/views/partials/layout/subheader/subheader3/subheader3.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_base_layout__WEBPACK_IMPORTED_MODULE_2__["SubheaderService"]])
    ], Subheader3Component);
    return Subheader3Component;
}());



/***/ }),

/***/ "./src/app/views/partials/layout/subheader/subheader4/subheader4.component.scss":
/*!**************************************************************************************!*\
  !*** ./src/app/views/partials/layout/subheader/subheader4/subheader4.component.scss ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host .kt-subheader__desc {\n  margin: 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2thbWlsL1BSU19ORVdfVmVyc2lvbi9zcmMvYXBwL3ZpZXdzL3BhcnRpYWxzL2xheW91dC9zdWJoZWFkZXIvc3ViaGVhZGVyNC9zdWJoZWFkZXI0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUUsU0FBUyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvdmlld3MvcGFydGlhbHMvbGF5b3V0L3N1YmhlYWRlci9zdWJoZWFkZXI0L3N1YmhlYWRlcjQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG5cdC5rdC1zdWJoZWFkZXJfX2Rlc2Mge1xuXHRcdG1hcmdpbjogMDtcblx0fVxufVxuIl19 */"

/***/ }),

/***/ "./src/app/views/partials/layout/subheader/subheader4/subheader4.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/views/partials/layout/subheader/subheader4/subheader4.component.ts ***!
  \************************************************************************************/
/*! exports provided: Subheader4Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Subheader4Component", function() { return Subheader4Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_base_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../core/_base/layout */ "./src/app/core/_base/layout/index.ts");

// Angular

// Layout

var Subheader4Component = /** @class */ (function () {
    /**
     * Component constructor
     *
     * @param subheaderService: SubheaderService
     */
    function Subheader4Component(subheaderService) {
        this.subheaderService = subheaderService;
        this.today = Date.now();
        this.title = '';
        this.desc = '';
        this.breadcrumbs = [];
        // Private properties
        this.subscriptions = [];
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    Subheader4Component.prototype.ngOnInit = function () {
    };
    /**
     * After view init
     */
    Subheader4Component.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.subscriptions.push(this.subheaderService.title$.subscribe(function (bt) {
            // breadcrumbs title sometimes can be undefined
            if (bt) {
                Promise.resolve(null).then(function () {
                    _this.title = bt.title;
                    _this.desc = bt.desc;
                });
            }
        }));
        this.subscriptions.push(this.subheaderService.breadcrumbs$.subscribe(function (bc) {
            Promise.resolve(null).then(function () {
                _this.breadcrumbs = bc;
            });
        }));
    };
    /**
     * On destroy
     */
    Subheader4Component.prototype.ngOnDestroy = function () {
        this.subscriptions.forEach(function (sb) { return sb.unsubscribe(); });
    };
    Subheader4Component.ctorParameters = function () { return [
        { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_2__["SubheaderService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], Subheader4Component.prototype, "fluid", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], Subheader4Component.prototype, "clear", void 0);
    Subheader4Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-subheader4',
            template: __webpack_require__(/*! raw-loader!./subheader4.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/subheader/subheader4/subheader4.component.html"),
            styles: [__webpack_require__(/*! ./subheader4.component.scss */ "./src/app/views/partials/layout/subheader/subheader4/subheader4.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_base_layout__WEBPACK_IMPORTED_MODULE_2__["SubheaderService"]])
    ], Subheader4Component);
    return Subheader4Component;
}());



/***/ }),

/***/ "./src/app/views/partials/layout/subheader/subheader5/subheader5.component.scss":
/*!**************************************************************************************!*\
  !*** ./src/app/views/partials/layout/subheader/subheader5/subheader5.component.scss ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host .kt-subheader__desc {\n  margin: 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2thbWlsL1BSU19ORVdfVmVyc2lvbi9zcmMvYXBwL3ZpZXdzL3BhcnRpYWxzL2xheW91dC9zdWJoZWFkZXIvc3ViaGVhZGVyNS9zdWJoZWFkZXI1LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUUsU0FBUyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvdmlld3MvcGFydGlhbHMvbGF5b3V0L3N1YmhlYWRlci9zdWJoZWFkZXI1L3N1YmhlYWRlcjUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG5cdC5rdC1zdWJoZWFkZXJfX2Rlc2Mge1xuXHRcdG1hcmdpbjogMDtcblx0fVxufVxuIl19 */"

/***/ }),

/***/ "./src/app/views/partials/layout/subheader/subheader5/subheader5.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/views/partials/layout/subheader/subheader5/subheader5.component.ts ***!
  \************************************************************************************/
/*! exports provided: Subheader5Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Subheader5Component", function() { return Subheader5Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_base_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../core/_base/layout */ "./src/app/core/_base/layout/index.ts");

// Angular

// Layout

var Subheader5Component = /** @class */ (function () {
    /**
     * Component constructor
     *
     * @param subheaderService: SubheaderService
     */
    function Subheader5Component(subheaderService) {
        this.subheaderService = subheaderService;
        this.today = Date.now();
        this.title = '';
        this.desc = '';
        this.breadcrumbs = [];
        // Private properties
        this.subscriptions = [];
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    Subheader5Component.prototype.ngOnInit = function () {
    };
    /**
     * After view init
     */
    Subheader5Component.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.subscriptions.push(this.subheaderService.title$.subscribe(function (bt) {
            // breadcrumbs title sometimes can be undefined
            if (bt) {
                Promise.resolve(null).then(function () {
                    _this.title = bt.title;
                    _this.desc = bt.desc;
                });
            }
        }));
        this.subscriptions.push(this.subheaderService.breadcrumbs$.subscribe(function (bc) {
            Promise.resolve(null).then(function () {
                _this.breadcrumbs = bc;
            });
        }));
    };
    /**
     * On destroy
     */
    Subheader5Component.prototype.ngOnDestroy = function () {
        this.subscriptions.forEach(function (sb) { return sb.unsubscribe(); });
    };
    Subheader5Component.ctorParameters = function () { return [
        { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_2__["SubheaderService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], Subheader5Component.prototype, "fluid", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], Subheader5Component.prototype, "clear", void 0);
    Subheader5Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-subheader5',
            template: __webpack_require__(/*! raw-loader!./subheader5.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/subheader/subheader5/subheader5.component.html"),
            styles: [__webpack_require__(/*! ./subheader5.component.scss */ "./src/app/views/partials/layout/subheader/subheader5/subheader5.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_base_layout__WEBPACK_IMPORTED_MODULE_2__["SubheaderService"]])
    ], Subheader5Component);
    return Subheader5Component;
}());



/***/ }),

/***/ "./src/app/views/partials/layout/topbar/cart/cart.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/views/partials/layout/topbar/cart/cart.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhcnRpYWxzL2xheW91dC90b3BiYXIvY2FydC9jYXJ0LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/views/partials/layout/topbar/cart/cart.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/views/partials/layout/topbar/cart/cart.component.ts ***!
  \*********************************************************************/
/*! exports provided: CartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartComponent", function() { return CartComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

// Angular

var CartComponent = /** @class */ (function () {
    /**
     * Component constructor
     */
    function CartComponent() {
        // Public properties
        // Set icon class name
        this.icon = 'flaticon2-shopping-cart-1';
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * After view init
     */
    CartComponent.prototype.ngAfterViewInit = function () {
    };
    /**
     * On init
     */
    CartComponent.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], CartComponent.prototype, "icon", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], CartComponent.prototype, "iconType", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], CartComponent.prototype, "useSVG", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], CartComponent.prototype, "bgImage", void 0);
    CartComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-cart',
            template: __webpack_require__(/*! raw-loader!./cart.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/topbar/cart/cart.component.html"),
            styles: [__webpack_require__(/*! ./cart.component.scss */ "./src/app/views/partials/layout/topbar/cart/cart.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CartComponent);
    return CartComponent;
}());



/***/ }),

/***/ "./src/app/views/partials/layout/topbar/language-selector/language-selector.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/views/partials/layout/topbar/language-selector/language-selector.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: LanguageSelectorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LanguageSelectorComponent", function() { return LanguageSelectorComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _core_base_layout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../core/_base/layout */ "./src/app/core/_base/layout/index.ts");

// Angular


// RxJS

// Translate

var LanguageSelectorComponent = /** @class */ (function () {
    /**
     * Component constructor
     *
     * @param translationService: TranslationService
     * @param router: Router
     */
    function LanguageSelectorComponent(translationService, router) {
        this.translationService = translationService;
        this.router = router;
        // Public properties
        this.classes = '';
        this.languages = [
            {
                lang: 'en',
                name: 'English',
                flag: './assets/media/flags/012-uk.svg'
            },
            {
                lang: 'ch',
                name: 'Mandarin',
                flag: './assets/media/flags/015-china.svg'
            },
            {
                lang: 'es',
                name: 'Spanish',
                flag: './assets/media/flags/016-spain.svg'
            },
            {
                lang: 'jp',
                name: 'Japanese',
                flag: './assets/media/flags/014-japan.svg'
            },
            {
                lang: 'de',
                name: 'German',
                flag: './assets/media/flags/017-germany.svg'
            },
            {
                lang: 'fr',
                name: 'French',
                flag: './assets/media/flags/019-france.svg'
            },
        ];
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    LanguageSelectorComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.setSelectedLanguage();
        this.router.events
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["filter"])(function (event) { return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationStart"]; }))
            .subscribe(function (event) {
            _this.setSelectedLanguage();
        });
    };
    /**
     * Set language
     *
     * @param lang: any
     */
    LanguageSelectorComponent.prototype.setLanguage = function (lang) {
        var _this = this;
        this.languages.forEach(function (language) {
            if (language.lang === lang) {
                language.active = true;
                _this.language = language;
            }
            else {
                language.active = false;
            }
        });
        this.translationService.setLanguage(lang);
    };
    /**
     * Set selected language
     */
    LanguageSelectorComponent.prototype.setSelectedLanguage = function () {
        this.setLanguage(this.translationService.getSelectedLanguage());
    };
    LanguageSelectorComponent.ctorParameters = function () { return [
        { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_4__["TranslationService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('class'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], LanguageSelectorComponent.prototype, "classes", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], LanguageSelectorComponent.prototype, "iconType", void 0);
    LanguageSelectorComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-language-selector',
            template: __webpack_require__(/*! raw-loader!./language-selector.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/topbar/language-selector/language-selector.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_base_layout__WEBPACK_IMPORTED_MODULE_4__["TranslationService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], LanguageSelectorComponent);
    return LanguageSelectorComponent;
}());



/***/ }),

/***/ "./src/app/views/partials/layout/topbar/notification/notification.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/views/partials/layout/topbar/notification/notification.component.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ::ng-deep ngb-tabset > .nav-tabs {\n  display: none; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2thbWlsL1BSU19ORVdfVmVyc2lvbi9zcmMvYXBwL3ZpZXdzL3BhcnRpYWxzL2xheW91dC90b3BiYXIvbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUdHLGFBQWEsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhcnRpYWxzL2xheW91dC90b3BiYXIvbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcblx0OjpuZy1kZWVwIHtcblx0XHRuZ2ItdGFic2V0ID4gLm5hdi10YWJzIHtcblx0XHRcdGRpc3BsYXk6IG5vbmU7XG5cdFx0fVxuXHR9XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/views/partials/layout/topbar/notification/notification.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/views/partials/layout/topbar/notification/notification.component.ts ***!
  \*************************************************************************************/
/*! exports provided: NotificationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationComponent", function() { return NotificationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");

// Angular


var NotificationComponent = /** @class */ (function () {
    /**
     * Component constructor
     *
     * @param sanitizer: DomSanitizer
     */
    function NotificationComponent(sanitizer) {
        this.sanitizer = sanitizer;
        // Set icon class name
        this.icon = 'flaticon2-bell-alarm-symbol';
        // Set skin color, default to light
        this.skin = 'light';
        this.type = 'success';
    }
    NotificationComponent.prototype.backGroundStyle = function () {
        if (!this.bgImage) {
            return 'none';
        }
        return 'url(' + this.bgImage + ')';
    };
    NotificationComponent.ctorParameters = function () { return [
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], NotificationComponent.prototype, "dot", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], NotificationComponent.prototype, "pulse", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], NotificationComponent.prototype, "pulseLight", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], NotificationComponent.prototype, "icon", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], NotificationComponent.prototype, "iconType", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], NotificationComponent.prototype, "useSVG", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], NotificationComponent.prototype, "bgImage", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], NotificationComponent.prototype, "skin", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], NotificationComponent.prototype, "type", void 0);
    NotificationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-notification',
            template: __webpack_require__(/*! raw-loader!./notification.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/topbar/notification/notification.component.html"),
            styles: [__webpack_require__(/*! ./notification.component.scss */ "./src/app/views/partials/layout/topbar/notification/notification.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"]])
    ], NotificationComponent);
    return NotificationComponent;
}());



/***/ }),

/***/ "./src/app/views/partials/layout/topbar/quick-action/quick-action.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/views/partials/layout/topbar/quick-action/quick-action.component.ts ***!
  \*************************************************************************************/
/*! exports provided: QuickActionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuickActionComponent", function() { return QuickActionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

// Angular

var QuickActionComponent = /** @class */ (function () {
    /**
     * Component constructor
     */
    function QuickActionComponent() {
        // Public properties
        // Set icon class name
        this.icon = 'flaticon2-gear';
        // Set skin color, default to light
        this.skin = 'light';
        this.gridNavSkin = 'light';
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * After view init
     */
    QuickActionComponent.prototype.ngAfterViewInit = function () {
    };
    /**
     * On init
     */
    QuickActionComponent.prototype.ngOnInit = function () {
    };
    QuickActionComponent.prototype.onSVGInserted = function (svg) {
        svg.classList.add('kt-svg-icon', 'kt-svg-icon--success', 'kt-svg-icon--lg');
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], QuickActionComponent.prototype, "icon", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], QuickActionComponent.prototype, "iconType", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], QuickActionComponent.prototype, "useSVG", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], QuickActionComponent.prototype, "bgImage", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], QuickActionComponent.prototype, "skin", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], QuickActionComponent.prototype, "gridNavSkin", void 0);
    QuickActionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-quick-action',
            template: __webpack_require__(/*! raw-loader!./quick-action.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/topbar/quick-action/quick-action.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], QuickActionComponent);
    return QuickActionComponent;
}());



/***/ }),

/***/ "./src/app/views/partials/layout/topbar/search-default/search-default.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/views/partials/layout/topbar/search-default/search-default.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: SearchDefaultComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchDefaultComponent", function() { return SearchDefaultComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

// Angular

var SearchDefaultComponent = /** @class */ (function () {
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    function SearchDefaultComponent(cdr) {
        this.cdr = cdr;
        // Public properties
        // Set icon class name
        this.icon = 'flaticon2-search-1';
    }
    /**
     * On init
     */
    SearchDefaultComponent.prototype.ngOnInit = function () {
        // simulate result from API
        // type 0|1 as separator or item
        this.result = [
            {
                text: 'Documents',
                type: 0
            }, {
                icon: '<i class="flaticon-interface-3 kt-font-warning">',
                text: 'Annual finance report',
                type: 1
            }, {
                icon: '<i class="flaticon-share kt-font-success"></i>',
                text: 'Company meeting schedule',
                type: 1
            }, {
                icon: '<i class="flaticon-paper-plane kt-font-info"></i>',
                text: 'Project quotations',
                type: 1
            }, {
                text: 'Customers',
                type: 0
            }, {
                img: '<img src="assets/media/users/user1.jpg" alt="">',
                text: 'Amanda Anderson',
                type: 1
            }, {
                img: '<img src="assets/media/users/user2.jpg" alt="">',
                text: 'Kennedy Lloyd',
                type: 1
            }, {
                img: '<img src="assets/media/users/user3.jpg" alt="">',
                text: 'Megan Weldon',
                type: 1
            }, {
                img: '<img src="assets/media/users/user4.jpg" alt="">',
                text: 'Marc-André ter Stegen',
                type: 1
            }, {
                text: 'Files',
                type: 0
            }, {
                icon: '<i class="flaticon-lifebuoy kt-font-warning"></i>',
                text: 'Revenue report',
                type: 1
            }, {
                icon: '<i class="flaticon-coins kt-font-primary"></i>',
                text: 'Anual finance report',
                type: 1
            }, {
                icon: '<i class="flaticon-calendar kt-font-danger"></i>',
                text: 'Tax calculations',
                type: 1
            }
        ];
    };
    /**
     * Search
     * @param e: Event
     */
    SearchDefaultComponent.prototype.search = function (e) {
        var _this = this;
        this.data = null;
        if (e.target.value.length > 2) {
            this.loading = true;
            // simulate getting search result
            setTimeout(function () {
                _this.data = _this.result;
                _this.loading = false;
                _this.cdr.markForCheck();
            }, 500);
        }
    };
    /**
     * Clear search
     *
     * @param e: Event
     */
    SearchDefaultComponent.prototype.clear = function (e) {
        this.data = null;
        this.searchInput.nativeElement.value = '';
    };
    SearchDefaultComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], SearchDefaultComponent.prototype, "icon", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], SearchDefaultComponent.prototype, "useSVG", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('searchInput', { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], SearchDefaultComponent.prototype, "searchInput", void 0);
    SearchDefaultComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-search-default',
            template: __webpack_require__(/*! raw-loader!./search-default.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/topbar/search-default/search-default.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]])
    ], SearchDefaultComponent);
    return SearchDefaultComponent;
}());



/***/ }),

/***/ "./src/app/views/partials/layout/topbar/search-dropdown/search-dropdown.component.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/views/partials/layout/topbar/search-dropdown/search-dropdown.component.ts ***!
  \*******************************************************************************************/
/*! exports provided: SearchDropdownComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchDropdownComponent", function() { return SearchDropdownComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

// Angular

var SearchDropdownComponent = /** @class */ (function () {
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    function SearchDropdownComponent(cdr) {
        this.cdr = cdr;
        // Public properties
        // Set icon class name
        this.icon = 'flaticon2-search-1';
        this.type = 'success';
    }
    /**
     * On init
     */
    SearchDropdownComponent.prototype.ngOnInit = function () {
        // simulate result from API
        // type 0|1 as separator or item
        this.result = [
            {
                text: 'Documents',
                type: 0
            }, {
                icon: '<i class="flaticon-interface-3 kt-font-warning">',
                text: 'Annual finance report',
                type: 1
            }, {
                icon: '<i class="flaticon-share kt-font-success"></i>',
                text: 'Company meeting schedule',
                type: 1
            }, {
                icon: '<i class="flaticon-paper-plane kt-font-info"></i>',
                text: 'Project quotations',
                type: 1
            }, {
                text: 'Customers',
                type: 0
            }, {
                img: '<img src="assets/media/users/user1.jpg" alt="">',
                text: 'Amanda Anderson',
                type: 1
            }, {
                img: '<img src="assets/media/users/user2.jpg" alt="">',
                text: 'Kennedy Lloyd',
                type: 1
            }, {
                img: '<img src="assets/media/users/user3.jpg" alt="">',
                text: 'Megan Weldon',
                type: 1
            }, {
                img: '<img src="assets/media/users/user4.jpg" alt="">',
                text: 'Marc-André ter Stegen',
                type: 1
            }, {
                text: 'Files',
                type: 0
            }, {
                icon: '<i class="flaticon-lifebuoy kt-font-warning"></i>',
                text: 'Revenue report',
                type: 1
            }, {
                icon: '<i class="flaticon-coins kt-font-primary"></i>',
                text: 'Anual finance report',
                type: 1
            }, {
                icon: '<i class="flaticon-calendar kt-font-danger"></i>',
                text: 'Tax calculations',
                type: 1
            }
        ];
    };
    /**
     * Search
     * @param e: Event
     */
    SearchDropdownComponent.prototype.search = function (e) {
        var _this = this;
        this.data = null;
        if (e.target.value.length > 2) {
            this.loading = true;
            // simulate getting search result
            setTimeout(function () {
                _this.data = _this.result;
                _this.loading = false;
                _this.cdr.markForCheck();
            }, 500);
        }
    };
    /**
     * Clear search
     *
     * @param e: Event
     */
    SearchDropdownComponent.prototype.clear = function (e) {
        this.data = null;
        this.searchInput.nativeElement.value = '';
    };
    SearchDropdownComponent.prototype.openChange = function () {
        var _this = this;
        setTimeout(function () { return _this.searchInput.nativeElement.focus(); });
    };
    SearchDropdownComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], SearchDropdownComponent.prototype, "icon", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], SearchDropdownComponent.prototype, "useSVG", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], SearchDropdownComponent.prototype, "type", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('searchInput', { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], SearchDropdownComponent.prototype, "searchInput", void 0);
    SearchDropdownComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-search-dropdown',
            template: __webpack_require__(/*! raw-loader!./search-dropdown.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/topbar/search-dropdown/search-dropdown.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]])
    ], SearchDropdownComponent);
    return SearchDropdownComponent;
}());



/***/ }),

/***/ "./src/app/views/partials/layout/topbar/user-profile/user-profile.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/views/partials/layout/topbar/user-profile/user-profile.component.ts ***!
  \*************************************************************************************/
/*! exports provided: UserProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProfileComponent", function() { return UserProfileComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");

// Angular

// NGRX


var UserProfileComponent = /** @class */ (function () {
    /**
     * Component constructor
     *
     * @param store: Store<AppState>
     */
    function UserProfileComponent(store, router) {
        var _this = this;
        this.store = store;
        this.router = router;
        this.avatar = true;
        this.greeting = true;
        this.router.events.subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_3__["NavigationEnd"]) {
                _this.returnUrl = event.url;
            }
        });
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    UserProfileComponent.prototype.ngOnInit = function () {
        // this.user$ = this.store.pipe(select(currentUser));
        this._user = JSON.parse(sessionStorage.getItem('user'));
    };
    /**
     * Log out
     */
    UserProfileComponent.prototype.logout = function () {
        // this.store.dispatch(new Logout());
        localStorage.clear();
        sessionStorage.clear();
        this.router.navigateByUrl('/auth/login');
        // this.router.navigate(['/auth/login'], {queryParams: {returnUrl: this.returnUrl}});
    };
    UserProfileComponent.ctorParameters = function () { return [
        { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], UserProfileComponent.prototype, "avatar", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], UserProfileComponent.prototype, "greeting", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], UserProfileComponent.prototype, "badge", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], UserProfileComponent.prototype, "icon", void 0);
    UserProfileComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-user-profile',
            template: __webpack_require__(/*! raw-loader!./user-profile.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/topbar/user-profile/user-profile.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], UserProfileComponent);
    return UserProfileComponent;
}());



/***/ }),

/***/ "./src/app/views/partials/layout/topbar/user-profile2/user-profile2.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/views/partials/layout/topbar/user-profile2/user-profile2.component.ts ***!
  \***************************************************************************************/
/*! exports provided: UserProfile2Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProfile2Component", function() { return UserProfile2Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _core_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../core/auth */ "./src/app/core/auth/index.ts");

// Angular

// NGRX


var UserProfile2Component = /** @class */ (function () {
    /**
     * Component constructor
     *
     * @param store: Store<AppState>
     */
    function UserProfile2Component(store) {
        this.store = store;
        this.avatar = true;
        this.greeting = true;
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    UserProfile2Component.prototype.ngOnInit = function () {
        this.user$ = this.store.pipe(Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_2__["select"])(_core_auth__WEBPACK_IMPORTED_MODULE_3__["currentUser"]));
    };
    /**
     * Log out
     */
    UserProfile2Component.prototype.logout = function () {
        this.store.dispatch(new _core_auth__WEBPACK_IMPORTED_MODULE_3__["Logout"]());
    };
    UserProfile2Component.ctorParameters = function () { return [
        { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], UserProfile2Component.prototype, "avatar", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], UserProfile2Component.prototype, "greeting", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], UserProfile2Component.prototype, "badge", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], UserProfile2Component.prototype, "icon", void 0);
    UserProfile2Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-user-profile2',
            template: __webpack_require__(/*! raw-loader!./user-profile2.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/topbar/user-profile2/user-profile2.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"]])
    ], UserProfile2Component);
    return UserProfile2Component;
}());



/***/ }),

/***/ "./src/app/views/partials/layout/topbar/user-profile3/user-profile3.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/views/partials/layout/topbar/user-profile3/user-profile3.component.ts ***!
  \***************************************************************************************/
/*! exports provided: UserProfile3Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProfile3Component", function() { return UserProfile3Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _core_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../core/auth */ "./src/app/core/auth/index.ts");

// Angular

// NGRX


var UserProfile3Component = /** @class */ (function () {
    /**
     * Component constructor
     *
     * @param store: Store<AppState>
     */
    function UserProfile3Component(store) {
        this.store = store;
        this.avatar = true;
        this.greeting = true;
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    UserProfile3Component.prototype.ngOnInit = function () {
        this.user$ = this.store.pipe(Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_2__["select"])(_core_auth__WEBPACK_IMPORTED_MODULE_3__["currentUser"]));
    };
    /**
     * Log out
     */
    UserProfile3Component.prototype.logout = function () {
        this.store.dispatch(new _core_auth__WEBPACK_IMPORTED_MODULE_3__["Logout"]());
    };
    UserProfile3Component.ctorParameters = function () { return [
        { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], UserProfile3Component.prototype, "avatar", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], UserProfile3Component.prototype, "greeting", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], UserProfile3Component.prototype, "badge", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], UserProfile3Component.prototype, "icon", void 0);
    UserProfile3Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-user-profile3',
            template: __webpack_require__(/*! raw-loader!./user-profile3.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/partials/layout/topbar/user-profile3/user-profile3.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"]])
    ], UserProfile3Component);
    return UserProfile3Component;
}());



/***/ }),

/***/ "./src/app/views/partials/partials.module.ts":
/*!***************************************************!*\
  !*** ./src/app/views/partials/partials.module.ts ***!
  \***************************************************/
/*! exports provided: PartialsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PartialsModule", function() { return PartialsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-perfect-scrollbar */ "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../core/core.module */ "./src/app/core/core.module.ts");
/* harmony import */ var _content_crud__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./content/crud */ "./src/app/views/partials/content/crud/index.ts");
/* harmony import */ var _layout__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./layout */ "./src/app/views/partials/layout/index.ts");
/* harmony import */ var _content_general_notice_notice_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./content/general/notice/notice.component */ "./src/app/views/partials/content/general/notice/notice.component.ts");
/* harmony import */ var _content_general_portlet_portlet_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./content/general/portlet/portlet.module */ "./src/app/views/partials/content/general/portlet/portlet.module.ts");
/* harmony import */ var _content_general_error_error_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./content/general/error/error.component */ "./src/app/views/partials/content/general/error/error.component.ts");
/* harmony import */ var ng_inline_svg__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ng-inline-svg */ "./node_modules/ng-inline-svg/lib_esmodule/index.js");
/* harmony import */ var _layout_topbar_cart_cart_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./layout/topbar/cart/cart.component */ "./src/app/views/partials/layout/topbar/cart/cart.component.ts");

// Angular





// NgBootstrap

// Perfect Scrollbar

// Core module

// CRUD Partials

// Layout partials

// General


// Errpr

// Extra module
// import {WidgetModule} from './content/widgets/widget.module';
// SVG inline


var PartialsModule = /** @class */ (function () {
    function PartialsModule() {
    }
    PartialsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _layout__WEBPACK_IMPORTED_MODULE_10__["ScrollTopComponent"],
                _content_general_notice_notice_component__WEBPACK_IMPORTED_MODULE_11__["NoticeComponent"],
                _content_crud__WEBPACK_IMPORTED_MODULE_9__["ActionNotificationComponent"],
                _content_crud__WEBPACK_IMPORTED_MODULE_9__["DeleteEntityDialogComponent"],
                _content_crud__WEBPACK_IMPORTED_MODULE_9__["FetchEntityDialogComponent"],
                _content_crud__WEBPACK_IMPORTED_MODULE_9__["UpdateStatusDialogComponent"],
                _content_crud__WEBPACK_IMPORTED_MODULE_9__["AlertComponent"],
                // topbar components
                _layout__WEBPACK_IMPORTED_MODULE_10__["ContextMenu2Component"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["ContextMenuComponent"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["QuickPanelComponent"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["ScrollTopComponent"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["SearchResultComponent"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["SplashScreenComponent"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["StickyToolbarComponent"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["Subheader1Component"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["Subheader2Component"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["Subheader3Component"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["Subheader4Component"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["Subheader5Component"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["SubheaderSearchComponent"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["LanguageSelectorComponent"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["NotificationComponent"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["QuickActionComponent"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["SearchDefaultComponent"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["SearchDropdownComponent"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["UserProfileComponent"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["UserProfile2Component"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["UserProfile3Component"],
                _layout_topbar_cart_cart_component__WEBPACK_IMPORTED_MODULE_15__["CartComponent"],
                _content_general_error_error_component__WEBPACK_IMPORTED_MODULE_13__["ErrorComponent"],
            ],
            exports: [
                // WidgetModule,
                _content_general_portlet_portlet_module__WEBPACK_IMPORTED_MODULE_12__["PortletModule"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["ScrollTopComponent"],
                _content_general_notice_notice_component__WEBPACK_IMPORTED_MODULE_11__["NoticeComponent"],
                _content_crud__WEBPACK_IMPORTED_MODULE_9__["ActionNotificationComponent"],
                _content_crud__WEBPACK_IMPORTED_MODULE_9__["DeleteEntityDialogComponent"],
                _content_crud__WEBPACK_IMPORTED_MODULE_9__["FetchEntityDialogComponent"],
                _content_crud__WEBPACK_IMPORTED_MODULE_9__["UpdateStatusDialogComponent"],
                _content_crud__WEBPACK_IMPORTED_MODULE_9__["AlertComponent"],
                // topbar components
                _layout__WEBPACK_IMPORTED_MODULE_10__["ContextMenu2Component"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["ContextMenuComponent"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["QuickPanelComponent"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["ScrollTopComponent"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["SearchResultComponent"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["SplashScreenComponent"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["StickyToolbarComponent"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["Subheader1Component"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["Subheader2Component"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["Subheader3Component"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["Subheader4Component"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["Subheader5Component"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["SubheaderSearchComponent"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["LanguageSelectorComponent"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["NotificationComponent"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["QuickActionComponent"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["SearchDefaultComponent"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["SearchDropdownComponent"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["UserProfileComponent"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["UserProfile2Component"],
                _layout__WEBPACK_IMPORTED_MODULE_10__["UserProfile3Component"],
                _layout_topbar_cart_cart_component__WEBPACK_IMPORTED_MODULE_15__["CartComponent"],
                _content_general_error_error_component__WEBPACK_IMPORTED_MODULE_13__["ErrorComponent"],
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_7__["PerfectScrollbarModule"],
                ng_inline_svg__WEBPACK_IMPORTED_MODULE_14__["InlineSVGModule"],
                _core_core_module__WEBPACK_IMPORTED_MODULE_8__["CoreModule"],
                _content_general_portlet_portlet_module__WEBPACK_IMPORTED_MODULE_12__["PortletModule"],
                // WidgetModule,
                // angular material modules
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatAutocompleteModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTooltipModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDialogModule"],
                // ng-bootstrap modules
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__["NgbDropdownModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__["NgbTabsetModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__["NgbTooltipModule"],
            ],
        })
    ], PartialsModule);
    return PartialsModule;
}());



/***/ }),

/***/ "./src/app/views/public-page/public-page.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/views/public-page/public-page.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ::ng-deep .mat-table-sticky {\n  z-index: 0 !important; }\n\n:host ::ng-deep .mat-header-cell {\n  z-index: 2 !important; }\n\nhtml {\n  overflow-y: scroll; }\n\ntd.mat-cell {\n  padding: 12px 0 16px 0;\n  font-size: 18px; }\n\n.mdc-chip {\n  border-radius: 16px;\n  background-color: #f2f3f8;\n  color: rgba(21, 21, 21, 0.87);\n  font-family: Poppins, Helvetica, sans-serif;\n  -moz-osx-font-smoothing: grayscale;\n  -webkit-font-smoothing: antialiased;\n  font-size: 0.875rem;\n  line-height: 1.25rem;\n  font-weight: 400;\n  letter-spacing: 0.0178571429em;\n  text-decoration: inherit;\n  text-transform: inherit;\n  height: 25px;\n  display: inline-flex;\n  position: relative;\n  align-items: center;\n  box-sizing: border-box;\n  padding: 0 12px;\n  border-width: 0;\n  outline: none;\n  -webkit-appearance: none; }\n\n.mdc-chip-set {\n  padding: 4px;\n  display: flex;\n  flex-wrap: wrap;\n  box-sizing: border-box;\n  margin-left: -7px;\n  margin-bottom: -11px; }\n\n.mdc-chip {\n  margin: 4px; }\n\n.demo-chip-shaped {\n  border-radius: 4px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2thbWlsL1BSU19ORVdfVmVyc2lvbi9zcmMvYXBwL3ZpZXdzL3B1YmxpYy1wYWdlL3B1YmxpYy1wYWdlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBR0cscUJBQXFCLEVBQUE7O0FBSHhCO0VBTXFCLHFCQUFxQixFQUFBOztBQUsxQztFQUFNLGtCQUFrQixFQUFBOztBQUd4QjtFQUNDLHNCQUFzQjtFQUN0QixlQUFlLEVBQUE7O0FBSWhCO0VBQ0MsbUJBQW1CO0VBRW5CLHlCQUF5QjtFQUN6Qiw2QkFBNkI7RUFDN0IsMkNBQTJDO0VBQzNDLGtDQUFrQztFQUNsQyxtQ0FBbUM7RUFDbkMsbUJBQW1CO0VBQ25CLG9CQUFvQjtFQUNwQixnQkFBZ0I7RUFDaEIsOEJBQThCO0VBQzlCLHdCQUF3QjtFQUN4Qix1QkFBdUI7RUFDdkIsWUFBWTtFQUVaLG9CQUFvQjtFQUNwQixrQkFBa0I7RUFFbEIsbUJBQW1CO0VBRW5CLHNCQUFzQjtFQUN0QixlQUFlO0VBQ2YsZUFBZTtFQUNmLGFBQWE7RUFFYix3QkFBd0IsRUFBQTs7QUFFekI7RUFDQyxZQUFZO0VBRVosYUFBYTtFQUViLGVBQWU7RUFFZixzQkFBc0I7RUFDdEIsaUJBQWlCO0VBQ2pCLG9CQUFvQixFQUFBOztBQUVyQjtFQUNDLFdBQVcsRUFBQTs7QUFJWjtFQUNDLGtCQUFrQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvdmlld3MvcHVibGljLXBhZ2UvcHVibGljLXBhZ2UuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG5cdDo6bmctZGVlcCB7XG5cdFx0Lm1hdC10YWJsZS1zdGlja3kge1xuXHRcdFx0ei1pbmRleDogMCAhaW1wb3J0YW50O1xuXHRcdH1cblxuXHRcdC5tYXQtaGVhZGVyLWNlbGwgeyB6LWluZGV4OiAyICFpbXBvcnRhbnQ7fVxuXG5cdH1cbn1cblxuaHRtbHsgb3ZlcmZsb3cteTogc2Nyb2xsO31cblxuXG50ZC5tYXQtY2VsbCB7XG5cdHBhZGRpbmc6IDEycHggMCAxNnB4IDA7XG5cdGZvbnQtc2l6ZTogMThweDtcblxufVxuXG4ubWRjLWNoaXAge1xuXHRib3JkZXItcmFkaXVzOiAxNnB4O1xuXHQvL2JhY2tncm91bmQtY29sb3I6ICNlMGUwZTA7XG5cdGJhY2tncm91bmQtY29sb3I6ICNmMmYzZjg7XG5cdGNvbG9yOiByZ2JhKDIxLCAyMSwgMjEsIDAuODcpO1xuXHRmb250LWZhbWlseTogUG9wcGlucywgSGVsdmV0aWNhLCBzYW5zLXNlcmlmO1xuXHQtbW96LW9zeC1mb250LXNtb290aGluZzogZ3JheXNjYWxlO1xuXHQtd2Via2l0LWZvbnQtc21vb3RoaW5nOiBhbnRpYWxpYXNlZDtcblx0Zm9udC1zaXplOiAwLjg3NXJlbTtcblx0bGluZS1oZWlnaHQ6IDEuMjVyZW07XG5cdGZvbnQtd2VpZ2h0OiA0MDA7XG5cdGxldHRlci1zcGFjaW5nOiAwLjAxNzg1NzE0MjllbTtcblx0dGV4dC1kZWNvcmF0aW9uOiBpbmhlcml0O1xuXHR0ZXh0LXRyYW5zZm9ybTogaW5oZXJpdDtcblx0aGVpZ2h0OiAyNXB4O1x0XHRcdFx0XHRcdC8vIGhlaWdodFxuXHQvL2Rpc3BsYXk6IC1tcy1pbmxpbmUtZmxleGJveDtcblx0ZGlzcGxheTogaW5saW5lLWZsZXg7XG5cdHBvc2l0aW9uOiByZWxhdGl2ZTtcblx0Ly8tbXMtZmxleC1hbGlnbjogc3RhcnQ7XG5cdGFsaWduLWl0ZW1zOiBjZW50ZXI7XG5cdC13ZWJraXQtYm94LXNpemluZzogYm9yZGVyLWJveDtcblx0Ym94LXNpemluZzogYm9yZGVyLWJveDtcblx0cGFkZGluZzogMCAxMnB4O1xuXHRib3JkZXItd2lkdGg6IDA7XG5cdG91dGxpbmU6IG5vbmU7XG5cdC8vY3Vyc29yOiBwb2ludGVyO1xuXHQtd2Via2l0LWFwcGVhcmFuY2U6IG5vbmU7XG59XG4ubWRjLWNoaXAtc2V0IHsgLy8vL1xuXHRwYWRkaW5nOiA0cHg7XG5cdGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xuXHRkaXNwbGF5OiBmbGV4O1xuXHQtbXMtZmxleC13cmFwOiB3cmFwO1xuXHRmbGV4LXdyYXA6IHdyYXA7XG5cdC13ZWJraXQtYm94LXNpemluZzogYm9yZGVyLWJveDtcblx0Ym94LXNpemluZzogYm9yZGVyLWJveDtcblx0bWFyZ2luLWxlZnQ6IC03cHg7XG5cdG1hcmdpbi1ib3R0b206IC0xMXB4O1xufVxuLm1kYy1jaGlwIHtcblx0bWFyZ2luOiA0cHg7XG5cdC8vbWFyZ2luLWxlZnQ6IDBweDtcbn1cblxuLmRlbW8tY2hpcC1zaGFwZWQge1xuXHRib3JkZXItcmFkaXVzOiA0cHg7XG59XG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9zdHlsZXMvQ2hpcHNDYXRhbG9nLnNjc3NcbiJdfQ== */"

/***/ }),

/***/ "./src/app/views/public-page/public-page.component.ts":
/*!************************************************************!*\
  !*** ./src/app/views/public-page/public-page.component.ts ***!
  \************************************************************/
/*! exports provided: PublicPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PublicPageComponent", function() { return PublicPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _author_service_work_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../author/service/work.service */ "./src/app/views/author/service/work.service.ts");




var PublicPageComponent = /** @class */ (function () {
    function PublicPageComponent(workService) {
        this.workService = workService;
        this.title = 'The Highest Scored Works';
        this.displayedColumns = ['Tags', 'Title', 'AuthorName', 'Score'];
        this.works = [];
        this.tagList = [];
        this.selectable = false;
        this.removable = false;
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
    }
    PublicPageComponent.prototype.ngOnInit = function () {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.loadWorks();
    };
    PublicPageComponent.prototype.ngOnDestroy = function () {
        this.wSub.unsubscribe();
    };
    PublicPageComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    };
    PublicPageComponent.prototype.loadWorks = function () {
        var _this = this;
        this.wSub = this.workService.getWorksForPublic()
            .subscribe(function (res) {
            _this.dataSource.data = res;
            _this.dataSource.sort = _this.sort;
            _this.dataSource.paginator = _this.paginator;
            // console.log("AAAA");
            // console.log(this.dataSource.data);
        }, function (error) {
            console.log('There was an error while retrieving Posts !!!' + error);
        });
    };
    PublicPageComponent.ctorParameters = function () { return [
        { type: _author_service_work_service__WEBPACK_IMPORTED_MODULE_3__["WorkService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"], { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], PublicPageComponent.prototype, "paginator", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"], { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], PublicPageComponent.prototype, "sort", void 0);
    PublicPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-public-page',
            template: __webpack_require__(/*! raw-loader!./public-page.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/public-page/public-page.component.html"),
            styles: [__webpack_require__(/*! ./public-page.component.scss */ "./src/app/views/public-page/public-page.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_author_service_work_service__WEBPACK_IMPORTED_MODULE_3__["WorkService"]])
    ], PublicPageComponent);
    return PublicPageComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    isMockEnabled: true,
    authTokenKey: 'authce9d77b308c149d5992a80073637e4d5',
    baseUrl: 'http://3.95.8.94/example/'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/kamil/PRS_NEW_Version/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map